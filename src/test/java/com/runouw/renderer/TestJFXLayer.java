/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.runouw.renderer.SceneInfo.ScissorInfo;
import com.runouw.renderer.SceneInfo.ViewportInfo;
import java.io.IOException;
import java.util.Queue;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import org.joml.Matrix4f;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestJFXLayer {
    
    private static final boolean USE_VULKAN = true;
    private static final boolean VSYNC = true;

    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
        System.setProperty("com.runouw.renderer.RenderEngine.vk_layers", "VK_LAYER_LUNARG_core_validation, VK_LAYER_LUNARG_parameter_validation");
        if (VSYNC) {
            System.setProperty("com.longlinkislong.gloop2.vk.VKContext.vsync", "true");
        }

        System.setProperty("com.runouw.renderer.RenderEngine.async_present", "true");
        System.setProperty("com.runouw.renderer.RenderEngine.async_transfer", "true");
    }
    
    @Test
    public void runTest() throws IOException {
        RenderEngineInfo engineInfo = new RenderEngineInfo()                
                .withTitle("JFXLayer Test")
                .withVsync(VSYNC)
                .withWindowSize(800, 600);
        
        if (USE_VULKAN) {
            engineInfo = engineInfo.withAPI(RenderEngineInfo.API.VULKAN).withVersion(1, 0, 0);
        } else {
            engineInfo = engineInfo.withAPI(RenderEngineInfo.API.OPENGL).withVersion(4, 5, 0);
        }
        
        final SceneInfo sceneInfo = new SceneInfo()
                .withViewport(new ViewportInfo(0, 0, 800, 600, 0, 1))
                .withScissor(new ScissorInfo(0, 0, 800, 600))
                .withClearColor(new RGBA(0.2F, 0.2F, 0.3F, 1.0F))
                .withLayerInfos(new SceneInfo.LayerOrderInfo()
                        .withLookup("_stage")
                        .withOrder(0)
                        .withInfo(new JFXLayerInfo()
                                .withExtent(800, 600)));
        
        try (RenderEngine engine = engineInfo.create()) {
            try (Scene scene = engine.createScene(sceneInfo)) {
                engine.setScene(scene);
                final Button btn = new Button();
                
                btn.setText("Say 'Hello World'");
                btn.setOnAction(event -> {
                    System.out.println("Hello World!");
                });
                
                final StackPane root = new StackPane();
                
                root.getChildren().add(btn);
                
                final JFXLayer jfxLayer = scene.getJFXLayer("_stage").get();
                
                jfxLayer.setScene(new javafx.scene.Scene(root, 800, 600));
                
                final float[] projection = new float[16];
                
                new Matrix4f()
                        .identity()
                        .get(projection);
                
                jfxLayer.setProjection(projection);
                
                engine.window.show();
                while (engine.isValid()) {
                    final Queue<RenderStep> steps = engine.getRenderSteps();
                    
                    while (!steps.isEmpty()) {
                        final RenderStep step = steps.poll();
                        
                        step.run();
                    }
                }
            } catch (Exception ex) {
                throw new RuntimeException("Error closing scene!", ex);
            }
        }
    }
}
