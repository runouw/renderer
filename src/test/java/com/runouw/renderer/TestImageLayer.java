/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.image.ImageIO;
import com.runouw.renderer.ImageLayerInfo.ImageSublayerInfo;
import com.runouw.renderer.SceneInfo.ScissorInfo;
import com.runouw.renderer.SceneInfo.ViewportInfo;
import java.io.IOException;
import java.util.Queue;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestImageLayer {

    private static final boolean USE_VULKAN = false;
    private static final boolean VSYNC = true;

    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
        System.setProperty("com.runouw.renderer.RenderEngine.vk_layers", "VK_LAYER_LUNARG_core_validation, VK_LAYER_LUNARG_parameter_validation");
        if (VSYNC) {
            System.setProperty("com.longlinkislong.gloop2.vk.VKContext.vsync", "true");
        }

        System.setProperty("com.runouw.renderer.RenderEngine.async_present", "true");
        System.setProperty("com.runouw.renderer.RenderEngine.async_transfer", "true");
    }

    @Test
    public void runTest() throws IOException {
        RenderEngineInfo engineInfo = new RenderEngineInfo()                
                .withTitle("Image Layer Test")
                .withVsync(VSYNC)
                .withWindowSize(800, 600);
        
        if (USE_VULKAN) {
            engineInfo = engineInfo.withAPI(RenderEngineInfo.API.VULKAN).withVersion(1, 0, 0);
        } else {
            engineInfo = engineInfo.withAPI(RenderEngineInfo.API.OPENGL).withVersion(4, 5, 0);
        }

        final SceneInfo sceneInfo = new SceneInfo()
                .withViewport(new ViewportInfo(0, 0, 800, 600, 0, 1))
                .withScissor(new ScissorInfo(0, 0, 800, 600))
                .withClearColor(new RGBA(0.2F, 0.2F, 0.3F, 1.0F))
                .withLayerInfos(new SceneInfo.LayerOrderInfo()
                        .withLookup("_env")
                        .withOrder(0)
                        .withInfo(new ImageLayerInfo()
                                .withColorTransformSupport(false)
                                .withImageInfo(new ImageSublayerInfo()
                                        .withColorTransform(ColorTransform.IDENTITY.withHSV(new HSV(180F, 1F, 1F)))
                                        .withImage(ImageIO.read(TestImageLayer.class.getResourceAsStream("/com/runouw/renderer/environment.png"), 4))
                                        .withHScroll(ImageScrollType.STATIC)
                                        .withVScroll(ImageScrollType.STATIC)
                                        .withMagFilter(ImageFilter.LINEAR))));

        try (RenderEngine engine = engineInfo.create()) {
            try (Scene scene = engine.createScene(sceneInfo)) {
                engine.setScene(scene);

                final ImageLayer imageLayer = scene.getImageLayer("_env").get();
                float t = 0.0F;
                float u = 0F;
                float v = 0F;
                int frames = 0;
                long lastFPSPrint = System.currentTimeMillis();

                engine.window.show();
                while (engine.isValid()) {
                    final Queue<RenderStep> steps = engine.getRenderSteps();

                    while (!steps.isEmpty()) {
                        final RenderStep step = steps.poll();

                        step.run();

                        switch (step.getRenderState()) {
                            case IDLE:
                                final long now = System.currentTimeMillis();
                                
                                if (now - lastFPSPrint > 3000) {
                                    final double dt = (now - lastFPSPrint) * 0.001;
                                    
                                    System.out.printf("FPS: %.2f\n", (frames / dt));
                                    frames = 0;
                                    lastFPSPrint = now;
                                }
                                
                                frames++;
                                
                                u = (float) (0.25 * Math.cos(t));
                                v = (float) (0.25 * Math.sin(t));
                                t += 0.01F;
                                break;
                            case START:
                                imageLayer.scrollImage(u + 0.25F, v + 0.25F, u + 0.75F, v + 0.75F);
                                break;
                            default:

                        }
                    }
                }
            } catch (Exception ex) {
                throw new RuntimeException("Error closing scene!", ex);
            }
        }
    }
}
