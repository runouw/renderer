/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.image.Image;
import com.longlinkislong.gloop2.image.ImageIO;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import org.joml.Matrix4f;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestTileLayer {
    
    @Test
    public void runTest() throws IOException {
        RenderEngineInfo engineInfo = new RenderEngineInfo()
                .withTitle("Tile Layer Test")
                .withVsync(true)
                .withWindowSize(800, 600);
        
        engineInfo = engineInfo.withAPI(RenderEngineInfo.API.OPENGL).withVersion(4, 5, 0);
        
        final Image img = ImageIO.read(TestTileLayer.class.getResourceAsStream("/com/runouw/renderer/tile.png"), 4);
        final TileMap tileMap = new TileMap(1024, 1024);
        
        tileMap.setTile(img, 0, 0);
        tileMap.setTile(img, 0, 1);
        tileMap.setTile(img, 0, 2);
        tileMap.setTile(img, 0, 3);
        tileMap.setTile(img, 0, 4);
        tileMap.setTile(img, 1, 2);
        tileMap.setTile(img, 2, 2);
        tileMap.setTile(img, 3, 0);
        tileMap.setTile(img, 3, 1);
        tileMap.setTile(img, 3, 2);
        tileMap.setTile(img, 3, 3);
        tileMap.setTile(img, 3, 4);
        
        tileMap.setTile(img, 5, 0);
        tileMap.setTile(img, 5, 4);
        tileMap.setTile(img, 6, 0);
        tileMap.setTile(img, 6, 1);
        tileMap.setTile(img, 6, 2);
        tileMap.setTile(img, 6, 3);
        tileMap.setTile(img, 6, 4);
        tileMap.setTile(img, 7, 0);
        tileMap.setTile(img, 7, 4);
        
        final List<TileFilter> tileFilters = new ArrayList<>();
        
        tileFilters.add(new TileFilter()
                .withColorTransform(ColorTransform.IDENTITY.withHSV(new HSV(180, 1, 1)))
                .withExtent(400, 300)
                .withOffset(50, 50));
        
        tileFilters.add(new TileFilter()
                .withColorTransform(ColorTransform.IDENTITY
                        .withRGBAScale(new RGBA(0.5F, 0.25F, 0.75F, 1F))
                        .withRGBAOffset(new RGBA(0.5F, 0.75F, 0.25F, 0F)))
                .withExtent(100, 100)
                .withOffset(100, 100));
        
        tileFilters.add(new TileFilter()
                .withColorTransform(ColorTransform.IDENTITY
                        .withHSV(new HSV(0, 0.5F, 1F)))
                .withExtent(120, 300)
                .withOffset(0, 0));
        
        assert img.getWidth() == img.getHeight() : "Tiles must be symmetrical in dimensions!";
        
        final TileLayerInfo baseTileLayerInfo = new TileLayerInfo()
                .withSupertileSize(1024)
                .withTileSize(img.getWidth())
                .withMaxVisibleSupertiles(16)
                .withTileFilters(tileFilters)
                .withTileMap(tileMap);
        
        System.out.printf("File Size: %.2fGB%n", (baseTileLayerInfo.getSize() / 1024.0 / 1024.0 / 1024.0));
        
        final TileCacheInfo tileCacheInfo = TileCacheInfo.deriveFrom(baseTileLayerInfo)
                .withDiskCache(Paths.get("tile_cache"))
                .withMaxCacheSize(1024 * 1024 * 128);
        
        final TileCache tileCache = new LMDBTileCache(tileCacheInfo);
        tileCache.invalidate();
        
        final SceneInfo sceneInfo = new SceneInfo()
                .withViewport(new SceneInfo.ViewportInfo(0, 0, 800, 600, 0, 1))
                .withScissor(new SceneInfo.ScissorInfo(0, 0, 800, 600))
                .withClearColor(new RGBA(0.2F, 0.2F, 0.3F, 1.0F))
                .withLayerInfos(new SceneInfo.LayerOrderInfo()
                        .withLookup("_tiles")
                        .withOrder(0)
                        .withInfo(baseTileLayerInfo.withTileCache(tileCache)));
        
        try (RenderEngine engine = engineInfo.create()) {
            try (Scene scene = engine.createScene(sceneInfo)) {
                engine.setScene(scene);
                
                final TileLayer tileLayer = scene.getTileLayer("_tiles").get();
                
                final float[] projection = new float[16];
                
                new Matrix4f()
                        .ortho(0, 800, 600, 0, 0, 1, true)
                        .get(projection);
                
                engine.window.show();
                
                while (engine.isValid()) {
                    final Queue<RenderStep> steps = engine.getRenderSteps();
                    
                    while (!steps.isEmpty()) {
                        final RenderStep step = steps.poll();
                        
                        step.run();
                        
                        switch (step.getRenderState()) {
                            case IDLE:
                                tileLayer.setProjection(projection);
                                break;
                            case START:
                                int i = 0;
                                
                                for (int x = 0; x < 4; x++) {
                                    for (int y = 0; y < 4; y++) {
                                        if (!tileLayer.isSupertileEmpty(x, y)) {
                                            tileLayer.setSupertileMapping(x, y, i++);
                                        }
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
}
