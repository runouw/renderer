/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.DataBlock;
import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.al.ALBuffer;
import com.longlinkislong.gloop2.al.ALFormat;
import com.longlinkislong.gloop2.al.ALSource;
import com.longlinkislong.gloop2.al.ALSourceState;
import com.longlinkislong.gloop2.sound.SeekableSoundChannel;
import com.longlinkislong.gloop2.sound.SoundChannels;
import java.io.EOFException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Assert;
import org.junit.Test;
import org.lwjgl.system.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class TestSoundDB {
    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }
    
    private static final SoundDBInfo DB_INFO = new SoundDBInfo()
            .withCacheName(Tools.APPDATA.resolve("sounds"))
            .withSize(10 * 1024 * 1024);
    
    private static final Logger LOGGER = LoggerFactory.getLogger(TestSoundDB.class);
    
    private static final Set<String> RESOURCES;
    
    static {
        RESOURCES = new HashSet<>(Arrays.asList("at.ogg", "atpcmalaw.wav"));
    }
    
    private static int alignUp(int a, int b) {
        return (a + b - 1) / b * b;
    }
    
    @Test
    public void testStream() throws Exception {
        final ALBuffer[] buffers = new ALBuffer[3];
        final ALSource source = new ALSource();
        ByteBuffer transfer = null;
        
        try (SoundDB db = DB_INFO.create()) {
            try (SeekableSoundChannel sch = db.getSound("at.ogg")) {
                final ALFormat format = sch.getALFormat().orElseThrow(() -> new UnsupportedOperationException("Sound does not have compatible format!"));
                final int bufferSize = alignUp(sch.getByteRate() / 20, MemoryUtil.PAGE_SIZE);
                
                LOGGER.info("Format: {}", format);
                LOGGER.info("Length: {}", sch.getLength());
                LOGGER.info("Buffer Size: {}KB", (bufferSize / 1024.0));
                LOGGER.info("Sample Rate: {}", sch.getSampleRate());
                LOGGER.info("Channels: {}", sch.getChannels());
                
                transfer = MemoryUtil.memAlloc(bufferSize).order(sch.getByteOrder());
                
                for (int i = 0; i < buffers.length; i++) {
                    transfer.clear();
                                        
                    int read = 0;
                    while (transfer.hasRemaining() && read != -1) {
                        read = sch.read(transfer);                                                                        
                    }
                                        
                    transfer.flip();
                    
                    buffers[i] = new ALBuffer();                    
                    buffers[i].setData(format, transfer, sch.getSampleRate());
                    source.queueBuffers(buffers[i]);
                    
                    if (read == -1) {
                        source.play();
                        throw new EOFException();
                    }
                }
                
                source.play();
                
                while (true) {
                    final List<ALBuffer> ready = source.unqueueBuffers();
                    
                    if (ready.isEmpty()) {
                        Thread.sleep(30);
                        continue;
                    }                    
                    
                    for (ALBuffer buffer : ready) {
                        transfer.clear();
                        
                        int read = 0;
                        
                        while (transfer.hasRemaining() && read != -1) {
                            read = sch.read(transfer);
                        }
                        
                        transfer.flip();
                        buffer.setData(format, transfer, sch.getSampleRate());
                        source.queueBuffers(buffer);
                        
                        if (read == -1) {
                            throw new EOFException();
                        }
                    }
                }
            }
        } catch (EOFException ex) {
            LOGGER.info("EOF reached!");
            while (source.getState() == ALSourceState.PLAYING) {
                Thread.sleep(30);
            }
        } finally {
            if (transfer != null) {
                MemoryUtil.memFree(transfer);
            }
            
            source.close();
           
            for (int i = 0; i < buffers.length; i++) {
                if (buffers[i] != null) {
                    buffers[i].close();
                }
            }
        }
    }
    
    //@Test
    public void testLoad() throws Exception {
        try (SoundDB db = DB_INFO.create()) {
            for (String res : RESOURCES) {
                try (InputStream in = TestSoundDB.class.getResourceAsStream(res)) {
                    try (DataBlock block = DataBlock.of(in)) {
                        try (SeekableSoundChannel sound = SoundChannels.open(block.data)) {
                            final SeekableSoundChannel dbSound = db.getSound(res);
                            
                            Assert.assertEquals(sound.getALFormat().get(), dbSound.getALFormat().get());
                            Assert.assertEquals(sound.getByteRate(), dbSound.getByteRate());
                            Assert.assertEquals(sound.getChannels(), dbSound.getChannels());
                            Assert.assertEquals(sound.getSampleRate(), dbSound.getSampleRate());
                            Assert.assertEquals(sound.getLength(), dbSound.getLength(), 0.01F);
                            Assert.assertEquals(sound.getBitsPerSample(), dbSound.getBitsPerSample());
                        }
                    }
                }
            }
        }
    }
    
    //@Test
    public void testIdentify() throws Exception {
        try (SoundDB db = DB_INFO.create()) {
            Assert.assertTrue(RESOURCES.containsAll(db.getResourceNames()));
        }
    }
    
    @Test
    public void testStore() throws Exception {
        try (SoundDB db = DB_INFO.create()) {
            for (String res : RESOURCES) {
                try (InputStream in = TestSoundDB.class.getResourceAsStream(res)) {
                    try (DataBlock block = DataBlock.of(in)) {
                        db.storeSound(res, block.data);
                    }
                }
            }
        }
    }
    
}
