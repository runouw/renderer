/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.image.Image;
import com.longlinkislong.gloop2.image.ImageIO;
import com.runouw.renderer.SceneInfo.ScissorInfo;
import com.runouw.renderer.SceneInfo.ViewportInfo;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.joml.Matrix4f;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class TestSpriteLayer {    
    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(TestSpriteLayer.class);
    private static final boolean USE_CT = false;
    private static final int SPRITE_W = 64;
    private static final int SPRITE_H = 64;
    private static final int MAX_SPRITES = 1024;
    private static final Set<String> DUKE_FRAMES = IntStream.range(0, 10)
            .mapToObj(i -> "duke" + i + ".png")
            .collect(Collectors.toSet());

    @Test
    public void runTest() throws IOException {
        final RenderEngineInfo engineInfo = new RenderEngineInfo()
                .withAPI(RenderEngineInfo.API.OPENGL)
                .withVersion(4, 3, 0)
                .withTitle("Sprite Layer Test")
                .withVsync(true)
                .withWindowSize(800, 600);

        final ImageDB imageDB = new ImageDBInfo()
                .withCacheName(Tools.APPDATA.resolve("duke"))
                .withSize(1024 * 1024)
                .create();

        if (!imageDB.hasImages(DUKE_FRAMES)) {
            for (String frame : DUKE_FRAMES) {
                try (InputStream in = TestSpriteLayer.class.getResourceAsStream("/com/runouw/renderer/" + frame)) {
                    try (Image data = ImageIO.read(in, 4)) {
                        imageDB.storeImage(frame, data);
                    }
                }
            }
        }

        final List<SpriteResourceInfo> sprites = new ArrayList<>();

        for (String frame : DUKE_FRAMES) {
            sprites.add(new SpriteResourceInfo()
                    .withLookup(frame)
                    .withColorTransform(ColorTransform.IDENTITY.withHSV(new HSV(180, 1, 1)))
                    .withImage(imageDB.getImage(frame)));
        }

        final SceneInfo sceneInfo = new SceneInfo()
                .withViewport(new ViewportInfo(0, 0, 800, 600, 0, 1))
                .withScissor(new ScissorInfo(0, 0, 800, 600))
                .withClearColor(new RGBA(0.2F, 0.2F, 0.3F, 1.0F))
                .withLayerInfos(new SceneInfo.LayerOrderInfo()
                        .withLookup("_duke")
                        .withOrder(0)
                        .withInfo(new SpriteLayerInfo()
                                .withValidSprites(sprites)
                                .withMaxColorTransforms(USE_CT ? 1 : 0)
                                .withMaxSpriteSlots(8 * 1024)));

        try (RenderEngine engine = engineInfo.create()) {
            try (Scene scene = engine.createScene(sceneInfo)) {
                engine.setScene(scene);

                final SpriteLayer spriteLayer = scene.getSpriteLayer("_duke").get();
                final List<ImageView> views = DUKE_FRAMES.stream()
                        .map(spriteLayer::getSpriteView)
                        .collect(Collectors.toList());
                final SpriteSlot[] spriteSlots = spriteLayer.getSpriteSlots();
                final ColorTransformSlot colorTransformSlot = USE_CT ? spriteLayer.getColorTransformSlot(0) : null;
                float t = 0.0F;
                int frames = 0;
                long lastFPSPrint = System.currentTimeMillis();

                final float[] projOrtho = new float[16];

                new Matrix4f()
                        .ortho(0F, 800F, 600F, 0F, 0F, 1F, true)
                        .get(projOrtho);

                spriteLayer.setProjection(projOrtho);

                engine.window.show();
                while (engine.isValid()) {
                    final Queue<RenderStep> steps = engine.getRenderSteps();

                    while (!steps.isEmpty()) {
                        final RenderStep step = steps.poll();

                        step.run();

                        switch (step.getRenderState()) {
                            case IDLE:
                                final long now = System.currentTimeMillis();

                                if (now - lastFPSPrint > 3000) {
                                    final double dt = (now - lastFPSPrint) * 0.001;

                                    LOGGER.info("FPS: {}", (frames / dt));
                                    frames = 0;
                                    lastFPSPrint = now;
                                }

                                frames++;
                                t += 0.01F;
                                break;
                            case START:
                                float x = 4.0F;
                                float y = 4.0F;

                                if (colorTransformSlot != null) {
                                    colorTransformSlot
                                            .setRGBAOffset(RGBA.IDENTITY_OFFSET)
                                            .setRGBAScale(RGBA.IDENTITY_SCALE)
                                            .setHSV(new HSV(t, 1F, 1F));
                                }

                                for (int i = 0; i < MAX_SPRITES; i++) {
                                    spriteSlots[i]
                                            .setPosition(x + SPRITE_W * 1.5F * (float) Math.cos(t + i), y + SPRITE_H * 1.5F * (float) Math.sin(t + i))
                                            .setSpriteView(views.get(((int) (t * 5.0) + i) % 10))
                                            .setMaskView(ImageView.BLANK)
                                            .setScale(SPRITE_W, SPRITE_H)
                                            .setOffset(SPRITE_W * 0.5F, SPRITE_H * 0.5F)
                                            .setColorTransformSlot(USE_CT ? 0 : -1)
                                            .setRotation(t + i);

                                    x += (SPRITE_W * 0.5F);

                                    if ((x + SPRITE_W) > 800.0F + SPRITE_W) {
                                        x = 0.0F;
                                        y += (SPRITE_H * 0.5F);
                                    }
                                }

                        }
                    }
                }

            } catch (Exception ex) {
                throw new RuntimeException("Error closing scene!", ex);
            }
        }
    }
}
