/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.Tools;
import java.io.IOException;
import java.util.Queue;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestSoundLayer {
    
    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }
    
    private static final RenderEngineInfo ENGINE_INFO = new RenderEngineInfo()
            .withTitle("Sound Layer Test")
            .withVsync(true);
    
    private static final SoundDBInfo DB_INFO = new SoundDBInfo()
            .withCacheName(Tools.APPDATA.resolve("sounds"))
            .withSize(10 * 1024 * 1024);
    
    private static final SceneInfo SCENE_INFO = new SceneInfo()
            .withAsyncSound(true)
            .withLayerInfos(new SceneInfo.LayerOrderInfo()
                    .withLookup("_bgm")
                    .withOrder(0)
                    .withInfo(new SoundLayerInfo()));
    
    @Test
    public void runTest() throws IOException {
        try (SoundDB db = DB_INFO.create();
                RenderEngine engine = ENGINE_INFO.create()) {
            
            try (Scene scene = engine.createScene(SCENE_INFO)) {
                engine.setScene(scene);
                
                final SoundLayer soundLayer = scene.getSoundLayer("_bgm").get();
                
                engine.window.show();
                
                final Sound sound = soundLayer.open(new SoundInfo()
                        .withChannel(db.getSound("at.ogg")));
                
                sound.play();
                
                mainLoop:
                while (engine.isValid()) {
                    final Queue<RenderStep> steps = engine.getRenderSteps();
                    
                    while (!steps.isEmpty()) {
                        final RenderStep step = steps.poll();
                        
                        step.run();
                        
                        switch (step.getRenderState()) {
                            case IDLE:
                                if (sound.getState() == Sound.State.STOPPED) {
                                    break mainLoop;
                                }
                        }
                    }
                }
            }
        }
    }
}
