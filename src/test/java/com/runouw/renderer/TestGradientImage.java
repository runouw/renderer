/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.image.ImageFileFormat;
import com.longlinkislong.gloop2.image.ImageIO;
import com.runouw.renderer.GradientImage.Stop;
import java.io.IOException;
import java.nio.file.Paths;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestGradientImage {
    @Test
    public void runTest() throws IOException {
        try (GradientImage img = new GradientImage(32, 
                new Stop(0, RGBA.BLACK),
                new Stop(0.5, new RGBA(0.5F, 0.5F, 0F, 1F)),
                new Stop(1.0, new RGBA(1F, 1F, 0F, 1F)))) {
            
            ImageIO.write(img, ImageFileFormat.PNG, Paths.get("testGradient.png"));
        }
    }
}
