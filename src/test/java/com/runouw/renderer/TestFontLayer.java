/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.runouw.renderer.SceneInfo.ScissorInfo;
import com.runouw.renderer.SceneInfo.ViewportInfo;
import java.io.IOException;
import java.util.Arrays;
import java.util.Queue;
import org.joml.Matrix4f;
import org.junit.Test;

/**
 *
 * @author zmichaels
 */
public class TestFontLayer {

    private static final boolean USE_VULKAN = true;
    private static final boolean VSYNC = true;

    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
        System.setProperty("com.runouw.renderer.RenderEngine.vk_layers", "VK_LAYER_LUNARG_core_validation, VK_LAYER_LUNARG_parameter_validation");
        if (VSYNC) {
            System.setProperty("com.longlinkislong.gloop2.vk.VKContext.vsync", "true");
        }

        System.setProperty("com.runouw.renderer.RenderEngine.async_present", "true");
        System.setProperty("com.runouw.renderer.RenderEngine.async_transfer", "true");
    }

    //<editor-fold defaultstate="collapsed">
    private static final String[] TEXT = {
        "    I am writing this under an appreciable mental strain, since by tonight I shall be no more.",
        "Penniless, and at the end of my supply of the drug which alone makes life endurable,",
        "I can bear the torture no longer; and shall cast myself from this garret window",
        "into the squalid street below. Do not think from my slavery to morphine that",
        "I am a weakling or a degenerate. When you have read these hastily scrawled pages",
        "you may guess, though never fully realise, why it is that I must have forgetfulness",
        "or death.",
        "",
        "    It was in one of the most open and least frequented parts of the broad Pacific that",
        "the packet of which I was supercargo fell a victim to the German sea-raider.",
        "The great war was then at its very beginning, and the ocean forces of the Hun had not",
        "completely sunk to their later degradation; so that our vessel was made a legitimate prize,",
        "whilst we of her crew were treated with all the fairness and consideration due us as",
        "naval prisoners. So liberal, indeed, was the discipline of our captors, that five days",
        "after we were taken I managed to escape alone in a small boat with water and provisions",
        "for a good length of time.",
        "",
        "    When I finally found myself adrift and free, I had but little idea of my surroundings.",
        "Never a competent navigator, I could only guess vaguely by the sun and stars that I was",
        "somewhat south of the equator. Of the longitude I knew nothing, and no island or coast-line",
        "was in sight. The weather kept fair, and for uncounted days I drifted aimlessly beneath",
        "the scorching sun; waiting either for some passing ship, or to be cast on the shores of",
        "some habitable land. But neither ship nor land appeared, and I began to despair in my",
        "solitude upon the heaving vastnesses of unbroken blue."};
    //</editor-fold>

    private static final int TOTAL_CHARACTERS = Arrays.stream(TEXT)
            .mapToInt(String::length)
            .sum();

    @Test
    public void runTest() throws IOException {
        RenderEngineInfo engineInfo = new RenderEngineInfo()
                .withTitle("Font Layer Test")
                .withVsync(VSYNC)
                .withWindowSize(800, 600);

        if (USE_VULKAN) {
            engineInfo = engineInfo.withAPI(RenderEngineInfo.API.VULKAN).withVersion(1, 0, 0);
        } else {
            engineInfo = engineInfo.withAPI(RenderEngineInfo.API.OPENGL).withVersion(4, 5, 0);
        }

        final SceneInfo sceneInfo = new SceneInfo()
                .withViewport(new ViewportInfo(0, 0, 800, 600, 0, 1))
                .withScissor(new ScissorInfo(0, 0, 800, 600))
                .withClearColor(new RGBA(0.2F, 0.2F, 0.3F, 1.0F))
                .withLayerInfos(new SceneInfo.LayerOrderInfo()
                        .withLookup("_dagon")
                        .withOrder(0)
                        .withInfo(new FontLayerInfo()
                                .withCharacterCount(TOTAL_CHARACTERS)
                                .withLatinRange()
                                .withFontFile(TestFontLayer.class.getResource("/fonts/roboto/Roboto-Regular.ttf"))));

        try (RenderEngine engine = engineInfo.create()) {
            try (Scene scene = engine.createScene(sceneInfo)) {
                engine.setScene(scene);

                final float[] projOrtho = new float[16];

                new Matrix4f()
                        .ortho(0F, 800F, 600F, 0F, 0F, 1F, true)
                        .get(projOrtho);

                final FontLayer fontLayer = scene.getFontLayer("_dagon").get();

                fontLayer.setProjection(projOrtho);

                float x = 16F;
                float y = fontLayer.getVerticalSpacing();
                int frames = 0;
                long lastFPSPrint = System.currentTimeMillis();
                int limit = 0;

                for (String line : TEXT) {
                    fontLayer.pushText(new TextInfo()
                            .withPosition(x, y)
                            .withRGBA(new RGBA(1F, 0F, 0F, 1F))
                            .withText(line));

                    y += fontLayer.getVerticalSpacing();
                }

                engine.window.show();
                while (engine.isValid()) {
                    final Queue<RenderStep> steps = engine.getRenderSteps();

                    while (!steps.isEmpty()) {
                        final RenderStep step = steps.poll();

                        step.run();

                        switch (step.getRenderState()) {
                            case IDLE:
                                final long now = System.currentTimeMillis();

                                if (now - lastFPSPrint > 3000) {
                                    final double dt = (now - lastFPSPrint) * 0.001;

                                    System.out.printf("FPS: %.2f%n", (frames / dt));
                                    frames = 0;
                                    lastFPSPrint = now;
                                }

                                frames++;

                                if (limit < fontLayer.getMaxDrawLimit()) {
                                    limit++;
                                    fontLayer.setDrawLimit(limit);
                                }
                                break;
                        }
                    }
                }
            } catch (Exception ex) {
                throw new RuntimeException("Error closing scene!", ex);
            }
        }
    }
}
