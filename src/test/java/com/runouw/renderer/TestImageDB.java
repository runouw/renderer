/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.image.Image;
import com.longlinkislong.gloop2.image.ImageIO;
import java.io.File;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class TestImageDB {
    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }
    
    private static final ImageDBInfo DB_INFO = new ImageDBInfo()
            .withCacheName(Tools.APPDATA.resolve("duke"))
            .withSize(10 * 1024 * 1024);
    
    private static final Logger LOGGER = LoggerFactory.getLogger(TestImageDB.class);
    
    private static final Set<String> RESOURCES;
    
    static {
        RESOURCES = IntStream.range(0, 10)
                .mapToObj(i -> "duke" + i + ".png")
                .collect(Collectors.toSet());
    }
    
    @Test
    public void testStoreLoad() throws Exception {
        final Path db = Paths.get("duke");
        
        if (Files.exists(db)) {
            Files.walk(db, FileVisitOption.FOLLOW_LINKS)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
        
        testStore();
        testIdentify();
        testLoad();
    }
    
    @Test
    public void testLoad() throws Exception {
        try (ImageDB db = DB_INFO.create()) {
            for (String res : RESOURCES) {                
                try (InputStream in = TestImageDB.class.getResourceAsStream(res)) {
                    try (Image img = ImageIO.read(in)) {
                        final Image dbImg = db.getImage(res);
                        
                        Assert.assertEquals(img.getWidth(), dbImg.getWidth());
                        Assert.assertEquals(img.getHeight(), dbImg.getHeight());
                        Assert.assertEquals(img.getFormat(), dbImg.getFormat());
                        
                        final ByteBuffer imgdata = img.getData();
                        final ByteBuffer dbdata = dbImg.getData();
                        
                        while (imgdata.hasRemaining()) {
                            Assert.assertEquals(imgdata.getInt(), dbdata.getInt());
                        }
                    }
                }
            }
        }
    }
    
    //@Test
    public void testIdentify() throws Exception {
        try (ImageDB db = DB_INFO.create()) {
            Assert.assertTrue(RESOURCES.containsAll(db.getResourceNames()));
        }
    }
    
    //@Test
    public void testStore() throws Exception {
        try (ImageDB db = DB_INFO.create()) {
            
            for (String res : RESOURCES) {                
                try (InputStream in = TestImageDB.class.getResourceAsStream(res)) {
                    try (Image img = ImageIO.read(in)) {
                        db.storeImage(res, img);
                    }
                }
            }
        }
    }
}
