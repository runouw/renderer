/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.image.Image;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public class TileMap {
    final Image[] tiles;    
    public final int width;
    public final int height;
    
    public TileMap(final int width, final int height) {
        this.width = width;
        this.height = height;
        this.tiles = new Image[width * height];
    }
    
    public TileMap(final TileMap base) {
        this(base.width, base.height);
        
        System.arraycopy(base.tiles, 0, this.tiles, 0, base.tiles.length);
    }
    
    private int idx(final int x, final int y) {
        return y * this.width + x;
    }
    
    public void fillTile(final Image tile, final int startX, final int startY, final int width, final int height) {
        for (int y = startY; y < startY + height; y++) {
            for (int x = startX; x < startX + width; x++) {
                final int idx = idx(x, y);
                
                this.tiles[idx] = tile;
            }
        }
    }
    
    public Optional<Image> setTile(final Image tile, final int x, final int y) {
        final int idx = idx(x, y);
        final Image oldTile = this.tiles[idx];
        
        this.tiles[idx] = tile;
        
        return Optional.ofNullable(oldTile);
    }
    
    public Optional<Image> getTile(final int x, final int y) {
        final int idx = idx(x, y);
        
        if (idx >= this.tiles.length) {
            return Optional.empty();
        }
        
        final Image tile = this.tiles[idx];
        
        return Optional.ofNullable(tile);
    }
    
    public void clear() {
        for (int i = 0; i < this.tiles.length; i++) {
            this.tiles[i] = null;
        }
    }
}
