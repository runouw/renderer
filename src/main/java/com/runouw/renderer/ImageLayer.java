/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public interface ImageLayer extends Layer {
    ImageLayerInfo getInfo();
    
    void setColorTransform(ColorTransform ct);
    
    void scrollImage(float s0, float t0, float s1, float t1);
    
    void scrollMask(float s0, float t0, float s1, float t1);
}
