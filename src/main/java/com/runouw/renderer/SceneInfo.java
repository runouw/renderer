/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public class SceneInfo {
    public static final class LayerOrderInfo implements Comparable<LayerOrderInfo> {
        public final String lookup;
        public final LayerInfo info;
        public final int order;
        
        public LayerOrderInfo(final String lookup, final LayerInfo info, final int order) {
            this.lookup = lookup;
            this.info = info;
            this.order = order;
        }
        
        public LayerOrderInfo() {
            this("", null, -1);
        }
        
        public LayerOrderInfo withLookup(final String lookup) {
            return new LayerOrderInfo(lookup, info, order);
        }
        
        public LayerOrderInfo withInfo(final LayerInfo info) {
            return new LayerOrderInfo(lookup, info, order);
        }
        
        public LayerOrderInfo withOrder(final int order) {
            return new LayerOrderInfo(lookup, info, order);
        }

        @Override
        public int compareTo(LayerOrderInfo o) {
            return this.order - o.order;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == null) {
                return false;
            } else if (this == o) {
                return true;
            } else if(o instanceof LayerOrderInfo) {
                final LayerOrderInfo other = (LayerOrderInfo) o;
                
                return Objects.equals(this.info, other.info)
                        && this.lookup.equals(other.lookup);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 47 * hash + Objects.hashCode(this.lookup);
            hash = 47 * hash + Objects.hashCode(this.info);
            return hash;
        }
    }
    
    public static final class ViewportInfo {
        public final float x;
        public final float y;
        public final float width;
        public final float height;
        public final float near;
        public final float far;
        
        public ViewportInfo(final float x, final float y, final float width, final float height, final float near, final float far) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.near = near;
            this.far = far;
        }
        
        public ViewportInfo() {
            this(0, 0, 640, 480, 0, 1);
        }
        
        public ViewportInfo withOffset(final float x, final float y) {
            return new ViewportInfo(x, y, width, height, near, far);
        }
        
        public ViewportInfo withExtent(final float width, final float height) {
            return new ViewportInfo(x, y, width, height, near, far);
        }
        
        public ViewportInfo withDepthRange(final float near, final float far) {
            return new ViewportInfo(x, y, width, height, near, far);
        }
    }
    
    public static final class ScissorInfo {
        public final int x;
        public final int y;
        public final int width;
        public final int height;
        
        public ScissorInfo(final int x, final int y, final int width, final int height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
        
        public ScissorInfo() {
            this(0, 0, 640, 480);
        }
        
        public ScissorInfo withOffset(final int x, final int y) {
            return new ScissorInfo(x, y, width, height);
        }
        
        public ScissorInfo withExtent(final int width, final int height) {
            return new ScissorInfo(x, y, width, height);
        }
    }
    
    public final Set<LayerOrderInfo> layerInfos;        
    public final ViewportInfo viewport;
    public final ScissorInfo scissor;
    public final RGBA clearColor;
    public final float clearDepth;
    public final boolean asyncSound;
    
    public SceneInfo(
            final Collection<LayerOrderInfo> layerInfos, 
            final ViewportInfo viewport, final ScissorInfo scissor,
            final RGBA clearColor, final float clearDepth,
            final boolean asyncSound) {
        
        this.layerInfos = Tools.copySet(layerInfos);
        this.clearColor = clearColor;
        this.clearDepth = clearDepth;
        this.viewport = viewport;
        this.scissor = scissor;
        this.asyncSound = asyncSound;
    }
    
    public SceneInfo() {
        this(null, new ViewportInfo(), new ScissorInfo(), RGBA.TRANSPARENT_BLACK, 1.0F, false);
    }
    
    public SceneInfo withLayerInfos(final Collection<LayerOrderInfo> layerInfos) {
        return new SceneInfo(layerInfos, viewport, scissor, clearColor, clearDepth, asyncSound);
    }
    
    public SceneInfo withViewport(final ViewportInfo viewport) {
        return new SceneInfo(layerInfos, viewport, scissor, clearColor, clearDepth, asyncSound);
    }
    
    public SceneInfo withScissor(final ScissorInfo scissor) {
        return new SceneInfo(layerInfos, viewport, scissor, clearColor, clearDepth, asyncSound);
    }
    
    public SceneInfo withClearColor(final RGBA clearColor) {
        return new SceneInfo(layerInfos, viewport, scissor, clearColor, clearDepth, asyncSound);
    }
    
    public SceneInfo withClearDepth(final float clearDepth) {
        return new SceneInfo(layerInfos, viewport, scissor, clearColor, clearDepth, asyncSound);
    }
    
    public SceneInfo withLayerInfos(final LayerOrderInfo... layerInfos) {
        return withLayerInfos(Arrays.asList(layerInfos));
    }
    
    public SceneInfo withAsyncSound(final boolean asyncSound) {
        return new SceneInfo(layerInfos, viewport, scissor, clearColor, clearDepth, asyncSound);
    }
}
