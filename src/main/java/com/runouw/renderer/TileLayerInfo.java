/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author zmichaels
 */
public final class TileLayerInfo implements LayerInfo {
    public final int supertileSize;
    public final int tileSize;
    public final TileMap tileMap;
    public final int maxVisibleSupertiles;
    public final TileCache cache;    
    public final List<TileFilter> filters;
    
    public TileLayerInfo(final int superTileSize, final int tileSize, final TileMap tileMap, final int maxVisibleSupertiles, final TileCache cache, final List<TileFilter> filters) {
        this.supertileSize = superTileSize;
        this.tileSize = tileSize;
        this.tileMap = tileMap != null ? new TileMap(tileMap) : null;
        this.maxVisibleSupertiles = maxVisibleSupertiles;
        this.cache = cache;
        this.filters = Tools.copyList(filters);
    }
    
    public TileLayerInfo() {
        this(256, 32, null, 256, null, null);
    }
    
    public TileLayerInfo withSupertileSize(final int supertileSize) {
        return new TileLayerInfo(supertileSize, tileSize, tileMap, maxVisibleSupertiles, cache, filters);
    }
    
    public TileLayerInfo withTileSize(final int tileSize) {
        return new TileLayerInfo(supertileSize, tileSize, tileMap, maxVisibleSupertiles, cache, filters);
    }
    
    public TileLayerInfo withTileMap(final TileMap tileMap) {
        return new TileLayerInfo(supertileSize, tileSize, tileMap, maxVisibleSupertiles, cache, filters);
    }
    
    public TileLayerInfo withMaxVisibleSupertiles(final int maxVisibleSupertiles) {
        return new TileLayerInfo(supertileSize, tileSize, tileMap, maxVisibleSupertiles, cache, filters);
    }
    
    public TileLayerInfo withTileCache(final TileCache cache) {
        return new TileLayerInfo(supertileSize, tileSize, tileMap, maxVisibleSupertiles, cache, filters);
    }
    
    public TileLayerInfo withTileFilters(final List<TileFilter> filters) {
        return new TileLayerInfo(supertileSize, tileSize, tileMap, maxVisibleSupertiles, cache, filters);
    }
    
    public TileLayerInfo withTileFilters(final TileFilter... filters) {
        return withTileFilters(Arrays.asList(filters));
    }

    public int getWidth() {
        return this.tileSize * this.tileMap.width;
    }
    
    public int getHeight() {
        return this.tileSize * this.tileMap.height;
    }
    
    public int getSupertilesAcross() {
        return Math.max(1, this.getWidth() / this.supertileSize);
    }
    
    public int getSupertilesDown() {
        return Math.max(1, this.getHeight() / this.supertileSize);
    }
    
    public long getSize() {
        final long w = this.getWidth();
        final long h = this.getHeight();
        
        return w * h * 4;
    }
}
