/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.glfw.GLFWHints;

/**
 *
 * @author zmichaels
 */
public final class RenderEngineInfo {
    public static enum API {
        OPENGL,
        OPENGLES,
        VULKAN
    }
    
    public static final class Version {
        public final int major;
        public final int minor;
        public final int patch;
        
        public Version(final int major, final int minor, final int patch) {
            this.major = major;
            this.minor = minor;
            this.patch = patch;
        }
        
        public Version() {
            this(0, 0, 0);
        }
        
        public Version withMajor(final int major) {
            return new Version(major, minor, patch);
        }
        
        public Version withMinor(final int minor) {
            return new Version(major, minor, patch);
        }
        
        public Version withPatch(final int patch) {
            return new Version(major, minor, patch);
        }
    }
    
    public final String title;
    public final int width;
    public final int height;
    public final API api;
    public final Version version;        
    public final boolean vsync;
    
    public RenderEngineInfo(final String title, final int width, final int height, final API api, final Version version, final boolean vsync) {
        this.api = api;
        this.version = version;
        this.title = title;
        this.width = width;
        this.height = height;
        this.vsync = vsync;
    }
    
    public RenderEngineInfo() {
        this("Renderer", 640, 480, API.OPENGL, new Version(4, 5, 0), true);
    }
    
    public RenderEngineInfo withAPI(final API api) {
        return new RenderEngineInfo(title, width, height, api, version, vsync);
    }
    
    public RenderEngineInfo withTitle(final String title) {
        return new RenderEngineInfo(title, width, height, api, version, vsync);
    }
    
    public RenderEngineInfo withWindowSize(final int width, final int height) {
        return new RenderEngineInfo(title, width, height, api, version, vsync);
    }
    
    public RenderEngineInfo withVersion(final Version version) {
        return new RenderEngineInfo(title, width, height, api, version, vsync);
    }
    
    public RenderEngineInfo withVersion(final int major, final int minor, final int patch) {
        return withVersion(new Version(major, minor, patch));
    }
    
    public RenderEngineInfo withVsync(final boolean vsync) {
        return new RenderEngineInfo(title, width, height, api, version, vsync);
    }
    
    public RenderEngine create() {
        return new RenderEngine(this);
    }
}
