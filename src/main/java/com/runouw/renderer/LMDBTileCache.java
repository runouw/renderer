/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.file.Files;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.util.lmdb.LMDB;
import org.lwjgl.util.lmdb.MDBVal;

/**
 *
 * @author zmichaels
 */
public class LMDBTileCache implements TileCache {

    private final TileCacheInfo info;
    private long env;
    private boolean valid;
    private final LMDBTileCache parent;

    public LMDBTileCache(final TileCacheInfo info) throws IOException {
        this.info = info;
        this.parent = null;
        
        this.valid = Files.exists(info.diskCache);

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pEnv = mem.callocPointer(1);
            int rc;

            if ((rc = LMDB.mdb_env_create(pEnv)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            final long tEnv = pEnv.get();
            final long pageSize = MemoryUtil.PAGE_SIZE;
            final long dbSize = (info.maxCacheSize + pageSize - 1L) / pageSize * pageSize;

            if ((rc = LMDB.mdb_env_set_mapsize(tEnv, dbSize)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            if ((rc = LMDB.mdb_env_set_maxdbs(tEnv, 1)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            Files.createDirectories(info.diskCache);

            if ((rc = LMDB.mdb_env_open(tEnv, info.diskCache.toAbsolutePath().toString(), 0, 0644)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            this.env = tEnv;
        }
    }

    public LMDBTileCache(final TileCacheInfo info, final LMDBTileCache parent) {
        this.info = info;
        this.env = parent.getEnv();
        this.parent = parent;
    }
    
    private long getEnv() {
        if (this.parent != null) {
            return this.parent.getEnv();
        } else {
            return this.env;
        }
    }
    
    @Override
    public ByteBuffer fetchSupertile(int stx, int sty) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pTxn = mem.callocPointer(1);
            final long txn;
            int rc;

            if ((rc = LMDB.mdb_txn_begin(env, 0, LMDB.MDB_RDONLY, pTxn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            } else {
                txn = pTxn.get();
            }

            final IntBuffer pDbi = mem.callocInt(1);
            final int dbi;

            if ((rc = LMDB.mdb_dbi_open(txn, (ByteBuffer) null, 0, pDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            } else {
                dbi = pDbi.get();
            }

            final ByteBuffer keyData = mem.calloc(info.uid.length() + 2 * Integer.BYTES);

            keyData.put(mem.UTF8(info.uid, false));
            keyData.putInt(stx);
            keyData.putInt(sty);
            keyData.flip();

            final MDBVal key = MDBVal.callocStack(mem)
                    .mv_data(keyData)
                    .mv_size(keyData.remaining());

            final MDBVal value = MDBVal.callocStack(mem);

            if ((rc = LMDB.mdb_get(txn, dbi, key, value)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            LMDB.mdb_txn_abort(txn);

            return value.mv_data();
        } catch (IOException ex) {
            throw new RuntimeException("Error while fetching Supertile!", ex);
        }
    }

    @Override
    public void storeSupertile(int stx, int sty, ByteBuffer data) {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pTxn = mem.callocPointer(1);
            final long txn;
            int rc;

            if ((rc = LMDB.mdb_txn_begin(env, 0, 0, pTxn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            } else {
                txn = pTxn.get();
            }

            final IntBuffer pDbi = mem.callocInt(1);
            final int dbi;

            if ((rc = LMDB.mdb_dbi_open(txn, (ByteBuffer) null, LMDB.MDB_CREATE, pDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            } else {
                dbi = pDbi.get();
            }
            
            final ByteBuffer keyData = mem.calloc(info.uid.length() + 2 * Integer.BYTES);
            
            keyData.put(mem.UTF8(info.uid, false));
            keyData.putInt(stx);
            keyData.putInt(sty);
            keyData.flip();
            
            final MDBVal key = MDBVal.callocStack(mem)
                    .mv_data(keyData)
                    .mv_size(keyData.remaining());
            
            final MDBVal value = MDBVal.callocStack(mem)
                    .mv_data(data)
                    .mv_size(data.remaining());
            
            if ((rc = LMDB.mdb_put(txn, dbi, key, value, 0)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }
            
            if ((rc = LMDB.mdb_txn_commit(txn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error committing Supertile change to LMDB!", ex);
        }
    }

    @Override
    public void invalidate() {
        this.valid = false;
    }

    @Override
    public void validate() {
        this.valid = true;
    }

    @Override
    public boolean isValid() {
        return env != 0L && this.valid;
    }

    @Override
    public void close() {
        if (env == 0L) {
            return;
        }
        
        if (this.parent == null) {
            LMDB.mdb_env_sync(env, true);
            LMDB.mdb_env_close(env);
            this.env = 0L;
        }
    }
    
    @Override
    public TileCacheInfo getInfo() {
        return this.info;
    }

}
