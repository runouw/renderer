/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.runouw.rcodec.Fixed;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author zmichaels
 */
public final class RGBA {
    private static final float EPSILON = Float.parseFloat(System.getProperty("com.runouw.renderer.rgba.epsilon", Float.toString(1.0F / 512.0F)));        
    public static final RGBA TRANSPARENT_BLACK = new RGBA(0F, 0F, 0F, 0F);
    public static final RGBA BLACK = new RGBA(0F, 0F, 0F, 1F);
    public static final RGBA WHITE = new RGBA(1F, 1F, 1F, 1F);
    
    public static final RGBA IDENTITY_SCALE = WHITE;
    public static final RGBA IDENTITY_OFFSET = TRANSPARENT_BLACK;
        
    public final float red;
    public final float green;
    public final float blue;
    public final float alpha;
    
    public RGBA(final float red, final float green, final float blue, final float alpha) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }
    
    private int clamp(int val, int low, int high) {
        return Math.min(Math.max(val, low), high);
    }
    
    public int toR8G8B8Unorm() {
        final int r = clamp((int) (red * 255F), 0, 255);
        final int g = clamp((int) (green * 255F), 0, 255);
        final int b = clamp((int) (blue * 255F), 0, 255);
        final int a = clamp((int) (alpha * 255F), 0, 255);
        
        return (a & 0xFF) << 24
                | (b & 0xFF) << 16
                | (g & 0xFF) << 8
                | (r & 0xFF);
    }
    
    public RGBA() {
        this(0.0F, 0.0F, 0.0F, 0.0F);
    }
    
    public RGBA withRed(final float red) {
        return new RGBA(red, green, blue, alpha);
    }
    
    public RGBA withGreen(final float green) {
        return new RGBA(red, green, blue, alpha);
    }
    
    public RGBA withBlue(final float blue) {
        return new RGBA(red, green, blue, alpha);
    }
    
    public RGBA withAlpha(final float alpha) {
        return new RGBA(red, green, blue, alpha);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (obj == this) {
            return true;
        } else if (obj instanceof RGBA) {
            final RGBA other = (RGBA) obj;
            
            return Math.abs(other.red - this.red) < EPSILON
                    && Math.abs(other.green - this.green) < EPSILON
                    && Math.abs(other.blue - this.blue) < EPSILON
                    && Math.abs(other.alpha - this.alpha) < EPSILON;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Float.floatToIntBits(this.red);
        hash = 89 * hash + Float.floatToIntBits(this.green);
        hash = 89 * hash + Float.floatToIntBits(this.blue);
        hash = 89 * hash + Float.floatToIntBits(this.alpha);
        return hash;
    }
    
    public static RGBA fromMap(final Map<String, Object> mapping) {
        final float r = ((Number) mapping.get("r")).floatValue();
        final float g = ((Number) mapping.get("g")).floatValue();
        final float b = ((Number) mapping.get("b")).floatValue();
        final float a = ((Number) mapping.get("a")).floatValue();
        
        return new RGBA(r, g, b, a);
    }
    
    public static Map<String, Object> toMap(final RGBA rgba) {
        final Map<String, Object> mapping = new HashMap<>();
        
        mapping.put("r", new Fixed(rgba.red));
        mapping.put("g", new Fixed(rgba.green));
        mapping.put("b", new Fixed(rgba.blue));
        mapping.put("a", new Fixed(rgba.alpha));
        
        return mapping;
    }
    
    public Map<String, Object> toMap() {
        return toMap(this);
    }
    
    @Override
    public String toString() {
        return new StringBuilder(4096)
                .append("RGBA: <")
                .append(red)
                .append(", ")
                .append(green)
                .append(", ")
                .append(blue)
                .append(", ")
                .append(alpha)
                .append(">")
                .toString();
    }
    
    public float[] getVector() {
        return new float[] {red, green, blue, alpha};
    }
}
