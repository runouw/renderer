/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.image.Image;
import java.util.Objects;

/**
 *
 * @author zmichaels
 */
public final class SpriteResourceInfo {
    public final String lookup;
    public final Image image;
    public final ColorTransform colorTransform;
    
    public SpriteResourceInfo(final String lookup, final Image image, final ColorTransform colorTransform) {
        this.lookup = lookup;
        this.image = image;
        this.colorTransform = colorTransform;
    }
    
    public SpriteResourceInfo() {
        this("", null, ColorTransform.IDENTITY);
    }
    
    public SpriteResourceInfo withLookup(final String lookup) {
        return new SpriteResourceInfo(lookup, image, colorTransform);
    }
    
    public SpriteResourceInfo withImage(final Image image) {
        return new SpriteResourceInfo(lookup, image, colorTransform);
    }
    
    public SpriteResourceInfo withColorTransform(final ColorTransform colorTransform) {
        return new SpriteResourceInfo(lookup, image, colorTransform);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (obj == this) {
            return true;
        } else if (obj instanceof SpriteResourceInfo) {
            final SpriteResourceInfo other = (SpriteResourceInfo) obj;
            
            return other.colorTransform.equals(this.colorTransform)
                    && other.image == this.image
                    && other.lookup.equals(this.lookup);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.lookup);
        hash = 41 * hash + Objects.hashCode(this.image);
        hash = 41 * hash + Objects.hashCode(this.colorTransform);
        return hash;
    }
}
