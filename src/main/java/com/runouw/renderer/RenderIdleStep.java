/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
final class RenderIdleStep implements RenderStep {    
    private final RenderEngine engine;
    
    RenderIdleStep(final RenderEngine engine) {
        this.engine = engine;
    }
    
    @Override
    public void run() {
        try {
            this.engine.lock.acquire();
            this.engine.renderState = RenderState.IDLE;        
            
            this.engine.window.pollEvents();            
        } catch (InterruptedException ex) {
            throw new RuntimeException("RenderEngine idle was interrupted!", ex);
        } finally {
            this.engine.lock.release();
        }
    }

    @Override
    public RenderState getRenderState() {
        return RenderState.IDLE;
    }
    
}
