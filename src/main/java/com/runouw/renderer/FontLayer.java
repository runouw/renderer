/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public interface FontLayer extends Layer {
    float getAscent();
    
    float getDescent();
    
    float getLineGap();
    
    default float getVerticalSpacing() {
        return getAscent() - getDescent() + getLineGap();
    }
    
    void pushText(TextInfo info);
    
    void clear();
    
    FontLayerInfo getInfo();        
    
    void setDrawLimit(int drawLimit);
    
    int getMaxDrawLimit();
}
