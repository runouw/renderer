/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public final class SpriteInfo {

    public final float positionX;
    public final float positionY;
    public final float offsetX;
    public final float offsetY;
    public final float scaleX;
    public final float scaleY;
    public final float skewX;
    public final float skewY;
    public final float rotation;
    public final ImageView spriteView;
    public final ImageView maskView;
    public final int colorTransformId;

    public SpriteInfo(
            final float positionX, final float positionY,
            final float offsetX, final float offsetY,
            final float scaleX, final float scaleY,
            final float skewX, final float skewY,
            final float rotation,
            final ImageView spriteView, final ImageView maskView,
            final int colorTransformId) {

        this.positionX = positionX;
        this.positionY = positionY;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.skewX = skewX;
        this.skewY = skewY;
        this.rotation = rotation;
        this.spriteView = spriteView;
        this.maskView = maskView;
        this.colorTransformId = colorTransformId;
    }

    public SpriteInfo() {
        this(0F, 0F, 16F, 16F, 32F, 32F, 0F, 0F, 0F, new ImageView(-1, 0F, 0F, 0F, 0F), new ImageView(-1, 0F, 0F, 0F, 0F), -1);
    }

    public SpriteInfo withPosition(final float positionX, final float positionY) {
        return new SpriteInfo(positionX, positionY, offsetX, offsetY, scaleX, scaleY, skewX, skewY, rotation, spriteView, maskView,colorTransformId);
    }

    public SpriteInfo withOffset(final float offsetX, final float offsetY) {
        return new SpriteInfo(positionX, positionY, offsetX, offsetY, scaleX, scaleY, skewX, skewY, rotation, spriteView, maskView,colorTransformId);
    }

    public SpriteInfo withSkew(final float skewX, final float skewY) {
        return new SpriteInfo(positionX, positionY, offsetX, offsetY, scaleX, scaleY, skewX, skewY, rotation, spriteView, maskView,colorTransformId);
    }

    public SpriteInfo withScale(final float scaleX, final float scaleY) {
        return new SpriteInfo(positionX, positionY, offsetX, offsetY, scaleX, scaleY, skewX, skewY, rotation, spriteView, maskView,colorTransformId);
    }

    public SpriteInfo withSpriteView(final ImageView spriteView) {
        return new SpriteInfo(positionX, positionY, offsetX, offsetY, scaleX, scaleY, skewX, skewY, rotation, spriteView, maskView,colorTransformId);
    }

    public SpriteInfo withMaskView(final ImageView maskView) {
        return new SpriteInfo(positionX, positionY, offsetX, offsetY, scaleX, scaleY, skewX, skewY, rotation, spriteView, maskView,colorTransformId);
    }

    public SpriteInfo withColorTransformId(final int colorTransformId) {
        return new SpriteInfo(positionX, positionY, offsetX, offsetY, scaleX, scaleY, skewX, skewY, rotation, spriteView, maskView,colorTransformId);
    }
    
    public SpriteInfo withRotation(final float rotation) {
        return new SpriteInfo(positionX, positionY, offsetX, offsetY, scaleX, scaleY, skewX, skewY, rotation, spriteView, maskView,colorTransformId);
    }
}
