/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.gl;

import com.longlinkislong.gloop2.gl.GLBuffer;
import com.longlinkislong.gloop2.gl.GLBufferInfo;
import com.longlinkislong.gloop2.gl.GLBufferTarget;
import com.longlinkislong.gloop2.gl.GLCompute;
import com.longlinkislong.gloop2.gl.GLContextLimits;
import com.longlinkislong.gloop2.gl.GLFilter;
import com.longlinkislong.gloop2.gl.GLFormat;
import com.longlinkislong.gloop2.gl.GLImageAccess;
import com.longlinkislong.gloop2.gl.GLInternalFormat;
import com.longlinkislong.gloop2.gl.GLMemoryBarrier;
import com.longlinkislong.gloop2.gl.GLPixelInfo;
import com.longlinkislong.gloop2.gl.GLSamplerInfo;
import com.longlinkislong.gloop2.gl.GLTexture;
import com.longlinkislong.gloop2.gl.GLTextureInfo;
import com.longlinkislong.gloop2.gl.GLUniform;
import com.longlinkislong.gloop2.image.BufferedImage;
import com.longlinkislong.gloop2.image.PixelFormat;
import com.runouw.renderer.ColorTransform;
import com.runouw.renderer.ImageView;
import com.runouw.renderer.MaskResourceInfo;
import com.runouw.renderer.SpriteResourceInfo;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
final class GLSpritesheet implements AutoCloseable {

    final GLTexture texture = new GLTexture();
    final Map<String, ImageView> spriteRefs = new HashMap<>();
    final Map<String, ImageView> maskRefs = new HashMap<>();
    
    private int bestFitPowerOf2(final int value) {
        if (value > 16384) {
            throw new IllegalArgumentException("Maximum value cannot exceed 16384!");
        } else if (value > 8192) {
            return 16384;
        } else if (value > 4096) {
            return 8192;
        } else if (value > 2048) {
            return 4096;
        } else if (value > 1024) {
            return 2048;
        } else if (value > 512) {
            return 1024;
        } else if (value > 256) {
            return 512;
        } else if (value > 128) {
            return 256;
        } else if (value > 64) {
            return 128;
        } else if (value > 32) {
            return 64;
        } else {
            return 32;
        }
    }

    GLSpritesheet(final SpritesheetInfo info) {
        int width = -1;
        int height = -1;

        final int images = info.validSprites.size() + info.validMasks.size();

        assert images <= GLContextLimits.getLimits().maxArrayTextureLayers : "Required image count exceeds max texture array layer count!";

        for (SpriteResourceInfo spriteInfo : info.validSprites) {
            width = Math.max(width, spriteInfo.image.getWidth());
            height = Math.max(height, spriteInfo.image.getHeight());
        }

        for (MaskResourceInfo maskInfo : info.validMasks) {
            width = Math.max(width, maskInfo.image.getWidth());
            height = Math.max(height, maskInfo.image.getHeight());
        }

        width = bestFitPowerOf2(width);
        height = bestFitPowerOf2(height);

        final int imageSize = width * height * Integer.BYTES;
        final int stagingBufferSize = imageSize * images;
        final ByteBuffer stagingBuffer = MemoryUtil.memCalloc(stagingBufferSize);

        boolean hasColorTransforms = false;
        final List<ColorTransform> colorTransforms = new ArrayList<>();
        int imageId = 0;

        for (SpriteResourceInfo spriteInfo : info.validSprites) {
            try (BufferedImage tmp = new BufferedImage(width, height, PixelFormat.R8G8B8A8_UNORM)) {
                final int iw = spriteInfo.image.getWidth();
                final int ih = spriteInfo.image.getHeight();
                final float s1 = ((float) iw) / ((float) width);
                final float t1 = ((float) ih) / ((float) height);

                this.spriteRefs.put(spriteInfo.lookup, new ImageView(imageId++, 0F, 0F, s1, t1));

                colorTransforms.add(spriteInfo.colorTransform);

                if (spriteInfo.colorTransform != ColorTransform.IDENTITY) {
                    hasColorTransforms = true;
                }

                tmp.setSubimage(0, 0, iw, ih, spriteInfo.image);
                stagingBuffer.put(tmp.getData());
            }
        }

        for (MaskResourceInfo maskInfo : info.validMasks) {
            try (BufferedImage tmp = new BufferedImage(width, height, PixelFormat.R8G8B8A8_UNORM)) {
                final int iw = maskInfo.image.getWidth();
                final int ih = maskInfo.image.getHeight();
                final float s1 = ((float) iw) / ((float) width);
                final float t1 = ((float) ih) / ((float) height);

                this.maskRefs.put(maskInfo.lookup, new ImageView(imageId++, 0F, 0F, s1, t1));

                colorTransforms.add(ColorTransform.IDENTITY);

                tmp.setSubimage(0, 0, iw, ih, maskInfo.image);
                stagingBuffer.put(tmp.getData());
            }
        }
        
        stagingBuffer.flip();

        try (GLTexture newTex = new GLTextureInfo()
                .withExtent(width, height, 1)
                .withFormat(GLInternalFormat.RGBA8)
                .withLayers(images)
                .withLevels(1)
                .withSamplerInfo(new GLSamplerInfo()
                        .withMagFilter(GLFilter.NEAREST)
                        .withMinFilter(GLFilter.NEAREST)
                        .withWrapR(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE)
                        .withWrapS(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE)
                        .withWrapT(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE))
                .create()) {

            final GLPixelInfo basePixelInfo = new GLPixelInfo()
                    .withData(stagingBuffer)
                    .withFormat(GLFormat.RGBA)
                    .withType(GLPixelInfo.Type.UNSIGNED_BYTE);

            newTex.subImage(0, 0, 0, 0, width, height, images, basePixelInfo);            

            this.texture.swap(newTex);
        } finally {
            MemoryUtil.memFree(stagingBuffer);
        }

        if (hasColorTransforms) {
            final int localCTSize = 4 * 3 * Float.BYTES * images;
            final ByteBuffer localCTData = MemoryUtil.memCalloc(localCTSize);

            for (ColorTransform colorTransform : colorTransforms) {
                localCTData.putFloat((float) Math.cos(colorTransform.hsv.hue));
                localCTData.putFloat((float) Math.sin(colorTransform.hsv.hue));
                localCTData.putFloat(colorTransform.hsv.saturation);
                localCTData.putFloat(colorTransform.hsv.value);
                localCTData.putFloat(colorTransform.rgbaOffset.red);
                localCTData.putFloat(colorTransform.rgbaOffset.green);
                localCTData.putFloat(colorTransform.rgbaOffset.blue);
                localCTData.putFloat(colorTransform.rgbaOffset.alpha);
                localCTData.putFloat(colorTransform.rgbaScale.red);
                localCTData.putFloat(colorTransform.rgbaScale.green);
                localCTData.putFloat(colorTransform.rgbaScale.blue);
                localCTData.putFloat(colorTransform.rgbaScale.alpha);
            }

            colorTransforms.clear();
            localCTData.flip();

            final GLTextureInfo txInfo = this.texture.getInfo();

            try (GLTexture transformedTexture = txInfo.create();
                    GLBuffer ctdata = new GLBufferInfo()
                            .withUsage(GLBufferInfo.Usage.STREAM_DRAW)
                            .withPreferredTarget(GLBufferTarget.UNIFORM)
                            .withSize(localCTSize)
                            .withInitialData(localCTData)
                            .create()) {

                final GLConstants.ArrayColorTransformProgram program = GLConstants.getArrayColorTransformProgramInstance();

                program.program.use();
                GLUniform.uniform1(program.uSrc, 0);
                GLUniform.uniform1(program.uDst, 1);

                ctdata.bindBase(GLBufferTarget.UNIFORM, 0);

                this.texture.bindImage(0, 0, GLTexture.ALL_LAYERS, GLImageAccess.READ_ONLY, GLInternalFormat.RGBA8);
                transformedTexture.bindImage(1, 0, GLTexture.ALL_LAYERS, GLImageAccess.WRITE_ONLY, GLInternalFormat.RGBA8);
                
                GLCompute.dispatchCompute(txInfo.width / 32, txInfo.height / 32, images);

                GLMemoryBarrier.insertBarrier(EnumSet.of(GLMemoryBarrier.TEXTURE_FETCH));

                this.texture.swap(transformedTexture);
            } finally {
                MemoryUtil.memFree(localCTData);
            }
        }
    }

    @Override
    public void close() {
        this.texture.close();
        this.spriteRefs.clear();
        this.maskRefs.clear();
    }
}
