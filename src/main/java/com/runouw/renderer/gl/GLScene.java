/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.gl;

import com.longlinkislong.gloop2.gl.GLBlendStateInfo;
import com.longlinkislong.gloop2.gl.GLClearBuffer;
import com.longlinkislong.gloop2.gl.GLClearStateInfo;
import com.longlinkislong.gloop2.gl.GLContextLimits;
import com.longlinkislong.gloop2.gl.GLProgram;
import com.longlinkislong.gloop2.gl.GLScissorTest;
import com.longlinkislong.gloop2.gl.GLViewport;
import com.runouw.renderer.FontLayer;
import com.runouw.renderer.FontLayerInfo;
import com.runouw.renderer.ImageLayer;
import com.runouw.renderer.ImageLayerInfo;
import com.runouw.renderer.JFXLayer;
import com.runouw.renderer.JFXLayerInfo;
import com.runouw.renderer.Layer;
import com.runouw.renderer.Scene;
import com.runouw.renderer.SceneInfo;
import com.runouw.renderer.SketchLayer;
import com.runouw.renderer.SoundLayer;
import com.runouw.renderer.SoundLayerInfo;
import com.runouw.renderer.SpriteInfo;
import com.runouw.renderer.SpriteLayer;
import com.runouw.renderer.SpriteLayerInfo;
import com.runouw.renderer.SpriteResourceInfo;
import com.runouw.renderer.TileLayer;
import com.runouw.renderer.TileLayerInfo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class GLScene implements Scene {

    private final ExecutorService asyncSound;
    private Future<?> soundSync;
    private static final Logger LOGGER = LoggerFactory.getLogger(GLScene.class);

    private final List<String> comments = new ArrayList<>();
    private final Map<String, GLJFXLayer> jfxLayers = new HashMap<>();
    private final Map<String, GLTileLayer> tileLayers = new HashMap<>();
    private final Map<String, GLFontLayer> fontLayers = new HashMap<>();
    private final Map<String, GLImageLayer> imageLayers = new HashMap<>();
    private final Map<String, GLSpriteLayer> spriteLayers = new HashMap<>();
    private final Map<String, SoundLayer> soundLayers = new HashMap<>();
    private final List<GLLayer> layers = new ArrayList<>();
    private final List<Layer> soundLayersOrdered = new ArrayList<>();
    private final SceneInfo info;
    private final GLViewport viewport;
    private final GLScissorTest scissor;
    private final GLClearStateInfo clear;
    private final List<GLSpritesheet> spritesheets;

    public GLScene(final SceneInfo info) throws IOException {
        this.info = info;

        if (info.asyncSound) {
            this.asyncSound = Executors.newSingleThreadExecutor(task -> {
                final Thread thread = new Thread(task);

                thread.setName("Async Sound thread");
                thread.setDaemon(true);

                return thread;
            });
        } else {
            this.asyncSound = null;
        }

        final List<SpritesheetInfo> spritesheetInfos = new ArrayList<>();
        final GLContextLimits limits = GLContextLimits.getLimits();

        for (SceneInfo.LayerOrderInfo layerInfo : info.layerInfos) {
            if (layerInfo.info instanceof SpriteLayerInfo) {
                final SpriteLayerInfo spriteLayerInfo = (SpriteLayerInfo) layerInfo.info;
                final SpritesheetInfo spritesheetInfo = new SpritesheetInfo(spriteLayerInfo.validSprites, spriteLayerInfo.validMasks);

                if (spritesheetInfos.isEmpty()) {
                    spritesheetInfos.add(spritesheetInfo);
                } else {
                    if (spritesheetInfos.stream()
                            .filter(a -> a.isCompatible(spritesheetInfo))
                            .findFirst()
                            .isPresent()) {
                        
                        continue;
                    }

                    boolean canExpand = false;
                    
                    for (int i = 0; i < spritesheetInfos.size(); i++) {
                        final SpritesheetInfo test = SpritesheetInfo.merge(spritesheetInfos.get(i), spritesheetInfo);
                        
                        if (test.size <= limits.maxArrayTextureLayers) {
                            spritesheetInfos.set(i, test);
                            canExpand = true;
                            break;
                        }
                    }
                    
                    if (!canExpand) {
                        spritesheetInfos.add(spritesheetInfo);
                    }
                }
            }
        }                
        
        final Map<SpritesheetInfo, GLSpritesheet> spritesheets = spritesheetInfos.stream()
                .collect(Collectors.toMap(self -> self, GLSpritesheet::new));        
        final Set<SpritesheetInfo> unused = new HashSet<>(spritesheets.keySet());
        final Queue<SceneInfo.LayerOrderInfo> orderedLayers = new PriorityQueue<>(info.layerInfos);

        while (!orderedLayers.isEmpty()) {
            final SceneInfo.LayerOrderInfo orderInfo = orderedLayers.poll();

            if (orderInfo.info == null) {
                this.comments.add(orderInfo.lookup);
            } else if (orderInfo.info instanceof FontLayerInfo) {
                final GLFontLayer fontLayer = new GLFontLayer((FontLayerInfo) orderInfo.info);

                this.fontLayers.put(orderInfo.lookup, fontLayer);
                this.layers.add(fontLayer);
            } else if (orderInfo.info instanceof ImageLayerInfo) {
                final GLImageLayer imageLayer = new GLImageLayer((ImageLayerInfo) orderInfo.info);

                this.imageLayers.put(orderInfo.lookup, imageLayer);
                this.layers.add(imageLayer);
            } else if (orderInfo.info instanceof SpriteLayerInfo) {
                final SpriteLayerInfo spriteLayerInfo = (SpriteLayerInfo) orderInfo.info;
                final SpritesheetInfo spritesheetInfo = new SpritesheetInfo(spriteLayerInfo.validSprites, spriteLayerInfo.validMasks);
                GLSpritesheet spritesheet = null;
                
                for (Map.Entry<SpritesheetInfo, GLSpritesheet> entry : spritesheets.entrySet()) {
                    final SpritesheetInfo test = entry.getKey();
                    
                    if (test.isCompatible(spritesheetInfo)) {
                        unused.remove(test);
                        spritesheet = entry.getValue();
                        break;
                    }
                }
                
                if (spritesheet == null) {
                    throw new IllegalStateException("Somehow there is no compatible spritesheet");
                }
                
                final GLSpriteLayer spriteLayer = new GLSpriteLayer(spriteLayerInfo, spritesheet);                                             

                this.spriteLayers.put(orderInfo.lookup, spriteLayer);
                this.layers.add(spriteLayer);
            } else if (orderInfo.info instanceof JFXLayerInfo) {
                final GLJFXLayer jfxLayer = new GLJFXLayer((JFXLayerInfo) orderInfo.info);

                this.jfxLayers.put(orderInfo.lookup, jfxLayer);
                this.layers.add(jfxLayer);
            } else if (orderInfo.info instanceof TileLayerInfo) {
                final GLTileLayer tileLayer = new GLTileLayer((TileLayerInfo) orderInfo.info);

                this.tileLayers.put(orderInfo.lookup, tileLayer);
                this.layers.add(tileLayer);
            } else if (orderInfo.info instanceof SoundLayerInfo) {
                final SoundLayer soundLayer = new SoundLayer((SoundLayerInfo) orderInfo.info);

                this.soundLayers.put(orderInfo.lookup, soundLayer);
                this.soundLayersOrdered.add(soundLayer);
            } else {
                throw new UnsupportedOperationException("Layer type: " + orderInfo.info.getClass().getSimpleName() + " is not supported!");
            }
        }                                
        
        unused.stream()
                .map(spritesheets::remove)
                .filter(Objects::nonNull)
                .forEach(spritesheet -> spritesheet.close());
        unused.clear();

        this.spritesheets = new ArrayList<>(spritesheets.values());
        
        LOGGER.trace("Using {} spritesheets", this.spritesheets.size());
        
        this.viewport = new GLViewport()
                .withOffset((int) info.viewport.x, (int) info.viewport.y)
                .withExtent((int) info.viewport.width, (int) info.viewport.height)
                .withDepthRange(info.viewport.near, info.viewport.far);

        this.scissor = new GLScissorTest(true, info.scissor.x, info.scissor.y, info.scissor.width, info.scissor.height);
        this.clear = new GLClearStateInfo()
                .withBuffers(GLClearBuffer.COLOR, GLClearBuffer.DEPTH)
                .withColor(info.clearColor.red, info.clearColor.green, info.clearColor.blue, info.clearColor.alpha)
                .withDepth(info.clearDepth);
    }

    @Override
    public SceneInfo getInfo() {
        return this.info;
    }

    @Override
    public void onFrameStart() {
        if (this.soundSync != null) {
            try {
                this.soundSync.get();
            } catch (InterruptedException ex) {
                throw new RuntimeException("Interrupted while synchronizing with async sound thread!", ex);
            } catch (ExecutionException ex) {
                throw new RuntimeException("Error occurred while executing sound thread!", ex);
            }
        }

        this.layers.forEach(GLLayer::onFrameStart);
    }

    @Override
    public void onFrameEnd() {
        this.viewport.apply();
        this.scissor.apply();
        this.clear.apply();

        GLProgram currentProgram = null;
        GLBlendStateInfo currentBlendState = null;

        for (GLLayer layer : layers) {
            final GLProgram layerProgram = layer.getProgram();
            final GLBlendStateInfo blendState = layer.getBlendState();

            if (currentProgram != layerProgram) {
                layerProgram.use();
                currentProgram = layerProgram;
            }

            if (currentBlendState != blendState) {
                blendState.apply();
                currentBlendState = blendState;
            }

            layer.onFrameEnd();
        }

        if (this.info.asyncSound) {
            this.soundSync = asyncSound.submit(() -> this.soundLayersOrdered.forEach(Layer::onFrameEnd));
        } else {
            this.soundLayersOrdered.forEach(Layer::onFrameEnd);
        }
    }

    @Override
    public void close() {
        for (GLLayer layer : this.layers) {
            try {
                layer.close();
            } catch (Exception ex) {
                LOGGER.warn("Error closing layer!");
                LOGGER.debug(ex.getMessage(), ex);
            }
        }

        this.layers.clear();

        if (this.soundSync != null) {
            try {
                this.soundSync.get();
            } catch (InterruptedException ex) {
                throw new RuntimeException("Interrupted while syncing to sound thread!", ex);
            } catch (ExecutionException ex) {
                throw new RuntimeException("Error occurred on sound thread!", ex);
            }

            this.soundSync = null;
        }

        if (this.asyncSound != null) {
            this.asyncSound.shutdown();
        }

        this.soundLayers.clear();

        for (Layer layer : this.soundLayersOrdered) {
            try {
                layer.close();
            } catch (Exception ex) {
                LOGGER.warn("Error closing layer!");
                LOGGER.debug(ex.getMessage(), ex);
            }
        }

        this.soundLayersOrdered.clear();
        
        this.spritesheets.forEach(GLSpritesheet::close);
        this.spritesheets.clear();
    }

    @Override
    public Optional<FontLayer> getFontLayer(String lookup) {
        return Optional.ofNullable(this.fontLayers.get(lookup));
    }

    @Override
    public Optional<ImageLayer> getImageLayer(String lookup) {
        return Optional.ofNullable(this.imageLayers.get(lookup));
    }

    @Override
    public Optional<SketchLayer> getSketchLayer(String lookup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Optional<SpriteLayer> getSpriteLayer(String lookup) {
        return Optional.ofNullable(this.spriteLayers.get(lookup));
    }

    @Override
    public Optional<TileLayer> getTileLayer(String lookup) {
        return Optional.ofNullable(this.tileLayers.get(lookup));
    }

    @Override
    public List<String> getComments() {
        return Collections.unmodifiableList(this.comments);
    }

    @Override
    public Set<String> getFontLayerNames() {
        return Collections.unmodifiableSet(this.fontLayers.keySet());
    }

    @Override
    public Set<String> getImageLayerNames() {
        return Collections.unmodifiableSet(this.imageLayers.keySet());
    }

    @Override
    public Set<String> getSketchLayerNames() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<String> getSpriteLayerNames() {
        return Collections.unmodifiableSet(this.spriteLayers.keySet());
    }

    @Override
    public Set<String> getTileLayerNames() {
        return Collections.unmodifiableSet(this.tileLayers.keySet());
    }

    @Override
    public Optional<JFXLayer> getJFXLayer(String lookup) {
        return Optional.ofNullable(this.jfxLayers.get(lookup));
    }

    @Override
    public Set<String> getJFXLayerNames() {
        return Collections.unmodifiableSet(this.jfxLayers.keySet());
    }

    @Override
    public List<Layer> getLayersOrdered() {
        return Collections.unmodifiableList(this.layers);
    }

    @Override
    public Optional<SoundLayer> getSoundLayer(String lookup) {
        return Optional.ofNullable(this.soundLayers.get(lookup));
    }

    @Override
    public Set<String> getSoundLayerNames() {
        return Collections.unmodifiableSet(this.soundLayers.keySet());
    }
}
