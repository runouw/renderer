/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.gl;

import com.longlinkislong.gloop2.gl.GLBlendStateInfo;
import com.longlinkislong.gloop2.gl.GLBuffer;
import com.longlinkislong.gloop2.gl.GLBufferAccess;
import com.longlinkislong.gloop2.gl.GLBufferInfo;
import com.longlinkislong.gloop2.gl.GLDraw;
import com.longlinkislong.gloop2.gl.GLFilter;
import com.longlinkislong.gloop2.gl.GLInternalFormat;
import com.longlinkislong.gloop2.gl.GLProgram;
import com.longlinkislong.gloop2.gl.GLSamplerInfo;
import com.longlinkislong.gloop2.gl.GLTexture;
import com.longlinkislong.gloop2.gl.GLTextureInfo;
import com.longlinkislong.gloop2.gl.GLUniform;
import com.longlinkislong.gloop2.gl.GLVertexArray;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo.AttributeDescription;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo.BindingDescription;
import com.longlinkislong.gloop2.gl.GLVertexFormat;
import com.longlinkislong.gloop2.image.FontImage;
import com.runouw.renderer.Constants;
import com.runouw.renderer.FontLayer;
import com.runouw.renderer.FontLayerInfo;
import com.runouw.renderer.TextInfo;
import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public final class GLFontLayer implements FontLayer, GLLayer {

    private static final int BYTES_PER_VERTEX = (2 + 2 + 4) * Float.BYTES;
    private static final int VERTICES_PER_CHARACTER = 6;
    private static final int BYTES_PER_CHARACTER = BYTES_PER_VERTEX * VERTICES_PER_CHARACTER;

    private final AtomicBoolean isWritable = new AtomicBoolean(true);
    private FontLayerInfo info;
    private FloatBuffer projection;
    private FontImage font;
    private final GLTexture texture = new GLTexture();
    private final GLVertexArray model = new GLVertexArray();
    private final GLBuffer vtext = new GLBuffer();
    private GLBuffer.MappedBuffer mapping;
    private FloatBuffer writeData;
    private int characters;
    private int drawLimit;
    
    @Override
    public int getMaxDrawLimit() {
        return this.characters;
    }
    
    @Override
    public void setDrawLimit(final int drawLimit) {
        this.drawLimit = drawLimit;
        
        assert drawLimit <= this.getMaxDrawLimit();                
    }

    public GLFontLayer(final FontLayerInfo info) throws IOException {
        this.info = info;
        this.projection = MemoryUtil.memAllocFloat(16);
        
        try (InputStream inFont = info.fontFile.openStream()) {
            this.font = FontImage.loadASCII(inFont, info.fontPointSize * Constants.PIXELS_PER_POINT);
        }

        try (GLBuffer newVText = new GLBufferInfo()
                .withSize(info.maxCharacters * BYTES_PER_CHARACTER)
                .withUsage(GLBufferInfo.Usage.DYNAMIC_DRAW)
                .create()) {

            this.vtext.swap(newVText);
        }

        try (GLVertexArray newModel = new GLVertexArrayInfo()
                .withBindings(new BindingDescription()
                        .withBinding(0)
                        .withBuffer(vtext)
                        .withStride((2 + 2 + 4) * Float.BYTES))
                .withAttributes(
                        new AttributeDescription()
                                .withBinding(0)
                                .withLocation(0)
                                .withFormat(GLVertexFormat.VEC2)
                                .withOffset(0),
                        new AttributeDescription()
                                .withBinding(0)
                                .withLocation(1)
                                .withFormat(GLVertexFormat.VEC2)
                                .withOffset(2 * Float.BYTES),
                        new AttributeDescription()
                                .withBinding(0)
                                .withLocation(2)
                                .withFormat(GLVertexFormat.VEC4)
                                .withOffset(4 * Float.BYTES))
                .create()) {

            this.model.swap(newModel);
        }

        try (GLTexture newTexture = GLTextureInfo.wrapImage(font)
                .withFormat(GLInternalFormat.R8)
                .withSamplerInfo(new GLSamplerInfo()
                        .withMagFilter(GLFilter.NEAREST)
                        .withMinFilter(GLFilter.NEAREST)
                        .withWrapR(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE)
                        .withWrapS(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE)
                        .withWrapT(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE))
                .create()) {

            this.texture.swap(newTexture);
        }
    }

    @Override
    public FontLayerInfo getInfo() {
        return this.info;
    }

    private static final Set<GLBufferAccess> MAP_FLAGS = EnumSet.of(GLBufferAccess.WRITE, GLBufferAccess.INVALIDATE_BUFFER);    

    @Override
    public GLBlendStateInfo getBlendState() {
        return GLConstants.PREMULTIPLY_BLEND_STATE;
    }
    
    @Override
    public void onFrameStart() {
    }

    @Override
    public boolean isWritable() {
        return this.isWritable.get();
    }

    @Override
    public void onFrameEnd() {        
        this.isWritable.set(false);
        
        if (this.mapping != null) {
            this.mapping.close();
            this.mapping = null;
            this.writeData = null;
        }

        // exit early if there is nothing to draw.
        if (this.characters == 0) {
            return;
        }

        this.texture.bind(0);

        final GLConstants.FontProgram fontProgram = GLConstants.getFontProgramInstance();

        GLUniform.uniformMatrix4(fontProgram.uProjection, false, this.projection);
        GLUniform.uniform1(fontProgram.uFont, 0);

        this.model.bind();

        GLDraw.drawArrays(GLDraw.Mode.TRIANGLES, 0, this.drawLimit * 6);
        
        this.isWritable.set(true);
    }

    @Override
    public void pushText(TextInfo info) {
        if (!this.isWritable.get()) {
            throw new IllegalStateException("Cannot write to GLFontLayer while drawing!");
        }

        if (this.mapping == null) {
            final long off = (this.characters) * BYTES_PER_CHARACTER;
            final long size = (this.info.maxCharacters * BYTES_PER_CHARACTER) - off;

            // only map the writable section of the FontLayer
            this.mapping = this.vtext.map(off, size, MAP_FLAGS);
            this.writeData = this.mapping.getPointer().asFloatBuffer();
        }

        final List<FontImage.CharSprite> glyphs = this.font.encode(info.x, info.y, info.text);
        final float r = info.color.red;
        final float g = info.color.green;
        final float b = info.color.blue;
        final float a = info.color.alpha;

        for (FontImage.CharSprite glyph : glyphs) {
            this.writeData.put(new float[]{
                glyph.x0, glyph.y0, glyph.s0, glyph.t0, r, g, b, a,
                glyph.x1, glyph.y0, glyph.s1, glyph.t0, r, g, b, a,
                glyph.x0, glyph.y1, glyph.s0, glyph.t1, r, g, b, a,
                glyph.x1, glyph.y0, glyph.s1, glyph.t0, r, g, b, a,
                glyph.x0, glyph.y1, glyph.s0, glyph.t1, r, g, b, a,
                glyph.x1, glyph.y1, glyph.s1, glyph.t1, r, g, b, a
            });
        }

        this.characters += info.text.length();
    }

    @Override
    public void setProjection(float[] projection) {
        assert projection.length == 16 : "Projection matrices must be 4x4!";

        this.projection.put(projection).flip();
    }

    @Override
    public void close() {
        if (this.info != null) {
            MemoryUtil.memFree(this.projection);
            this.font.close();

            this.projection = null;
            this.info = null;
            this.font = null;
        }
    }

    @Override
    public float getAscent() {
        return this.font.ascent;
    }

    @Override
    public float getDescent() {
        return this.font.descent;
    }

    @Override
    public float getLineGap() {
        return this.font.lineGap;
    }

    @Override
    public GLProgram getProgram() {
        return GLConstants.getFontProgramInstance().program;
    }

    @Override
    public void clear() {
        this.characters = 0;
    }
}
