/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.gl;

import com.longlinkislong.gloop2.gl.GLBlendStateInfo;
import com.longlinkislong.gloop2.gl.GLBuffer;
import com.longlinkislong.gloop2.gl.GLBufferAccess;
import com.longlinkislong.gloop2.gl.GLBufferInfo;
import com.longlinkislong.gloop2.gl.GLBufferTarget;
import com.longlinkislong.gloop2.gl.GLCompute;
import com.longlinkislong.gloop2.gl.GLDraw;
import com.longlinkislong.gloop2.gl.GLFilter;
import com.longlinkislong.gloop2.gl.GLImageAccess;
import com.longlinkislong.gloop2.gl.GLInternalFormat;
import com.longlinkislong.gloop2.gl.GLMemoryBarrier;
import com.longlinkislong.gloop2.gl.GLProgram;
import com.longlinkislong.gloop2.gl.GLSamplerInfo;
import com.longlinkislong.gloop2.gl.GLTexture;
import com.longlinkislong.gloop2.gl.GLTextureInfo;
import com.longlinkislong.gloop2.gl.GLUniform;
import com.longlinkislong.gloop2.gl.GLVertexArray;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo;
import com.runouw.renderer.ColorTransform;
import com.runouw.renderer.ImageFilter;
import com.runouw.renderer.ImageLayer;
import com.runouw.renderer.ImageLayerInfo;
import com.runouw.renderer.ImageScrollType;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.EnumSet;
import java.util.concurrent.Semaphore;
import org.lwjgl.system.MemoryStack;

/**
 *
 * @author zmichaels
 */
public class GLImageLayer implements ImageLayer, GLLayer {

    private ImageLayerInfo info;
    private final GLTexture image = new GLTexture();
    private final GLTexture mask = new GLTexture();
    private final GLVertexArray model = new GLVertexArray();
    private final GLBuffer uniforms = new GLBuffer();
    private final Semaphore writeLock = new Semaphore(1);
    private boolean isWritable = false;
    private GLBuffer.MappedBuffer uniformMapping;
    private FloatBuffer uniformData;
    public final boolean supportsMask;
    public final boolean supportsColorTransform;

    private static GLSamplerInfo.AddressMode addressMode(final ImageScrollType scrollType) {
        switch (scrollType) {
            case STATIC:
                return GLSamplerInfo.AddressMode.CLAMP_TO_EDGE;
            case REPEAT:
                return GLSamplerInfo.AddressMode.REPEAT;
            case MIRRORED_REPEAT:
                return GLSamplerInfo.AddressMode.MIRRORED_REPEAT;
            default:
                throw new UnsupportedOperationException("Repeat mode not supported!");
        }
    }

    private static GLTexture newTexture(final ImageLayerInfo.ImageSublayerInfo info) {
        final GLTexture out = new GLTexture();
        final GLSamplerInfo samplerInfo = new GLSamplerInfo()
                .withMagFilter(info.magFilter == ImageFilter.NEAREST ? GLFilter.NEAREST : GLFilter.LINEAR)
                .withMinFilter(info.minFilter == ImageFilter.NEAREST ? GLFilter.NEAREST : GLFilter.LINEAR)
                .withWrapS(addressMode(info.hScroll))
                .withWrapT(addressMode(info.vScroll));

        try (GLTexture newTexture = GLTextureInfo.wrapImage(info.image)
                .withSamplerInfo(samplerInfo)
                .create()) {

            out.swap(newTexture);
        }

        if (info.colorTransform != ColorTransform.IDENTITY) {
            try (GLTexture transformedTexture = new GLTextureInfo()
                    .withExtent(info.image.getWidth(), info.image.getHeight(), 1)
                    .withFormat(GLInternalFormat.RGBA8)
                    .withLayers(1)
                    .withLevels(1)
                    .withSamplerInfo(samplerInfo)
                    .create()) {

                final GLConstants.SingleColorTransformProgram program = GLConstants.getSingleColorTransformProgramInstance();

                program.program.use();

                GLUniform.uniform4(program.uHsv,
                        (float) Math.cos(info.colorTransform.hsv.hue),
                        (float) Math.sin(info.colorTransform.hsv.hue),
                        info.colorTransform.hsv.saturation,
                        info.colorTransform.hsv.value);
                
                GLUniform.uniform4(program.uOffset, 
                        info.colorTransform.rgbaOffset.red,
                        info.colorTransform.rgbaOffset.green,
                        info.colorTransform.rgbaOffset.blue,
                        info.colorTransform.rgbaOffset.alpha);
                
                GLUniform.uniform4(program.uScale, 
                        info.colorTransform.rgbaScale.red,
                        info.colorTransform.rgbaScale.green,
                        info.colorTransform.rgbaScale.blue,
                        info.colorTransform.rgbaScale.alpha);
                
                GLUniform.uniform1(program.uSrc, 0);
                GLUniform.uniform1(program.uDst, 1);
                
                out.bindImage(0, 0, 0, GLImageAccess.READ_ONLY, GLInternalFormat.RGBA8);                
                transformedTexture.bindImage(1, 0, 0, GLImageAccess.WRITE_ONLY, GLInternalFormat.RGBA8);
                
                GLCompute.dispatchCompute(info.image.getWidth() / 32, info.image.getHeight() / 32);
                                
                out.swap(transformedTexture);
                GLMemoryBarrier.insertBarrier(EnumSet.of(GLMemoryBarrier.TEXTURE_FETCH));
            }
        }

        return out;
    }

    public GLImageLayer(final ImageLayerInfo info) {
        this.info = info;
        this.supportsColorTransform = info.supportsColorTransform;

        if (info.image == null) {
            throw new IllegalArgumentException("ImageLayer requires an image!");
        }

        this.image.swap(newTexture(info.image));

        if (info.mask != null) {
            this.mask.swap(newTexture(info.mask));
            this.supportsMask = true;
        } else {
            this.supportsMask = false;
        }

        try (GLVertexArray newModel = new GLVertexArrayInfo()
                .create()) {

            this.model.swap(newModel);
        }

        try (GLBuffer newUniforms = new GLBufferInfo()
                .withPreferredTarget(GLBufferTarget.UNIFORM)
                .withUsage(GLBufferInfo.Usage.DYNAMIC_DRAW)
                .withSize(20 * Float.BYTES)
                .create()) {

            try (MemoryStack mem = MemoryStack.stackPush()) {
                final ByteBuffer initialData = mem.calloc(20 * Float.BYTES);

                initialData
                        .putFloat(8 * Float.BYTES, 1F)
                        .putFloat(10 * Float.BYTES, 1F)
                        .putFloat(11 * Float.BYTES, 1F)
                        .putFloat(16 * Float.BYTES, 1F)
                        .putFloat(17 * Float.BYTES, 1F)
                        .putFloat(18 * Float.BYTES, 1F)
                        .putFloat(19 * Float.BYTES, 1F);

                newUniforms.subData(0L, initialData);
            }

            this.uniforms.swap(newUniforms);
        }
    }

    private void checkMapping() {
        if (this.uniformMapping == null) {
            this.uniformMapping = this.uniforms.map(EnumSet.of(GLBufferAccess.WRITE));
            this.uniformData = this.uniformMapping.getPointer().asFloatBuffer();
        }
    }

    @Override
    public void setColorTransform(final ColorTransform ct) {
        if (!this.supportsColorTransform) {
            throw new UnsupportedOperationException("ImageLayer does not support color transform!");
        }

        try {
            this.writeLock.acquire();

            this.checkMapping();

            final float ca = (float) Math.cos(Math.toRadians(ct.hsv.hue));
            final float sa = (float) Math.sin(Math.toRadians(ct.hsv.hue));

            this.uniformData
                    .put(8, ca)
                    .put(9, sa)
                    .put(10, ct.hsv.saturation)
                    .put(11, ct.hsv.value)
                    .put(12, ct.rgbaOffset.red)
                    .put(13, ct.rgbaOffset.green)
                    .put(14, ct.rgbaOffset.blue)
                    .put(15, ct.rgbaOffset.alpha)
                    .put(16, ct.rgbaScale.red)
                    .put(17, ct.rgbaScale.green)
                    .put(18, ct.rgbaScale.blue)
                    .put(19, ct.rgbaScale.alpha);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while writing color transform!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void scrollImage(float s0, float t0, float s1, float t1) {
        try {
            this.writeLock.acquire();

            this.checkMapping();

            this.uniformData
                    .put(0, s0)
                    .put(1, t0)
                    .put(2, s1)
                    .put(3, t1);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while writing image scroll!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void scrollMask(float s0, float t0, float s1, float t1) {
        if (!this.supportsMask) {
            throw new UnsupportedOperationException("ImageLayer does not support mask!");
        }

        try {
            this.writeLock.acquire();

            this.checkMapping();

            this.uniformData
                    .put(4, s0)
                    .put(5, t0)
                    .put(6, s1)
                    .put(7, t1);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while writing mask scroll!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void onFrameStart() {
        try {
            this.writeLock.acquire();
            this.isWritable = true;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while entering frame!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public GLBlendStateInfo getBlendState() {
        return GLConstants.PREMULTIPLY_BLEND_STATE;
    }

    @Override
    public void onFrameEnd() {
        try {
            this.writeLock.acquire();
            this.isWritable = false;

            if (this.uniformMapping != null) {
                this.uniformMapping.close();
                this.uniformMapping = null;
                this.uniformData = null;
            }

            final GLConstants.ImageProgram imgProgram = GLConstants.getImageProgramInstance();

            this.uniforms.bindBase(GLBufferTarget.UNIFORM, 0);

            GLUniform.uniform1(imgProgram.uImage, 0);
            this.image.bind(0);

            if (this.supportsMask) {
                GLUniform.uniform1(imgProgram.uMask, 1);
                GLUniform.uniform1(imgProgram.uSupportsMask, 1);

                this.mask.bind(1);
            } else {
                GLUniform.uniform1(imgProgram.uSupportsMask, 0);
            }

            if (this.supportsColorTransform) {
                GLUniform.uniform1(imgProgram.uSupportsCT, 1);
            } else {
                GLUniform.uniform1(imgProgram.uSupportsCT, 0);
            }

            this.model.bind();

            GLDraw.drawArrays(GLDraw.Mode.TRIANGLE_STRIP, 0, 4);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while exiting frame!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void setProjection(float[] projection) {
        throw new UnsupportedOperationException("Image Layer does not support projection matrix!");
    }

    @Override
    public boolean isWritable() {
        try {
            this.writeLock.acquire();

            return this.isWritable;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while checking if writable!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void close() {
        if (this.info != null) {
            if (this.uniformMapping != null) {
                this.uniformMapping.close();
                this.uniformMapping = null;
            }

            this.uniforms.close();
            this.model.close();
            this.image.close();
            this.mask.close();
            this.info = null;
        }
    }

    @Override
    public GLProgram getProgram() {
        return GLConstants.getImageProgramInstance().program;
    }

    @Override
    public ImageLayerInfo getInfo() {
        return this.info;
    }

}
