/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.gl;

import com.longlinkislong.gloop2.gl.GLBlendStateInfo;
import com.longlinkislong.gloop2.gl.GLBuffer;
import com.longlinkislong.gloop2.gl.GLBufferAccess;
import com.longlinkislong.gloop2.gl.GLBufferInfo;
import com.longlinkislong.gloop2.gl.GLBufferTarget;
import com.longlinkislong.gloop2.gl.GLDraw;
import com.longlinkislong.gloop2.gl.GLProgram;
import com.longlinkislong.gloop2.gl.GLUniform;
import com.longlinkislong.gloop2.gl.GLVertexArray;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo.AttributeDescription;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo.BindingDescription;
import com.longlinkislong.gloop2.gl.GLVertexFormat;
import com.runouw.renderer.ColorTransformSlot;
import com.runouw.renderer.ImageView;
import com.runouw.renderer.SpriteLayer;
import com.runouw.renderer.SpriteLayerInfo;
import com.runouw.renderer.SpriteSlot;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public final class GLSpriteLayer implements SpriteLayer, GLLayer {

    private final Set<GLBufferAccess> mapFlags;
    private final SpriteLayerInfo info;
    private final GLSpritesheet spritesheet;
    private final GLBuffer instanceData = new GLBuffer();
    private final GLBuffer ctData = new GLBuffer();
    private final GLVertexArray model = new GLVertexArray();
    private final FloatBuffer projection;
    private final GLSpriteSlot[] spriteSlots;
    private final GLColorTransformSlot[] ctSlots;
    private final PointerBuffer pInstance;
    private final PointerBuffer pColorTransform;
    private GLBuffer.MappedBuffer instanceMapping;
    private GLBuffer.MappedBuffer ctMapping;
    private final AtomicBoolean isWritable = new AtomicBoolean(false);
    private int drawLimit;
    public final boolean supportsColorTransforms;
    public final boolean supportsMask;   
    
    GLSpriteLayer(final SpriteLayerInfo info, final GLSpritesheet spritesheet) {
        this.supportsColorTransforms = info.maxColorTransforms > 0;
        this.supportsMask = info.validMasks.size() > 0;
        this.drawLimit = info.maxSpriteSlots;
        this.info = info;
        this.spritesheet = spritesheet;

        if (info.streaming) {
            this.mapFlags = EnumSet.of(GLBufferAccess.WRITE, GLBufferAccess.INVALIDATE_BUFFER);
        } else {
            this.mapFlags = EnumSet.of(GLBufferAccess.WRITE);
        }

        this.pInstance = MemoryUtil.memAllocPointer(1);
        this.pColorTransform = MemoryUtil.memAllocPointer(1);
        this.projection = MemoryUtil.memAllocFloat(16);
        this.spriteSlots = new GLSpriteSlot[info.maxSpriteSlots];

        for (int i = 0; i < info.maxSpriteSlots; i++) {
            this.spriteSlots[i] = new GLSpriteSlot(this.pInstance, i * GLSpriteSlot.BYTES);
        }

        final int instanceDataSize = info.maxSpriteSlots * GLSpriteSlot.BYTES;
        final ByteBuffer initialInstanceData = MemoryUtil.memAlloc(instanceDataSize);
        
        while (initialInstanceData.hasRemaining()) {            
            initialInstanceData.putFloat(Float.NaN);
        }
        
        initialInstanceData.flip();

        try (GLBuffer newInstanceData = new GLBufferInfo()
                .withSize(instanceDataSize)
                .withInitialData(initialInstanceData)
                .withUsage(info.streaming ? GLBufferInfo.Usage.STREAM_DRAW : GLBufferInfo.Usage.DYNAMIC_DRAW)
                .withPreferredTarget(GLBufferTarget.ARRAY)
                .create()) {

            this.instanceData.swap(newInstanceData);
        } finally {
            MemoryUtil.memFree(initialInstanceData);
        }

        if (info.maxColorTransforms > 0) {
            try (GLBuffer newCTData = new GLBufferInfo()
                    .withSize(info.maxColorTransforms * GLColorTransformSlot.BYTES)
                    .withUsage(GLBufferInfo.Usage.DYNAMIC_DRAW)
                    .withPreferredTarget(GLBufferTarget.UNIFORM)
                    .create()) {

                this.ctData.swap(newCTData);
            }

            this.ctSlots = new GLColorTransformSlot[info.maxColorTransforms];

            for (int i = 0; i < info.maxColorTransforms; i++) {
                this.ctSlots[i] = new GLColorTransformSlot(this.pColorTransform, i * GLColorTransformSlot.BYTES);
            }
        } else {
            this.ctSlots = null;
        }

        try (GLVertexArray newModel = new GLVertexArrayInfo()
                .withBindings(new BindingDescription()
                        .withBinding(0)
                        .withBuffer(this.instanceData)
                        .withDivisor(1)
                        .withOffset(0L)
                        .withStride(GLSpriteSlot.BYTES))
                .withAttributes(
                        new AttributeDescription()
                                .withBinding(0)
                                .withLocation(0)
                                .withOffset(0)
                                .withFormat(GLVertexFormat.VEC2),
                        new AttributeDescription()
                                .withBinding(0)
                                .withLocation(1)
                                .withOffset(2 * Float.BYTES)
                                .withFormat(GLVertexFormat.VEC2),
                        new AttributeDescription()
                                .withBinding(0)
                                .withLocation(2)
                                .withOffset(4 * Float.BYTES)
                                .withFormat(GLVertexFormat.VEC2),
                        new AttributeDescription()
                                .withBinding(0)
                                .withLocation(3)
                                .withOffset(6 * Float.BYTES)
                                .withFormat(GLVertexFormat.VEC2),
                        new AttributeDescription()
                                .withBinding(0)
                                .withLocation(4)
                                .withOffset(8 * Float.BYTES)
                                .withFormat(GLVertexFormat.FLOAT),
                        new AttributeDescription()
                                .withBinding(0)
                                .withLocation(5)
                                .withOffset(9 * Float.BYTES)
                                .withFormat(GLVertexFormat.VEC3),
                        new AttributeDescription()
                                .withBinding(0)
                                .withLocation(6)
                                .withOffset(12 * Float.BYTES)
                                .withFormat(GLVertexFormat.VEC3),
                        new AttributeDescription()
                                .withBinding(0)
                                .withLocation(7)
                                .withOffset(15 * Float.BYTES)
                                .withFormat(GLVertexFormat.INT)
                ).create()) {

            this.model.swap(newModel);
        }
    }

    @Override
    public int getMaxDrawLimit() {
        return this.info.maxSpriteSlots;
    }

    @Override
    public void setDrawLimit(final int drawLimit) {
        this.drawLimit = drawLimit;
    }

    @Override
    public ImageView getSpriteView(String spriteId) {
        return this.spritesheet.spriteRefs.get(spriteId);
    }

    @Override
    public ImageView getMaskView(String maskId) {
        return this.spritesheet.maskRefs.get(maskId);
    }

    @Override
    public ColorTransformSlot[] getColorTransformSlots() {
        return this.ctSlots;
    }

    @Override
    public ColorTransformSlot getColorTransformSlot(final int ctid) {
        return this.ctSlots[ctid];
    }

    @Override
    public SpriteSlot[] getSpriteSlots() {
        return this.spriteSlots;
    }

    @Override
    public SpriteSlot getSpriteSlot(int slotId) {
        return this.spriteSlots[slotId];
    }

    @Override
    public SpriteLayerInfo getInfo() {
        return this.info;
    }

    @Override
    public void onFrameStart() {
        this.instanceMapping = this.instanceData.map(mapFlags);

        final ByteBuffer instance = this.instanceMapping.getPointer();

        this.pInstance.put(0, instance);

        if (info.maxColorTransforms > 0) {
            this.ctMapping = this.ctData.map(mapFlags);
            final ByteBuffer colorTransform = this.ctMapping.getPointer();

            this.pColorTransform.put(0, colorTransform);
        }

        if (this.info.streaming) {
            this.isWritable.set(true);
        }
    }

    @Override
    public void onFrameEnd() {
        if (this.info.streaming) {
            this.isWritable.set(false);
        }

        this.instanceMapping.close();
        this.instanceMapping = null;

        if (this.ctMapping != null) {
            this.ctMapping.close();

            this.ctData.bindBase(GLBufferTarget.UNIFORM, 0);
        }

        this.pInstance.put(0, 0L);

        final GLConstants.SpriteProgram spriteProgram = GLConstants.getSpriteProgramInstance();

        GLUniform.uniformMatrix4(spriteProgram.uProjection, false, projection);
        GLUniform.uniform1(spriteProgram.uSpritesheet, 0);

        if (this.supportsColorTransforms) {
            GLUniform.uniform1(spriteProgram.uSupportsCT, 1);
        } else {
            GLUniform.uniform1(spriteProgram.uSupportsCT, 0);
        }

        if (this.supportsMask) {
            GLUniform.uniform1(spriteProgram.uSupportsMask, 1);
        } else {
            GLUniform.uniform1(spriteProgram.uSupportsMask, 0);
        }

        this.spritesheet.texture.bind(0);

        this.model.bind();

        GLDraw.drawArraysInstanced(GLDraw.Mode.TRIANGLE_STRIP, 0, 4, this.drawLimit);
    }

    @Override
    public void setProjection(float[] projection) {
        this.projection.put(projection).flip();
    }

    @Override
    public boolean isWritable() {
        return this.isWritable.get();
    }

    @Override
    public void close() {
        this.ctData.close();
        this.instanceData.close();
        this.model.close();

        MemoryUtil.memFree(this.pColorTransform);
        MemoryUtil.memFree(this.pInstance);
        MemoryUtil.memFree(this.projection);
    }

    @Override
    public GLProgram getProgram() {
        return GLConstants.getSpriteProgramInstance().program;
    }

    @Override
    public GLBlendStateInfo getBlendState() {
        return GLConstants.PREMULTIPLY_BLEND_STATE;
    }
}
