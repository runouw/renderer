/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.gl;

import com.runouw.renderer.ColorTransformSlot;
import com.runouw.renderer.HSV;
import com.runouw.renderer.RGBA;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public final class GLColorTransformSlot implements ColorTransformSlot {
    public static final int BYTES = 12 * Float.BYTES;
    private final PointerBuffer pptr;
    private final int offset;
    
    GLColorTransformSlot(final PointerBuffer pptr, final int offset) {
        this.pptr = pptr;
        this.offset = offset;
    }        

    @Override
    public GLColorTransformSlot setHSV(float[] hsv) {
        final long ptr = pptr.get(0) + offset;
        
        MemoryUtil.memPutFloat(ptr, hsv[0]);
        MemoryUtil.memPutFloat(ptr + 4, hsv[1]);
        MemoryUtil.memPutFloat(ptr + 8, hsv[2]);
        MemoryUtil.memPutFloat(ptr + 12, hsv[3]);                
        
        return this;
    }

    @Override
    public GLColorTransformSlot setHSV(FloatBuffer hsv) {               
        this.pptr.getByteBuffer(offset, BYTES)
                .asFloatBuffer()
                .put(hsv);
        
        return this;
    }

    @Override
    public GLColorTransformSlot setRGBAOffset(float[] rgba) {
        final long ptr = pptr.get(0) + offset + 4 * Float.BYTES;
        
        MemoryUtil.memPutFloat(ptr, rgba[0]);
        MemoryUtil.memPutFloat(ptr + 4, rgba[1]);
        MemoryUtil.memPutFloat(ptr + 8, rgba[2]);
        MemoryUtil.memPutFloat(ptr + 12, rgba[3]);
        
        return this;
    }

    @Override
    public GLColorTransformSlot setRGBAOffset(FloatBuffer rgba) {
        this.pptr.getByteBuffer(this.offset + 4 * Float.BYTES, BYTES)
                .asFloatBuffer()
                .put(rgba);                
        
        return this;
    }

    @Override
    public GLColorTransformSlot setRGBAScale(float[] rgba) {
        final long ptr = pptr.get(0) + offset + 8 * Float.BYTES;
        
        MemoryUtil.memPutFloat(ptr, rgba[0]);
        MemoryUtil.memPutFloat(ptr + 4, rgba[1]);
        MemoryUtil.memPutFloat(ptr + 8, rgba[2]);
        MemoryUtil.memPutFloat(ptr + 12, rgba[3]);
        
        return this;
    }

    @Override
    public GLColorTransformSlot setRGBAScale(FloatBuffer rgba) {
        this.pptr.getByteBuffer(this.offset + 8 * Float.BYTES, BYTES)
                .asFloatBuffer()
                .put(rgba);
        
        return this;
    }

    @Override
    public GLColorTransformSlot set(HSV hsv, RGBA rgbaOffset, RGBA rgbaScale) {
        final FloatBuffer data = this.pptr.getByteBuffer(this.offset, BYTES).asFloatBuffer();
        final double hue = Math.toRadians(hsv.hue);
        
        data.put((float) Math.cos(hue)).put((float) Math.sin(hue)).put(hsv.saturation).put(hsv.value);
        data.put(rgbaOffset.red).put(rgbaOffset.green).put(rgbaOffset.blue).put(rgbaOffset.alpha);
        data.put(rgbaScale.red).put(rgbaScale.green).put(rgbaScale.blue).put(rgbaScale.alpha);
        
        return this;
    }
    
    @Override
    public GLColorTransformSlot clear() {
        final IntBuffer data = this.pptr.getByteBuffer(this.offset, BYTES).asIntBuffer();
        
        for (int i = 0; i < 12; i++) {
            data.put(0);
        }
        
        return this;
    }
}
