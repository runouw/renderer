/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.gl;

import com.longlinkislong.gloop2.gl.GLBlendStateInfo;
import com.longlinkislong.gloop2.gl.GLBuffer;
import com.longlinkislong.gloop2.gl.GLBufferAccess;
import com.longlinkislong.gloop2.gl.GLBufferInfo;
import com.longlinkislong.gloop2.gl.GLBufferTarget;
import com.longlinkislong.gloop2.gl.GLDraw;
import com.longlinkislong.gloop2.gl.GLFilter;
import com.longlinkislong.gloop2.gl.GLFormat;
import com.longlinkislong.gloop2.gl.GLInternalFormat;
import com.longlinkislong.gloop2.gl.GLPixelInfo;
import com.longlinkislong.gloop2.gl.GLProgram;
import com.longlinkislong.gloop2.gl.GLSamplerInfo;
import com.longlinkislong.gloop2.gl.GLTexture;
import com.longlinkislong.gloop2.gl.GLTextureInfo;
import com.longlinkislong.gloop2.gl.GLUniform;
import com.longlinkislong.gloop2.gl.GLVertexArray;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo;
import com.runouw.renderer.AbstractJFXLayer;
import com.runouw.renderer.JFXLayerInfo;
import com.runouw.renderer.gl.GLConstants.JFXProgram;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.EnumSet;
import org.lwjgl.system.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class GLJFXLayer extends AbstractJFXLayer implements GLLayer {

    private static final Logger LOGGER = LoggerFactory.getLogger(GLJFXLayer.class);

    @Override
    public GLProgram getProgram() {
        return GLConstants.getJFXProgramInstance().program;
    }

    @Override
    public GLBlendStateInfo getBlendState() {
        return GLConstants.PREMULTIPLY_BLEND_STATE;
    }

    private final GLVertexArray model = new GLVertexArray();
    private final GLTexture raster = new GLTexture();
    private final GLBuffer staging = new GLBuffer();
    private final FloatBuffer proj;
    private GLBuffer.MappedBuffer stagingMapping;

    public GLJFXLayer(final JFXLayerInfo info) {
        super(info);

        try (GLVertexArray newVertexArray = new GLVertexArrayInfo()
                .create()) {

            this.model.swap(newVertexArray);
        }

        try (GLTexture newRaster = new GLTextureInfo()
                .withExtent(info.width, info.height, 1)
                .withFormat(GLInternalFormat.R8G8B8A8_UNORM)
                .withLayers(1)
                .withLevels(1)
                .withSamplerInfo(new GLSamplerInfo()
                        .withMinFilter(GLFilter.LINEAR)
                        .withMagFilter(GLFilter.LINEAR)
                        .withWrapR(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE)
                        .withWrapS(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE))
                .create()) {

            this.raster.swap(newRaster);
        }

        if (info.initialScene != null) {
            this.setScene(info.initialScene);
        }

        try (GLBuffer newStaging = new GLBufferInfo()
                .withPreferredTarget(GLBufferTarget.PIXEL_UNPACK)
                .withSize(info.width * info.height * Integer.BYTES)
                .withUsage(GLBufferInfo.Usage.STREAM_COPY)
                .create()) {

            this.staging.swap(newStaging);
        }
        
        this.proj = MemoryUtil.memAllocFloat(16);
    }

    @Override
    public void onFrameStart() {
    }

    @Override
    public void onFrameEnd() {
        super.onFrameEnd();

        final JFXProgram program = GLConstants.getJFXProgramInstance();

        this.raster.bind(0);
        GLUniform.uniform1(program.uImage, 0);
        GLUniform.uniformMatrix4(program.uProjection, false, this.proj);

        this.model.bind();

        GLDraw.drawArrays(GLDraw.Mode.TRIANGLE_STRIP, 0, 4);
    }

    @Override
    public void close() {
        super.close();

        if (this.stagingMapping != null) {
            this.stagingMapping.close();
        }

        this.staging.close();
        this.model.close();
        this.raster.close();
        
        MemoryUtil.memFree(this.proj);
    }

    @Override
    protected IntBuffer getImageTransferBuffer() {
        this.stagingMapping = this.staging.map(EnumSet.of(GLBufferAccess.WRITE, GLBufferAccess.INVALIDATE_BUFFER));

        return this.stagingMapping.getPointer().asIntBuffer();
    }

    private final GLPixelInfo uploadInfo = new GLPixelInfo()
            .withFormat(GLFormat.RGBA)
            .withType(GLPixelInfo.Type.UNSIGNED_BYTE);

    @Override
    protected void applyImageTransfer() {
        this.stagingMapping.close();
        this.stagingMapping = null;

        final JFXLayerInfo info = this.getInfo();

        this.staging.bind();
        this.raster.subImage(0, 0, 0, 0, info.width, info.height, 1, uploadInfo);
        GLBuffer.DEFAULT_BUFFER.bind(GLBufferTarget.PIXEL_UNPACK);
    }

    @Override
    public void setProjection(float[] projection) {
        assert projection.length == 16 : "Projection matrix must be 4x4!";
        
        this.proj.put(projection).flip();
    }
}
