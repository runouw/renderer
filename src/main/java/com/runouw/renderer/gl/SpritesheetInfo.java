/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.gl;

import com.longlinkislong.gloop2.Tools;
import com.runouw.renderer.MaskResourceInfo;
import com.runouw.renderer.SpriteResourceInfo;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public final class SpritesheetInfo {
    public final Set<SpriteResourceInfo> validSprites;
    public final Set<MaskResourceInfo> validMasks;
    public final int size;
    
    public SpritesheetInfo(final Set<SpriteResourceInfo> validSprites, final Set<MaskResourceInfo> validMasks) {
        this.validSprites = Tools.copySet(validSprites);
        this.validMasks = Tools.copySet(validMasks);
        this.size = this.validSprites.size() + this.validMasks.size();
    }
    
    public boolean isCompatible(final SpritesheetInfo other) {
        return this.validSprites.containsAll(other.validSprites)
                && this.validMasks.containsAll(other.validMasks);
    }
    
    public static SpritesheetInfo merge(final SpritesheetInfo a, final SpritesheetInfo b) {
        final Set<SpriteResourceInfo> validSprites = new HashSet<>();
        
        validSprites.addAll(a.validSprites);
        validSprites.addAll(b.validSprites);
        
        final Set<MaskResourceInfo> validMasks = new HashSet<>();
        
        validMasks.addAll(a.validMasks);
        validMasks.addAll(b.validMasks);
        
        return new SpritesheetInfo(validSprites, validMasks);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (this == obj) {
            return true;
        } else if (obj instanceof SpritesheetInfo) {
            final SpritesheetInfo other = (SpritesheetInfo) obj;
            
            return this.validSprites.equals(other.validSprites)
                    && this.validMasks.equals(other.validMasks);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.validSprites);
        hash = 41 * hash + Objects.hashCode(this.validMasks);
        return hash;
    }
}
