/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.gl;

import com.longlinkislong.gloop2.gl.GLBlendStateInfo;
import com.longlinkislong.gloop2.gl.GLProgram;
import com.runouw.renderer.Layer;

/**
 *
 * @author zmichaels
 */
public interface GLLayer extends Layer {
    GLProgram getProgram();
    
    GLBlendStateInfo getBlendState();
}
