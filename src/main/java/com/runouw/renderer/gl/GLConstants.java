/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.gl;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.gl.GLBlendFunction;
import com.longlinkislong.gloop2.gl.GLBlendStateInfo;
import com.longlinkislong.gloop2.gl.GLCompileException;
import com.longlinkislong.gloop2.gl.GLShader;
import com.longlinkislong.gloop2.gl.GLProgram;
import com.longlinkislong.gloop2.gl.GLProgramInfo;
import com.longlinkislong.gloop2.gl.GLProgramInfo.AttributeStateInfo;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
final class GLConstants {

    private static final Logger LOGGER = LoggerFactory.getLogger(GLConstants.class);
    private static final String BASE_PROP = "com.runouw.renderer.gl.GLConstants";
    static final ExecutorService ASYNC = Executors.newCachedThreadPool(new ThreadFactory() {
        private final AtomicInteger THREAD_ID = new AtomicInteger(0);

        @Override
        public Thread newThread(Runnable r) {
            final Thread thread = new Thread(r);

            thread.setName("OpenGL Worker Thread - " + THREAD_ID.getAndIncrement());

            return thread;
        }
    });

    static final GLBlendStateInfo PREMULTIPLY_BLEND_STATE = new GLBlendStateInfo()
            .withBlendEnable(true)
            .withSrcColorBlend(GLBlendFunction.ONE)
            .withDstColorBlend(GLBlendFunction.ONE_MINUS_SRC_ALPHA)
            .withSrcAlphaBlend(GLBlendFunction.ONE)
            .withDstAlphaBlend(GLBlendFunction.ONE_MINUS_SRC_ALPHA);

    static BoundColorTransformProgram getBoundColorTransformProgramInstance() {
        return BoundColorTransformProgramHolder.INSTANCE;
    }
    
    static final class BoundColorTransformProgramHolder {
        private static final BoundColorTransformProgram INSTANCE;
        
        static {
            try {
                INSTANCE = new BoundColorTransformProgram();
            } catch (IOException ex) {
                throw new RuntimeException("Unable to load BoundColorTransform shader!", ex);
            }
        }
    }
    
    static final class BoundColorTransformProgram {
        private static final String COMPUTE_SHADER_URL = System.getProperty(BASE_PROP + ".BoundColorTransformProgram.compute_shader_url", "BoundColorTransform.comp");
        
        final GLProgram program = new GLProgram();
        final int uSrc;
        final int uDst;
        final int uBounds;
        final int uPosition;
        final int uHsv;
        final int uOffset;
        final int uScale;
        
        private BoundColorTransformProgram() throws IOException {
            final String csrc;
            
            try (InputStream cin = GLConstants.class.getResourceAsStream(COMPUTE_SHADER_URL)) {
                csrc = Tools.readAll(cin);
            }
            
            try (GLShader csh = GLShader.newComputeShader(csrc)) {
                try (GLProgram newProgram = new GLProgramInfo().withShaders(csh).create()) {
                    this.uSrc = newProgram.getUniformLocation("uSrc");
                    this.uDst = newProgram.getUniformLocation("uDst");
                    this.uBounds = newProgram.getUniformLocation("uBounds");
                    this.uPosition = newProgram.getUniformLocation("uPosition");
                    this.uHsv = newProgram.getUniformLocation("uHsv");
                    this.uOffset = newProgram.getUniformLocation("uOffset");
                    this.uScale = newProgram.getUniformLocation("uScale");
                    
                    assert this.uSrc >= 0 : "Uniform uSrc is not defined!";
                    assert this.uDst >= 0 : "Uniform uDst is not defined!";
                    assert this.uBounds >= 0 : "Uniform uBounds is not defined!";
                    assert this.uHsv >= 0 : "Uniform uHsv is not defined!";
                    assert this.uOffset >=0 : "Uniform uOffset is not defined!";
                    assert this.uScale >= 0 : "Uniform uScale is not defined!";
                    
                    this.program.swap(newProgram);
                }
            }
        }
    }

    static ArrayColorTransformProgram getArrayColorTransformProgramInstance() {
        return ArrayColorTransformProgramHolder.INSTANCE;
    }

    static final class ArrayColorTransformProgram {

        private static final String COMPUTE_SHADER_URL = System.getProperty(BASE_PROP + ".ArrayColorTransformProgram.compute_shader_url", "ArrayColorTransform.comp");

        final GLProgram program = new GLProgram();
        final int uSrc;
        final int uDst;
        final int uColorTransformBlock;

        private ArrayColorTransformProgram() throws IOException {
            final String csrc;

            try (InputStream cin = GLConstants.class.getResourceAsStream(COMPUTE_SHADER_URL)) {
                csrc = Tools.readAll(cin);
            }

            try (GLShader csh = GLShader.newComputeShader(csrc)) {
                try (GLProgram newProgram = new GLProgramInfo()
                        .withShaders(csh)
                        .create()) {

                    this.uSrc = newProgram.getUniformLocation("uSrc");
                    this.uDst = newProgram.getUniformLocation("uDst");
                    this.uColorTransformBlock = newProgram.getUniformBlockIndex("uColorTransformBlock");

                    assert this.uSrc >= 0 : "Uniform uSrc is not defined!";
                    assert this.uDst >= 0 : "Uniform uDst is not defined!";
                    assert this.uColorTransformBlock >= 0 : "UniformBlock uColorTransformBlock is not defined!";

                    newProgram.uniformBlockBinding(this.uColorTransformBlock, 0);

                    program.swap(newProgram);
                }
            }
        }
    }

    static final class ArrayColorTransformProgramHolder {

        private static final ArrayColorTransformProgram INSTANCE;

        static {
            try {
                INSTANCE = new ArrayColorTransformProgram();
            } catch (IOException ex) {
                throw new RuntimeException("Unable to load ArrayColorTransform shader!", ex);
            }
        }
    }

    static final class SingleColorTransformProgram {

        private static final String COMPUTE_SHADER_URL = System.getProperty(BASE_PROP + ".SingleColorTransformProgram.compute_shader_url", "SingleColorTransform.comp");

        final GLProgram program = new GLProgram();
        final int uHsv;
        final int uOffset;
        final int uScale;
        final int uSrc;
        final int uDst;

        private SingleColorTransformProgram() throws IOException {
            final String csrc;

            try (InputStream cin = GLConstants.class.getResourceAsStream(COMPUTE_SHADER_URL)) {
                csrc = Tools.readAll(cin);
            }

            try (GLShader csh = GLShader.newComputeShader(csrc)) {
                try (GLProgram newProgram = new GLProgramInfo()
                        .withShaders(csh)
                        .create()) {

                    this.uHsv = newProgram.getUniformLocation("uHsv");
                    this.uOffset = newProgram.getUniformLocation("uOffset");
                    this.uScale = newProgram.getUniformLocation("uScale");
                    this.uSrc = newProgram.getUniformLocation("uSrc");
                    this.uDst = newProgram.getUniformLocation("uDst");

                    assert this.uHsv >= 0 : "Uniform uHsv is not defined!";
                    assert this.uOffset >= 0 : "Uniform uOffset is not defined!";
                    assert this.uScale >= 0 : "Uniform uScale is not defined!";
                    assert this.uSrc >= 0 : "Uniform uSrc is not defined!";
                    assert this.uDst >= 0 : "Uniform uDst is not defined!";

                    this.program.swap(newProgram);
                }
            }
        }
    }

    private static final class SingleColorTransformProgramHolder {

        private static final SingleColorTransformProgram INSTANCE;

        static {
            try {
                INSTANCE = new SingleColorTransformProgram();
            } catch (IOException ex) {
                throw new RuntimeException("Unable to load SingleColorTransformProgram shader!", ex);
            }
        }
    }

    static final class TileProgram {

        private static final String VERTEX_SHADER_URL = System.getProperty(BASE_PROP + ".TileProgram.vertex_shader_url", "TileLayer.vert");
        private static final String FRAGMENT_SHADER_URL = System.getProperty(BASE_PROP + ".TileProgram.fragment_shader_url", "TileLayer.frag");

        final GLProgram program = new GLProgram();
        final int uProjection;
        final int uImage;

        private TileProgram() throws IOException {
            final String vsrc;
            final String fsrc;

            try (InputStream vin = GLConstants.class.getResourceAsStream(VERTEX_SHADER_URL)) {
                vsrc = Tools.readAll(vin);
            }

            try (InputStream fin = GLConstants.class.getResourceAsStream(FRAGMENT_SHADER_URL)) {
                fsrc = Tools.readAll(fin);
            }

            try (GLShader vsh = GLShader.newVertexShader(vsrc);
                    GLShader fsh = GLShader.newFragmentShader(fsrc)) {

                try (GLProgram newProgram = new GLProgramInfo()
                        .withShaders(vsh, fsh)
                        .create()) {

                    this.uImage = newProgram.getUniformLocation("uImage");
                    this.uProjection = newProgram.getUniformLocation("uProjection");

                    assert this.uImage >= 0 : "Uniform uImage is not defined!";
                    assert this.uProjection >= 0 : "Uniform uProjection is not defined!";

                    this.program.swap(newProgram);
                }
            }
        }
    }

    static final class JFXProgram {

        private static final String VERTEX_SHADER_URL = System.getProperty(BASE_PROP + ".JFXProgram.vertex_shader_url", "JFXLayer.vert");
        private static final String FRAGMENT_SHADER_URL = System.getProperty(BASE_PROP + ".JFXProgram.fragment_shader_url", "JFXLayer.frag");

        final GLProgram program = new GLProgram();
        final int uImage;
        final int uProjection;

        private JFXProgram() throws IOException {
            final String vsrc;
            final String fsrc;

            try (InputStream vin = GLConstants.class.getResourceAsStream(VERTEX_SHADER_URL)) {
                vsrc = Tools.readAll(vin);
            }

            try (InputStream fin = GLConstants.class.getResourceAsStream(FRAGMENT_SHADER_URL)) {
                fsrc = Tools.readAll(fin);
            }

            try (GLShader vsh = GLShader.newVertexShader(vsrc);
                    GLShader fsh = GLShader.newFragmentShader(fsrc)) {

                try (GLProgram newProgram = new GLProgramInfo()
                        .withShaders(vsh, fsh)
                        .create()) {

                    this.uImage = newProgram.getUniformLocation("uImage");
                    this.uProjection = newProgram.getUniformLocation("uProjection");

                    assert this.uImage >= 0 : "Uniform uImage is not defined!";
                    assert this.uProjection >= 0 : "Uniform uProjection is not defined!";

                    this.program.swap(newProgram);
                }
            }
        }
    }

    static final class FontProgram {

        private static final String VERTEX_SHADER_URL = System.getProperty(BASE_PROP + ".FontProgram.vertex_shader_url", "FontLayer.vert");
        private static final String FRAGMENT_SHADER_URL = System.getProperty(BASE_PROP + ".FontProgram.fragment_shader_url", "FontLayer.frag");

        final GLProgram program = new GLProgram();
        final int uFont;
        final int uProjection;

        private FontProgram() throws IOException {
            final String vsrc;
            final String fsrc;

            try (InputStream vin = GLConstants.class.getResourceAsStream(VERTEX_SHADER_URL);
                    InputStream fin = GLConstants.class.getResourceAsStream(FRAGMENT_SHADER_URL)) {

                vsrc = Tools.readAll(vin);
                fsrc = Tools.readAll(fin);
            }

            try (GLShader vsh = GLShader.newVertexShader(vsrc);
                    GLShader fsh = GLShader.newFragmentShader(fsrc)) {

                try (GLProgram newProgram = new GLProgramInfo()
                        .withAttributes(
                                new AttributeStateInfo()
                                        .withIndex(0)
                                        .withName("vPos"),
                                new AttributeStateInfo()
                                        .withIndex(1)
                                        .withName("vTex"),
                                new AttributeStateInfo()
                                        .withIndex(2)
                                        .withName("vColor"))
                        .withShaders(vsh, fsh)
                        .create()) {

                    this.uFont = newProgram.getUniformLocation("uFont");
                    this.uProjection = newProgram.getUniformLocation("uProjection");
                    this.program.swap(newProgram);

                    assert this.uFont >= 0 : "Uniform uFont was not defined!";
                    assert this.uProjection >= 0 : "Uniform uProjection was not defined!";
                }
            }
        }
    }

    static final class ImageProgram {

        private static final String VERTEX_SHADER_URL = System.getProperty(BASE_PROP + ".ImageProgram.vertex_shader_url", "ImageLayer.vert");
        private static final String FRAGMENT_SHADER_URL = System.getProperty(BASE_PROP + ".ImageProgram.fragment_shader_url", "ImageLayer.frag");

        final GLProgram program = new GLProgram();
        final int uImage;
        final int uMask;
        final int uDataBlock;
        final int uSupportsMask;
        final int uSupportsCT;

        private ImageProgram() throws IOException {
            final String vsrc;
            final String fsrc;

            try (InputStream vin = GLConstants.class.getResourceAsStream(VERTEX_SHADER_URL);
                    InputStream fin = GLConstants.class.getResourceAsStream(FRAGMENT_SHADER_URL)) {

                vsrc = Tools.readAll(vin);
                fsrc = Tools.readAll(fin);
            }

            try (GLShader vsh = GLShader.newVertexShader(vsrc);
                    GLShader fsh = GLShader.newFragmentShader(fsrc)) {

                try (GLProgram newProgram = new GLProgramInfo()
                        .withShaders(vsh, fsh)
                        .create()) {

                    this.uDataBlock = newProgram.getUniformBlockIndex("uDataBlock");
                    this.uImage = newProgram.getUniformLocation("uImage");
                    this.uMask = newProgram.getUniformLocation("uMask");
                    this.uSupportsMask = newProgram.getUniformLocation("uSupportsMask");
                    this.uSupportsCT = newProgram.getUniformLocation("uSupportsCT");

                    assert this.uDataBlock >= 0 : "Uniform Block uDataBlock is not defined!";
                    assert this.uSupportsCT >= 0 : "Uniform uSupportsCT is not defined!";
                    assert this.uImage >= 0 : "Uniform uImage is not defined!";
                    assert this.uMask >= 0 : "Uniform uMask is not defined!";
                    assert this.uSupportsMask >= 0 : "Uniform uSupportsMask is not defined!";

                    newProgram.uniformBlockBinding(uDataBlock, 0);

                    this.program.swap(newProgram);
                }
            } catch (GLCompileException ex) {
                LOGGER.error("ERR: {}", ex.getMessage());
                throw new RuntimeException("Unable to compile ImageProgram shader(s)!", ex);
            }
        }
    }

    static final class SpriteProgram {

        private static final String VERTEX_SHADER_URL = System.getProperty(BASE_PROP + ".SpriteProgram.vertex_shader_url", "SpriteLayer.vert");
        private static final String FRAGMENT_SHADER_URL = System.getProperty(BASE_PROP + ".SpriteProgram.fragment_shader_url", "SpriteLayer.frag");

        final GLProgram program = new GLProgram();
        final int uProjection;
        final int uSpritesheet;
        final int uColorTransformBlock;
        final int uSupportsMask;
        final int uSupportsCT;

        private SpriteProgram() throws IOException {
            final String vsrc;
            final String fsrc;

            try (InputStream vin = GLConstants.class.getResourceAsStream(VERTEX_SHADER_URL)) {
                vsrc = Tools.readAll(vin);
            }

            try (InputStream fin = GLConstants.class.getResourceAsStream(FRAGMENT_SHADER_URL)) {
                fsrc = Tools.readAll(fin);
            }

            try (GLShader vsh = GLShader.newVertexShader(vsrc)) {
                try (GLShader fsh = GLShader.newFragmentShader(fsrc)) {
                    try (GLProgram newProgram = new GLProgramInfo()
                            .withShaders(vsh, fsh)
                            .create()) {

                        this.uProjection = newProgram.getUniformLocation("uProjection");
                        this.uSpritesheet = newProgram.getUniformLocation("uSpritesheet");
                        this.uColorTransformBlock = newProgram.getUniformBlockIndex("uColorTransformBlock");
                        this.uSupportsCT = newProgram.getUniformLocation("uSupportsCT");
                        this.uSupportsMask = newProgram.getUniformLocation("uSupportsMask");

                        assert this.uProjection >= 0 : "Uniform uProjection is not defined!";
                        assert this.uSpritesheet >= 0 : "Uniform uSpritesheet is not defined!";
                        assert this.uColorTransformBlock >= 0 : "Uniform Block uColorTransformBlock is not defined!";
                        assert this.uSupportsCT >= 0 : "Uniform uSupportsCT is not defined!";

                        newProgram.uniformBlockBinding(this.uColorTransformBlock, 0);

                        this.program.swap(newProgram);
                    }
                }
            }
        }
    }

    private static final class TileProgramHolder {

        private static final TileProgram INSTANCE;

        static {
            try {
                INSTANCE = new TileProgram();
            } catch (IOException ex) {
                throw new RuntimeException("Unable to load TileProgram shader(s)!", ex);
            }
        }
    }

    static TileProgram getTileProgramInstance() {
        return TileProgramHolder.INSTANCE;
    }

    private static final class JFXProgramHolder {

        private static final JFXProgram INSTANCE;

        static {
            try {
                INSTANCE = new JFXProgram();
            } catch (IOException ex) {
                throw new RuntimeException("Unable to load JFXProgram shader(s)!", ex);
            }
        }
    }

    static JFXProgram getJFXProgramInstance() {
        return JFXProgramHolder.INSTANCE;
    }

    private static final class SpriteProgramHolder {

        private static final SpriteProgram INSTANCE;

        static {
            try {
                INSTANCE = new SpriteProgram();
            } catch (IOException ex) {
                throw new RuntimeException("Unable to load SpriteProgram shader(s)!", ex);
            }
        }

        private SpriteProgramHolder() {
        }
    }

    static SpriteProgram getSpriteProgramInstance() {
        return SpriteProgramHolder.INSTANCE;
    }

    private static final class FontProgramHolder {

        private static final FontProgram INSTANCE;

        static {
            try {
                INSTANCE = new FontProgram();
            } catch (IOException ex) {
                throw new RuntimeException("Unable to load FontProgram shader(s)!", ex);
            }
        }

        private FontProgramHolder() {
        }
    }

    private static final class ImageProgramHolder {

        private static final ImageProgram INSTANCE;

        static {
            try {
                INSTANCE = new ImageProgram();
            } catch (IOException ex) {
                throw new RuntimeException("Unable to load ImageProgram shader(s)!", ex);
            }
        }
    }

    static ImageProgram getImageProgramInstance() {
        return ImageProgramHolder.INSTANCE;
    }

    static FontProgram getFontProgramInstance() {
        return FontProgramHolder.INSTANCE;
    }

    static SingleColorTransformProgram getSingleColorTransformProgramInstance() {
        return SingleColorTransformProgramHolder.INSTANCE;
    }
}
