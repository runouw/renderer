/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.gl;

import com.longlinkislong.gloop2.gl.GLBlendStateInfo;
import com.longlinkislong.gloop2.gl.GLBuffer;
import com.longlinkislong.gloop2.gl.GLBufferAccess;
import com.longlinkislong.gloop2.gl.GLBufferInfo;
import com.longlinkislong.gloop2.gl.GLBufferTarget;
import com.longlinkislong.gloop2.gl.GLCompute;
import com.longlinkislong.gloop2.gl.GLDraw;
import com.longlinkislong.gloop2.gl.GLFilter;
import com.longlinkislong.gloop2.gl.GLFormat;
import com.longlinkislong.gloop2.gl.GLImageAccess;
import com.longlinkislong.gloop2.gl.GLInternalFormat;
import com.longlinkislong.gloop2.gl.GLMemoryBarrier;
import com.longlinkislong.gloop2.gl.GLPixelInfo;
import com.longlinkislong.gloop2.gl.GLProgram;
import com.longlinkislong.gloop2.gl.GLSamplerInfo;
import com.longlinkislong.gloop2.gl.GLTexture;
import com.longlinkislong.gloop2.gl.GLTextureInfo;
import com.longlinkislong.gloop2.gl.GLUniform;
import com.longlinkislong.gloop2.gl.GLVertexArray;
import com.longlinkislong.gloop2.gl.GLVertexArrayInfo;
import com.longlinkislong.gloop2.gl.GLVertexFormat;
import com.longlinkislong.gloop2.image.BufferedImage;
import com.longlinkislong.gloop2.image.Image;
import com.longlinkislong.gloop2.image.PixelFormat;
import com.runouw.renderer.TileFilter;
import com.runouw.renderer.TileLayer;
import com.runouw.renderer.TileLayerInfo;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;
import org.lwjgl.system.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class GLTileLayer implements TileLayer, GLLayer {
    private static final Logger LOGGER = LoggerFactory.getLogger(GLTileLayer.class);
    
    private static final class Update {

        private final int supertileX;
        private final int supertileY;

        public Update(final int tileX, final int tileY) {
            this.supertileX = tileX;
            this.supertileY = tileY;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            } else if (o == null) {
                return false;
            } else if (o instanceof Update) {
                final Update other = (Update) o;

                return this.supertileX == other.supertileX
                        && this.supertileY == other.supertileY;
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 23 * hash + this.supertileX;
            hash = 23 * hash + this.supertileY;
            return hash;
        }
    }

    private TileLayerInfo info;

    private final GLTexture temp0 = new GLTexture();
    private final GLTexture temp1 = new GLTexture();
    private final GLTexture tiles = new GLTexture();
    private final Semaphore writeLock = new Semaphore(1);
    private final FloatBuffer projMatrix;
    private final GLBuffer vSupertileRect = new GLBuffer();
    private final GLBuffer vLayerSelect = new GLBuffer();
    private final GLVertexArray model = new GLVertexArray();
    private final int totalSupertiles;
    private final int supertilesAcross;
    private final int supertilesDown;
    private final int tilesPerSupertile;
    private final Set<Update> updates = new HashSet<>();
    private boolean isWritable = false;
    private GLBuffer.MappedBuffer layerSelectMapping;
    private ByteBuffer ptrLayerSelect;
    private final int[] supertileMapping;
    private final List<TileFilter> filters;    
    
    @Override
    public void addTileFilter(TileFilter filter) {
        try {
            this.writeLock.acquire();
            this.filters.add(filter);

            for (int sty = 0; sty < this.supertilesDown; sty++) {
                final int y = sty * info.supertileSize;                
                
                for (int stx = 0; stx < this.supertilesAcross; stx++) {
                    final int x = stx * info.supertileSize;
                    
                    if (filter.intersects(x, y, info.supertileSize, info.supertileSize)) {                        
                        this.rebuildSupertile(stx, sty);
                    }
                }
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while applying TileFilter!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public boolean removeTileFilter(TileFilter filter) {
        try {
            this.writeLock.acquire();                        
            
            // skip rebuild if nothing changed.
            if (!this.filters.remove(filter)) {
                return false;
            }
            
            for (int sty = 0; sty < this.supertilesDown; sty++) {
                final int y = sty * info.supertileSize;
                
                for (int stx = 0; stx < this.supertilesAcross; stx++) {
                    final int x = stx * info.supertileSize;
                    
                    if (filter.intersects(x, y, info.supertileSize, info.supertileSize)) {
                        this.rebuildSupertile(stx, sty);
                    }
                }
            }
            
            return true;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while removing TileFilter!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    /*     
     * Max layers should instead be maximum number of visible/cached layers.
     * vLayerSelect.x should be adjusted for layer indirection.
     * vLayerSelect.zw should be set to NaN for hidden Supertiles.
     * Loading a Supertile will invoke all needed rebuilds!
     * Supertiles with no visible tiles should be skipped!
     */
    public GLTileLayer(final TileLayerInfo info) {
        this.info = info;
        this.tilesPerSupertile = info.supertileSize / info.tileSize;

        this.supertilesAcross = Math.max(1, info.tileSize * info.tileMap.width / info.supertileSize);
        this.supertilesDown = Math.max(1, info.tileSize * info.tileMap.height / info.supertileSize);

        final int tileLayers = supertilesAcross * supertilesDown;

        this.totalSupertiles = Math.max(2, tileLayers); // needs to be 2 minimum to force Texture2DArray                
        this.supertileMapping = new int[this.totalSupertiles];

        for (int i = 0; i < this.totalSupertiles; i++) {
            this.supertileMapping[i] = -1;
        }

        final int vsrBytesNeeded = 2 * 4 * Float.BYTES * totalSupertiles;
        final ByteBuffer vsrTmp = MemoryUtil.memCalloc(vsrBytesNeeded);

        for (int y = 0; y < this.supertilesDown; y++) {
            for (int x = 0; x < supertilesAcross; x++) {
                final float supertileX0 = x * info.supertileSize;
                final float supertileY0 = y * info.supertileSize;
                final float supertileX1 = supertileX0 + info.supertileSize;
                final float supertileY1 = supertileY0 + info.supertileSize;

                vsrTmp.putFloat(supertileX0).putFloat(supertileY0)
                        .putFloat(supertileX1).putFloat(supertileY1);
            }
        }

        // cant use flip here since tile layer 1 might be unused
        vsrTmp.position(0);

        try (GLBuffer newSupertileRect = new GLBufferInfo()
                .withSize(vsrBytesNeeded)
                .withPreferredTarget(GLBufferTarget.ARRAY)
                .withUsage(GLBufferInfo.Usage.STATIC_DRAW)
                .withInitialData(vsrTmp)
                .create()) {

            this.vSupertileRect.swap(newSupertileRect);
        } finally {
            MemoryUtil.memFree(vsrTmp);
        }

        final int vlsBytesNeeded = 4 * Float.BYTES * this.totalSupertiles;
        final ByteBuffer vlsTmp = MemoryUtil.memCalloc(vlsBytesNeeded);

        for (int i = 0; i < this.totalSupertiles; i++) {
            vlsTmp.putFloat(Float.NaN).putFloat(Float.NaN).putFloat(Float.NaN).putFloat(Float.NaN);
        }

        vlsTmp.flip();

        try (GLBuffer newLayerSelect = new GLBufferInfo()
                .withSize(vlsBytesNeeded)
                .withPreferredTarget(GLBufferTarget.ARRAY)
                .withUsage(GLBufferInfo.Usage.DYNAMIC_DRAW)
                .withInitialData(vlsTmp)
                .create()) {

            this.vLayerSelect.swap(newLayerSelect);
        } finally {
            MemoryUtil.memFree(vlsTmp);
        }
        
        final GLSamplerInfo samplerInfo = new GLSamplerInfo()
                .withMagFilter(GLFilter.NEAREST)
                .withMinFilter(GLFilter.NEAREST)
                .withWrapR(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE)
                .withWrapS(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE)
                .withWrapT(GLSamplerInfo.AddressMode.CLAMP_TO_EDGE);
        
        final GLTextureInfo tempInfo = new GLTextureInfo()
                .withExtent(info.supertileSize, info.supertileSize, 1)
                .withLayers(1)
                .withLevels(1)
                .withFormat(GLInternalFormat.RGBA16F)
                .withSamplerInfo(samplerInfo);
        
        try (GLTexture newTemp0 = tempInfo.create()) {            
            this.temp0.swap(newTemp0);
        }
        
        try (GLTexture newTemp1 = tempInfo.create()) {
            this.temp1.swap(newTemp1);
        }

        try (GLTexture newTiles = new GLTextureInfo()
                .withExtent(info.supertileSize, info.supertileSize, 1)
                .withLayers(info.maxVisibleSupertiles)
                .withFormat(GLInternalFormat.RGB10_A2)
                .withSamplerInfo(samplerInfo)
                .create()) {

            this.tiles.swap(newTiles);
        }

        this.projMatrix = MemoryUtil.memAllocFloat(16);

        try (GLVertexArray newModel = new GLVertexArrayInfo()
                .withAttributes(
                        new GLVertexArrayInfo.AttributeDescription()
                                .withLocation(0)
                                .withBinding(0)
                                .withFormat(GLVertexFormat.VEC4),
                        new GLVertexArrayInfo.AttributeDescription()
                                .withLocation(1)
                                .withBinding(1)
                                .withFormat(GLVertexFormat.VEC4))
                .withBindings(
                        new GLVertexArrayInfo.BindingDescription()
                                .withBinding(0)
                                .withBuffer(this.vSupertileRect)
                                .withStride(4 * Float.BYTES)
                                .withDivisor(1),
                        new GLVertexArrayInfo.BindingDescription()
                                .withBinding(1)
                                .withBuffer(this.vLayerSelect)
                                .withStride(4 * Float.BYTES)
                                .withDivisor(1))
                .create()) {

            this.model.swap(newModel);
        }

        this.filters = new ArrayList<>(info.filters);
        LOGGER.info("Tile Filters: {}", this.filters.size());
        
        if (!this.info.cache.isValid()) {            
            for (int sty = 0; sty < this.supertilesDown; sty++) {
                for (int stx = 0; stx < this.supertilesAcross; stx++) {
                    if (!this.isSupertileEmptyNoLock(stx, sty)) {
                        this.rebuildSupertile(stx, sty);
                    }
                }
            }

            this.info.cache.validate();
        }
    }   
    
    private void rebuildSupertile(int stx, int sty) {
        final int startTileX = stx * this.tilesPerSupertile;
        final int startTileY = sty * this.tilesPerSupertile;

        try (BufferedImage temp = new BufferedImage(info.supertileSize, info.supertileSize, PixelFormat.R8G8B8A8_UNORM)) {
            for (int y = 0; y < this.tilesPerSupertile; y++) {
                final int tileY = startTileY + y;

                for (int x = 0; x < this.tilesPerSupertile; x++) {
                    final int tileX = startTileX + x;

                    final Optional<Image> tileImage = this.info.tileMap.getTile(tileX, tileY);

                    if (tileImage.isPresent()) {
                        temp.setSubimage(x * info.tileSize, y * info.tileSize, info.tileSize, info.tileSize, tileImage.get());
                    }
                }
            }                
            
            final int x = stx * info.supertileSize;
            final int y = sty * info.supertileSize;            
            
            final List<TileFilter> filtersInRange = this.filters.stream()
                    .filter(filter -> filter.intersects(x, y, info.supertileSize, info.supertileSize))
                    .collect(Collectors.toList());
            
            // write directly to the cache if there are no filters.
            if (filtersInRange.isEmpty()) {
                this.info.cache.storeSupertile(stx, sty, temp.getData());
            } else {
                LOGGER.info("Applying Tile Filter(s)!");
                // write the image to temp0
                GLTexture src = temp0;
                GLTexture dst = temp1;
                
                src.subImage(0, 0, 0, 0, info.supertileSize, info.supertileSize, 1, GLPixelInfo.of(temp));
                
                final GLConstants.BoundColorTransformProgram program = GLConstants.getBoundColorTransformProgramInstance();                                
                
                program.program.use();
                
                GLUniform.uniform1(program.uSrc, 0);
                GLUniform.uniform1(program.uDst, 1);
                
                
                for (TileFilter filter : filtersInRange) {                                        
                    //TODO: these should use higher precision.
                    src.bindImage(0, 0, 0, GLImageAccess.READ_ONLY, GLInternalFormat.RGBA16F);
                    dst.bindImage(1, 0, 0, GLImageAccess.WRITE_ONLY, GLInternalFormat.RGBA16F);
                    
                    GLUniform.uniform4(program.uBounds, filter.x, filter.y, filter.w, filter.h);
                    GLUniform.uniform2(program.uPosition, stx * info.supertileSize, sty * info.supertileSize);
                    GLUniform.uniform4(program.uHsv, (float) Math.cos(filter.colorTransform.hsv.hue), (float) Math.sin(filter.colorTransform.hsv.hue), filter.colorTransform.hsv.saturation, filter.colorTransform.hsv.value);
                    GLUniform.uniform4(program.uOffset, filter.colorTransform.rgbaOffset.red, filter.colorTransform.rgbaOffset.green, filter.colorTransform.rgbaOffset.blue, filter.colorTransform.rgbaOffset.alpha);
                    GLUniform.uniform4(program.uScale, filter.colorTransform.rgbaScale.red, filter.colorTransform.rgbaScale.green, filter.colorTransform.rgbaScale.blue, filter.colorTransform.rgbaScale.alpha);
                    
                    GLCompute.dispatchCompute(info.supertileSize / 32, info.supertileSize / 32);
                    GLMemoryBarrier.insertBarrier(EnumSet.of(GLMemoryBarrier.SHADER_IMAGE_ACCESS));
                    
                    src.swap(dst);
                }
                
                src.swap(dst);
                     
                final ByteBuffer transferCopy = MemoryUtil.memCalloc(info.supertileSize * info.supertileSize * 4 * Short.BYTES);
                
                try {                                        
                    dst.getImage(0, new GLPixelInfo()
                            .withData(transferCopy)
                            .withFormat(GLFormat.RGBA)
                            .withType(GLPixelInfo.Type.HALF_FLOAT));
                    
                    this.info.cache.storeSupertile(stx, sty, transferCopy);
                } finally {
                    MemoryUtil.memFree(transferCopy);
                }
            }
        }
    }

    @Override
    public TileLayerInfo getInfo() {
        return this.info;
    }

    @Override
    public void fillTile(Image img, final int startX, final int startY, final int width, final int height) {
        try {
            this.writeLock.acquire();

            this.info.tileMap.fillTile(img, startX, startY, width, height);

            for (int y = 0; y < startY + height; y++) {
                for (int x = 0; x < startX + width; x++) {
                    final int supertileX = x * info.tileSize / info.supertileSize;
                    final int supertileY = y * info.tileSize / info.supertileSize;
                    final Update update = new Update(supertileX, supertileY);

                    this.updates.add(update);
                }
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting tile!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void setTile(Image img, int x, int y) {
        try {
            this.writeLock.acquire();
            this.info.tileMap.setTile(img, x, y);

            final int supertileX = x * info.tileSize / info.supertileSize;
            final int supertileY = y * info.tileSize / info.supertileSize;
            final Update update = new Update(supertileX, supertileY);

            this.updates.add(update);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting tile!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public Optional<Image> getTile(int x, int y) {
        try {
            this.writeLock.acquire();
            return this.info.tileMap.getTile(x, y);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while getting tile!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public boolean isSupertileResident(int stx, int sty) {
        try {
            this.writeLock.acquire();

            final int supertileId = sty * this.supertilesAcross + stx;

            return this.supertileMapping[supertileId] >= 0;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while checking Supertile residency!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    private void applyUpdates() {
        for (Update update : this.updates) {
            final int supertileId = update.supertileY * this.supertilesAcross + update.supertileX;

            this.rebuildSupertile(update.supertileX, update.supertileY);

            final int mappedId = this.supertileMapping[supertileId];

            // update the texture layer if the Supertile is resident.
            if (mappedId >= 0) {
                this.makeSupertileResident(update.supertileX, update.supertileY, mappedId);
            }
        }

        this.updates.forEach(update -> this.rebuildSupertile(update.supertileX, update.supertileY));
        this.updates.clear();
    }

    private void makeSupertileResident(int stx, int sty, int mappedId) {
        final ByteBuffer data = this.info.cache.fetchSupertile(stx, sty);

        // make the supertile resident
        this.tiles.subImage(0, 0, 0, mappedId, info.supertileSize, info.supertileSize, 1, new GLPixelInfo()
                .withData(data)
                .withFormat(GLFormat.RGBA)
                .withType(GLPixelInfo.Type.HALF_FLOAT));
    }

    private boolean isSupertileEmptyNoLock(int stx, int sty) {
        final int startTileX = stx * this.tilesPerSupertile;
        final int startTileY = sty * this.tilesPerSupertile;

        for (int y = 0; y < this.tilesPerSupertile; y++) {
            final int tileY = startTileY + y;

            for (int x = 0; x < this.tilesPerSupertile; x++) {
                final int tileX = startTileX + x;

                if (this.info.tileMap.getTile(tileX, tileY).isPresent()) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public boolean isSupertileEmpty(int stx, int sty) {
        try {
            this.writeLock.acquire();

            return this.isSupertileEmptyNoLock(stx, sty);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while checking if Supertile is empty!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void setSupertileMapping(int stx, int sty, int mappedId) {
        try {
            this.writeLock.acquire();

            if (!this.isWritable) {
                throw new IllegalStateException("Cannot set Supertile Mapping while TileLayer is not writable!");
            }

            final int supertileId = sty * this.supertilesAcross + stx;

            // early out if the supertile is already mapped to a tile layer.
            if (this.supertileMapping[supertileId] == mappedId) {
                return;
            }

            if (this.layerSelectMapping == null) {
                this.layerSelectMapping = this.vLayerSelect.map(EnumSet.of(GLBufferAccess.WRITE));
                this.ptrLayerSelect = this.layerSelectMapping.getPointer();
            }

            final int offset = supertileId * 4 * Float.BYTES;

            this.supertileMapping[supertileId] = mappedId;

            //
            if (mappedId < 0) {
                this.ptrLayerSelect.putFloat(offset, 0F);
                this.ptrLayerSelect.putFloat(offset + 4, Float.NaN);
                this.ptrLayerSelect.putFloat(offset + 8, Float.NaN);
                this.ptrLayerSelect.putFloat(offset + 12, Float.NaN);
            } else {
                this.ptrLayerSelect.putFloat(offset, mappedId);
                this.ptrLayerSelect.putFloat(offset + 4, 0F);
                this.ptrLayerSelect.putFloat(offset + 8, 0F);
                this.ptrLayerSelect.putFloat(offset + 12, 1F);

                this.makeSupertileResident(stx, sty, mappedId);
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting Supertile Mapping!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void onFrameStart() {
        try {
            this.writeLock.acquire();
            this.isWritable = true;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while marking TileLayer as writable!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void onFrameEnd() {
        try {
            this.writeLock.acquire();
            this.isWritable = false;

            if (this.layerSelectMapping != null) {
                this.layerSelectMapping.close();
                this.layerSelectMapping = null;
                this.ptrLayerSelect = null;
            }

            this.applyUpdates();

            final GLConstants.TileProgram program = GLConstants.getTileProgramInstance();

            GLUniform.uniformMatrix4(program.uProjection, false, this.projMatrix);
            GLUniform.uniform1(program.uImage, 0);

            this.model.bind();
            this.tiles.bind(0);

            GLDraw.drawArraysInstanced(GLDraw.Mode.TRIANGLE_STRIP, 0, 4, this.totalSupertiles);

        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while drawing!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void setProjection(float[] projection) {
        try {
            this.writeLock.acquire();
            this.projMatrix.put(projection).flip();
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting projection matrix!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public boolean isWritable() {
        try {
            this.writeLock.acquire();
            return this.isWritable;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while checking if TileLayer is writable!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void close() {
        if (this.info != null) {
            this.model.close();
            this.tiles.close();
            this.vSupertileRect.close();
            MemoryUtil.memFree(this.projMatrix);
            this.info = null;
        }
    }

    @Override
    public GLProgram getProgram() {
        return GLConstants.getTileProgramInstance().program;
    }

    @Override
    public GLBlendStateInfo getBlendState() {
        return GLConstants.PREMULTIPLY_BLEND_STATE;
    }

}
