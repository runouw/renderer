/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.Tools;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public class SpriteLayerInfo implements LayerInfo {
    public final Set<SpriteResourceInfo> validSprites;
    public final Set<MaskResourceInfo> validMasks;   
    public final int maxSpriteSlots;
    public final int maxColorTransforms;
    public final boolean streaming;
    
    public SpriteLayerInfo(
            final Collection<SpriteResourceInfo> validSprites, 
            final Collection<MaskResourceInfo> validMasks,
            final int maxSpriteSlots, 
            final int maxColorTransforms,
            final boolean streaming) {

        this.validSprites = Tools.copySet(validSprites);
        this.validMasks = Tools.copySet(validMasks);
        this.maxSpriteSlots = maxSpriteSlots;
        this.maxColorTransforms = maxColorTransforms;
        this.streaming = streaming;
    }
    
    public SpriteLayerInfo() {
        this(null, null, 0, 0, true);
    }
    
    public SpriteLayerInfo withValidSprites(final Collection<SpriteResourceInfo> validSprites) {
        return new SpriteLayerInfo(validSprites, validMasks, maxSpriteSlots, maxColorTransforms, streaming);
    }
    
    public SpriteLayerInfo withValidSprites(final SpriteResourceInfo... validSprites) {
        return withValidSprites(Arrays.asList(validSprites));
    }
    
    public SpriteLayerInfo withStreaming(final boolean streaming) {
        return new SpriteLayerInfo(validSprites, validMasks, maxSpriteSlots, maxColorTransforms, streaming);
    }
    
    public SpriteLayerInfo withValidMasks(final Collection<MaskResourceInfo> validMasks) {
        return new SpriteLayerInfo(validSprites, validMasks, maxSpriteSlots, maxColorTransforms, streaming);
    }
    
    public SpriteLayerInfo withValidMasks(final MaskResourceInfo... validMasks) {
        return withValidMasks(Arrays.asList(validMasks));
    }
    
    public SpriteLayerInfo withMaxSpriteSlots(final int maxSpriteSlots) {
        return new SpriteLayerInfo(validSprites, validMasks, maxSpriteSlots, maxColorTransforms, streaming);
    }
    
    public SpriteLayerInfo withMaxColorTransforms(final int maxColorTransforms) {
        return new SpriteLayerInfo(validSprites, validMasks, maxSpriteSlots, maxColorTransforms, streaming);
    }
}
