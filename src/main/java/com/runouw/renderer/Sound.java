/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.al.ALBuffer;
import com.longlinkislong.gloop2.al.ALFormat;
import com.longlinkislong.gloop2.al.ALSource;
import com.longlinkislong.gloop2.al.ALSourceState;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.List;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public class Sound implements AutoCloseable {

    private static int alignUp(final int a, final int b) {
        return (a + b + 1) / b * b;
    }

    public static enum State {
        PLAYING,
        LOOPING,
        STOPPED
    }

    private final SoundInfo info;
    private final WeakReference<SoundLayer> parent;

    boolean gainChanged;
    boolean pitchChanged;
    boolean positionChanged;
    private float gain, pitch;
    private float x, y, z;

    float time;
    boolean active;

    private boolean eof;

    private ALSource source;
    private final ALBuffer[] buffers = new ALBuffer[Constants.SOUND_BUFFER_COUNT];
    private ByteBuffer transfer;
    private final ALFormat format;

    State state = State.STOPPED;

    Sound(final SoundLayer parent, final SoundInfo info) {
        this.parent = new WeakReference<>(parent);
        this.format = info.channel.getALFormat().orElseThrow(() -> new UnsupportedOperationException("Unsupported OpenAL Format!"));
        this.info = info;
        this.gain = info.gain;
        this.pitch = info.pitch;
        this.x = info.x;
        this.y = info.y;
        this.z = info.z;
    }

    @Override
    public void close() {
        if (this.source != null) {
            this.source.close();
            this.source = null;

            for (int i = 0; i < this.buffers.length; i++) {
                if (this.buffers[i] != null) {
                    this.buffers[i].close();
                    this.buffers[i] = null;
                }
            }

            if (this.transfer != null) {
                MemoryUtil.memFree(this.transfer);
                this.transfer = null;
            }

            this.parent.clear();
        }
    }

    void deactivate() {
        this.active = false;
        
        // muting the sound is more immediate than stopping it.        
        this.source.setGain(0F);
        
        // free resources once possible.
        if (this.source.getState() == ALSourceState.STOPPED) {
            this.source.close();
            this.source = null;
            
            for (int i = 0; i < this.buffers.length; i++) {
                if (this.buffers[i] != null) {
                    this.buffers[i].close();
                    this.buffers[i] = null;
                }
            }
        }
    }

    void activate() {
        assert !this.active : "Sound is already active!";

        // check if the sound has completed playing before being active
        if (this.time > this.info.channel.getLength()) {
            if (this.state == State.PLAYING) {
                this.state = State.STOPPED;
                this.active = false;
                return;
            } else {
                final float adjustedLength = this.info.channel.getLength() - this.info.loopPoint;

                this.time = this.info.loopPoint + (this.time % adjustedLength);
            }
        }

        this.source = new ALSource();

        final int bufferSize = this.info.channel.getByteRate() / Constants.SOUND_REFRESH_RATE;

        if (transfer == null) {
            this.transfer = MemoryUtil.memAlloc(alignUp(bufferSize, Constants.SOUND_BUFFER_ALIGNMENT));            
        }

        this.source.setPitch(this.pitch);
        this.source.setGain(this.gain);
        this.source.setPosition(x, y, z);
        this.pitchChanged = false;
        this.gainChanged = false;
        this.positionChanged = false;
        this.eof = false;                

        try {
            final int startSample = (int) (this.time * info.channel.getSampleRate());

            this.info.channel.seek(startSample);

            for (int i = 0; i < this.buffers.length; i++) {
                this.buffers[i] = new ALBuffer();

                transfer.clear();

                if (this.state == State.LOOPING) {
                    final int loopSample = (int) (info.loopPoint * info.channel.getSampleRate());

                    while (transfer.hasRemaining()) {
                        if (info.channel.read(transfer) == -1) {
                            info.channel.seek(loopSample);
                        }
                    }

                    transfer.flip();
                    this.buffers[i].setData(format, transfer, info.channel.getSampleRate());
                    this.source.queueBuffers(this.buffers[i]);
                } else {
                    final int read = info.channel.read(transfer);

                    transfer.flip();
                    this.buffers[i].setData(format, transfer, info.channel.getSampleRate());
                    this.source.queueBuffers(this.buffers[i]);

                    // I guess we have more buffers than sound. just end it.
                    if (read == -1) {
                        eof = true;
                        break;
                    }
                }
            }

            this.source.play();
            this.active = true;
        } catch (IOException ex) {
            throw new RuntimeException("Error activating sound!", ex);
        }
    }

    void onFrameEnd() {       
        assert this.active : "Sound is not active!";
        
        // mark the sound as stopped if EOF is reached and the source finishes playing.
        if (this.eof && this.source.getState() == ALSourceState.STOPPED) {
            this.state = State.STOPPED;            
            return;
        }        

        if (this.gainChanged) {
            this.source.setGain(this.gain * this.parent.get().gain);
            this.gainChanged = false;
        }

        if (this.pitchChanged) {
            this.source.setPitch(this.pitch * this.parent.get().pitch);
            this.pitchChanged = false;
        }

        if (this.positionChanged) {
            this.source.setPosition(x, y, z);
            this.positionChanged = false;
        }
        
        final List<ALBuffer> ready = this.source.unqueueBuffers();

        if (ready.isEmpty()) {
            return;
        }

        try {
            for (ALBuffer buffer : ready) {
                transfer.clear();

                if (this.state == State.PLAYING) {
                    final int read = info.channel.read(transfer);

                    transfer.flip();
                    buffer.setData(format, transfer, info.channel.getSampleRate());
                    source.queueBuffers(buffer);

                    // mark that the stream has reached end of file
                    if (read == -1) {
                        this.eof = true;
                        break;
                    }
                } else {
                    final int loopSample = (int) (info.loopPoint * info.channel.getSampleRate());

                    while (transfer.hasRemaining()) {
                        if (info.channel.read(transfer) == -1) {
                            info.channel.seek(loopSample);
                        }
                    }

                    transfer.flip();
                    buffer.setData(format, transfer, info.channel.getSampleRate());
                    this.source.queueBuffers(buffer);
                }
            }

            // restart if underfed
            if (this.source.getState() == ALSourceState.STOPPED) {
                this.source.play();
            }
        } catch (IOException ex) {
            throw new RuntimeException("Error streaming sound!", ex);
        }
    }

    /**
     * Sets the Sound's position. These values should be in world space.
     *
     * @param x the Sound's x-position
     * @param y the Sound's y-position
     * @param z the Sound's z-position
     */
    public void setPosition(final float x, final float y, final float z) {
        try {
            this.parent.get().lock.acquire();
            this.x = x;
            this.y = y;
            this.z = z;
            this.positionChanged = true;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while settings position!", ex);
        } finally {
            this.parent.get().lock.release();
        }
    }

    /**
     * Retrieves the Sound's position in world space.
     *
     * @return the Sound's position.
     */
    public float[] getPosition() {
        try {
            this.parent.get().lock.acquire();
            return new float[]{x, y, z};
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while retrieving position!", ex);
        } finally {
            this.parent.get().lock.release();
        }
    }

    /**
     * Retrieve's the Sound's state. This value is only set automatically if a
     * PLAYING sound reaches EOF.
     *
     * @return the current state.
     */
    public State getState() {
        try {
            this.parent.get().lock.acquire();
            return this.state;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while retrieving state!");
        } finally {
            this.parent.get().lock.release();
        }
    }

    /**
     * Checks if the Sound is active. The parent SoundLayer determines which
     * Sound objects are active depending on the Sound's state and its relative
     * index. SoundLayer only allows a fixed number of active sounds at a time.
     * Inactive sounds will still have their internal timers incremented each
     * frame and will begin playing from that offset once activated.
     *
     * @return true if the Sound is active.
     */
    public boolean isActive() {
        try {
            this.parent.get().lock.acquire();
            return this.active;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while checking if sound is active!", ex);
        } finally {
            this.parent.get().lock.release();
        }
    }

    /**
     * Plays the Sound. This sets the Sound's state to PLAYING. Only active
     * Sounds will be audible.
     */
    public void play() {
        try {
            this.parent.get().lock.acquire();
            this.state = State.PLAYING;
            this.time = 0F;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while playing Sound!", ex);
        } finally {
            this.parent.get().lock.release();
        }
    }

    public float getTime() {
        try {
            this.parent.get().lock.acquire();
            return this.time;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while retrieving time!", ex);
        } finally {
            this.parent.get().lock.release();
        }
    }

    public void stop() {
        try {
            this.parent.get().lock.acquire();
            this.state = State.STOPPED;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while stopping Sound!", ex);
        } finally {
            this.parent.get().lock.release();
        }
    }

    public void loop() {
        try {
            this.parent.get().lock.acquire();
            this.state = State.LOOPING;
            this.time = 0F;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while looping Sound!", ex);
        } finally {
            this.parent.get().lock.release();
        }
    }

    public void setGain(final float gain) {
        this.gain = gain;
    }

    public float getGain() {
        return this.gain;
    }

    public void setPitch(final float pitch) {
        this.pitch = pitch;
    }

    public float getPitch() {
        return this.pitch;
    }

    public SoundInfo getInfo() {
        return this.info;
    }
}
