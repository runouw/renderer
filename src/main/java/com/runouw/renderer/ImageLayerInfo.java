/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.image.Image;

/**
 *
 * @author zmichaels
 */
public final class ImageLayerInfo implements LayerInfo {
    public static final class ImageSublayerInfo {
        public final Image image;
        public final ImageScrollType hScroll;
        public final ImageScrollType vScroll;
        public final ColorTransform colorTransform;
        public final ImageFilter magFilter;
        public final ImageFilter minFilter;
        
        public ImageSublayerInfo(
                final Image image, 
                final ImageScrollType hScroll, final ImageScrollType vScroll, 
                final ColorTransform colorTransform,
                final ImageFilter minFilter, final ImageFilter magFilter) {
            
            this.image = image;
            this.hScroll = hScroll;
            this.vScroll = vScroll;
            this.colorTransform = colorTransform;
            this.minFilter = minFilter;
            this.magFilter = magFilter;
        }
        
        public ImageSublayerInfo() {
            this(null, ImageScrollType.STATIC, ImageScrollType.STATIC, ColorTransform.IDENTITY, ImageFilter.NEAREST, ImageFilter.NEAREST);
        }
        
        public ImageSublayerInfo withImage(final Image image) {
            return new ImageSublayerInfo(image, hScroll, vScroll, colorTransform, minFilter, magFilter);
        }
        
        public ImageSublayerInfo withHScroll(final ImageScrollType hScroll) {
            return new ImageSublayerInfo(image, hScroll, vScroll, colorTransform, minFilter, magFilter);
        }
        
        public ImageSublayerInfo withVScroll(final ImageScrollType vScroll) {
            return new ImageSublayerInfo(image, hScroll, vScroll, colorTransform, minFilter, magFilter);
        }
        
        public ImageSublayerInfo withColorTransform(final ColorTransform colorTransform) {
            return new ImageSublayerInfo(image, hScroll, vScroll, colorTransform, minFilter, magFilter);
        }
        
        public ImageSublayerInfo withMinFilter(final ImageFilter minFilter) {
            return new ImageSublayerInfo(image, hScroll, vScroll, colorTransform, minFilter, magFilter);
        }
        
        public ImageSublayerInfo withMagFilter(final ImageFilter magFilter) {
            return new ImageSublayerInfo(image, hScroll, vScroll, colorTransform, minFilter, magFilter);
        }
    }
    
    public final ImageSublayerInfo image;
    public final ImageSublayerInfo mask;
    public final boolean supportsColorTransform;
    
    public ImageLayerInfo(final ImageSublayerInfo image, final ImageSublayerInfo mask, final boolean supportsColorTransform) {
        this.image = image;
        this.mask = mask;
        this.supportsColorTransform = supportsColorTransform;
    }
    
    public ImageLayerInfo() {
        this(null, null, false);
    }
    
    public ImageLayerInfo withImageInfo(final ImageSublayerInfo image) {
        return new ImageLayerInfo(image, mask, supportsColorTransform);
    }
    
    public ImageLayerInfo withMaskInfo(final ImageSublayerInfo mask) {
        return new ImageLayerInfo(image, mask, supportsColorTransform);
    }    
    
    public ImageLayerInfo withColorTransformSupport(final boolean supportsColorTransform) {
        return new ImageLayerInfo(image, mask, supportsColorTransform);
    }
}
