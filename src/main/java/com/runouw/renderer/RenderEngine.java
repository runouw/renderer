/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.glfw.GLFWHints;
import com.longlinkislong.gloop2.glfw.GLFWWindow;
import com.longlinkislong.gloop2.glfw.GLFWWindowCreateInfo;
import com.longlinkislong.gloop2.vk.VKDeviceCreateInfo;
import com.longlinkislong.gloop2.vk.VKInstance;
import com.longlinkislong.gloop2.vk.VKInstanceCreateInfo;
import com.longlinkislong.gloop2.vk.VKPhysicalDevice;
import com.runouw.renderer.gl.GLScene;
import com.runouw.renderer.vk.VKConstants;
import com.runouw.renderer.vk.VKScene;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class RenderEngine implements AutoCloseable {

    private static final boolean ASYNC_TRANSFER = Boolean.getBoolean("com.runouw.renderer.RenderEngine.async_transfer");
    private static final boolean ASYNC_PRESENT = Boolean.getBoolean("com.runouw.renderer.RenderEngine.async_present");
    private static final String VK_LAYERS = System.getProperty("com.runouw.renderer.RenderEngine.vk_layers", "");
    private static final String VK_EXTENSIONS = System.getProperty("com.runouw.renderer.RenderEngine.vk_extensions", "");
    private static final Logger LOGGER = LoggerFactory.getLogger(RenderEngine.class);

    static {
        if (ASYNC_PRESENT) {
            System.setProperty("com.longlinkislong.gloop2.vk.VKContext.async_present", "true");
        }
    }

    private final RenderEngineInfo info;

    final Semaphore lock = new Semaphore(1);
    public final GLFWWindow window;
    Scene scene;
    RenderState renderState = RenderState.IDLE;
    private VKInstance vkInstance = null;

    private GLFWHints initVulkan(final RenderEngineInfo info) {
        final List<String> enabledLayers = VK_LAYERS.isEmpty()
                ? Collections.emptyList()
                : Arrays.stream(VK_LAYERS.split(","))
                        .map(String::trim)
                        .collect(Collectors.toList());

        final List<String> enabledExtensions = VK_EXTENSIONS.isEmpty()
                ? Collections.emptyList()
                : Arrays.stream(VK_EXTENSIONS.split(","))
                        .map(String::trim)
                        .collect(Collectors.toList());

        this.vkInstance = new VKInstanceCreateInfo()
                .withApplicationName(info.title)
                .withVersion(info.version.major, info.version.minor, info.version.patch)
                .withLayers(enabledLayers)
                .withExtensions(enabledExtensions)
                .create();

        final VKPhysicalDevice selectedPhysicalDevice = vkInstance.physicalDevices()
                .filter(VKPhysicalDevice::isGPU)
                .findFirst()
                .get();

        final List<VKDeviceCreateInfo.DeviceQueueCreateInfo> queueCreateInfos = new ArrayList<>();

        queueCreateInfos.add(selectedPhysicalDevice.queueFamilies()
                .filter(qf -> qf.canDraw())
                .filter(qf -> ASYNC_PRESENT || qf.canPresent())
                .findFirst()
                .map(qf -> new VKDeviceCreateInfo.DeviceQueueCreateInfo().withQueueFamily(qf))
                .orElseThrow(() -> new UnsupportedOperationException("Could not find a draw queue!")));

        selectedPhysicalDevice.queueFamilies()
                .filter(qf -> ASYNC_TRANSFER)
                .filter(qf -> qf.canTransfer())
                .filter(qf -> !qf.canCompute())
                .filter(qf -> !qf.canDraw())
                .findFirst()
                .map(qf -> new VKDeviceCreateInfo.DeviceQueueCreateInfo().withQueueFamily(qf))
                .ifPresent(queueCreateInfos::add);

        selectedPhysicalDevice.queueFamilies()
                .filter(qf -> ASYNC_PRESENT)
                .filter(qf -> !qf.canDraw())
                .filter(qf -> qf.canPresent() && qf.canCompute())
                .findFirst()
                .map(qf -> new VKDeviceCreateInfo.DeviceQueueCreateInfo().withQueueFamily(qf))
                .ifPresent(queueCreateInfos::add);

        return new GLFWHints()
                .withContextHints(new GLFWHints.VulkanHints(new VKDeviceCreateInfo()
                        .withLayers(enabledLayers)
                        .withQueueCreateInfos(queueCreateInfos)));
    }

    public RenderEngine(final RenderEngineInfo info) {
        this.info = info;

        final GLFWHints hints;

        switch (info.api) {
            case OPENGL:
                hints = new GLFWHints()
                        .withContextHints(new GLFWHints.OpenGLHints()
                                .withVersion(info.version.major, info.version.minor)
                                .withProfile(GLFWHints.OpenGLProfile.CORE));
                break;
            case OPENGLES:
                hints = new GLFWHints()
                        .withContextHints(new GLFWHints.OpenGLESHints()
                                .withVersion(info.version.major, info.version.minor));
                break;
            case VULKAN:
                hints = initVulkan(info);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported rendering API: " + info.api);
        }

        this.window = new GLFWWindowCreateInfo()
                .withTitle(info.title)
                .withHints(hints)
                .withWidth(info.width)
                .withHeight(info.height)
                .create();

        if (this.info.api != RenderEngineInfo.API.VULKAN) {
            if (info.vsync) {
                this.window.setSwapInterval(1);
            } else {
                this.window.setSwapInterval(0);
            }
        }
    }

    public RenderEngineInfo getInfo() {
        return this.info;
    }

    public RenderState getRenderState() {
        try {
            this.lock.acquire();

            return this.renderState;
        } catch (InterruptedException ex) {
            throw new RuntimeException("RenderEngine interrupted while acquiring RenderState!", ex);
        } finally {
            this.lock.release();
        }
    }

    public boolean isValid() {
        return !this.window.shouldClose();
    }

    public Queue<RenderStep> getRenderSteps() {
        if (this.window.shouldClose()) {
            return new ArrayDeque<>();
        }

        try {
            this.lock.acquire();

            if (this.scene != null) {
                final Queue<RenderStep> out = new ArrayDeque<>();

                out.add(new RenderAcquireStep(this));
                out.add(new RenderFrameStartStep(this));
                out.add(new RenderFrameEndStep(this));
                out.add(new RenderPresentStep(this));
                out.add(new RenderIdleStep(this));

                return out;
            } else {
                final Queue<RenderStep> out = new ArrayDeque<>();

                out.add(new RenderAcquireStep(this));
                out.add(new RenderPresentStep(this));
                out.add(new RenderIdleStep(this));

                return out;
            }
        } catch (InterruptedException ex) {
            LOGGER.warn("RenderEngine step building was interrupted!");
            LOGGER.debug(ex.getMessage(), ex);

            Thread.currentThread().interrupt();
            return new ArrayDeque<>();
        } finally {
            this.lock.release();
        }
    }

    private JFXLayer.InputMultiplexer inputHandler = null;

    public void setScene(final Scene scene) {
        try {
            this.lock.acquire();
            this.scene = scene;

            if (scene != null) {
                this.inputHandler = new JFXLayer.InputMultiplexer(scene.getLayersOrdered()
                        .stream()
                        .filter(layer -> layer instanceof JFXLayer)
                        .map(layer -> (JFXLayer) layer));

                this.window.setCursorPositionCallback(inputHandler);
                this.window.setMouseButtonCallback(inputHandler);
                this.window.setKeyCallback(inputHandler);
                this.window.setScrollCallback(inputHandler);
                this.window.setCharCallback(inputHandler);
            } else {
                this.inputHandler = null;
                this.window.setCursorPositionCallback(null);
                this.window.setMouseButtonCallback(null);
                this.window.setKeyCallback(null);
                this.window.setScrollCallback(null);
                this.window.setCharCallback(null);
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("RenderEngine interrupted while acquiring scene lock!", ex);
        } finally {
            this.lock.release();
        }
    }

    public Optional<Scene> getScene() {
        try {
            this.lock.acquire();

            return Optional.ofNullable(this.scene);
        } catch (InterruptedException ex) {
            throw new RuntimeException("RenderEngine interrupted while acquiring scene lock!", ex);
        } finally {
            this.lock.release();
        }
    }

    @Override
    public void close() {
        try {
            this.lock.acquire();

            if (this.scene != null) {
                try {
                    this.scene.close();
                } catch (Exception ex) {
                    LOGGER.warn("Error while closing scene!");
                    LOGGER.debug(ex.getMessage(), ex);
                }
            }

            this.window.getVulkanContext()
                    .map(VKConstants::getConstants)
                    .ifPresent(constants -> {
                        try {
                            constants.dumpPipelineCache();
                        } catch (IOException ex) {
                            LOGGER.error("Error dumping pipeline cache!", ex);
                            LOGGER.debug(ex.getMessage(), ex);
                        }
                    });

            this.window.close();

            if (this.vkInstance != null) {
                this.vkInstance.close();
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("RenderEngine close was interrupted!", ex);
        } finally {
            this.lock.release();
        }
    }

    public Scene createScene(final SceneInfo info) throws IOException {
        switch (this.info.api) {
            case OPENGL:
                return new GLScene(info);
            case VULKAN:
                return new VKScene(this.window.getVulkanContext().get(), info);
            default:
                throw new UnsupportedOperationException("API: " + this.info.api + " does not support scenes!");
        }
    }
}
