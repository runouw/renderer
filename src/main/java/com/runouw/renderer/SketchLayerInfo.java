/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public class SketchLayerInfo implements LayerInfo {
    public final int maxLines;
    
    public SketchLayerInfo(final int maxLines) {
        this.maxLines = maxLines;
    }
    
    public SketchLayerInfo() {
        this(0);
    }
}
