/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public interface SpriteSlot {
    SpriteSlot setSprite(SpriteInfo info);        
    
    SpriteSlot setPosition(float x, float y);
    
    SpriteSlot setOffset(float x, float y);
    
    SpriteSlot setScale(float x, float y);
    
    SpriteSlot setSkew(float x, float y);
    
    SpriteSlot setRotation(float a);
    
    SpriteSlot setSpriteView(ImageView view);
    
    SpriteSlot setMaskView(ImageView view);
    
    SpriteSlot setColorTransformSlot(int ctoff);
    
    SpriteSlot clear();
}
