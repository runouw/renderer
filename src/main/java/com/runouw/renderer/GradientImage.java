/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.image.Image;
import com.longlinkislong.gloop2.image.PixelFormat;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public class GradientImage implements AutoCloseable, Image {
    private static final int WIDTH = Integer.getInteger("com.runouw.renderer.GradientImage.width", 1);
    public static final class Stop {
        public final double offset;
        public final RGBA color;
        
        public Stop(final double offset, final RGBA color) {
            this.offset = offset;
            this.color = color;
        }
        
        public Stop() {
            this(0.0, RGBA.TRANSPARENT_BLACK);
        }
        
        public Stop withOffset(final double offset) {
            return new Stop(offset, color);
        }
        
        public Stop withColor(final RGBA color) {
            return new Stop(offset, color);
        }
    }
    
    private ByteBuffer data;
    private final int steps;
    
    private static double getStopAt(final List<Stop> stops, final int idx) {
        if (idx >= stops.size()) {
            return Double.POSITIVE_INFINITY;
        } else {
            return stops.get(idx).offset;
        }
    }
    
    private static Stop getStop(final List<Stop> stops, final int idx) {
        if (idx >= 0 && idx < stops.size()) {
            return stops.get(idx);
        } else {
            return null;
        }
    }
    
    private static int clamp(final int val) {
        return Math.max(Math.min(val, 255), 0);
    }
    
    private static int lerp(final double at, final Stop left, final Stop right) {
        final double distance = right.offset - left.offset;
        final double dToLeft = at - left.offset;
        final double t = 1.0 - (dToLeft / distance);
        
        final double r0 = left.color.red * t;
        final double g0 = left.color.green * t;
        final double b0 = left.color.blue * t;
        final double a0 = left.color.alpha * t;
        
        final double r1 = right.color.red * (1.0 - t);
        final double g1 = right.color.green * (1.0 - t);
        final double b1 = right.color.blue * (1.0 - t);
        final double a1 = right.color.alpha * (1.0 - t);
        
        final int r = clamp((int) ((r0 + r1) * 255F));
        final int g = clamp((int) ((g0 + g1) * 255F));
        final int b = clamp((int) ((b0 + b1) * 255F));
        final int a = clamp((int) ((a0 + a1) * 255F));
        
        return (a & 0xFF) << 24
                | (b & 0xFF) << 16
                | (g & 0xFF) << 8
                | (r & 0xFF);
    }
    
    public GradientImage(final int steps, final Stop... stops) {
        this(steps, Arrays.asList(stops));
    }
    
    public GradientImage(final int steps, final List<Stop> stops) {
        this.data = MemoryUtil.memAlloc(steps * WIDTH * 4);
        this.steps = steps;
        
        int idx = 0;
        for (int x = 0; x < steps; x++) {
            final double pos = ((double) x) / ((double) steps);
            
            while (pos > getStopAt(stops, idx)) {
                idx++;
            }
            
            final Stop leftStop = getStop(stops, idx - 1);
            final Stop rightStop = getStop(stops, idx);            
            
            if (leftStop == null) {
                if (rightStop == null) {
                    for (int y = 0; y < WIDTH; y++) {
                        this.data.putInt(0);
                    }
                } else {
                    for (int y = 0; y < WIDTH; y++) {
                        this.data.putInt(rightStop.color.toR8G8B8Unorm());
                    }
                }
            } else {
                if (rightStop == null) {
                    for (int y = 0; y < WIDTH; y++) {
                        this.data.putInt(leftStop.color.toR8G8B8Unorm());
                    }
                } else {
                    for (int y = 0; y < WIDTH; y++) {
                        final int interp = lerp(pos, leftStop, rightStop);                    
                        this.data.putInt(interp);
                    }
                }
            }            
        }
        
        this.data.flip();
    }
    
    @Override
    public void close() {
        if (this.data != null) {
            MemoryUtil.memFree(this.data);
            this.data = null;
        }
    }

    @Override
    public int getWidth() {
        return 1; //maybe 2?
    }

    @Override
    public int getHeight() {
        return this.steps;
    }

    @Override
    public ByteBuffer getData() {
        return this.data.asReadOnlyBuffer();
    }

    @Override
    public PixelFormat getFormat() {
        return PixelFormat.R8G8B8A8_UNORM;
    }
    
}
