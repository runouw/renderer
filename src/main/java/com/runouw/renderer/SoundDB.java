/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.sound.SeekableSoundChannel;
import com.longlinkislong.gloop2.sound.VorbisMemoryChannel;
import com.longlinkislong.gloop2.sound.WaveMemoryChannel;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.util.lmdb.LMDB;
import org.lwjgl.util.lmdb.MDBVal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class SoundDB implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SoundDB.class);
    private static final String DB_DATA = "DATA";
    private static final String DB_META = "META";

    private final SoundDBInfo info;
    private final Map<String, MetaSound> metaSounds = new HashMap<>();
    private final Map<MetaSound, ByteBuffer> loadedSounds = new HashMap<>();
    private long env;

    private static enum SoundType {
        WAVE(0x52494646),
        VORBIS(0x4F676753);
        
        private final int value;
        
        private SoundType(final int value) {
            this.value = value;
        }
        
        public static SoundType valueOf(final int value) {
            switch (value) {
                case 0x4F676753:
                    return SoundType.VORBIS;
                case 0x52494646:
                    return SoundType.WAVE;
                default:
                    return null;
            }
        }
    }

    private static final class MetaSound {

        private final String name;
        private final SoundType type;

        private MetaSound(final String name, final SoundType type) {
            this.name = name;
            this.type = type;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj == null) {
                return false;
            } else if (obj == this) {
                return true;
            } else if (obj instanceof MetaSound) {
                final MetaSound other = (MetaSound) obj;

                return other.type == this.type
                        && Objects.equals(other.name, this.name);
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 67 * hash + Objects.hashCode(this.name);
            hash = 67 * hash + Objects.hashCode(this.type);
            return hash;
        }
    }

    public SoundDB(final SoundDBInfo info) throws IOException {
        this.info = info;

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pEnv = mem.callocPointer(1);
            int rc;

            if ((rc = LMDB.mdb_env_create(pEnv)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            env = pEnv.get();

            final long dbSize = (info.size + MemoryUtil.PAGE_SIZE - 1) / MemoryUtil.PAGE_SIZE * MemoryUtil.PAGE_SIZE;

            LOGGER.trace("Using DB size: {}", dbSize);

            if ((rc = LMDB.mdb_env_set_mapsize(env, dbSize)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            if ((rc = LMDB.mdb_env_set_maxdbs(env, 2)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            Files.createDirectories(info.cacheName);

            if ((rc = LMDB.mdb_env_open(env, info.cacheName.toAbsolutePath().toString(), 0, 0644)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            this.identifySounds();
        }
    }

    public boolean hasSounds(final Collection<String> sounds) {
        return this.metaSounds.keySet().containsAll(sounds);
    }

    public boolean hasSounds(final String... sounds) {
        return this.hasSounds(Arrays.asList(sounds));
    }

    public Set<String> getResourceNames() {
        return Collections.unmodifiableSet(new HashSet<>(this.metaSounds.keySet()));
    }

    public Stream<String> resourceNames() {
        return this.metaSounds.keySet().stream();
    }

    public SoundDBInfo getInfo() {
        return this.info;
    }

    public void flush() throws IOException {
        this.metaSounds.clear();
        this.loadedSounds.clear();
        this.identifySounds();
    }

    public void clear() throws IOException {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pTxn = mem.callocPointer(1);
            final long txn;
            int rc;

            if ((rc = LMDB.mdb_txn_begin(this.env, 0, 0, pTxn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            txn = pTxn.get();

            final IntBuffer pMetaDbi = mem.callocInt(1);
            final int metaDbi;

            if ((rc = LMDB.mdb_dbi_open(txn, (ByteBuffer) null, LMDB.MDB_CREATE | LMDB.MDB_REVERSEKEY, pMetaDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            metaDbi = pMetaDbi.get();

            final IntBuffer pDataDbi = mem.callocInt(1);
            final int dataDbi;

            if ((rc = LMDB.mdb_dbi_open(txn, DB_DATA, LMDB.MDB_CREATE | LMDB.MDB_REVERSEKEY, pDataDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            dataDbi = pDataDbi.get();

            if ((rc = LMDB.mdb_drop(txn, metaDbi, false)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            if ((rc = LMDB.mdb_drop(txn, dataDbi, false)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            if ((rc = LMDB.mdb_txn_commit(txn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }
        }

        this.metaSounds.clear();
        this.loadedSounds.clear();
    }

    public void sync() {
        LMDB.mdb_env_sync(this.env, true);
    }

    private void identifySounds() throws IOException {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pTxn = mem.callocPointer(1);
            final long txn;
            int rc;

            if ((rc = LMDB.mdb_txn_begin(this.env, 0, 0, pTxn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            txn = pTxn.get();

            final IntBuffer pDbi = mem.callocInt(1);
            final int dbi;

            if ((rc = LMDB.mdb_dbi_open(txn, DB_META, LMDB.MDB_CREATE | LMDB.MDB_REVERSEKEY, pDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            dbi = pDbi.get();

            final PointerBuffer pCurs = mem.callocPointer(1);
            final long curs;

            if ((rc = LMDB.mdb_cursor_open(txn, dbi, pCurs)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            curs = pCurs.get();

            final MDBVal key = MDBVal.callocStack(mem);
            final MDBVal value = MDBVal.callocStack(mem);

            while (LMDB.mdb_cursor_get(curs, key, value, LMDB.MDB_NEXT) == LMDB.MDB_SUCCESS) {
                final String name = MemoryUtil.memUTF8(key.mv_data(), (int) key.mv_size());
                final ByteBuffer data = value.mv_data();
                final SoundType type;

                switch (data.getInt()) {
                    case 0x52494646:
                        type = SoundType.WAVE;
                        break;
                    case 0x4F676753:
                        type = SoundType.VORBIS;
                        break;
                    default:
                        throw new UnsupportedOperationException("Unsupported sound type!");

                }

                this.metaSounds.put(name, new MetaSound(name, type));
            }

            LMDB.mdb_cursor_close(curs);
            LMDB.mdb_txn_abort(txn);
        }
    }

    private ByteBuffer loadSound(final MetaSound meta) throws IOException {
        ByteBuffer out = this.loadedSounds.get(meta);
        
        if (out != null) {
            return out.asReadOnlyBuffer().order(ByteOrder.nativeOrder());
        }
        
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pTxn = mem.callocPointer(1);
            final long txn;
            int rc;
            
            if ((rc = LMDB.mdb_txn_begin(this.env, 0, LMDB.MDB_RDONLY, pTxn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }
            
            txn = pTxn.get();
            
            final IntBuffer pDbi = mem.callocInt(1);
            final int dbi;
            
            if ((rc = LMDB.mdb_dbi_open(txn, DB_DATA, LMDB.MDB_REVERSEKEY, pDbi)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }
            
            dbi = pDbi.get();
            
            final ByteBuffer pName = mem.UTF8(meta.name, false);
            final MDBVal key = MDBVal.callocStack(mem)
                    .mv_data(pName)
                    .mv_size(pName.limit());
            
            final MDBVal value = MDBVal.callocStack(mem);
            
            if ((rc = LMDB.mdb_get(txn, dbi, key, value)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }
            
            LMDB.mdb_txn_abort(txn);
            
            out = value.mv_data();
        }
        
        this.loadedSounds.put(meta, out);
        
        return out.asReadOnlyBuffer().order(ByteOrder.nativeOrder());
    }
    
    public SeekableSoundChannel getSound(final String key) throws IOException {
        final MetaSound meta = this.metaSounds.get(key);
        
        if (meta == null) {
            throw new IOException("Sound: \"" + key + "\" is not stored in the database!");
        }
        
        final ByteBuffer data = this.loadSound(meta);
        
        switch (meta.type) {
            case WAVE:
                return new WaveMemoryChannel(data);
            case VORBIS:
                return new VorbisMemoryChannel(data);
            default:
                throw new UnsupportedOperationException("Sound type: \"" + meta.type + "\" is not supported!");
        }
    }
    
    public void storeSound(final String res, final ByteBuffer sound) throws IOException {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pTxn = mem.callocPointer(1);
            final long txn;
            int rc;
            
            if ((rc = LMDB.mdb_txn_begin(this.env, 0, 0, pTxn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }
            
            txn = pTxn.get();
            
            final IntBuffer pMetaDbi = mem.callocInt(1);
            final int metaDbi;
            
            if ((rc = LMDB.mdb_dbi_open(txn, DB_META, LMDB.MDB_CREATE | LMDB.MDB_REVERSEKEY, pMetaDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }
            
            metaDbi = pMetaDbi.get();
            
            final ByteBuffer pName = mem.UTF8(res, false);
            final MDBVal key = MDBVal.callocStack(mem)
                    .mv_data(pName)
                    .mv_size(pName.remaining());
            
            final ByteBuffer metaData = mem.calloc(Integer.BYTES);
            final SoundType type = SoundType.valueOf(sound.order(ByteOrder.BIG_ENDIAN).getInt(0));
            
            metaData.putInt(type.value).flip();
            
            final MDBVal metaValue = MDBVal.callocStack(mem)
                    .mv_data(metaData)
                    .mv_size(metaData.remaining());
            
            if ((rc = LMDB.mdb_put(txn, metaDbi, key, metaValue, 0)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }
            
            final IntBuffer pDataDbi = mem.callocInt(1);
            final int dataDbi;
            
            if ((rc = LMDB.mdb_dbi_open(txn, DB_DATA, LMDB.MDB_CREATE | LMDB.MDB_REVERSEKEY, pDataDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }
            
            dataDbi = pDataDbi.get();
            
            final MDBVal dataValue = MDBVal.callocStack(mem)
                    .mv_data(sound.order(ByteOrder.nativeOrder()))
                    .mv_size(sound.remaining());
            
            if ((rc = LMDB.mdb_put(txn, dataDbi, key, dataValue, 0)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }
            
            if ((rc = LMDB.mdb_txn_commit(txn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }
            
            final MetaSound old = this.metaSounds.put(res, new MetaSound(res, type));
            
            if (old != null) {
                this.loadedSounds.remove(old);
            }
        }
    }
    
    @Override
    public void close() {
        if (this.env != 0L) {
            LMDB.mdb_env_close(this.env);
            this.env = 0L;
        }
    }
}
