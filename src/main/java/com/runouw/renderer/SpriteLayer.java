/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public interface SpriteLayer extends Layer {
    
    ImageView getSpriteView(String spriteId);
    
    ImageView getMaskView(String maskId);
    
    ColorTransformSlot[] getColorTransformSlots();
    
    ColorTransformSlot getColorTransformSlot(int slotId);
    
    SpriteSlot[] getSpriteSlots();
    
    SpriteSlot getSpriteSlot(int slotId);
        
    SpriteLayerInfo getInfo();
    
    void setDrawLimit(int drawCount);
    
    int getMaxDrawLimit();
}
