/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public class SoundLayerInfo implements LayerInfo {
    public final int maxConcurrentSounds;
    
    public SoundLayerInfo() {
        this(256);
    }
    
    public SoundLayerInfo(final int maxConcurrentSounds) {
        this.maxConcurrentSounds = maxConcurrentSounds;
    }
    
    public SoundLayerInfo withMaxConcurrentSounds(final int maxConcurrentSounds) {
        return new SoundLayerInfo(maxConcurrentSounds);
    }
}
