/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

/**
 *
 * @author zmichaels
 */
public class MappedTileCache implements TileCache {
    private FileChannel fch;
    private final ByteBuffer mapping;
    private final TileCacheInfo info;
    private boolean valid;    
    private final int totalSupertiles;
    private final int bytesPerSupertile;    
    
    public MappedTileCache(final TileCacheInfo info) throws IOException {
        this.info = info;        
        this.totalSupertiles = info.supertilesAcross * info.supertilesDown;
        
        final int supertileArea = info.supertileSize * info.supertileSize;
        
        this.bytesPerSupertile = supertileArea * Integer.BYTES;
        
        if (Files.exists(info.diskCache)) {
            this.valid = true;
            this.fch = FileChannel.open(info.diskCache, StandardOpenOption.READ, StandardOpenOption.WRITE);
        } else {
            this.valid = false;
            this.fch = FileChannel.open(info.diskCache, StandardOpenOption.CREATE_NEW, StandardOpenOption.SPARSE, StandardOpenOption.READ, StandardOpenOption.WRITE);
        }
        
        final int bytesNeeded = this.bytesPerSupertile * this.totalSupertiles;
        
        this.mapping = this.fch.map(FileChannel.MapMode.READ_WRITE, 0L, bytesNeeded);
    }
    
    @Override
    public ByteBuffer fetchSupertile(int stx, int sty) {
        if (stx < 0 || stx >= info.supertilesAcross || sty < 0 || sty >= info.supertilesDown) {
            throw new ArrayIndexOutOfBoundsException("Supertile ID is out of bounds!");
        }
        
        final int supertileId = sty * info.supertilesAcross + stx;
        final int offset = supertileId * this.bytesPerSupertile;
        
        this.mapping.position(offset);
        
        final ByteBuffer out = this.mapping.slice();
        
        out.limit(this.bytesPerSupertile);
        
        return out;
    }

    @Override
    public void storeSupertile(int stx, int sty, ByteBuffer data) {
        if (stx < 0 || stx >= info.supertilesAcross || sty < 0 || sty >= info.supertilesDown) {
            throw new ArrayIndexOutOfBoundsException("Supertile ID is out of bounds!");
        }
        
        final int supertileId = sty * info.supertilesAcross + stx;
        final int offset = supertileId * this.bytesPerSupertile;
        
        this.mapping.position(offset);
        this.mapping.put(data);
    }

    @Override
    public void invalidate() {
        this.valid = false;
    }

    @Override
    public void validate() {
        this.valid = true;
    }

    @Override
    public boolean isValid() {
        return this.fch != null && this.valid;
    }

    @Override
    public void close() throws IOException {
        if (fch != null) {
            fch.close();
            fch = null;
        }
    }

    @Override
    public TileCacheInfo getInfo() {
        return this.info;
    }
    
}
