/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import java.nio.file.Path;

/**
 *
 * @author zmichaels
 */
public final class TileCacheInfo {
    public final int supertilesAcross;
    public final int supertilesDown;
    public final int supertileSize;
    public final String uid;
    public final Path diskCache;
    public final long maxCacheSize;
    
    public TileCacheInfo(final int supertilesAcross, final int supertilesDown, final int supertileSize, final String uid, final Path diskCache, final long maxCacheSize) {
        this.supertileSize = supertileSize;
        this.supertilesAcross = supertilesAcross;
        this.supertilesDown = supertilesDown;
        this.uid = uid;
        this.diskCache = diskCache;
        this.maxCacheSize = maxCacheSize;
    }
    
    public TileCacheInfo() {
        this(16, 16, 4096, "", null, 16 * 16 * 4096 * 4);
    }
    
    public TileCacheInfo withBestFitMaxCacheSize() {
        return withMaxCacheSize(this.supertilesAcross * this.supertilesDown * this.supertileSize * this.supertileSize * 4);
    }
    
    public TileCacheInfo withSupertilesAcross(final int supertilesAcross) {
        return new TileCacheInfo(supertilesAcross, supertilesDown, supertileSize, uid, diskCache, maxCacheSize);
    }
    
    public TileCacheInfo withSupertilesDown(final int supertilesDown) {
        return new TileCacheInfo(supertilesAcross, supertilesDown, supertileSize, uid, diskCache, maxCacheSize);
    }
    
    public TileCacheInfo withSupertileSize(final int supertileSize) {
        return new TileCacheInfo(supertilesAcross, supertilesDown, supertileSize, uid, diskCache, maxCacheSize);
    }
    
    public TileCacheInfo withSupertileUID(final String uid) {
        return new TileCacheInfo(supertilesAcross, supertilesDown, supertileSize, uid, diskCache, maxCacheSize);
    }
    
    public TileCacheInfo withDiskCache(final Path diskCache) {
        return new TileCacheInfo(supertilesAcross, supertilesDown, supertileSize, uid, diskCache, maxCacheSize);
    }
    
    public TileCacheInfo withMaxCacheSize(final long maxCacheSize) {
        return new TileCacheInfo(supertilesAcross, supertilesDown, supertileSize, uid, diskCache, maxCacheSize);
    }
 
    public static TileCacheInfo deriveFrom(final TileLayerInfo tli) {
        return new TileCacheInfo()
                .withSupertilesAcross(tli.getSupertilesAcross())
                .withSupertilesDown(tli.getSupertilesDown())
                .withSupertileSize(tli.supertileSize)
                .withBestFitMaxCacheSize();
    }
}
