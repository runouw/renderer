/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.image.Image;
import java.util.Objects;

/**
 *
 * @author zmichaels
 */
public class MaskResourceInfo {
    public final String lookup;
    public final Image image;
    
    public MaskResourceInfo(final String lookup, final Image mask) {
        this.lookup = lookup;
        this.image = mask;
    }
    
    public MaskResourceInfo() {
        this("", null);
    }
    
    public MaskResourceInfo withLookup(final String lookup) {
        return new MaskResourceInfo(lookup, image);
    }
    
    public MaskResourceInfo withImage(final Image image) {
        return new MaskResourceInfo(lookup, image);
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (obj == this) {
            return true;
        } else if (obj instanceof MaskResourceInfo) {
            final MaskResourceInfo other = (MaskResourceInfo) obj;
            
            return other.image == this.image
                    && other.lookup.equals(this.lookup);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.lookup);
        hash = 79 * hash + Objects.hashCode(this.image);
        return hash;
    }
}
