/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
final class RenderFrameEndStep implements RenderStep {
    private final RenderEngine engine;
    private final Scene scene;
    
    RenderFrameEndStep(final RenderEngine engine) {
        this.engine = engine;
        this.scene = engine.scene;
    }
    
    @Override
    public void run() {
        try {
            this.engine.lock.acquire();
            this.engine.renderState = RenderState.END;
            this.scene.onFrameEnd();
        } catch (InterruptedException ex) {
            throw new RuntimeException("RenderEngine frameEnd was interrupted!", ex);
        } finally {
            this.engine.lock.release();
        }
    }

    @Override
    public RenderState getRenderState() {
        return RenderState.END;
    }
    
}
