/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.Tools;
import java.io.IOException;
import java.nio.file.Path;

/**
 *
 * @author zmichaels
 */
public class SoundDBInfo {
    public final Path cacheName;
    public final long size;
    
    public SoundDBInfo() {
        this(Tools.APPDATA.resolve(".soundcache"), 1024 * 1024 * 1024);
    }
    
    public SoundDBInfo(final Path cacheName, final long size) {
        this.cacheName = cacheName;
        this.size = size;
    }
    
    public SoundDBInfo withCacheName(final Path cacheName) {
        return new SoundDBInfo(cacheName, size);
    }
    
    public SoundDBInfo withSize(final long size) {
        return new SoundDBInfo(cacheName, size);
    }
    
    public SoundDB create() throws IOException {
        return new SoundDB(this);
    }
}
