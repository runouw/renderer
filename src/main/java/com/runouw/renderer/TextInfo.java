/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public class TextInfo {
    public final RGBA color;
    public final String text;
    public final float x;
    public final float y;
    
    public TextInfo(final String text, final float x, final float y, final RGBA color) {
        this.text = text;
        this.x = x;
        this.y = y;
        this.color = color;
    }
    
    public TextInfo() {
        this("", 0F, 0F, RGBA.TRANSPARENT_BLACK);
    }
    
    public TextInfo withRGBA(final RGBA color) {
        return new TextInfo(text, x, y, color);
    }
    
    public TextInfo withPosition(final float x, final float y) {
        return new TextInfo(text, x, y, color);
    }
    
    public TextInfo withText(final String text) {
        return new TextInfo(text, x, y, color);
    }
}
