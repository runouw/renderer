/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import java.nio.FloatBuffer;

/**
 *
 * @author zmichaels
 */
public interface ColorTransformSlot {
    default ColorTransformSlot setColorTransform(ColorTransform ct) {
        return this.set(ct.hsv, ct.rgbaOffset, ct.rgbaScale);
    }
    
    ColorTransformSlot set(HSV hsv, RGBA rgbaOffset, RGBA rgbaScale);
    
    default ColorTransformSlot setHSV(HSV hsv) {
        final double hue = Math.toRadians(hsv.hue);
        
        return this.setHSV(new float[] {(float) Math.cos(hue), (float) Math.sin(hue), hsv.saturation, hsv.value});
    }
    
    ColorTransformSlot setHSV(float[] hsv);
    
    ColorTransformSlot setHSV(final FloatBuffer hsv);
    
    default ColorTransformSlot setRGBAOffset(RGBA rgba) {
        return this.setRGBAOffset(rgba.getVector());
    }
    
    ColorTransformSlot setRGBAOffset(float[] rgba);
    
    ColorTransformSlot setRGBAOffset(FloatBuffer rgba);
    
    default ColorTransformSlot setRGBAScale(RGBA rgba) {
        return this.setRGBAScale(rgba.getVector());
    }
    
    ColorTransformSlot setRGBAScale(float[] rgba);
    
    ColorTransformSlot setRGBAScale(FloatBuffer rgba);
    
    ColorTransformSlot clear();
}
