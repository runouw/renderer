/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
final class RenderPresentStep implements RenderStep {
    private final RenderEngine engine;
    
    RenderPresentStep(final RenderEngine engine) {
        this.engine = engine;
    }
    
    @Override
    public void run() {
        try {
            this.engine.lock.acquire();
            this.engine.renderState = RenderState.PRESENT;
            this.engine.window.endFrame();
        } catch (InterruptedException ex) {
            throw new RuntimeException("RenderEngine framePresent was interrupted!", ex);
        } finally {
            this.engine.lock.release();
        }
    }

    @Override
    public RenderState getRenderState() {
        return RenderState.PRESENT;
    }
    
}
