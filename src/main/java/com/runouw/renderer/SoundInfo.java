/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.sound.SeekableSoundChannel;

/**
 *
 * @author zmichaels
 */
public class SoundInfo {
    public final float loopPoint;
    public final SeekableSoundChannel channel;
    public final float x;
    public final float y;
    public final float z;
    public final float gain;
    public final float pitch;
    
    public SoundInfo(final float loopPoint, final SeekableSoundChannel channel, final float x, final float y, final float z, final float gain, final float pitch) {
        this.loopPoint = loopPoint;
        this.x = x;
        this.y = y;
        this.z = z;
        this.gain = gain;
        this.pitch = pitch;
        this.channel = channel;
    }
    
    public SoundInfo() {
        this(0, null, 0f, 0f, 0f, 1f, 1f);
    }
    
    public SoundInfo withPitch(final float pitch) {
        return new SoundInfo(loopPoint, channel, x, y, z, gain, pitch);
    }
    
    public SoundInfo withPosition(final float x, final float y, final float z) {
        return new SoundInfo(loopPoint, channel, x, y, z, gain, pitch);
    }
    
    public SoundInfo withGain(final float gain) {
        return new SoundInfo(loopPoint, channel, x, y, z, gain, pitch);
    }
    
    public SoundInfo withLoopPoint(final float loopPoint) {
        return new SoundInfo(loopPoint, channel, x, y, z, gain, pitch);
    }
    
    public SoundInfo withChannel(final SeekableSoundChannel channel) {
        return new SoundInfo(loopPoint, channel, x, y, z, gain, pitch);
    }
}
