/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public interface Layer extends AutoCloseable {
    void onFrameStart();
    
    void onFrameEnd();
    
    void setProjection(float[] projection);
    
    boolean isWritable();
    
    @Override
    void close();
}
