/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author zmichaels
 */
public final class ColorTransform {
    public static final ColorTransform IDENTITY = new ColorTransform();
    
    public final HSV hsv;
    public final RGBA rgbaOffset;
    public final RGBA rgbaScale;
    
    public ColorTransform(final HSV hsv, final RGBA rgbaOffset, final RGBA rgbaScale) {
        this.hsv = hsv;
        this.rgbaOffset = rgbaOffset;
        this.rgbaScale = rgbaScale;
    }
    
    public ColorTransform() {
        this(HSV.IDENTITY, RGBA.IDENTITY_OFFSET, RGBA.IDENTITY_SCALE);
    }
    
    public ColorTransform withHSV(final HSV hsv) {
        return new ColorTransform(hsv, rgbaOffset, rgbaScale);
    }
    
    public ColorTransform withRGBAOffset(final RGBA rgbaOffset) {
        return new ColorTransform(hsv, rgbaOffset, rgbaScale);
    }
    
    public ColorTransform withRGBAScale(final RGBA rgbaScale) {
        return new ColorTransform(hsv, rgbaOffset, rgbaScale);
    }
    
    @SuppressWarnings("unchecked")
    public static ColorTransform fromMap(final Map<String, Object> mapping) {
        final HSV hsv = HSV.fromMap((Map<String, Object> ) mapping.get("hsv"));
        final RGBA rgbaOffset = RGBA.fromMap(((Map<String, Object>) mapping.get("rgbaOffset")));
        final RGBA rgbaScale = RGBA.fromMap((Map<String, Object>) mapping.get("rgbaScale"));
        
        return new ColorTransform(hsv, rgbaOffset, rgbaScale);
    }
    
    public static Map<String, Object> toMap(final ColorTransform ct) {
        final Map<String, Object> out = new HashMap<>();
        
        out.put("hsv", ct.hsv.toMap());
        out.put("rgbaOffset", ct.rgbaOffset.toMap());
        out.put("rgbaScale", ct.rgbaScale.toMap());
        
        return out;
    }
    
    public Map<String, Object> toMap() {
        return toMap(this);
    }
    
    @Override
    public String toString() {
        return new StringBuilder(4096)
                .append("ColorTransform: [HSV: <")
                .append(hsv.hue).append(", ").append(hsv.saturation).append(", ").append(hsv.value)
                .append("> rgbaOffset: <")
                .append(rgbaOffset.red).append(", ").append(rgbaOffset.green).append(", ").append(rgbaOffset.blue).append(", ").append(rgbaOffset.alpha)
                .append("> rgbaScale: <")
                .append(rgbaScale.red).append(", ").append(rgbaScale.green).append(", ").append(rgbaScale.blue).append(", ").append(rgbaScale.alpha)
                .append(">]")
                .toString();
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (obj == this) {
            return true;
        } else if (obj instanceof ColorTransform) {
            final ColorTransform other = (ColorTransform) obj;
            
            return other.hsv.equals(this.hsv)
                    && other.rgbaOffset.equals(this.rgbaOffset)
                    && other.rgbaScale.equals(this.rgbaScale);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.hsv);
        hash = 79 * hash + Objects.hashCode(this.rgbaOffset);
        hash = 79 * hash + Objects.hashCode(this.rgbaScale);
        return hash;
    }
}
