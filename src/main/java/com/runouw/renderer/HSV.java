/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.runouw.rcodec.Fixed;
import java.util.HashMap;
import java.util.Map;

/**
 * Hue, Saturation, and Value
 *
 * @author zmichaels
 */
public final class HSV {

    private static final float EPSILON = Float.parseFloat(System.getProperty("com.runouw.renderer.hsv.epsilon", "0.01"));
    public static final HSV IDENTITY = new HSV(0.0F, 1.0F, 1.0F);

    public final float hue;
    public final float saturation;
    public final float value;

    public HSV(final float hue, final float saturation, final float value) {
        this.hue = hue;
        this.saturation = saturation;
        this.value = value;
    }

    public HSV() {
        this(0.0F, 1.0F, 1.0F);
    }

    public HSV withHue(final float hue) {
        return new HSV(hue, saturation, value);
    }

    public HSV withSaturation(final float saturation) {
        return new HSV(hue, saturation, value);
    }

    public HSV withValue(final float value) {
        return new HSV(hue, saturation, value);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (obj == this) {
            return true;
        } else if (obj instanceof HSV) {
            final HSV other = (HSV) obj;

            return Math.abs(other.hue - this.hue) < EPSILON
                    && Math.abs(other.saturation - this.saturation) < EPSILON
                    && Math.abs(other.value - this.value) < EPSILON;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Float.floatToIntBits(this.hue);
        hash = 79 * hash + Float.floatToIntBits(this.saturation);
        hash = 79 * hash + Float.floatToIntBits(this.value);
        return hash;
    }

    public static HSV fromMap(final Map<String, Object> mapping) {
        final float hue = ((Number) mapping.get("h")).floatValue();
        final float saturation = ((Number) mapping.get("s")).floatValue();
        final float value = ((Number) mapping.get("v")).floatValue();

        return new HSV(hue, saturation, value);
    }

    public static Map<String, Object> toMap(final HSV hsv) {
        final Map<String, Object> out = new HashMap<>();

        out.put("h", new Fixed(hsv.hue));
        out.put("s", new Fixed(hsv.saturation));
        out.put("v", new Fixed(hsv.value));

        return out;
    }

    public Map<String, Object> toMap() {
        return toMap(this);
    }

    @Override
    public String toString() {
        return new StringBuilder(4096)
                .append("HSV: <")
                .append(hue)
                .append(", ")
                .append(saturation)
                .append(", ")
                .append(value)
                .append(">")
                .toString();
    }
}
