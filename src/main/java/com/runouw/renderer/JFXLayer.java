/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.glfw.GLFWCharCallback;
import com.longlinkislong.gloop2.glfw.GLFWCursorPosCallback;
import com.longlinkislong.gloop2.glfw.GLFWKeyAction;
import com.longlinkislong.gloop2.glfw.GLFWKeyCallback;
import com.longlinkislong.gloop2.glfw.GLFWKeyModifier;
import com.longlinkislong.gloop2.glfw.GLFWMouseButtonAction;
import com.longlinkislong.gloop2.glfw.GLFWMouseButtonCallback;
import com.longlinkislong.gloop2.glfw.GLFWScrollCallback;
import com.longlinkislong.gloop2.glfw.GLFWWindow;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;

/**
 *
 * @author zmichaels
 */
public interface JFXLayer extends Layer, GLFWKeyCallback, GLFWMouseButtonCallback, GLFWCursorPosCallback, GLFWCharCallback, GLFWScrollCallback {
    void setFocus(boolean focus);    
    
    Optional<javafx.scene.Scene> getScene();
    
    Optional<Parent> getRootNode();
    
    ObservableList<Node> getRootChildren();
    
    void setScene(javafx.scene.Scene scene);
    
    void setStageAbsLocation(int x, int y);    
    
    JFXLayerInfo getInfo();    
    
    @Override
    default boolean isWritable() {
        return false;
    }
    
    static final class InputMultiplexer implements GLFWKeyCallback, GLFWMouseButtonCallback, GLFWCursorPosCallback, GLFWCharCallback, GLFWScrollCallback {
        private final List<JFXLayer> layers;
        
        InputMultiplexer(final Stream<JFXLayer> layers) {
            this.layers = layers.collect(Collectors.toList());
        }
        
        @Override
        public void onKey(GLFWWindow glfww, int i, int i1, GLFWKeyAction action, Set<GLFWKeyModifier> set) {
            this.layers.forEach(layer -> layer.onKey(glfww, i, i1, action, set));
        }

        @Override
        public void onMouseButton(GLFWWindow glfww, int i, GLFWMouseButtonAction action, Set<GLFWKeyModifier> set) {
            this.layers.forEach(layer -> layer.onMouseButton(glfww, i, action, set));
        }

        @Override
        public void onMouseMove(GLFWWindow glfww, double d, double d1) {
            this.layers.forEach(layer -> layer.onMouseMove(glfww, d, d1));
        }

        @Override
        public void onChar(GLFWWindow glfww, int i) {
            this.layers.forEach(layer -> layer.onChar(glfww, i));
        }

        @Override
        public void onScroll(GLFWWindow glfww, double d, double d1) {
            this.layers.forEach(layer -> layer.onScroll(glfww, d, d1));
        }
        
    }
}
