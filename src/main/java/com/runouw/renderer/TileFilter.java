/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public class TileFilter {
    public final int x;
    public final int y;
    public final int w;
    public final int h;
    public final float radius;
    public final ColorTransform colorTransform;
    
    public TileFilter(final ColorTransform colorTransform,
            final int x, final int y, final int w, final int h,
            final float radius) {
        
        this.colorTransform = colorTransform;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.radius = radius;
    }
    
    public TileFilter() {
        this(ColorTransform.IDENTITY, 0, 0, 0, 0, 0F);
    }
    
    public TileFilter withColorTransform(final ColorTransform colorTransform) {
        return new TileFilter(colorTransform, x, y, w, h, radius);
    }
    
    public TileFilter withOffset(final int x, final int y) {
        return new TileFilter(colorTransform, x, y, w, h, radius);
    }
    
    public TileFilter withExtent(final int w, final int h) {
        return new TileFilter(colorTransform, x, y, w, h, radius);
    }
    
    public TileFilter withRadius(final float radius) {
        return new TileFilter(colorTransform, x, y, w, h, radius);
    }
    
    public boolean intersects(int x, int y, int w, int h) {
        return x + w > this.x
                && y + h > this.y
                && x < this.x + this.w
                && y < this.y + h;
    }        
}
