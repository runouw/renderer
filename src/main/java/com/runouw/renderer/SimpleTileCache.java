/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import java.nio.ByteBuffer;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public class SimpleTileCache implements TileCache {

    private ByteBuffer cache;
    private final TileCacheInfo info;
    private final int totalSupertiles;
    private final int bytesPerSupertile;
    private boolean invalid = true;
    private final long ptr;

    public SimpleTileCache(final TileCacheInfo info) {
        this.totalSupertiles = info.supertilesAcross * info.supertilesDown;

        final int supertileArea = info.supertileSize * info.supertileSize;

        this.bytesPerSupertile = supertileArea * Integer.BYTES;

        this.cache = MemoryUtil.memCalloc(bytesPerSupertile * totalSupertiles);
        this.ptr = MemoryUtil.memAddress(this.cache);
        this.info = info;
    }

    @Override
    public void storeSupertile(int stx, int sty, ByteBuffer data) {
        if (stx < 0 || stx >= info.supertilesAcross || sty < 0 || sty >= info.supertilesDown) {
            throw new ArrayIndexOutOfBoundsException("Supertile ID is out of bounds!");
        }

        final int supertileId = sty * info.supertilesAcross + stx;
        final int offset = supertileId * this.bytesPerSupertile;

        if (data.isDirect()) {
            MemoryUtil.memCopy(MemoryUtil.memAddress(data), this.ptr + offset, this.bytesPerSupertile);
        } else {
            this.cache.position(offset);
            this.cache.put(data);
        }
    }

    @Override
    public void invalidate() {
        this.invalid = true;
    }

    @Override
    public void validate() {
        this.invalid = false;
    }

    @Override
    public boolean isValid() {
        return this.cache != null && !this.invalid;
    }

    @Override
    public void close() {
        if (this.cache != null) {
            MemoryUtil.memFree(this.cache);
            this.cache = null;
        }
    }

    @Override
    public ByteBuffer fetchSupertile(int stx, int sty) {
        if (stx < 0 || stx >= info.supertilesAcross || sty < 0 || sty >= info.supertilesDown) {
            throw new ArrayIndexOutOfBoundsException("Supertile ID is out of bounds!");
        }

        final int supertileId = sty * info.supertilesAcross + stx;
        final int offset = supertileId * this.bytesPerSupertile;

        return MemoryUtil.memByteBuffer(this.ptr + offset, this.bytesPerSupertile);
    }

    @Override
    public TileCacheInfo getInfo() {
        return this.info;
    }
}
