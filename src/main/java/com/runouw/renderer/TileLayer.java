/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.image.Image;
import java.util.Optional;

/**
 *
 * @author zmichaels
 */
public interface TileLayer extends Layer {

    /**
     * Special Mapped ID that indicates that hides the Supertile. Hidden
     * supertiles will not be rendered.
     */
    public static final int SUPERTILE_HIDDEN = -1;

    /**
     * Retrieves the TileLayerInfo used to build this TileLayer.
     *
     * @return the TileLayerInfo.
     */
    TileLayerInfo getInfo();

    /**
     * Writes an update to the TileLayer at the given tile id.
     *
     * @param img the image to write.
     * @param x the tile x-location in tile coordinates.
     * @param y the tile y-location in tile coordinates.
     */
    void setTile(Image img, int x, int y);

    /**
     * Writes multiple updates to the TileLayer.
     *
     * @param img the fill tile.
     * @param startX the fill start x-location in tile coordinates.
     * @param startY the fill start y-location in tile coordinates.
     * @param width the number of tiles across to fill.
     * @param height the number of tiles down to fill.
     */
    void fillTile(Image img, int startX, int startY, int width, int height);

    /**
     * Retrieves a tile at the given location. This is guaranteed to be atomic
     * as opposed to [code]getInfo().getTile(x, y)[/code]
     *
     * @param x the x-location in tile coordinates.
     * @param y the y-location in tile coordinates.
     * @return the tile if present.
     */
    Optional<Image> getTile(int x, int y);

    /**
     * Atomically checks if a Supertile is empty.
     *
     * @param stx the x-location of the Supertile in supertile coordinates.
     * @param sty the y-location of the Supertile in supertile coordinates.
     * @return true if the Supertile contains at least 1 tile.
     */
    boolean isSupertileEmpty(int stx, int sty);

    /**
     * Sets the Supertile mapping. This will either hide the Supertile or make
     * it visible. Assigning SUPERTILE_HIDDEN for the mappedId will make
     * resident Supertiles non-resident. Reusing a mappedId will evict the
     * previously bound Supertile.
     *
     * @param stx the x-location of the Supertile in supertile coordinates.
     * @param sty the y-location of the Supertile in supertile coordinates.
     * @param mappedId the mapped id. Use SUPERTILE_HIDDEN to release the
     * mapping.
     */
    void setSupertileMapping(int stx, int sty, int mappedId);

    /**
     * Checks if the Supertile is resident in vram. Only resident Supertiles are
     * rendered.
     *
     * @param stx the x-location of the Supertile in supertile coordinates.
     * @param sty the y-location of the Supertile in supertile coordinates.
     * @return true if the Supertile is resident in vram.
     */
    boolean isSupertileResident(int stx, int sty);
    
    void addTileFilter(TileFilter filter);
    
    boolean removeTileFilter(TileFilter filter);
}
