/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.image.Image;
import com.longlinkislong.gloop2.image.PixelFormat;
import com.longlinkislong.gloop2.image.ProxyImage;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;
import org.lwjgl.util.lmdb.LMDB;
import org.lwjgl.util.lmdb.MDBVal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A database that maps Strings to images and stores data onto the hard drive.
 *
 * @author zmichaels
 */
public final class ImageDB implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageDB.class);

    private static final String DB_DATA = "DATA";
    private static final String DB_META = "META";
    private final ImageDBInfo info;
    private long env;
    private final Map<String, MetaImage> metaImages = new HashMap<>();
    private final Map<MetaImage, ProxyImage> loadedImages = new HashMap<>();

    /**
     * Checks if the entire Collection of images is loaded.
     *
     * @param images the images to check
     * @return true if all images are loaded.
     */
    public boolean hasImages(final Collection<String> images) {
        return this.metaImages.keySet().containsAll(images);
    }

    /**
     * Checks if the entire list of images is loaded.
     *
     * @param images the image names to check
     * @return true if all images in the list are loaded.
     */
    public boolean hasImages(final String... images) {
        return this.hasImages(Arrays.asList(images));
    }

    /**
     * Retrieves a Set of loaded resource names. These are the keys used for
     * images.
     *
     * @return an unmodifiable copy of the key set.
     */
    public Set<String> getResourceNames() {
        return Collections.unmodifiableSet(new HashSet<>(this.metaImages.keySet()));
    }

    /**
     * Retrieves a Stream of all images stored inside the database. This will
     * cause each resource to be loaded.
     *
     * @return the stream of images
     */
    public Stream<Image> images() {
        return this.resourceNames()
                .map(this.metaImages::get)
                .map(metaImage -> {
                    try {
                        return this.loadImage(metaImage);
                    } catch (IOException ex) {
                        LOGGER.error("Unable to load image: {}!", metaImage.name);
                        LOGGER.debug(ex.getMessage(), ex);
                        return null;
                    }
                }).filter(Objects::nonNull);
    }

    /**
     * Retrieves a stream of loaded image names.
     *
     * @return the loaded names.
     */
    public Stream<String> resourceNames() {
        return this.metaImages.keySet().stream();
    }

    private static final class MetaImage {

        private final int width;
        private final int height;
        private final String name;

        private MetaImage(final String name, final int width, final int height) {
            this.name = name;
            this.width = width;
            this.height = height;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj == null) {
                return false;
            } else if (obj == this) {
                return true;
            } else if (obj instanceof MetaImage) {
                final MetaImage other = (MetaImage) obj;

                return other.name.equals(this.name)
                        && other.width == this.width
                        && other.height == this.height;
            } else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 73 * hash + this.width;
            hash = 73 * hash + this.height;
            hash = 73 * hash + Objects.hashCode(this.name);
            return hash;
        }
    }

    /**
     * Retrieves the info used to create the ImageDB.
     *
     * @return the info.
     */
    public ImageDBInfo getInfo() {
        return this.info;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (obj == this) {
            return true;
        } else if (obj instanceof ImageDB) {
            return ((ImageDB) obj).info.equals(this.info);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.info);
        return hash;
    }

    /**
     * Clears all loaded data and re-identifies all images stored in the
     * database.
     *
     * @throws IOException if an operation failed while scanning the database.
     */
    public void flush() throws IOException {
        this.metaImages.clear();
        this.loadedImages.clear();
        this.identifyImages();
    }

    /**
     * Clears all images stored in the database.
     *
     * @throws IOException if an operation failed while clearing the database.
     */
    public void clear() throws IOException {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pTxn = mem.callocPointer(1);
            final long txn;
            int rc;

            if ((rc = LMDB.mdb_txn_begin(this.env, 0, 0, pTxn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            txn = pTxn.get();

            final IntBuffer pMetaDbi = mem.callocInt(1);
            final int metaDbi;

            if ((rc = LMDB.mdb_dbi_open(txn, DB_META, LMDB.MDB_CREATE | LMDB.MDB_REVERSEKEY, pMetaDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            metaDbi = pMetaDbi.get();

            final IntBuffer pDataDbi = mem.callocInt(1);
            final int dataDbi;

            if ((rc = LMDB.mdb_dbi_open(txn, DB_DATA, LMDB.MDB_CREATE | LMDB.MDB_REVERSEKEY, pDataDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            dataDbi = pDataDbi.get();

            if ((rc = LMDB.mdb_drop(txn, metaDbi, false)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            if ((rc = LMDB.mdb_drop(txn, dataDbi, false)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            if ((rc = LMDB.mdb_txn_commit(txn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }
        }

        this.metaImages.clear();
        this.loadedImages.clear();
    }

    /**
     * Forces a writeback of all pending data to the database.
     */
    public void sync() {
        LMDB.mdb_env_sync(this.env, true);
    }

    /**
     * Constructs a new database from the construction information.
     *
     * @param info the construction information.
     * @throws IOException if an operation failed while creating the database or
     * scanning the database.
     */
    public ImageDB(final ImageDBInfo info) throws IOException {
        this.info = info;

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pEnv = mem.callocPointer(1);
            int rc;

            if ((rc = LMDB.mdb_env_create(pEnv)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            final long tEnv = pEnv.get();

            // align size to page size
            final long dbSize = (info.size + MemoryUtil.PAGE_SIZE - 1) / MemoryUtil.PAGE_SIZE * MemoryUtil.PAGE_SIZE;

            LOGGER.trace("Using DB size: {}", dbSize);

            if ((rc = LMDB.mdb_env_set_mapsize(tEnv, dbSize)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            if ((rc = LMDB.mdb_env_set_maxdbs(tEnv, 2)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            Files.createDirectories(info.cacheName);

            if ((rc = LMDB.mdb_env_open(tEnv, info.cacheName.toAbsolutePath().toString(), 0, 0644)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            this.env = tEnv;
            this.identifyImages();
        }
    }

    private void identifyImages() throws IOException {
        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pTxn = mem.callocPointer(1);
            final long txn;
            int rc;

            if ((rc = LMDB.mdb_txn_begin(this.env, 0, 0, pTxn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            txn = pTxn.get();

            final IntBuffer pDbi = mem.callocInt(1);
            final int dbi;

            if ((rc = LMDB.mdb_dbi_open(txn, DB_META, LMDB.MDB_CREATE | LMDB.MDB_REVERSEKEY, pDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            dbi = pDbi.get();

            final PointerBuffer pCurs = mem.callocPointer(1);
            final long curs;

            if ((rc = LMDB.mdb_cursor_open(txn, dbi, pCurs)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            curs = pCurs.get();

            final MDBVal key = MDBVal.callocStack(mem);
            final MDBVal value = MDBVal.callocStack(mem);

            while (LMDB.mdb_cursor_get(curs, key, value, LMDB.MDB_NEXT) == LMDB.MDB_SUCCESS) {
                final String name = MemoryUtil.memUTF8(key.mv_data(), (int) key.mv_size());
                final ByteBuffer data = value.mv_data();
                final int width = data.getInt();
                final int height = data.getInt();

                this.metaImages.put(name, new MetaImage(name, width, height));
            }

            LMDB.mdb_cursor_close(curs);
            LMDB.mdb_txn_abort(txn);
        }
    }

    private Image loadImage(final MetaImage meta) throws IOException {
        ProxyImage out = this.loadedImages.get(meta);

        if (out != null) {
            return out;
        }

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pTxn = mem.callocPointer(1);
            final long txn;
            int rc;

            if ((rc = LMDB.mdb_txn_begin(this.env, 0, LMDB.MDB_RDONLY, pTxn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            txn = pTxn.get();

            final IntBuffer pDbi = mem.callocInt(1);
            final int dbi;

            if ((rc = LMDB.mdb_dbi_open(txn, DB_DATA, LMDB.MDB_REVERSEKEY, pDbi)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            dbi = pDbi.get();

            final ByteBuffer pName = mem.UTF8(meta.name, false);
            final MDBVal key = MDBVal.callocStack(mem)
                    .mv_data(pName)
                    .mv_size(pName.remaining());

            final MDBVal value = MDBVal.callocStack(mem);

            if ((rc = LMDB.mdb_get(txn, dbi, key, value)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            LMDB.mdb_txn_abort(txn);

            out = new ProxyImage(meta.width, meta.height, PixelFormat.R8G8B8A8_UNORM, value.mv_data());
        }

        this.loadedImages.put(meta, out);

        return out;
    }

    /**
     * Retrieves an image from the database.
     *
     * @param key the name of the image.
     * @return the image.
     * @throws IOException if the image load operation fails or the key doesn't exist.
     * database.
     */
    public Image getImage(final String key) throws IOException {
        final MetaImage meta = this.metaImages.get(key);

        if (meta == null) {
            throw new IOException("Image: \"" + key + "\" is not stored in the database!");
        }

        return loadImage(meta);
    }

    /**
     * Stores an image inside the database. This will overwrite any image that
     * already occupies the same exact key.
     *
     * @param res the key to associate to the image.
     * @param image the image to store.
     * @throws IOException if the image store operation fails.
     */
    public void storeImage(final String res, final Image image) throws IOException {
        final int width = image.getWidth();
        final int height = image.getHeight();

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pTxn = mem.callocPointer(1);
            final long txn;
            int rc;

            if ((rc = LMDB.mdb_txn_begin(this.env, 0, 0, pTxn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            txn = pTxn.get();

            final IntBuffer pMetaDbi = mem.callocInt(1);
            final int metaDbi;

            if ((rc = LMDB.mdb_dbi_open(txn, DB_META, LMDB.MDB_CREATE | LMDB.MDB_REVERSEKEY, pMetaDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            metaDbi = pMetaDbi.get();

            final ByteBuffer pName = mem.UTF8(res, false);
            final MDBVal key = MDBVal.callocStack(mem)
                    .mv_data(pName)
                    .mv_size(pName.remaining());

            final ByteBuffer metaData = mem.malloc(Integer.BYTES * 2);

            metaData.putInt(width).putInt(height).flip();

            final MDBVal metaValue = MDBVal.callocStack(mem)
                    .mv_data(metaData)
                    .mv_size(metaData.remaining());

            if ((rc = LMDB.mdb_put(txn, metaDbi, key, metaValue, 0)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            final IntBuffer pDataDbi = mem.callocInt(1);
            final int dataDbi;

            if ((rc = LMDB.mdb_dbi_open(txn, DB_DATA, LMDB.MDB_CREATE | LMDB.MDB_REVERSEKEY, pDataDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            dataDbi = pDataDbi.get();

            final ByteBuffer data = image.getData();

            final MDBVal dataValue = MDBVal.callocStack(mem)
                    .mv_data(data)
                    .mv_size(data.remaining());

            if ((rc = LMDB.mdb_put(txn, dataDbi, key, dataValue, 0)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            if ((rc = LMDB.mdb_txn_commit(txn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            final MetaImage old = this.metaImages.put(res, new MetaImage(res, width, height));

            if (old != null) {
                this.loadedImages.remove(old);
            }
        }
    }

    /**
     * Performs a bulk load operation on a Collection of image keys.
     *
     * @param imageNames the Collection of image keys.
     * @return the mapping of key to image.
     * @throws IOException if the load operation fails.
     */
    public Map<String, Image> loadImages(final Collection<String> imageNames) throws IOException {
        final Set<String> names = new HashSet<>(imageNames);
        final Map<String, Image> out = new HashMap<>();

        try (MemoryStack mem = MemoryStack.stackPush()) {
            final PointerBuffer pTxn = mem.callocPointer(1);
            final long txn;
            int rc;

            if ((rc = LMDB.mdb_txn_begin(this.env, 0, LMDB.MDB_RDONLY, pTxn)) != LMDB.MDB_SUCCESS) {
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            txn = pTxn.get();

            final IntBuffer pDbi = mem.callocInt(1);
            final int dbi;

            if ((rc = LMDB.mdb_dbi_open(txn, DB_DATA, LMDB.MDB_REVERSEKEY, pDbi)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            dbi = pDbi.get();

            final PointerBuffer pCurs = mem.callocPointer(1);
            final long curs;

            if ((rc = LMDB.mdb_cursor_open(txn, dbi, pCurs)) != LMDB.MDB_SUCCESS) {
                LMDB.mdb_txn_abort(txn);
                throw new IOException(LMDB.mdb_strerror(rc));
            }

            curs = pCurs.get();

            final MDBVal key = MDBVal.callocStack(mem);
            final MDBVal value = MDBVal.callocStack(mem);

            while (LMDB.mdb_cursor_get(curs, key, value, LMDB.MDB_NEXT) == LMDB.MDB_SUCCESS) {
                final String name = MemoryUtil.memUTF8(key.mv_data(), (int) key.mv_size());

                if (names.contains(name)) {
                    final MetaImage meta = this.metaImages.get(name);
                    final ProxyImage img = new ProxyImage(meta.width, meta.height, PixelFormat.R8G8B8A8_UNORM, value.mv_data());

                    out.put(name, img);

                    this.loadedImages.put(meta, img);
                }
            }

            LMDB.mdb_cursor_close(curs);
            LMDB.mdb_txn_abort(txn);

            return out;
        }
    }

    @Override
    public void close() {
        if (this.env != 0L) {
            LMDB.mdb_env_close(this.env);
            this.env = 0L;
        }
    }
}
