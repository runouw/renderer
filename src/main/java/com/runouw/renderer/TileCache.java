/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import java.nio.ByteBuffer;

/**
 *
 * @author zmichaels
 */
public interface TileCache extends AutoCloseable {
    ByteBuffer fetchSupertile(int stx, int sty);
    
    void storeSupertile(int stx, int sty, ByteBuffer data);
    
    void invalidate();
    
    void validate();
    
    boolean isValid();        
    
    TileCacheInfo getInfo();
}
