/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public final class Constants {
    private Constants() {}
    
    public static final int PIXELS_PER_INCH = Integer.getInteger("com.runouw.renderer.Constants.ppi", 96);
    public static final int POINTS_PER_INCH = Integer.getInteger("com.runouw.renderer.Constants.ptsize", 72);
    public static final float PIXELS_PER_POINT = (float) PIXELS_PER_INCH / (float) POINTS_PER_INCH;
    public static final int SOUND_REFRESH_RATE = Integer.getInteger("com.runouw.renderer.Constants.sound_refresh_rate", 20);
    public static final int SOUND_BUFFER_ALIGNMENT = Integer.getInteger("com.runouw.renderer.Constants.sound_buffer_alignment", 4096);
    public static final int SOUND_BUFFER_COUNT = Integer.getInteger("com.runouw.renderer.Constants.sound_buffer_count", 3);
}
