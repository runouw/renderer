/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.glfw.GLFWKeyAction;
import com.longlinkislong.gloop2.glfw.GLFWKeyModifier;
import com.longlinkislong.gloop2.glfw.GLFWMouseButtonAction;
import com.longlinkislong.gloop2.glfw.GLFWWindow;
import com.sun.javafx.cursor.CursorFrame;
import com.sun.javafx.embed.AbstractEvents;
import com.sun.javafx.embed.EmbeddedSceneInterface;
import com.sun.javafx.embed.EmbeddedStageInterface;
import com.sun.javafx.embed.HostInterface;
import com.sun.javafx.stage.EmbeddedWindow;
import com.sun.javafx.tk.Toolkit;
import java.nio.IntBuffer;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_BACKSPACE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_CAPS_LOCK;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_DELETE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_END;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ENTER;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_F1;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_F25;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_HOME;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_INSERT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_KP_0;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_KP_9;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT_ALT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT_CONTROL;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT_SHIFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_NUM_LOCK;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_PAGE_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_PAGE_UP;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_PAUSE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_PRINT_SCREEN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_RIGHT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_RIGHT_ALT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_RIGHT_CONTROL;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_RIGHT_SHIFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_SCROLL_LOCK;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_TAB;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public abstract class AbstractJFXLayer implements JFXLayer {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractJFXLayer.class);

    private static enum KeyMod {
        SHIFT,
        ALT,
        CTRL,
        META
    }

    private static enum MouseButton {
        PRIMARY,
        SECONDARY,
        MIDDLE
    }

    private final float SCALE_FACTOR = Float.parseFloat(System.getProperty("com.runouw.renderer.AbstractJFXLayer.scale_factor", "1.0"));
    private boolean needsUpdate = true;
    private EmbeddedStageInterface emStage;
    private EmbeddedSceneInterface emScene;
    private EmbeddedWindow stage;
    private boolean focus = false;
    private final Set<KeyMod> keyMods = new HashSet<>();
    private final Set<MouseButton> mouseButtons = new HashSet<>();
    private final Semaphore lock = new Semaphore(1);
    private int mouseX, mouseY;
    private int absMouseX, absMouseY;
    private int oldEMX, oldEMY;
    private JFXLayerInfo info;
    private final HostInterface hostContainer = new HostInterface() {
        @Override
        public void setEmbeddedStage(EmbeddedStageInterface embeddedStage) {
            if (info.width > 0 && info.height > 0) {
                embeddedStage.setSize(info.width, info.height);
            }

            embeddedStage.setLocation(0, 0);
            AbstractJFXLayer.this.emStage = embeddedStage;
        }

        @Override
        public void setEmbeddedScene(EmbeddedSceneInterface embeddedScene) {
            if (AbstractJFXLayer.this.emScene == embeddedScene) {
                return;
            }

            embeddedScene.setSize(info.width, info.height);
            embeddedScene.setPixelScaleFactor(SCALE_FACTOR);
            AbstractJFXLayer.this.emScene = embeddedScene;

            dndHandler = new JFXDNDHandler(embeddedScene);
            contextMenuHandler = new JFXContextMenuHandler(AbstractJFXLayer.this);
        }

        @Override
        public boolean requestFocus() {
            return false;
        }

        @Override
        public boolean traverseFocusOut(boolean forward) {
            return false;
        }

        @Override
        public void repaint() {
            AbstractJFXLayer.this.needsUpdate = true;
        }

        @Override
        public void setPreferredSize(int width, int height) {

        }

        @Override
        public void setEnabled(boolean enabled) {

        }

        @Override
        public void setCursor(CursorFrame cursorFrame) {
            //TODO: handle cursor 
        }

        @Override
        public boolean grabFocus() {
            return false;
        }

        @Override
        public void ungrabFocus() {

        }
    };
    private JFXDNDHandler dndHandler = null;
    private JFXContextMenuHandler contextMenuHandler = null;

    public AbstractJFXLayer(final JFXLayerInfo info) {
        this.info = info;
    }

    @Override
    public JFXLayerInfo getInfo() {
        return this.info;
    }

    @Override
    public void setFocus(boolean focus) {
        try {
            this.lock.acquire();

            if (this.focus == focus) {
                return;
            }

            this.focus = focus;

            if (!focus) {
                this.keyMods.clear();

                if (this.emStage != null) {
                    this.emStage.setFocused(false, AbstractEvents.FOCUSEVENT_DEACTIVATED);
                }
            } else if (this.emStage != null) {
                this.emStage.setFocused(true, AbstractEvents.FOCUSEVENT_ACTIVATED);
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting focus!", ex);
        } finally {
            this.lock.release();
        }
    }

    @Override
    public Optional<javafx.scene.Scene> getScene() {
        try {
            this.lock.acquire();

            if (this.stage != null) {
                return Optional.of(this.stage.getScene());
            } else {
                return Optional.empty();
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while getting scene!", ex);
        } finally {
            this.lock.release();
        }
    }

    @Override
    public Optional<Parent> getRootNode() {
        return this.getScene()
                .map(javafx.scene.Scene::getRoot);
    }

    @Override
    public ObservableList<Node> getRootChildren() {
        final Optional<Parent> oRoot = this.getRootNode();

        if (oRoot.isPresent()) {
            final Parent root = oRoot.get();

            if (root instanceof Group) {
                return ((Group) root).getChildren();
            } else if (root instanceof Pane) {
                return ((Pane) root).getChildren();
            }
        }

        return FXCollections.emptyObservableList();
    }

    private void setSceneImpl(final javafx.scene.Scene scene) {
        try {
            this.lock.acquire();

            if ((this.stage != null) && scene == null) {
                this.stage.hide();
                this.stage = null;
            }

            if ((this.stage == null) && (scene != null)) {
                this.stage = new EmbeddedWindow(this.hostContainer);
            }

            if (this.stage != null) {
                this.stage.setScene(scene);

                if (!this.stage.isShowing()) {
                    this.stage.show();
                }
            }

            this.needsUpdate = true;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting scene!", ex);
        } finally {
            this.lock.release();
        }
    }

    @Override
    public void setScene(final javafx.scene.Scene scene) {
        if (Toolkit.getToolkit().isFxUserThread()) {
            this.setSceneImpl(scene);
            return;
        }

        final CountDownLatch initLatch = new CountDownLatch(1);

        Platform.runLater(() -> {
            this.setSceneImpl(scene);
            initLatch.countDown();
        });

        try {
            initLatch.await();
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while synchronizing with JavaFX thread!", ex);
        }
    }

    protected abstract IntBuffer getImageTransferBuffer();

    protected abstract void applyImageTransfer();

    @Override
    public void onFrameEnd() {
        try {
            this.lock.acquire();

            if (this.needsUpdate) {
                if (this.emScene != null) {
                    final int neededSize = this.info.width * this.info.height * Integer.BYTES;

                    assert neededSize > 0 : "The window raster has an area of 0!";

                    final IntBuffer transferBuffer = this.getImageTransferBuffer();

                    this.emScene.getPixels(transferBuffer, this.info.width, this.info.height);
                    this.applyImageTransfer();

                    this.needsUpdate = false;
                }
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while transferring Scene image!", ex);
        } finally {
            this.lock.release();
        }
    }

    @Override
    public void close() {
        if (this.info != null) {
            this.emScene = null;
            this.emStage = null;
            this.stage = null;
            this.keyMods.clear();
            this.mouseButtons.clear();
            this.info = null;
        }
    }

    @Override
    public void setStageAbsLocation(final int x, final int y) {
        try {
            this.lock.acquire();

            if (this.emStage == null) {
                return;
            }

            if (this.oldEMX == x && this.oldEMY == y) {
                return;
            }

            this.emStage.setLocation(x, y);
            this.oldEMX = x;
            this.oldEMY = y;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting absolute stage position!", ex);
        } finally {
            this.lock.release();
        }
    }

    //<editor-fold defaultstate="collapsed" desc="key events">
    @Override
    public void onKey(final GLFWWindow win, final int key, final int scancode, final GLFWKeyAction action, final Set<GLFWKeyModifier> modifiers) {
        try {
            this.lock.acquire();
            if (!this.focus) {
                return;
            }

            int keyId = -1;
            int mods = 0;

            if (modifiers.contains(GLFWKeyModifier.ALT)) {
                mods |= AbstractEvents.MODIFIER_ALT;
                this.keyMods.add(KeyMod.ALT);
            }

            if (modifiers.contains(GLFWKeyModifier.CONTROL)) {
                mods |= AbstractEvents.MODIFIER_CONTROL;
                this.keyMods.add(KeyMod.CTRL);
            }

            if (modifiers.contains(GLFWKeyModifier.SHIFT)) {
                mods |= AbstractEvents.MODIFIER_SHIFT;
                this.keyMods.add(KeyMod.SHIFT);
            }

            if (modifiers.contains(GLFWKeyModifier.SUPER)) {
                mods |= AbstractEvents.MODIFIER_META;
                this.keyMods.add(KeyMod.META);
            }

            switch (key) {
                case GLFW_KEY_ENTER:
                    keyId = com.sun.glass.events.KeyEvent.VK_ENTER;
                    break;
                case GLFW_KEY_BACKSPACE:
                    keyId = com.sun.glass.events.KeyEvent.VK_BACKSPACE;
                    break;
                case GLFW_KEY_LEFT:
                    keyId = com.sun.glass.events.KeyEvent.VK_LEFT;
                    break;
                case GLFW_KEY_RIGHT:
                    keyId = com.sun.glass.events.KeyEvent.VK_RIGHT;
                    break;
                case GLFW_KEY_UP:
                    keyId = com.sun.glass.events.KeyEvent.VK_UP;
                    break;
                case GLFW_KEY_DOWN:
                    keyId = com.sun.glass.events.KeyEvent.VK_DOWN;
                    break;
                case GLFW_KEY_TAB:
                    keyId = com.sun.glass.events.KeyEvent.VK_TAB;
                    break;
                case GLFW_KEY_DELETE:
                    keyId = com.sun.glass.events.KeyEvent.VK_DELETE;
                    break;
                case GLFW_KEY_HOME:
                    keyId = com.sun.glass.events.KeyEvent.VK_HOME;
                    break;
                case GLFW_KEY_END:
                    keyId = com.sun.glass.events.KeyEvent.VK_END;
                    break;
                case GLFW_KEY_PAGE_UP:
                    keyId = com.sun.glass.events.KeyEvent.VK_PAGE_UP;
                    break;
                case GLFW_KEY_PAGE_DOWN:
                    keyId = com.sun.glass.events.KeyEvent.VK_PAGE_DOWN;
                    break;
                case GLFW_KEY_INSERT:
                    keyId = com.sun.glass.events.KeyEvent.VK_INSERT;
                    break;
                case GLFW_KEY_ESCAPE:
                    keyId = com.sun.glass.events.KeyEvent.VK_ESCAPE;
                    break;
                case GLFW_KEY_CAPS_LOCK:
                    keyId = com.sun.glass.events.KeyEvent.VK_CAPS_LOCK;
                    break;
                case GLFW_KEY_PAUSE:
                    keyId = com.sun.glass.events.KeyEvent.VK_PAUSE;
                    break;
                case GLFW_KEY_PRINT_SCREEN:
                    keyId = com.sun.glass.events.KeyEvent.VK_PRINTSCREEN;
                    break;
                case GLFW_KEY_LEFT_SHIFT:
                case GLFW_KEY_RIGHT_SHIFT:
                    keyId = com.sun.glass.events.KeyEvent.VK_SHIFT;
                    break;
                case GLFW_KEY_LEFT_CONTROL:
                case GLFW_KEY_RIGHT_CONTROL:
                    keyId = com.sun.glass.events.KeyEvent.VK_CONTROL;
                    break;
                case GLFW_KEY_LEFT_ALT:
                case GLFW_KEY_RIGHT_ALT:
                    keyId = com.sun.glass.events.KeyEvent.VK_ALT;
                    break;
                case GLFW_KEY_NUM_LOCK:
                    keyId = com.sun.glass.events.KeyEvent.VK_NUM_LOCK;
                    break;
                case GLFW_KEY_SCROLL_LOCK:
                    keyId = com.sun.glass.events.KeyEvent.VK_SCROLL_LOCK;
                    break;
                case 348:
                    keyId = com.sun.glass.events.KeyEvent.VK_CONTEXT_MENU;
                    break;
                default:
                    if (key >= GLFW_KEY_KP_0 && key <= GLFW_KEY_KP_9) {
                        // numpad keys
                        keyId = com.sun.glass.events.KeyEvent.VK_NUMPAD0 + (key - GLFW_KEY_KP_0);
                    } else if (key >= GLFW_KEY_F1 && key <= GLFW_KEY_F25) {
                        // F1 -> F25
                        keyId = com.sun.glass.events.KeyEvent.VK_F1 + (key - GLFW_KEY_F1);
                    } else if (key > 0) {
                        keyId = key; // yolo -\_0_0_/-
                    }
            }

            switch (action) {
                case PRESS:
                case REPEAT:
                    if (keyId > -1) {
                        this.emScene.keyEvent(AbstractEvents.KEYEVENT_PRESSED, keyId, new char[]{}, mods);
                    }
                    break;
                case RELEASE:
                    if (keyId > -1) {
                        if (this.keyMods.contains(KeyMod.SHIFT) && keyId == com.sun.glass.events.KeyEvent.VK_F10) {
                            contextMenuHandler.fireContextMenuFromKeyboard();
                        }

                        if (keyId == com.sun.glass.events.KeyEvent.VK_CONTEXT_MENU) {
                            contextMenuHandler.fireContextMenuFromKeyboard();
                        }
                        this.emScene.keyEvent(AbstractEvents.KEYEVENT_RELEASED, keyId, new char[]{}, mods);
                    }
                    break;
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while processing key event!", ex);
        } finally {
            this.lock.release();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="mouse button events">
    @Override
    public void onMouseButton(final GLFWWindow win, final int btn, final GLFWMouseButtonAction action, final Set<GLFWKeyModifier> modifiers) {
        try {
            this.lock.acquire();
            int btnId = 0;

            if (action == GLFWMouseButtonAction.PRESS) {
                switch (btn) {
                    case 0:
                        btnId = AbstractEvents.MOUSEEVENT_PRIMARY_BUTTON;
                        this.mouseButtons.add(MouseButton.PRIMARY);
                        break;
                    case 1:
                        btnId = AbstractEvents.MOUSEEVENT_SECONDARY_BUTTON;
                        this.mouseButtons.add(MouseButton.SECONDARY);
                        break;
                    case 2:
                        btnId = AbstractEvents.MOUSEEVENT_MIDDLE_BUTTON;
                        this.mouseButtons.add(MouseButton.MIDDLE);
                        break;
                    default:
                        LOGGER.debug("Captured unbound mouse button press: {}", btn);
                        break;
                }

                this.emScene.mouseEvent(
                        AbstractEvents.MOUSEEVENT_PRESSED, btnId,
                        this.mouseButtons.contains(MouseButton.PRIMARY), this.mouseButtons.contains(MouseButton.MIDDLE), this.mouseButtons.contains(MouseButton.SECONDARY),
                        this.mouseX, this.mouseY, this.absMouseX, this.absMouseY,
                        this.keyMods.contains(KeyMod.SHIFT), this.keyMods.contains(KeyMod.CTRL), this.keyMods.contains(KeyMod.ALT), this.keyMods.contains(KeyMod.META),
                        0, false);
            } else if (action == GLFWMouseButtonAction.RELEASE) {
                switch (btn) {
                    case 0:
                        btnId = AbstractEvents.MOUSEEVENT_PRIMARY_BUTTON;
                        this.mouseButtons.remove(MouseButton.PRIMARY);
                        break;
                    case 1:
                        btnId = AbstractEvents.MOUSEEVENT_SECONDARY_BUTTON;
                        this.mouseButtons.remove(MouseButton.SECONDARY);
                        break;
                    case 2:
                        btnId = AbstractEvents.MOUSEEVENT_MIDDLE_BUTTON;
                        this.mouseButtons.remove(MouseButton.MIDDLE);
                        break;
                    default:
                        LOGGER.debug("Captured unbound mouse button release: {}", btn);
                }

                this.emScene.mouseEvent(
                        AbstractEvents.MOUSEEVENT_RELEASED, btnId,
                        this.mouseButtons.contains(MouseButton.PRIMARY), this.mouseButtons.contains(MouseButton.MIDDLE), this.mouseButtons.contains(MouseButton.SECONDARY),
                        this.mouseX, this.mouseY, this.absMouseX, this.absMouseY,
                        this.keyMods.contains(KeyMod.SHIFT), this.keyMods.contains(KeyMod.CTRL), this.keyMods.contains(KeyMod.ALT), this.keyMods.contains(KeyMod.META),
                        0, false);

                if (this.mouseButtons.contains(MouseButton.PRIMARY)) {
                    this.dndHandler.updateMouseReleased(mouseX, mouseY, absMouseX, absMouseY);
                }

                if (this.mouseButtons.contains(MouseButton.SECONDARY)) {
                    this.contextMenuHandler.fireContextMenuFromMouse(mouseX, mouseY, absMouseX, absMouseY);
                }
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while processing mouse button event!", ex);
        } finally {
            this.lock.release();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="mouse move events">
    @Override
    public void onMouseMove(final GLFWWindow window, final double x, final double y) {
        try {
            this.lock.acquire();
            if (this.emScene == null) {
                return;
            }

            this.mouseX = (int) x;
            this.mouseY = (int) y;
            this.absMouseX = this.mouseX + this.oldEMX;
            this.absMouseY = this.mouseY + this.oldEMY;

            if (this.mouseButtons.contains(MouseButton.PRIMARY)) {
                this.emScene.mouseEvent(
                        AbstractEvents.MOUSEEVENT_DRAGGED, AbstractEvents.MOUSEEVENT_PRIMARY_BUTTON,
                        this.mouseButtons.contains(MouseButton.PRIMARY), this.mouseButtons.contains(MouseButton.MIDDLE), this.mouseButtons.contains(MouseButton.SECONDARY),
                        this.mouseX, this.mouseY, this.absMouseX, this.absMouseY,
                        this.keyMods.contains(KeyMod.SHIFT), this.keyMods.contains(KeyMod.CTRL), this.keyMods.contains(KeyMod.ALT), this.keyMods.contains(KeyMod.META),
                        0, false);
            } else if (this.mouseButtons.contains(MouseButton.SECONDARY)) {
                this.emScene.mouseEvent(
                        AbstractEvents.MOUSEEVENT_DRAGGED, AbstractEvents.MOUSEEVENT_SECONDARY_BUTTON,
                        this.mouseButtons.contains(MouseButton.PRIMARY), this.mouseButtons.contains(MouseButton.MIDDLE), this.mouseButtons.contains(MouseButton.SECONDARY),
                        this.mouseX, this.mouseY, this.absMouseX, this.absMouseY,
                        this.keyMods.contains(KeyMod.SHIFT), this.keyMods.contains(KeyMod.CTRL), this.keyMods.contains(KeyMod.ALT), this.keyMods.contains(KeyMod.META),
                        0, false);
            } else if (this.mouseButtons.contains(MouseButton.MIDDLE)) {
                this.emScene.mouseEvent(
                        AbstractEvents.MOUSEEVENT_DRAGGED, AbstractEvents.MOUSEEVENT_MIDDLE_BUTTON,
                        this.mouseButtons.contains(MouseButton.PRIMARY), this.mouseButtons.contains(MouseButton.MIDDLE), this.mouseButtons.contains(MouseButton.SECONDARY),
                        this.mouseX, this.mouseY, this.absMouseX, this.absMouseY,
                        this.keyMods.contains(KeyMod.SHIFT), this.keyMods.contains(KeyMod.CTRL), this.keyMods.contains(KeyMod.ALT), this.keyMods.contains(KeyMod.META),
                        0, false);
            } else {
                this.emScene.mouseEvent(
                        AbstractEvents.MOUSEEVENT_MOVED, AbstractEvents.MOUSEEVENT_NONE_BUTTON,
                        this.mouseButtons.contains(MouseButton.PRIMARY), this.mouseButtons.contains(MouseButton.MIDDLE), this.mouseButtons.contains(MouseButton.SECONDARY),
                        this.mouseX, this.mouseY, this.absMouseX, this.absMouseY,
                        this.keyMods.contains(KeyMod.SHIFT), this.keyMods.contains(KeyMod.CTRL), this.keyMods.contains(KeyMod.ALT), this.keyMods.contains(KeyMod.META),
                        0, false);
            }

            this.dndHandler.updateMousePosition(mouseX, mouseY, absMouseX, absMouseY);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while processing mouse move event!", ex);
        } finally {
            this.lock.release();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="type event">
    @Override
    public void onChar(final GLFWWindow window, final int codepoint) {
        try {
            this.lock.acquire();

            if (!this.focus) {
                return;
            }

            if (this.emScene == null) {
                return;
            }

            final int mods = this.keyMods.contains(KeyMod.CTRL) ? AbstractEvents.MODIFIER_CONTROL : 0;

            this.emScene.keyEvent(AbstractEvents.KEYEVENT_TYPED, com.sun.glass.events.KeyEvent.VK_UNDEFINED, new char[]{(char) codepoint}, mods);

        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while processing key type event!", ex);
        } finally {
            this.lock.release();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="scroll event">
    @Override
    public void onScroll(final GLFWWindow window, final double dx, final double dy) {
        try {
            this.lock.acquire();

            if (!this.focus) {
                return;
            }

            if (this.emScene == null) {
                return;
            }

            this.emScene.mouseEvent(
                    AbstractEvents.MOUSEEVENT_WHEEL, AbstractEvents.MOUSEEVENT_NONE_BUTTON,
                    this.mouseButtons.contains(MouseButton.PRIMARY), this.mouseButtons.contains(MouseButton.MIDDLE), this.mouseButtons.contains(MouseButton.SECONDARY),
                    mouseX, mouseY, absMouseX, absMouseY,
                    this.keyMods.contains(KeyMod.SHIFT), this.keyMods.contains(KeyMod.CTRL), this.keyMods.contains(KeyMod.ALT), this.keyMods.contains(KeyMod.META),
                    0, false);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while processing mouse scroll event!", ex);
        } finally {
            this.lock.release();
        }
    }
}
