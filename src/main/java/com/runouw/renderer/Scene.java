/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 *
 * @author zmichaels
 */
public interface Scene extends AutoCloseable {
    Optional<SoundLayer> getSoundLayer(final String lookup);
    
    Set<String> getSoundLayerNames();        
    
    Optional<JFXLayer> getJFXLayer(final String lookup);
    
    Set<String> getJFXLayerNames();
    
    List<Layer> getLayersOrdered();
    
    void onFrameStart();
    
    void onFrameEnd();        
    
    SceneInfo getInfo();
    
    Optional<FontLayer> getFontLayer(final String lookup);
    
    Set<String> getFontLayerNames();
    
    Optional<ImageLayer> getImageLayer(final String lookup);
    
    Set<String> getImageLayerNames();
    
    Optional<SketchLayer> getSketchLayer(final String lookup);        
    
    Set<String> getSketchLayerNames();
    
    Optional<SpriteLayer> getSpriteLayer(final String lookup);
    
    Set<String> getSpriteLayerNames();
    
    Optional<TileLayer> getTileLayer(final String lookup);
    
    Set<String> getTileLayerNames();
    
    List<String> getComments();
    
    @Override
    void close();
}
