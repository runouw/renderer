/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import java.net.URL;

/**
 *
 * @author zmichaels
 */
public final class FontLayerInfo implements LayerInfo {
    public final long maxCharacters;
    public final char firstCharacter;
    public final int characterCount;
    public final float fontPointSize;
    public final URL fontFile;
    
    public FontLayerInfo(final URL fontFile, final long maxCharacters, final char firstCharacter, final int characterCount, final float fontPointSize) {
        this.maxCharacters = maxCharacters;
        this.fontFile = fontFile;
        this.firstCharacter = firstCharacter;
        this.characterCount = characterCount;
        this.fontPointSize = fontPointSize;
    }
    
    public FontLayerInfo() {
        this(null, 4096, ' ', 96, 16F);
    }
    
    public FontLayerInfo withFirstCharacter(final char firstCharacter) {
        return new FontLayerInfo(fontFile, maxCharacters, firstCharacter, characterCount, fontPointSize);
    }
    
    public FontLayerInfo withCharacterCount(final int characterCount) {
        return new FontLayerInfo(fontFile, maxCharacters, firstCharacter, characterCount, fontPointSize);
    }
    
    public FontLayerInfo withLatinRange() {
        return withFirstCharacter(' ').withCharacterCount(96);
    }
    
    //TODO: other locales should probably be supported by using texture arrays
    
    public FontLayerInfo withFontFile(final URL fontFile) {
        return new FontLayerInfo(fontFile, maxCharacters, firstCharacter, characterCount, fontPointSize);
    }
    
    public FontLayerInfo withMaxCharacters(final long maxCharacters) {
        return new FontLayerInfo(fontFile, maxCharacters, firstCharacter, characterCount, fontPointSize);
    }
    
    public FontLayerInfo withFontPointSize(final float fontPointSize) {
        return new FontLayerInfo(fontFile, maxCharacters, firstCharacter, characterCount, fontPointSize);
    }
}
