/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.sun.javafx.tk.Toolkit;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.PickResult;

/**
 *
 * @author zmichaels
 */
final class JFXContextMenuHandler {
    private final WeakReference<JFXLayer> stage;
    
    JFXContextMenuHandler(final JFXLayer stage) {
        this.stage = new WeakReference<>(stage);
    }
    
    void fireContextMenuFromKeyboard() {
        if (Toolkit.getToolkit().isFxUserThread()) {
            this.stage.get().getScene()
                    .map(javafx.scene.Scene::getFocusOwner)
                    .ifPresent(owner -> fireContextMenuEventFromKeyboard(owner));
        } else {
            final CountDownLatch countDown = new CountDownLatch(1);
            
            Platform.runLater(() -> {
                this.stage.get().getScene()
                    .map(javafx.scene.Scene::getFocusOwner)
                    .ifPresent(owner -> fireContextMenuEventFromKeyboard(owner));
                countDown.countDown();
            });
            
            try {
                countDown.await();
            } catch (InterruptedException ex) {
                throw new RuntimeException("Interrupted while synchronizing with JavaFX thread!", ex);
            }
        }
    }
    
    private void fireContextMenuEventFromKeyboard(final Node node) {
        final Bounds tabBounds = node.getBoundsInLocal();
        final double centerX = tabBounds.getMinX() + tabBounds.getWidth() * 0.5;
        final double centerY = tabBounds.getMinY() + tabBounds.getHeight() * 0.5;
        
        final Point2D pos = node.localToScreen(centerX, centerY);
        final double x = pos.getX();
        final double y = pos.getY();
        
        final Event contextMenuEvent = new ContextMenuEvent(ContextMenuEvent.CONTEXT_MENU_REQUESTED, centerX, centerY, x, y, true, new PickResult(node, x, y));
        
        Event.fireEvent(node, contextMenuEvent);
    }
    
    void fireContextMenuFromMouse(final double mouseX, final double mouseY, final double absMouseX, final double absMouseY) {
        this.stage.get().getRootNode()
                .ifPresent(root -> forAllUnder(root, mouseX, mouseY, node -> fireContextMenuEventFromMouse(node, mouseX, mouseY, absMouseX, absMouseY)));
    }
    
    private void fireContextMenuEventFromMouse(final Node node, final double x, final double y, final double screenX, final double screenY) {
        final Event contextMenuEvent = new ContextMenuEvent(ContextMenuEvent.CONTEXT_MENU_REQUESTED, x, y, screenX, screenY, false, new PickResult(node, x, y));
        
        Event.fireEvent(node, contextMenuEvent);
    }
    
    void forAllUnder(final Node node, final double sceneX, final double sceneY, final Consumer<Node> onUnder) {
        Point2D p = node.sceneToLocal(sceneX, sceneY, true);
        
        if (!node.contains(p)) {
            return;
        }
        
        if (node instanceof Parent) {
            Node bestMatchingChild = null;
            final List<Node> children = ((Parent) node).getChildrenUnmodifiable();
            
            for (int i = children.size() - 1; i >= 0; i--) {
                final Node child = children.get(i);
                
                p = child.sceneToLocal(sceneX, sceneY, true);
                
                if (child.isVisible() && !child.isMouseTransparent() && child.contains(p)) {
                    bestMatchingChild = child;
                    break;
                }
            }
            
            if (bestMatchingChild != null) {
                onUnder.accept(bestMatchingChild);
                forAllUnder(bestMatchingChild, sceneX, sceneY, onUnder);
            }
        }
    }
}
