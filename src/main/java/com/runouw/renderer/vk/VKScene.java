/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.vk.VKCommandBuffer;
import com.longlinkislong.gloop2.vk.VKCommandBufferBeginInfo;
import com.longlinkislong.gloop2.vk.VKContext;
import com.longlinkislong.gloop2.vk.VKDescriptorPool;
import com.longlinkislong.gloop2.vk.VKDescriptorPoolInfo;
import com.longlinkislong.gloop2.vk.VKDescriptorPoolSize;
import com.longlinkislong.gloop2.vk.VKDescriptorType;
import com.longlinkislong.gloop2.vk.VKFence;
import com.longlinkislong.gloop2.vk.VKGraphicsPipeline;
import com.longlinkislong.gloop2.vk.VKQueue;
import com.longlinkislong.gloop2.vk.VKScissor;
import com.longlinkislong.gloop2.vk.VKSubpassContents;
import com.longlinkislong.gloop2.vk.VKViewport;
import com.runouw.renderer.FontLayer;
import com.runouw.renderer.FontLayerInfo;
import com.runouw.renderer.ImageLayer;
import com.runouw.renderer.ImageLayerInfo;
import com.runouw.renderer.JFXLayer;
import com.runouw.renderer.JFXLayerInfo;
import com.runouw.renderer.Layer;
import com.runouw.renderer.Scene;
import com.runouw.renderer.SceneInfo;
import com.runouw.renderer.SketchLayer;
import com.runouw.renderer.SoundLayer;
import com.runouw.renderer.SpriteLayer;
import com.runouw.renderer.TileLayer;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class VKScene implements Scene {    
    private static final Logger LOGGER = LoggerFactory.getLogger(VKScene.class);
    
    private final List<String> comments = new ArrayList<>();
    private final Map<String, VKJFXLayer> jfxLayers = new HashMap<>();
    private final Map<String, VKFontLayer> fontLayers = new HashMap<>();
    private final Map<String, VKImageLayer> imageLayers = new HashMap<>();
    private final List<VKLayer> layers = new ArrayList<>();
    private final SceneInfo info;
    private final VKViewport viewport;
    private final VKScissor scissor;
    private final VKContext ctx;
    private final Set<VKCommandBuffer> drawCommandBuffers = new HashSet<>();
    private final VKDescriptorPool descriptorPool;
    private final VKQueue stagingQueue;
    private final VKCommandBuffer cmdStaging;
    private final VKFence uploadsComplete;
    
    public VKScene(final VKContext ctx, final SceneInfo info) throws IOException {
        this.info = info;
        this.viewport = new VKViewport(info.viewport.x, info.viewport.y, info.viewport.width, info.viewport.height, info.viewport.near, info.viewport.far);
        this.scissor = new VKScissor(info.scissor.x, info.scissor.y, info.scissor.width, info.scissor.height);
        this.ctx = ctx;
        this.uploadsComplete = new VKFence(ctx, Collections.emptySet());        
        final VKConstants constants = VKConstants.getConstants(ctx);
        
        constants.initCmdPool.reset(true);
        constants.stagingCmdPool.reset(true);
        
        final Queue<SceneInfo.LayerOrderInfo> orderedLayers = new PriorityQueue<>(info.layerInfos);
        
        while (!orderedLayers.isEmpty()) {
            final SceneInfo.LayerOrderInfo orderInfo = orderedLayers.poll();
            
            if (orderInfo.info == null) {
                this.comments.add(orderInfo.lookup);
            } else if (orderInfo.info instanceof FontLayerInfo) {
                final VKFontLayer fontLayer = new VKFontLayer(ctx, (FontLayerInfo) orderInfo.info);
                
                this.fontLayers.put(orderInfo.lookup, fontLayer);
                this.layers.add(fontLayer);
            } else if (orderInfo.info instanceof ImageLayerInfo) {
                final VKImageLayer imageLayer=  new VKImageLayer(ctx, (ImageLayerInfo) orderInfo.info);
                
                this.imageLayers.put(orderInfo.lookup, imageLayer);
                this.layers.add(imageLayer);
            } else if (orderInfo.info instanceof JFXLayerInfo) {
                final VKJFXLayer jfxLayer = new VKJFXLayer(ctx, (JFXLayerInfo) orderInfo.info);
                
                this.jfxLayers.put(orderInfo.lookup, jfxLayer);
                this.layers.add(jfxLayer);
            } else {
                throw new UnsupportedOperationException("Layer type: " + orderInfo.info.getClass().getSimpleName() + " is not supported!");
            }
        }
        
        final int neededSets = this.layers.stream()
                .mapToInt(VKLayer::getNeededDescriptorSets)
                .sum();
        
        final int neededUniformBuffers = this.layers.stream()
                .mapToInt(VKLayer::getNeededUniformBufferDescriptors)
                .sum();
        
        final int neededImageSamplers = this.layers.stream()
                .mapToInt(VKLayer::getNeededImageSamplerDescriptors)
                .sum();
        
        this.descriptorPool = new VKDescriptorPoolInfo()
                .withMaxSets(neededSets)
                .withPoolSizes(
                        new VKDescriptorPoolSize()
                                .withType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                                .withDescriptorCount(neededImageSamplers),
                        new VKDescriptorPoolSize()
                                .withType(VKDescriptorType.UNIFORM_BUFFER)
                                .withDescriptorCount(neededUniformBuffers)
                )
                .create(ctx);
        
        this.layers.forEach(layer -> layer.initDescriptors(ctx, descriptorPool));
        this.stagingQueue = constants.stagingQueue;        
        this.cmdStaging = constants.stagingCmdPool.newPrimaryCommandBuffer();
        this.ctx.mainQueue.waitIdle();
        
    }
    
    @Override
    public void onFrameStart() {
        this.layers.forEach(VKLayer::onFrameStart);
    }
    
    @Override
    public void onFrameEnd() {
        final VKCommandBuffer cmdDraw = this.ctx.getPrimaryCommandBuffer();       
        VKCommandBuffer cmdUpdates = null;
        
        for (VKLayer layer : this.layers) {
            layer.onFrameEnd();
            
            if (layer.hasUpdates()) {
                if (cmdUpdates == null) {                                        
                    cmdUpdates = this.cmdStaging;
                    cmdUpdates.begin(VKCommandBufferBeginInfo.PRIMARY_ONE_TIME_USE);
                }
                
                layer.recordUpdates(ctx, cmdUpdates);
            }
        }
        
        if (cmdUpdates != null) {                                    
            cmdUpdates.end();
            this.stagingQueue.submit(VKQueue.SubmitInfo.wrap(cmdUpdates), this.uploadsComplete);
            
            try {                
                this.uploadsComplete.waitFor();
                this.uploadsComplete.reset();
            } catch (InterruptedException ex) {
                throw new RuntimeException("Interrupted while waiting on uploads to process!", ex);
            }
        }
        
        if (this.drawCommandBuffers.add(cmdDraw)) {
            final ByteBuffer[] pClearValues = {
                Tools.clearColor(this.info.clearColor.red, this.info.clearColor.green, this.info.clearColor.blue, this.info.clearColor.alpha),
                Tools.clearDepthStencil(this.info.clearDepth, 0)};
            
            cmdDraw.begin(new VKCommandBufferBeginInfo());
            cmdDraw.beginRenderPass(ctx.getFramebuffer(), VKConstants.getConstants(ctx).presentRenderpass, VKSubpassContents.INLINE, scissor, pClearValues);
            
            cmdDraw.setViewport(viewport);
            cmdDraw.setScissor(scissor);
            
            VKGraphicsPipeline currentPipeline = null;
            
            for (VKLayer layer : this.layers) {
                VKGraphicsPipeline layerPipeline = layer.getPipeline(this.ctx);
                
                if (currentPipeline != layerPipeline) {
                    currentPipeline = layerPipeline;
                    cmdDraw.bindPipeline(layerPipeline);
                }
                
                layer.recordDraw(ctx, cmdDraw);
            }
            
            cmdDraw.endRenderPass();
            cmdDraw.end();
        }
    }
    
    @Override
    public SceneInfo getInfo() {
        return this.info;
    }
    
    @Override
    public Optional<FontLayer> getFontLayer(String lookup) {
        return Optional.ofNullable(this.fontLayers.get(lookup));
    }
    
    @Override
    public Set<String> getFontLayerNames() {
        return Collections.unmodifiableSet(this.fontLayers.keySet());
    }
    
    @Override
    public Optional<ImageLayer> getImageLayer(String lookup) {
        return Optional.ofNullable(this.imageLayers.get(lookup));
    }
    
    @Override
    public Set<String> getImageLayerNames() {
        return Collections.unmodifiableSet(this.imageLayers.keySet());
    }
    
    @Override
    public Optional<SketchLayer> getSketchLayer(String lookup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Set<String> getSketchLayerNames() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Optional<SpriteLayer> getSpriteLayer(String lookup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Set<String> getSpriteLayerNames() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Optional<TileLayer> getTileLayer(String lookup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Set<String> getTileLayerNames() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public List<String> getComments() {
        return Collections.unmodifiableList(this.comments);
    }
    
    @Override
    public void close() {
        this.stagingQueue.waitIdle();
        this.ctx.mainQueue.waitIdle();
        this.ctx.presentQueue.waitIdle();        
        
        this.uploadsComplete.close();
        
        for (VKLayer layer : this.layers) {
            layer.close();
        }
        
        this.drawCommandBuffers.clear();        
        
        this.layers.clear();
        this.fontLayers.clear();
        this.imageLayers.clear();
        this.descriptorPool.close();
    }    

    @Override
    public Optional<JFXLayer> getJFXLayer(String lookup) {
        return Optional.ofNullable(this.jfxLayers.get(lookup));
    }

    @Override
    public Set<String> getJFXLayerNames() {
        return Collections.unmodifiableSet(this.jfxLayers.keySet());
    }

    @Override
    public List<Layer> getLayersOrdered() {
        return Collections.unmodifiableList(this.layers);
    }

    @Override
    public Optional<SoundLayer> getSoundLayer(String lookup) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<String> getSoundLayerNames() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
