/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

import com.runouw.renderer.ImageView;
import com.runouw.renderer.SpriteInfo;
import com.runouw.renderer.SpriteSlot;
import java.nio.ByteBuffer;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public final class VKSpriteSlot implements SpriteSlot {
    public static final int BYTES = 16 * Float.BYTES;
    private final PointerBuffer pptr;
    private final int offset;
    
    VKSpriteSlot(final PointerBuffer pptr, final int offset) {
        this.pptr = pptr;
        this.offset = offset;
    }
    
    @Override
    public SpriteSlot setSprite(SpriteInfo info) {
        final ByteBuffer data = pptr.getByteBuffer(this.offset, BYTES);
        
        data.putFloat(info.positionX).putFloat(info.positionY)
                .putFloat(info.offsetX).putFloat(info.offsetY)
                .putFloat(info.scaleX).putFloat(info.scaleY)
                .putFloat(info.skewX).putFloat(info.skewY)
                .putFloat(info.rotation)
                .putFloat(info.spriteView.s1).putFloat(info.spriteView.t1).putFloat(info.spriteView.index)
                .putFloat(info.maskView.s1).putFloat(info.maskView.t1).putFloat(info.maskView.index)
                .putInt(info.colorTransformId);
        
        return this;
    }

    @Override
    public SpriteSlot setPosition(float x, float y) {
        final long ptr = pptr.get(0) + offset;
        
        MemoryUtil.memPutFloat(ptr, x);
        MemoryUtil.memPutFloat(ptr + Float.BYTES, y);
        
        return this;
    }

    @Override
    public SpriteSlot setOffset(float x, float y) {
        final long ptr = pptr.get(0) + offset + 2 * Float.BYTES;
        
        MemoryUtil.memPutFloat(ptr, x);
        MemoryUtil.memPutFloat(ptr + Float.BYTES, y);
        
        return this;
    }

    @Override
    public SpriteSlot setScale(float x, float y) {
        final long ptr = pptr.get(0) + offset + 4 * Float.BYTES;
        
        MemoryUtil.memPutFloat(ptr, x);
        MemoryUtil.memPutFloat(ptr + Float.BYTES, y);
        
        return this;
    }

    @Override
    public SpriteSlot setSkew(float x, float y) {
        final long ptr = pptr.get(0) + offset + 6 * Float.BYTES;
        
        MemoryUtil.memPutFloat(ptr, x);
        MemoryUtil.memPutFloat(ptr + Float.BYTES, y);
        
        return this;
    }

    @Override
    public SpriteSlot setRotation(float a) {
        final long ptr = pptr.get(0) + offset + 8 * Float.BYTES;
        
        MemoryUtil.memPutFloat(ptr, a);
        
        return this;
    }

    @Override
    public SpriteSlot setSpriteView(ImageView view) {
        final long ptr = pptr.get(0) + offset + 9 * Float.BYTES;
        
        MemoryUtil.memPutFloat(ptr, view.s1);
        MemoryUtil.memPutFloat(ptr + Float.BYTES, view.t1);
        MemoryUtil.memPutFloat(ptr + 2 * Float.BYTES, view.index);
        
        return this;
    }

    @Override
    public SpriteSlot setMaskView(ImageView view) {
        final long ptr = pptr.get(0) + offset + 12 * Float.BYTES;
        
        MemoryUtil.memPutFloat(ptr, view.s1);
        MemoryUtil.memPutFloat(ptr + Float.BYTES, view.t1);
        MemoryUtil.memPutFloat(ptr + 2 * Float.BYTES, view.index);
        
        return this;
    }

    @Override
    public SpriteSlot setColorTransformSlot(int ctoff) {
        final long ptr = pptr.get(0) + offset + 15 * Float.BYTES;
        
        MemoryUtil.memPutInt(ptr, ctoff);
        
        return this;
    }

    @Override
    public SpriteSlot clear() {
        final long ptr = pptr.get(0) + offset;
        
        for (int i = 0; i < 16; i++) {
            MemoryUtil.memPutInt(ptr + i * Integer.BYTES, 0);
        }
        
        return this;
    }
    
}
