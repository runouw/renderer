/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

/**
 *
 * @author zmichaels
 */
final class VKUtil {
    static float[] matrixFlip(final float[] mat4) {
        final float[] copy = new float[16];
        
        System.arraycopy(mat4, 0, copy, 0, 16);
        
        copy[1] = -copy[1];
        copy[5] = -copy[5];
        copy[9] = -copy[9];
        copy[13] = -copy[13];
        
        return copy;
    }
}
