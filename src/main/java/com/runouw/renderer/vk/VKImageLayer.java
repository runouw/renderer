/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

import com.longlinkislong.gloop2.vk.VKAccess;
import com.longlinkislong.gloop2.vk.VKBufferCopy;
import com.longlinkislong.gloop2.vk.VKBufferImageCopy;
import com.longlinkislong.gloop2.vk.VKBufferInfo;
import com.longlinkislong.gloop2.vk.VKCommandBuffer;
import com.longlinkislong.gloop2.vk.VKCommandBufferBeginInfo;
import com.longlinkislong.gloop2.vk.VKContext;
import com.longlinkislong.gloop2.vk.VKDescriptorBufferInfo;
import com.longlinkislong.gloop2.vk.VKDescriptorImageInfo;
import com.longlinkislong.gloop2.vk.VKDescriptorPool;
import com.longlinkislong.gloop2.vk.VKDescriptorSet;
import com.longlinkislong.gloop2.vk.VKDescriptorType;
import com.longlinkislong.gloop2.vk.VKFence;
import com.longlinkislong.gloop2.vk.VKFilter;
import com.longlinkislong.gloop2.vk.VKFormat;
import com.longlinkislong.gloop2.vk.VKGraphicsPipeline;
import com.longlinkislong.gloop2.vk.VKImageInfo;
import com.longlinkislong.gloop2.vk.VKImageLayout;
import com.longlinkislong.gloop2.vk.VKImageMemoryBarrier;
import com.longlinkislong.gloop2.vk.VKImageTiling;
import com.longlinkislong.gloop2.vk.VKImageType;
import com.longlinkislong.gloop2.vk.VKImageUsage;
import com.longlinkislong.gloop2.vk.VKImageView;
import com.longlinkislong.gloop2.vk.VKManagedBuffer;
import com.longlinkislong.gloop2.vk.VKManagedImage;
import com.longlinkislong.gloop2.vk.VKMemoryProperty;
import com.longlinkislong.gloop2.vk.VKPipelineBarrier;
import com.longlinkislong.gloop2.vk.VKPipelineBindPoint;
import com.longlinkislong.gloop2.vk.VKPipelineStage;
import com.longlinkislong.gloop2.vk.VKQueue;
import com.longlinkislong.gloop2.vk.VKSampler;
import com.longlinkislong.gloop2.vk.VKSamplerAddressMode;
import com.longlinkislong.gloop2.vk.VKSamplerInfo;
import com.longlinkislong.gloop2.vk.VKShaderStage;
import com.longlinkislong.gloop2.vk.VKSharingMode;
import com.longlinkislong.gloop2.vk.VKWriteDescriptorSet;
import com.runouw.renderer.ColorTransform;
import com.runouw.renderer.ImageFilter;
import com.runouw.renderer.ImageLayer;
import com.runouw.renderer.ImageLayerInfo;
import com.runouw.renderer.ImageScrollType;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Semaphore;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public class VKImageLayer implements ImageLayer, VKLayer {

    private static final int OFF_IMAGE_SCROLL = 0;
    private static final int OFF_MASK_SCROLL = 4 * Float.BYTES;
    private static final int OFF_COLOR_TRANSFORM = 8 * Float.BYTES;
    private static final int SIZE_IMAGE_SCROLL = 4 * Float.BYTES;
    private static final int SIZE_MASK_SCROLL = 4 * Float.BYTES;
    private static final int SIZE_COLOR_TRANSFORM = 12 * Float.BYTES;
    private static final int SIZE_UNIFORM = 20 * Float.BYTES;
    private ImageLayerInfo info;
    private final VKManagedImage image;
    private final VKManagedImage mask;
    private final VKImageView imageView;
    private final VKImageView maskView;
    private final VKSampler imageSampler;
    private final VKSampler maskSampler;
    private final VKManagedBuffer uniforms;
    private final VKManagedBuffer uniformStaging;
    private final Semaphore writeLock = new Semaphore(1);
    private VKManagedBuffer.MappedBuffer stagingMapping;
    private FloatBuffer uniformData;
    private VKDescriptorSet descriptorSet;
    private boolean imageScrollChanged = false;
    private boolean maskScrollChanged = false;
    private boolean ctChanged = false;
    private final ByteBuffer pushConstants;
    public final boolean supportsMask;
    public final boolean supportsColorTransform;

    private static VKSamplerAddressMode addressMode(final ImageScrollType scrollType) {
        switch (scrollType) {
            case STATIC:
                return VKSamplerAddressMode.CLAMP_TO_EDGE;
            case REPEAT:
                return VKSamplerAddressMode.REPEAT;
            case MIRRORED_REPEAT:
                return VKSamplerAddressMode.MIRRORED_REPEAT;
            default:
                throw new UnsupportedOperationException("ImageScrollType: [" + scrollType + "] is not supported!");
        }
    }

    private VKSampler createSampler(final VKContext ctx, final ImageLayerInfo.ImageSublayerInfo info) {
        if (info == null) {
            return null;
        }

        return new VKSamplerInfo()
                .withAddressModeU(addressMode(info.hScroll))
                .withAddressModeV(addressMode(info.vScroll))
                .withAnisotropyEnable(false)
                .withMaxAnisotropy(1.0F)
                .withMagFilter(info.magFilter == ImageFilter.NEAREST ? VKFilter.NEAREST : VKFilter.LINEAR)
                .withMinFilter(info.minFilter == ImageFilter.NEAREST ? VKFilter.NEAREST : VKFilter.LINEAR)
                .create(ctx);

    }

    public VKImageLayer(final VKContext ctx, final ImageLayerInfo info) {
        this.info = info;
        this.supportsColorTransform = info.supportsColorTransform;
        this.supportsMask = info.mask != null;
        this.pushConstants = MemoryUtil.memCalloc(2 * Integer.BYTES);

        if (this.supportsMask) {
            this.pushConstants.putInt(0, 1);
        }

        if (this.supportsColorTransform) {
            this.pushConstants.putInt(4, 1);
        }

        final VKConstants constants = VKConstants.getConstants(ctx);
        final VKQueue stagingQueue = constants.stagingQueue;
        final VKSharingMode sharingMode = ctx.mainQueue == stagingQueue ? VKSharingMode.EXCLUSIVE : VKSharingMode.CONCURRENT;

        if (info.image == null) {
            throw new IllegalArgumentException("ImageLayer requires an image!");
        }

        this.uniformStaging = new VKBufferInfo()
                .withSize(SIZE_UNIFORM)
                .withUsage(VKBufferInfo.Usage.TRANSFER_SRC)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));

        this.uniforms = new VKBufferInfo()
                .withSize(SIZE_UNIFORM)
                .withQueueFamilies(ctx.mainQueue.family, stagingQueue.family)
                .withSharingMode(sharingMode)
                .withUsage(VKBufferInfo.Usage.UNIFORM, VKBufferInfo.Usage.TRANSFER_DST)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));

        final ByteBuffer imageData = info.image.image.getData();
        final ByteBuffer maskData = info.mask != null ? info.mask.image.getData() : null;
        final int stagingBufferSize = imageData.remaining() + (maskData != null ? maskData.remaining() : 0);
        final int maskStart = imageData.remaining();

        final VKManagedBuffer imageStaging = new VKBufferInfo()
                .withSize(stagingBufferSize)
                .withUsage(VKBufferInfo.Usage.TRANSFER_SRC)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));

        try (VKManagedBuffer.MappedBuffer mapping = imageStaging.map()) {
            final ByteBuffer stagingData = mapping.getPointer();

            stagingData.put(imageData);

            if (maskData != null) {
                stagingData.put(maskData);
            }
        }

        final List<VKImageMemoryBarrier> stage0Barriers = new ArrayList<>();
        final List<VKImageMemoryBarrier> stage1Barriers = new ArrayList<>();

        this.image = new VKImageInfo()
                .withImageType(VKImageType.IMAGE_2D)
                .withExtent(info.image.image.getWidth(), info.image.image.getHeight(), 1)
                .withMipLevels(1)
                .withArrayLayers(1)
                .withFormat(VKFormat.R8G8B8A8_UNORM)
                .withTiling(VKImageTiling.OPTIMAL)
                .withInitialLayout(VKImageLayout.UNDEFINED)
                .withUsage(VKImageUsage.TRANSFER_DST, VKImageUsage.SAMPLED)
                .withSharingMode(VKSharingMode.EXCLUSIVE)
                .withSamples(1)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));

        this.imageView = VKImageView.fullView(ctx, this.image);
        this.imageSampler = createSampler(ctx, info.image);

        stage0Barriers.add(new VKImageMemoryBarrier()
                .withOldLayout(VKImageLayout.UNDEFINED)
                .withNewLayout(VKImageLayout.TRANSFER_DST_OPTIMAL)
                .withImage(this.image)
                .withDstAccessMask(VKAccess.TRANSFER_WRITE)
                .withSubresourceRange(this.image.getFullRange()));

        stage1Barriers.add(new VKImageMemoryBarrier()
                .withOldLayout(VKImageLayout.TRANSFER_DST_OPTIMAL)
                .withNewLayout(VKImageLayout.SHADER_READ_ONLY_OPTIMAL)
                .withImage(this.image)
                .withSrcAccessMask(VKAccess.TRANSFER_WRITE)
                .withDstAccessMask(VKAccess.SHADER_READ)
                .withSubresourceRange(this.image.getFullRange()));

        if (this.supportsMask) {
            this.mask = new VKImageInfo()
                    .withImageType(VKImageType.IMAGE_2D)
                    .withExtent(info.mask.image.getWidth(), info.mask.image.getHeight(), 1)
                    .withMipLevels(1)
                    .withArrayLayers(1)
                    .withFormat(VKFormat.R8G8B8A8_UNORM)
                    .withTiling(VKImageTiling.OPTIMAL)
                    .withInitialLayout(VKImageLayout.UNDEFINED)
                    .withUsage(VKImageUsage.TRANSFER_DST, VKImageUsage.SAMPLED)
                    .withSharingMode(VKSharingMode.EXCLUSIVE)
                    .withSamples(1)
                    .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));

            this.maskView = VKImageView.fullView(ctx, this.mask);
            this.maskSampler = createSampler(ctx, info.mask);

            stage0Barriers.add(new VKImageMemoryBarrier()
                    .withOldLayout(VKImageLayout.UNDEFINED)
                    .withNewLayout(VKImageLayout.TRANSFER_DST_OPTIMAL)
                    .withImage(this.mask)
                    .withDstAccessMask(VKAccess.TRANSFER_WRITE)
                    .withSubresourceRange(this.mask.getFullRange()));

            stage1Barriers.add(new VKImageMemoryBarrier()
                    .withOldLayout(VKImageLayout.TRANSFER_DST_OPTIMAL)
                    .withNewLayout(VKImageLayout.SHADER_READ_ONLY_OPTIMAL)
                    .withImage(this.mask)
                    .withSrcAccessMask(VKAccess.TRANSFER_WRITE)
                    .withDstAccessMask(VKAccess.SHADER_READ)
                    .withSubresourceRange(this.mask.getFullRange()));
        } else {
            this.mask = null;
            this.maskView = null;
            this.maskSampler = null;
        }

        final VKPipelineBarrier stage0 = new VKPipelineBarrier()
                .withSrcStageMask(VKPipelineStage.TOP_OF_PIPE)
                .withDstStageMask(VKPipelineStage.TRANSFER)
                .withImageMemoryBarriers(stage0Barriers);

        final VKPipelineBarrier stage1 = new VKPipelineBarrier()
                .withSrcStageMask(VKPipelineStage.TRANSFER)
                .withDstStageMask(VKPipelineStage.FRAGMENT_SHADER)
                .withImageMemoryBarriers(stage1Barriers);

        final VKCommandBuffer cmdStaging = constants.initCmdPool.newPrimaryCommandBuffer();

        cmdStaging.begin(VKCommandBufferBeginInfo.PRIMARY_ONE_TIME_USE);

        cmdStaging.fillBuffer(this.uniforms, 0, SIZE_UNIFORM, 0);
        cmdStaging.pipelineBarrier(stage0);

        cmdStaging.copyBufferToImage(imageStaging, this.image, VKImageLayout.TRANSFER_DST_OPTIMAL, new VKBufferImageCopy()
                .withImageSubresource(this.image.getBaseLayer(0))
                .withImageExtent(info.image.image.getWidth(), info.image.image.getHeight(), 1));

        if (this.supportsMask) {
            cmdStaging.copyBufferToImage(imageStaging, this.image, VKImageLayout.TRANSFER_DST_OPTIMAL, new VKBufferImageCopy()
                    .withBufferOffset(maskStart)
                    .withImageSubresource(this.mask.getBaseLayer(0))
                    .withImageExtent(info.mask.image.getWidth(), info.mask.image.getHeight(), 1));
        }

        cmdStaging.pipelineBarrier(stage1);

        cmdStaging.end();

        final VKFence uploadComplete = new VKFence(ctx, Collections.emptySet());

        ctx.mainQueue.submit(VKQueue.SubmitInfo.wrap(cmdStaging), uploadComplete);

        VKConstants.ASYNC.submit(() -> {
            try {                
                this.writeLock.acquire();
                uploadComplete.waitFor();
                uploadComplete.close();
                imageStaging.close();
            } catch (InterruptedException ex) {
                throw new RuntimeException("VKImageLayer uploadComplete fence wait was interrupted!", ex);
            } finally {
                this.writeLock.release();
            }

            if (this.supportsColorTransform) {
                this.setColorTransform(ColorTransform.IDENTITY);
            }                        
        });
    }

    private void checkMapping() {
        if (this.stagingMapping == null) {
            this.stagingMapping = this.uniformStaging.map();
            this.uniformData = this.stagingMapping.getPointer().asFloatBuffer();
        }
    }

    @Override
    public void setColorTransform(ColorTransform ct) {
        if (!this.supportsColorTransform) {            
            throw new UnsupportedOperationException("ImageLayer does not support color transform!");
        }

        try {            
            this.writeLock.acquire();
            this.checkMapping();

            final float ca = (float) Math.cos(Math.toRadians(ct.hsv.hue));
            final float sa = (float) Math.sin(Math.toRadians(ct.hsv.hue));
            
            this.uniformData
                    .put(8, ca)
                    .put(9, sa)
                    .put(10, ct.hsv.saturation)
                    .put(11, ct.hsv.value)
                    .put(12, ct.rgbaOffset.red)
                    .put(13, ct.rgbaOffset.green)
                    .put(14, ct.rgbaOffset.blue)
                    .put(15, ct.rgbaOffset.alpha)
                    .put(16, ct.rgbaScale.red)
                    .put(17, ct.rgbaScale.green)
                    .put(18, ct.rgbaScale.blue)
                    .put(19, ct.rgbaScale.alpha);

            this.ctChanged = true;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while writing color transform!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void scrollImage(float s0, float t0, float s1, float t1) {
        try {
            this.writeLock.acquire();
            this.checkMapping();
            this.uniformData
                    .put(0, s0)
                    .put(1, t0)
                    .put(2, s1)
                    .put(3, t1);
            this.imageScrollChanged = true;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while writing image scroll!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void scrollMask(float s0, float t0, float s1, float t1) {
        try {
            this.writeLock.acquire();
            this.checkMapping();
            this.uniformData
                    .put(4, s0)
                    .put(5, t0)
                    .put(6, s1)
                    .put(7, t1);
            this.maskScrollChanged = true;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while writing mask scroll!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void onFrameStart() {
    }

    @Override
    public void onFrameEnd() {
    }

    @Override
    public void setProjection(float[] projection) {
        throw new UnsupportedOperationException("ImageLayer does not support projection matrices!");
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public void close() {
        if (this.info != null) {
            if (this.stagingMapping != null) {
                this.stagingMapping = null;
                this.uniformData = null;
            }

            MemoryUtil.memFree(this.pushConstants);

            this.image.close();
            this.imageSampler.close();
            this.imageView.close();
            Optional.ofNullable(this.mask).ifPresent(VKManagedImage::close);
            Optional.ofNullable(this.maskSampler).ifPresent(VKSampler::close);
            Optional.ofNullable(this.maskView).ifPresent(VKImageView::close);
            this.uniforms.close();
            this.info = null;
        }
    }

    @Override
    public boolean hasUpdates() {
        try {
            this.writeLock.acquire();
            return this.ctChanged || this.imageScrollChanged || this.maskScrollChanged;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while checking for buffer changes!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void initDescriptors(VKContext ctx, VKDescriptorPool descriptorPool) {
        final VKConstants constants = VKConstants.getConstants(ctx);

        this.descriptorSet = descriptorPool.allocate(constants.imagePipeline.descriptorSetLayout);

        final List<VKWriteDescriptorSet> writes = new ArrayList<>();

        writes.add(new VKWriteDescriptorSet()
                .withDstSet(this.descriptorSet)
                .withDstBinding(0)
                .withDescriptorType(VKDescriptorType.UNIFORM_BUFFER)
                .withBufferInfo(new VKDescriptorBufferInfo()
                        .withBuffer(this.uniforms)
                        .withRange(SIZE_UNIFORM)));

        writes.add(new VKWriteDescriptorSet()
                .withDstSet(this.descriptorSet)
                .withDstBinding(1)
                .withDescriptorType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                .withImageInfo(new VKDescriptorImageInfo()
                        .withImageLayout(VKImageLayout.SHADER_READ_ONLY_OPTIMAL)
                        .withImageView(this.imageView)
                        .withSampler(this.imageSampler)));

        if (this.supportsMask) {
            writes.add(new VKWriteDescriptorSet()
                    .withDstSet(this.descriptorSet)
                    .withDstBinding(2)
                    .withDescriptorType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                    .withImageInfo(new VKDescriptorImageInfo()
                            .withImageLayout(VKImageLayout.SHADER_READ_ONLY_OPTIMAL)
                            .withImageView(this.maskView)
                            .withSampler(this.maskSampler)));
        } else {
            writes.add(new VKWriteDescriptorSet()
                    .withDstSet(this.descriptorSet)
                    .withDstBinding(2)
                    .withDescriptorType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                    .withImageInfo(new VKDescriptorImageInfo()
                            .withImageLayout(VKImageLayout.SHADER_READ_ONLY_OPTIMAL)
                            .withImageView(this.imageView)
                            .withSampler(this.imageSampler)));
        }

        ctx.device.updateDescriptorSets(writes, Collections.emptyList());
    }

    @Override
    public VKGraphicsPipeline getPipeline(VKContext ctx) {
        return VKConstants.getConstants(ctx).imagePipeline.pipeline;
    }

    @Override
    public void recordUpdates(VKContext ctx, VKCommandBuffer cmdbuf) {
        try {
            this.writeLock.acquire();

            this.stagingMapping.close();
            this.stagingMapping = null;

            if (this.imageScrollChanged) {
                cmdbuf.copyBuffer(this.uniformStaging, this.uniforms, new VKBufferCopy()
                        .withSrcOffset(OFF_IMAGE_SCROLL)
                        .withDstOffset(OFF_IMAGE_SCROLL)
                        .withSize(SIZE_IMAGE_SCROLL));
                this.imageScrollChanged = false;
            }

            if (this.maskScrollChanged) {
                cmdbuf.copyBuffer(this.uniformStaging, this.uniforms, new VKBufferCopy()
                        .withSrcOffset(OFF_MASK_SCROLL)
                        .withDstOffset(OFF_MASK_SCROLL)
                        .withSize(SIZE_MASK_SCROLL));
                this.maskScrollChanged = false;
            }

            if (this.ctChanged) {
                cmdbuf.copyBuffer(this.uniformStaging, this.uniforms, new VKBufferCopy()
                        .withSrcOffset(OFF_COLOR_TRANSFORM)
                        .withDstOffset(OFF_COLOR_TRANSFORM)
                        .withSize(SIZE_COLOR_TRANSFORM));
                this.ctChanged = false;
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while recording updates!", ex);
        } finally {
            this.writeLock.release();
        }
    }

    @Override
    public void recordDraw(VKContext ctx, VKCommandBuffer cmdbuf) {
        final VKConstants constants = VKConstants.getConstants(ctx);

        cmdbuf.pushConstants(constants.imagePipeline.pipelineLayout, EnumSet.of(VKShaderStage.VERTEX, VKShaderStage.FRAGMENT), 0, pushConstants);
        cmdbuf.bindDescriptorSet(VKPipelineBindPoint.GRAPHICS, constants.imagePipeline.pipelineLayout, 0, descriptorSet);
        cmdbuf.draw(4, 1, 0, 0);
    }

    @Override
    public int getNeededDescriptorSets() {
        return 1;
    }

    @Override
    public int getNeededUniformBufferDescriptors() {
        return 1;
    }

    @Override
    public int getNeededImageSamplerDescriptors() {
        return 2;
    }

    @Override
    public ImageLayerInfo getInfo() {
        return this.info;
    }

}
