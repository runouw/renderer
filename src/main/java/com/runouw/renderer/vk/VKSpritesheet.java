/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

import com.longlinkislong.gloop2.vk.VKBufferImageCopy;
import com.longlinkislong.gloop2.vk.VKBufferInfo;
import com.longlinkislong.gloop2.vk.VKCommandBuffer;
import com.longlinkislong.gloop2.vk.VKCommandBufferBeginInfo;
import com.longlinkislong.gloop2.vk.VKContext;
import com.longlinkislong.gloop2.vk.VKFilter;
import com.longlinkislong.gloop2.vk.VKFormat;
import com.longlinkislong.gloop2.vk.VKImage;
import com.longlinkislong.gloop2.vk.VKImageAspect;
import com.longlinkislong.gloop2.vk.VKImageInfo;
import com.longlinkislong.gloop2.vk.VKImageLayout;
import com.longlinkislong.gloop2.vk.VKImageSubresourceLayers;
import com.longlinkislong.gloop2.vk.VKImageTiling;
import com.longlinkislong.gloop2.vk.VKImageType;
import com.longlinkislong.gloop2.vk.VKImageUsage;
import com.longlinkislong.gloop2.vk.VKImageView;
import com.longlinkislong.gloop2.vk.VKManagedBuffer;
import com.longlinkislong.gloop2.vk.VKMemoryProperty;
import com.longlinkislong.gloop2.vk.VKPipelineStage;
import com.longlinkislong.gloop2.vk.VKQueue;
import com.longlinkislong.gloop2.vk.VKSampler;
import com.longlinkislong.gloop2.vk.VKSamplerAddressMode;
import com.longlinkislong.gloop2.vk.VKSamplerInfo;
import com.longlinkislong.gloop2.vk.VKSharingMode;
import com.runouw.renderer.ImageView;
import com.runouw.renderer.MaskResourceInfo;
import com.runouw.renderer.SpriteLayerInfo;
import com.runouw.renderer.SpriteResourceInfo;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 *
 * @author zmichaels
 */
final class VKSpritesheet implements AutoCloseable {
    
    private static final int MAX_TEXTURE_ARRAY_LAYERS = Integer.getInteger("com.runouw.renderer.vk.VKSpritesheet.MAX_TEXTURE_ARRAY_LAYERS", 256);
    final VKImage image;
    final VKImageView imageView;
    final VKSampler sampler;
    final Map<String, ImageView> spriteRefs = new HashMap<>();
    final Map<String, ImageView> maskRefs = new HashMap<>();
    
    VKSpritesheet(final VKContext ctx, final SpriteLayerInfo info) {
        int width = -1;
        int height = -1;
        
        final int images = info.validSprites.size() + info.validMasks.size();
        
        assert images <= MAX_TEXTURE_ARRAY_LAYERS : "Required image count exceeds max texture array layer count!";
        assert images >= 0 : "No images were defined!";
        
        for (SpriteResourceInfo spriteInfo : info.validSprites) {
            width = Math.max(width, spriteInfo.image.getWidth());
            height = Math.max(height, spriteInfo.image.getHeight());
        }
        
        for (MaskResourceInfo maskInfo : info.validMasks) {
            width = Math.max(width, maskInfo.image.getWidth());
            height = Math.max(height, maskInfo.image.getHeight());
        }
        
        assert width >= 0 : "Invalid spritesheet width!";
        assert height >= 0 : "Invalid spritesheet height!";
        
        this.image = new VKImageInfo()
                .withImageType(VKImageType.IMAGE_2D)
                .withExtent(width, height, 1)
                .withMipLevels(1)
                .withArrayLayers(images)
                .withFormat(VKFormat.R8G8B8A8_UNORM)
                .withTiling(VKImageTiling.OPTIMAL)
                .withInitialLayout(VKImageLayout.UNDEFINED)
                .withUsage(VKImageUsage.TRANSFER_DST, VKImageUsage.SAMPLED)
                .withSharingMode(VKSharingMode.EXCLUSIVE)
                .withSamples(1)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));
        
        final int totalImageSize = Stream.concat(
                info.validSprites.stream().map(spriteInfo -> spriteInfo.image),
                info.validMasks.stream().map(maskInfo -> maskInfo.image))
                .mapToInt(img -> img.getWidth() * img.getHeight() * 4)
                .sum();
        
        try (VKManagedBuffer stagingBuffer = new VKBufferInfo()
                .withSize(totalImageSize)
                .withUsage(VKBufferInfo.Usage.TRANSFER_SRC)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT))) {
            
            int i = 0;
            final VKBufferImageCopy[] copies = new VKBufferImageCopy[images];
            
            try (VKManagedBuffer.MappedBuffer stagingMapping = stagingBuffer.map()) {
                final ByteBuffer stagingData = stagingMapping.getPointer();
                
                for (SpriteResourceInfo spriteInfo : info.validSprites) {
                    final int iw = spriteInfo.image.getWidth();
                    final int ih = spriteInfo.image.getHeight();
                    final float s1 = ((float) iw) / ((float) width);
                    final float t1 = ((float) ih) / ((float) height);
                    final int offset = stagingData.position();
                    
                    stagingData.put(spriteInfo.image.getData());
                    
                    copies[i] = new VKBufferImageCopy()
                            .withImageSubresource(new VKImageSubresourceLayers()
                                    .withAspectMask(VKImageAspect.COLOR)
                                    .withMipLevel(0)
                                    .withBaseArrayLayer(i)
                                    .withLayerCount(1))
                            .withImageExtent(iw, ih, 1)
                            .withBufferOffset(offset);
                    
                    this.spriteRefs.put(spriteInfo.lookup, new ImageView(i++, 0F, 0F, s1, t1));
                }
                
                for (MaskResourceInfo maskInfo : info.validMasks) {
                    final int iw = maskInfo.image.getWidth();
                    final int ih = maskInfo.image.getHeight();
                    final float s1 = ((float) iw) / ((float) width);
                    final float t1 = ((float) ih) / ((float) height);
                    final int offset = stagingData.position();
                    
                    stagingData.put(maskInfo.image.getData());
                    
                    copies[i] = new VKBufferImageCopy()
                            .withImageSubresource(new VKImageSubresourceLayers()
                                    .withAspectMask(VKImageAspect.COLOR)
                                    .withMipLevel(0)
                                    .withBaseArrayLayer(i)
                                    .withLayerCount(1))
                            .withImageExtent(iw, ih, 1)
                            .withBufferOffset(offset);
                    
                    this.maskRefs.put(maskInfo.lookup, new ImageView(i++, 0F, 0F, s1, t1));
                }
            }
            
            final VKCommandBuffer cmdstaging = ctx.primaryCommandPool.newPrimaryCommandBuffer();
            
            cmdstaging.begin(VKCommandBufferBeginInfo.PRIMARY_ONE_TIME_USE);
            
            cmdstaging.stageImageLayoutTransition(
                    image, image.getFullRange(),
                    EnumSet.of(VKPipelineStage.TOP_OF_PIPE), VKImageLayout.UNDEFINED,
                    EnumSet.of(VKPipelineStage.TRANSFER), VKImageLayout.TRANSFER_DST_OPTIMAL);
            
            cmdstaging.copyBufferToImage(stagingBuffer, image, VKImageLayout.SHADER_READ_ONLY_OPTIMAL, copies);
            
            cmdstaging.stageImageLayoutTransition(
                    image, image.getFullRange(),
                    EnumSet.of(VKPipelineStage.TRANSFER), VKImageLayout.UNDEFINED,
                    EnumSet.of(VKPipelineStage.FRAGMENT_SHADER), VKImageLayout.SHADER_READ_ONLY_OPTIMAL);
            
            cmdstaging.end();
            
            ctx.mainQueue.submit(VKQueue.SubmitInfo.wrap(cmdstaging));
            // replace this with an async free
            ctx.mainQueue.waitIdle();
        }
        
        this.imageView = VKImageView.fullView(ctx, image);
        
        this.sampler = new VKSamplerInfo()
                .withAddressModeU(VKSamplerAddressMode.CLAMP_TO_EDGE)
                .withAddressModeV(VKSamplerAddressMode.CLAMP_TO_EDGE)
                .withAddressModeW(VKSamplerAddressMode.CLAMP_TO_EDGE)
                .withMagFilter(VKFilter.NEAREST)
                .withMinFilter(VKFilter.NEAREST)
                .withAnisotropyEnable(false)
                .withMaxAnisotropy(1.0F)
                .create(ctx);
    }
    
    @Override
    public void close() {
        this.sampler.close();
        this.imageView.close();
        this.image.close();
    }
    
}
