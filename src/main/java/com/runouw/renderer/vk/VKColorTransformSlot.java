/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

import com.runouw.renderer.ColorTransformSlot;
import com.runouw.renderer.HSV;
import com.runouw.renderer.RGBA;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

/**
 *
 * @author zmichaels
 */
public final class VKColorTransformSlot implements ColorTransformSlot {

    public static final int BYTES = 12 * Float.BYTES;
    private final ByteBuffer ptr;

    VKColorTransformSlot(final ByteBuffer ptr) {
        this.ptr = ptr;
    }

    @Override
    public ColorTransformSlot set(HSV hsv, RGBA rgbaOffset, RGBA rgbaScale) {
        final FloatBuffer data = this.ptr.asFloatBuffer();
        final double hue = Math.toRadians(hsv.hue);

        data.put((float) Math.cos(hue)).put((float) Math.sin(hue)).put(hsv.saturation).put(hsv.value);
        data.put(rgbaOffset.red).put(rgbaOffset.green).put(rgbaOffset.blue).put(rgbaOffset.alpha);
        data.put(rgbaScale.red).put(rgbaScale.green).put(rgbaScale.blue).put(rgbaScale.alpha);

        return this;
    }

    @Override
    public ColorTransformSlot setHSV(float[] hsv) {
        ptr.putFloat(0, hsv[0])
                .putFloat(4, hsv[1])
                .putFloat(8, hsv[2])
                .putFloat(12, hsv[3]);        

        return this;
    }

    @Override
    public ColorTransformSlot setHSV(FloatBuffer hsv) {
       this.ptr.asFloatBuffer().put(hsv);
        
        return this;
    }

    @Override
    public ColorTransformSlot setRGBAOffset(float[] rgba) {
        final int off = 4 * Float.BYTES;
        
        ptr.putFloat(off, rgba[0])
                .putFloat(off + 4, rgba[1])
                .putFloat(off + 8, rgba[2])
                .putFloat(off + 12, rgba[3]);
        
        return this;
    }

    @Override
    public ColorTransformSlot setRGBAOffset(FloatBuffer rgba) {
        final int off = 4 * Float.BYTES;
        
        ptr.putFloat(off, rgba.get())
                .putFloat(off + 4, rgba.get())
                .putFloat(off + 8, rgba.get())
                .putFloat(off + 12, rgba.get());
        
        return this;
    }

    @Override
    public ColorTransformSlot setRGBAScale(float[] rgba) {
        final int off = 8 * Float.BYTES;
        
        ptr.putFloat(off, rgba[0])
                .putFloat(off + 4, rgba[1])
                .putFloat(off + 8, rgba[2])
                .putFloat(off + 12, rgba[3]);
        
        return this;
    }

    @Override
    public ColorTransformSlot setRGBAScale(FloatBuffer rgba) {
        final int off = 8 * Float.BYTES;
        
        ptr.putFloat(off, rgba.get())
                .putFloat(off + 4, rgba.get())
                .putFloat(off + 8, rgba.get())
                .putFloat(off + 12, rgba.get());
        
        return this;
    }

    @Override
    public ColorTransformSlot clear() {        
        for (int i = 0; i < 12; i++) {
            ptr.putInt(i * 4, 0);
        }
        
        return this;
    }

}
