/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

import com.longlinkislong.gloop2.vk.VKBuffer;
import com.longlinkislong.gloop2.vk.VKBufferInfo;
import com.longlinkislong.gloop2.vk.VKCommandBuffer;
import com.longlinkislong.gloop2.vk.VKContext;
import com.longlinkislong.gloop2.vk.VKDescriptorPool;
import com.longlinkislong.gloop2.vk.VKFrameLocal;
import com.longlinkislong.gloop2.vk.VKGraphicsPipeline;
import com.longlinkislong.gloop2.vk.VKManagedBuffer;
import com.longlinkislong.gloop2.vk.VKMemoryProperty;
import com.runouw.renderer.ColorTransformSlot;
import com.runouw.renderer.ImageView;
import com.runouw.renderer.SpriteLayer;
import com.runouw.renderer.SpriteLayerInfo;
import com.runouw.renderer.SpriteSlot;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryUtil;

/**
 *
 * @author zmichaels
 */
public final class VKSpriteLayer implements SpriteLayer, VKLayer {
    private final SpriteLayerInfo info;
    private final VKSpritesheet spritesheet;        
    private final VKManagedBuffer uniform;
    private final VKSpriteSlot[] spriteSlots;
    private final VKManagedBuffer cmdBuffer;
    private final ByteBuffer cmdDrawData;
    private final VKManagedBuffer.MappedBuffer cmdDrawMapping;
    private final VKFrameLocal<VKManagedBuffer> instanceData;
    private final Set<VKManagedBuffer> instanceDataRefs = new HashSet<>();
    private final FloatBuffer projection;
    private final VKColorTransformSlot[] ctSlots;
    private final PointerBuffer pInstance;
    private VKManagedBuffer.MappedBuffer instanceMapping;
    private final VKManagedBuffer.MappedBuffer uniformMapping;
    private final AtomicBoolean isWritable = new AtomicBoolean(false);
    
    public VKSpriteLayer(final VKContext ctx, final SpriteLayerInfo info) {
        this.info = info;
        this.spritesheet = new VKSpritesheet(ctx, info);        
        this.pInstance = MemoryUtil.memAllocPointer(1);
        
        this.cmdBuffer = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.INDIRECT)
                .withSize(4 * Integer.BYTES)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));
        
        this.cmdDrawMapping = this.cmdBuffer.map();
        this.cmdDrawData = this.cmdDrawMapping.getPointer();
        
        this.uniform = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.UNIFORM)
                .withSize(16 + info.maxColorTransforms * VKColorTransformSlot.BYTES)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));
        
        this.uniformMapping = this.uniform.map();
        this.spriteSlots = new VKSpriteSlot[info.maxSpriteSlots];
        
        for (int i = 0; i < info.maxSpriteSlots; i++) {
            this.spriteSlots[i] = new VKSpriteSlot(this.pInstance, i * VKSpriteSlot.BYTES);
        }
        
        this.instanceData = new VKFrameLocal<VKManagedBuffer> (ctx) {
            @Override
            protected VKManagedBuffer initialValue(final VKContext ctx) {
                return new VKBufferInfo()
                        .withUsage(VKBufferInfo.Usage.VERTEX)
                        .withSize(info.maxSpriteSlots * VKSpriteSlot.BYTES)
                        .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));
            }
        };
        
        final ByteBuffer ptrUniform = this.uniformMapping.getPointer();
        
        this.projection = ptrUniform.asFloatBuffer();
        
        if (info.maxColorTransforms > 0) {
            ptrUniform.position(16 * Float.BYTES);
            
            this.ctSlots = new VKColorTransformSlot[info.maxColorTransforms];
            
            for (int i = 0; i < info.maxColorTransforms; i++) {
                this.ctSlots[i] = new VKColorTransformSlot(ptrUniform.slice());
                
                ptrUniform.position(ptrUniform.position() + VKColorTransformSlot.BYTES);
            }
        } else {
            this.ctSlots = null;
        }
    }
    
    @Override
    public ImageView getSpriteView(String spriteId) {
        return this.spritesheet.spriteRefs.get(spriteId);
    }

    @Override
    public ImageView getMaskView(String maskId) {
        return this.spritesheet.maskRefs.get(maskId);
    }

    @Override
    public ColorTransformSlot[] getColorTransformSlots() {
        return this.ctSlots;
    }

    @Override
    public ColorTransformSlot getColorTransformSlot(int slotId) {
        return this.ctSlots[slotId];
    }

    @Override
    public SpriteSlot[] getSpriteSlots() {
        return this.spriteSlots;
    }

    @Override
    public SpriteSlot getSpriteSlot(int slotId) {
        return this.spriteSlots[slotId];
    }

    @Override
    public SpriteLayerInfo getInfo() {
        return this.info;
    }

    @Override
    public void setDrawLimit(int drawCount) {
        assert drawCount <= this.info.maxSpriteSlots;
        
        this.cmdDrawData.putInt(0, 4)
                .putInt(4, drawCount)
                .putInt(8, 0)
                .putInt(12, 0);
    }

    @Override
    public int getMaxDrawLimit() {
        return this.info.maxSpriteSlots;
    }

    @Override
    public void onFrameStart() {
        final VKManagedBuffer instanceBuffer = this.instanceData.get();
        
        this.instanceDataRefs.add(instanceBuffer);        
        this.instanceMapping = instanceBuffer.map();
        
        final ByteBuffer instance = this.instanceMapping.getPointer();
        
        this.pInstance.put(0, instance);
        this.isWritable.set(true);
    }

    @Override
    public void onFrameEnd() {
        this.isWritable.set(false);
        this.instanceMapping.close();
        this.instanceMapping = null;
        this.pInstance.put(0, 0L);
    }

    @Override
    public void setProjection(float[] projection) {
        this.projection.position(0);
        this.projection.put(projection);
    }

    @Override
    public boolean isWritable() {
        return this.isWritable.get();
    }

    @Override
    public void close() {
        this.cmdDrawMapping.close();
        this.cmdBuffer.close();                        
        
        if (this.instanceMapping != null) {
            this.instanceMapping.close();
            this.instanceMapping = null;
        }
        
        this.instanceDataRefs.forEach(VKBuffer::close);
        
        MemoryUtil.memFree(this.pInstance);
        
        this.spritesheet.close();
        this.uniformMapping.close();
        this.uniform.close();
    }

    @Override
    public boolean hasUpdates() {
        return false;
    }

    @Override
    public void initDescriptors(VKContext ctx, VKDescriptorPool descriptorPool) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VKGraphicsPipeline getPipeline(VKContext ctx) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void recordUpdates(VKContext ctx, VKCommandBuffer cmdbuf) {
        throw new UnsupportedOperationException("VKSpriteLayer does not submit updates!");
    }

    @Override
    public void recordDraw(VKContext ctx, VKCommandBuffer cmdbuf) {
        cmdbuf.bindVertexBuffer(0, this.instanceData.get(), 0L);
        //TODO: bind descriptor set
        cmdbuf.drawIndirect(this.cmdBuffer, 0L, 1, 0);
    }

    @Override
    public int getNeededDescriptorSets() {
        return 1;
    }

    @Override
    public int getNeededUniformBufferDescriptors() {
        return 1;
    }

    @Override
    public int getNeededImageSamplerDescriptors() {
        return 1;
    }
    
}
