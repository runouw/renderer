/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

import com.longlinkislong.gloop2.image.BufferedImage;
import com.longlinkislong.gloop2.image.Image;
import com.longlinkislong.gloop2.image.PixelFormat;
import com.longlinkislong.gloop2.vk.VKBufferCopy;
import com.longlinkislong.gloop2.vk.VKBufferImageCopy;
import com.longlinkislong.gloop2.vk.VKBufferInfo;
import com.longlinkislong.gloop2.vk.VKCommandBuffer;
import com.longlinkislong.gloop2.vk.VKCommandBufferBeginInfo;
import com.longlinkislong.gloop2.vk.VKContext;
import com.longlinkislong.gloop2.vk.VKDescriptorPool;
import com.longlinkislong.gloop2.vk.VKFence;
import com.longlinkislong.gloop2.vk.VKGraphicsPipeline;
import com.longlinkislong.gloop2.vk.VKImage;
import com.longlinkislong.gloop2.vk.VKImageAspect;
import com.longlinkislong.gloop2.vk.VKImageInfo;
import com.longlinkislong.gloop2.vk.VKImageLayout;
import com.longlinkislong.gloop2.vk.VKImageSubresourceLayers;
import com.longlinkislong.gloop2.vk.VKImageType;
import com.longlinkislong.gloop2.vk.VKImageUsage;
import com.longlinkislong.gloop2.vk.VKImageView;
import com.longlinkislong.gloop2.vk.VKManagedBuffer;
import com.longlinkislong.gloop2.vk.VKMemoryProperty;
import com.longlinkislong.gloop2.vk.VKPipelineStage;
import com.longlinkislong.gloop2.vk.VKQueue;
import com.longlinkislong.gloop2.vk.VKQueueFamily;
import com.longlinkislong.gloop2.vk.VKSharingMode;
import com.runouw.renderer.TileFilter;
import com.runouw.renderer.TileLayer;
import com.runouw.renderer.TileLayerInfo;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Semaphore;

/**
 *
 * @author zmichaels
 */
public class VKTileLayer implements TileLayer, VKLayer {

    @Override
    public void addTileFilter(TileFilter filter) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean removeTileFilter(TileFilter filter) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private static final class Update {
        
        private final int supertileX;
        private final int supertileY;
        
        private Update(final int supertileX, final int supertileY) {
            this.supertileX = supertileX;
            this.supertileY = supertileY;
        }
    }
    
    private TileLayerInfo info;
    
    private final VKImage tiles;
    private final VKImageView tileView;
    private final VKManagedBuffer uProjection;
    private final VKManagedBuffer vSupertileRect;
    private final VKManagedBuffer vLayerSelect;
    private final int totalSupertiles;
    private final int supertilesAcross;
    private final int supertilesDown;
    private final VKManagedBuffer staging;
    private final int tilesPerSupertile;
    private final Set<Update> updates = new HashSet<>();
    private final Semaphore writeLock = new Semaphore(1);
    private VKManagedBuffer.MappedBuffer stagingMapping;
    private VKManagedBuffer.MappedBuffer layerSelectMapping;
    private ByteBuffer ptrLayerSelect;
    private ByteBuffer ptrStaging;
    private final int[] supertileMapping;
    private final List<VKBufferImageCopy> imageUpdates = new ArrayList<>();
    
    public VKTileLayer(final VKContext ctx, final TileLayerInfo info) {
        this.info = info;
        this.tilesPerSupertile = info.supertileSize / info.tileSize;
        
        this.supertilesAcross = Math.max(1, info.tileSize * info.tileMap.width / info.supertileSize);
        this.supertilesDown = Math.max(1, info.tileSize * info.tileMap.height / info.supertileSize);
        
        final int tileLayers = supertilesAcross * supertilesDown;
        
        this.totalSupertiles = tileLayers;
        this.supertileMapping = new int[this.totalSupertiles];
        
        for (int i = 0; i < this.totalSupertiles; i++) {
            this.supertileMapping[i] = TileLayer.SUPERTILE_HIDDEN;
        }
        
        final int vSupertileRectBytesNeeded = 2 * 4 * Float.BYTES * this.totalSupertiles;        
        
        final VKManagedBuffer vtxStaging = new VKBufferInfo()
                .withSize(vSupertileRectBytesNeeded)
                .withUsage(VKBufferInfo.Usage.TRANSFER_SRC)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_COHERENT, VKMemoryProperty.HOST_VISIBLE));
        
        try (VKManagedBuffer.MappedBuffer mapping = vtxStaging.map()) {
            final ByteBuffer ptr = mapping.getPointer();
            
            for (int y = 0; y < this.supertilesDown; y++) {
                final float supertileY0 = y * info.supertileSize;
                final float supertileY1 = supertileY0 + info.supertileSize;
                
                for (int x = 0; x < this.supertilesAcross; x++) {
                    final float supertileX0 = x * info.supertileSize;
                    final float supertileX1 = supertileX0 + info.supertileSize;
                    
                    ptr.putFloat(supertileX0).putFloat(supertileY0)
                            .putFloat(supertileX1).putFloat(supertileY1);
                }
            }
        }
        
        final VKConstants constants = VKConstants.getConstants(ctx);
        final VKQueueFamily stagingQueueFamily = constants.stagingQueue.family;
        final VKQueueFamily mainQueueFamily = ctx.mainQueue.family;
        
        final VKSharingMode sharingMode = stagingQueueFamily == mainQueueFamily
                ? VKSharingMode.EXCLUSIVE
                : VKSharingMode.CONCURRENT;        
        
        this.vSupertileRect = new VKBufferInfo()
                .withSize(vSupertileRectBytesNeeded)
                .withUsage(VKBufferInfo.Usage.TRANSFER_DST, VKBufferInfo.Usage.VERTEX)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));
        
        this.vLayerSelect = new VKBufferInfo()
                .withSize(4 * Float.BYTES * this.totalSupertiles)
                .withUsage(VKBufferInfo.Usage.VERTEX)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));
        
        this.uProjection = new VKBufferInfo()
                .withSize(16 * Float.BYTES)
                .withUsage(VKBufferInfo.Usage.UNIFORM)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));
        
        final VKCommandBuffer initialDataUpload = ctx.primaryCommandPool.newPrimaryCommandBuffer();
        
        initialDataUpload.begin(VKCommandBufferBeginInfo.PRIMARY_ONE_TIME_USE);
        initialDataUpload.copyBuffer(vtxStaging, this.vSupertileRect, new VKBufferCopy().withSize(vSupertileRectBytesNeeded));
        initialDataUpload.fillBuffer(this.uProjection, 0, 16 * Float.BYTES, 0);
        initialDataUpload.fillBuffer(this.vLayerSelect, 0, 4 * Float.BYTES * this.totalSupertiles, Float.floatToRawIntBits(Float.NaN));
        initialDataUpload.end();
        
        final VKFence uploadComplete = new VKFence(ctx, Collections.emptySet());        
        
        ctx.mainQueue.submit(VKQueue.SubmitInfo.wrap(initialDataUpload), uploadComplete);        
        
        VKConstants.ASYNC.submit(() -> {
            try {
                this.writeLock.acquire();
                uploadComplete.waitFor();
                uploadComplete.close();
                vtxStaging.close();
            } catch (InterruptedException ex) {
                throw new RuntimeException("Interrupted while waiting on upload of TileLayer!", ex);
            } finally {
                this.writeLock.release();
            }
        });
        
        this.tiles = new VKImageInfo()
                .withUsage(VKImageUsage.TRANSFER_DST, VKImageUsage.SAMPLED)
                .withExtent(info.supertileSize, info.supertileSize, 1)
                .withImageType(VKImageType.IMAGE_2D)
                .withInitialLayout(VKImageLayout.UNDEFINED)
                .withMipLevels(1)
                .withArrayLayers(info.maxVisibleSupertiles)
                .withQueueFamilies(mainQueueFamily, stagingQueueFamily)
                .withSharingMode(sharingMode)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));
        
        this.tileView = VKImageView.fullView(ctx, tiles);
        
        this.staging = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.TRANSFER_SRC)
                .withQueueFamilies(mainQueueFamily, stagingQueueFamily)
                .withSize(info.supertileSize * info.supertileSize * Integer.BYTES * info.maxVisibleSupertiles)
                .withSharingMode(sharingMode)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));        
        
        if (!this.info.cache.isValid()) {
            
            for (int sty = 0; sty < this.supertilesDown; sty++) {
                for (int stx = 0; stx < this.supertilesAcross; stx++) {
                    this.rebuildSupertile(stx, sty);
                }
            }            
            
            this.info.cache.validate();
        }
    }
    
    private void rebuildSupertile(final int stx, final int sty) {
        final int startTileX = stx * this.tilesPerSupertile;
        final int startTileY = sty * this.tilesPerSupertile;
        
        try (BufferedImage temp = new BufferedImage(info.supertileSize, info.supertileSize, PixelFormat.R8G8B8A8_UNORM)) {
            for (int y = 0; y < this.tilesPerSupertile; y++) {
                final int tileY = startTileY + y;
                
                for (int x = 0; x < this.tilesPerSupertile; x++) {
                    final int tileX = startTileX + x;
                    final Optional<Image> tileImage = this.info.tileMap.getTile(tileX, tileY);
                    
                    if (tileImage.isPresent()) {
                        temp.setSubimage(x * info.tileSize, y * info.tileSize, info.tileSize, info.tileSize, tileImage.get());
                    }
                }
            }

            //TODO: apply filters here; this will require upload to graphics queue family, so expect slowdown
            //TODO: download a copy
            this.info.cache.storeSupertile(stx, sty, temp.getData());
        }
    }
    
    @Override
    public TileLayerInfo getInfo() {
        return this.info;
    }
    
    @Override
    public void setTile(Image img, int x, int y) {
        try {
            this.writeLock.acquire();
            this.info.tileMap.setTile(img, x, y);
            
            final int supertileX = x * info.tileSize / info.supertileSize;
            final int supertileY = y * info.tileSize / info.supertileSize;
            final Update update = new Update(supertileX, supertileY);
            
            this.updates.add(update);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting tile!", ex);
        } finally {
            this.writeLock.release();
        }
    }
    
    @Override
    public void fillTile(Image img, int startX, int startY, int width, int height) {
        try {
            this.writeLock.acquire();
            
            this.info.tileMap.fillTile(img, startX, startY, width, height);
            
            for (int y = 0; y < startY + height; y++) {
                for (int x = 0; x < startX + width; x++) {
                    final int supertileX = x * info.tileSize / info.supertileSize;
                    final int supertileY = y * info.tileSize / info.supertileSize;
                    final Update update = new Update(supertileX, supertileY);
                    
                    this.updates.add(update);
                }
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting tile!", ex);
        } finally {
            this.writeLock.release();
        }
    }
    
    @Override
    public Optional<Image> getTile(int x, int y) {
        try {
            this.writeLock.acquire();
            
            return this.info.tileMap.getTile(x, y);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while getting tile!", ex);
        } finally {
            this.writeLock.release();
        }
    }
    
    @Override
    public boolean isSupertileEmpty(int stx, int sty) {
        try {
            this.writeLock.acquire();
            
            final int supertileId = sty * this.supertilesAcross + stx;
            
            return this.supertileMapping[supertileId] >= 0;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while checking Supertile residency!", ex);
        } finally {
            this.writeLock.release();
        }        
    }
    
    private void applyUpdates() {
        for (Update update : this.updates) {
            final int supertileId = update.supertileY * this.supertilesAcross + update.supertileX;
            
            this.rebuildSupertile(update.supertileX, update.supertileY);
            
            final int mappedId = this.supertileMapping[supertileId];

            // update the texture layer if the Supertile is resident.
            if (mappedId >= 0) {
                this.makeSupertileResident(update.supertileX, update.supertileY, mappedId);
            }
        }
        
        this.updates.forEach(update -> this.rebuildSupertile(update.supertileX, update.supertileY));
        this.updates.clear();
    }
    
    private void makeSupertileResident(int stx, int sty, int mappedId) {
        final ByteBuffer data = this.info.cache.fetchSupertile(stx, sty);
        
        if (this.stagingMapping == null) {
            this.stagingMapping = this.staging.map();
            this.ptrStaging = this.stagingMapping.getPointer();
        }
        
        final int supertileArea = info.supertileSize * info.supertileSize;
        final int bytesPerSupertile = supertileArea * Integer.BYTES;        
        final int offset = bytesPerSupertile * mappedId;
        
        this.ptrStaging.position(offset);
        this.ptrStaging.put(data);
        this.imageUpdates.add(new VKBufferImageCopy()
                .withBufferOffset(offset)
                .withImageExtent(info.supertileSize, info.supertileSize, 1)
                .withImageSubresource(new VKImageSubresourceLayers()
                        .withAspectMask(VKImageAspect.COLOR)
                        .withBaseArrayLayer(mappedId)
                        .withLayerCount(1)
                        .withMipLevel(0)));
    }
    
    @Override
    public void setSupertileMapping(int stx, int sty, int mappedId) {
        try {
            this.writeLock.acquire();
            
            final int supertileId = sty * this.supertilesAcross + stx;
            
            if (this.supertileMapping[supertileId] == mappedId) {
                return;
            }
            
            if (this.layerSelectMapping == null) {
                this.layerSelectMapping = this.vLayerSelect.map();
                this.ptrLayerSelect = this.layerSelectMapping.getPointer();
            }
            
            final int offset = supertileId * 4 * Float.BYTES;
            
            if (mappedId < 0) {
                this.ptrLayerSelect.putFloat(offset, 0F);
                this.ptrLayerSelect.putFloat(offset + 4, Float.NaN);
                this.ptrLayerSelect.putFloat(offset + 8, Float.NaN);
                this.ptrLayerSelect.putFloat(offset + 12, Float.NaN);
            } else {
                this.ptrLayerSelect.putFloat(offset, mappedId);
                this.ptrLayerSelect.putFloat(offset + 4, 0F);
                this.ptrLayerSelect.putFloat(offset + 8, 0F);
                this.ptrLayerSelect.putFloat(offset + 12, 1F);
                
                this.makeSupertileResident(stx, sty, mappedId);
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting Supertile Mapping!", ex);
        } finally {
            this.writeLock.release();
        }
    }
    
    @Override
    public boolean isSupertileResident(int stx, int sty) {
        try {
            this.writeLock.acquire();
            
            final int supertileId = sty * this.supertilesAcross + stx;
            
            return this.supertileMapping[supertileId] >= 0;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while checking Supertile residency!", ex);
        } finally {
            this.writeLock.release();
        }
    }
    
    @Override
    public void onFrameStart() {
    }
    
    @Override
    public void onFrameEnd() {
    }
    
    @Override
    public void setProjection(float[] projection) {
        try (VKManagedBuffer.MappedBuffer mapping = this.uProjection.map()) {
            mapping.getPointer().asFloatBuffer().put(projection);
        }
    }
    
    @Override
    public boolean isWritable() {
        return true;
    }
    
    @Override
    public void close() {
        if (this.info != null) {
            this.info = null;
            
            if (this.layerSelectMapping != null) {
                this.layerSelectMapping.close();
                this.ptrLayerSelect = null;
            }
            
            if (this.stagingMapping != null) {
                this.stagingMapping.close();
                this.ptrStaging = null;
            }
            
            this.tileView.close();
            this.tiles.close();
            
            this.uProjection.close();
            this.vLayerSelect.close();
            this.vSupertileRect.close();            
        }
    }
    
    @Override
    public boolean hasUpdates() {
        try {
            this.writeLock.acquire();
            
            this.applyUpdates();            
            
            return !this.imageUpdates.isEmpty();
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while checking for updates!", ex);
        } finally {
            this.writeLock.release();
        }
    }
    
    @Override
    public void initDescriptors(VKContext ctx, VKDescriptorPool descriptorPool) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public VKGraphicsPipeline getPipeline(VKContext ctx) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public void recordUpdates(VKContext ctx, VKCommandBuffer cmdbuf) {        
        
        try {
            this.writeLock.acquire();
            
            this.stagingMapping.close();
            this.ptrStaging = null;
            
            cmdbuf.stageImageLayoutTransition(this.tiles, this.tiles.getFullRange(),
                    EnumSet.of(VKPipelineStage.TOP_OF_PIPE), VKImageLayout.GENERAL,
                    EnumSet.of(VKPipelineStage.TRANSFER), VKImageLayout.TRANSFER_DST_OPTIMAL);
            
            cmdbuf.copyBufferToImage(staging, tiles, VKImageLayout.TRANSFER_DST_OPTIMAL, this.imageUpdates);
            
            cmdbuf.stageImageLayoutTransition(this.tiles, this.tiles.getFullRange(),
                    EnumSet.of(VKPipelineStage.TRANSFER), VKImageLayout.TRANSFER_DST_OPTIMAL,
                    EnumSet.of(VKPipelineStage.BOTTOM_OF_PIPE), VKImageLayout.SHADER_READ_ONLY_OPTIMAL);

            this.imageUpdates.clear();
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while recording updates!", ex);
        } finally {
            this.writeLock.release();
        }
    }
    
    @Override
    public void recordDraw(VKContext ctx, VKCommandBuffer cmdbuf) {
        //TODO: bind descriptor set
        
        cmdbuf.bindVertexBuffer(0, this.vSupertileRect, 0L);
        cmdbuf.bindVertexBuffer(1, this.vLayerSelect, 0L);        
        cmdbuf.draw(4, this.totalSupertiles, 0, 0);
    }
    
    @Override
    public int getNeededDescriptorSets() {
        return 1;
    }
    
    @Override
    public int getNeededUniformBufferDescriptors() {
        return 1;
    }
    
    @Override
    public int getNeededImageSamplerDescriptors() {
        return 1;
    }    
}