/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

import com.longlinkislong.gloop2.image.FontImage;
import com.longlinkislong.gloop2.vk.VKBufferCopy;
import com.longlinkislong.gloop2.vk.VKBufferImageCopy;
import com.longlinkislong.gloop2.vk.VKBufferInfo;
import com.longlinkislong.gloop2.vk.VKCommandBuffer;
import com.longlinkislong.gloop2.vk.VKCommandBufferBeginInfo;
import com.longlinkislong.gloop2.vk.VKContext;
import com.longlinkislong.gloop2.vk.VKDescriptorBufferInfo;
import com.longlinkislong.gloop2.vk.VKDescriptorImageInfo;
import com.longlinkislong.gloop2.vk.VKDescriptorPool;
import com.longlinkislong.gloop2.vk.VKDescriptorSet;
import com.longlinkislong.gloop2.vk.VKDescriptorType;
import com.longlinkislong.gloop2.vk.VKFence;
import com.longlinkislong.gloop2.vk.VKFilter;
import com.longlinkislong.gloop2.vk.VKFormat;
import com.longlinkislong.gloop2.vk.VKGraphicsPipeline;
import com.longlinkislong.gloop2.vk.VKImageInfo;
import com.longlinkislong.gloop2.vk.VKImageLayout;
import com.longlinkislong.gloop2.vk.VKImageTiling;
import com.longlinkislong.gloop2.vk.VKImageType;
import com.longlinkislong.gloop2.vk.VKImageUsage;
import com.longlinkislong.gloop2.vk.VKImageView;
import com.longlinkislong.gloop2.vk.VKManagedBuffer;
import com.longlinkislong.gloop2.vk.VKManagedImage;
import com.longlinkislong.gloop2.vk.VKMemoryProperty;
import com.longlinkislong.gloop2.vk.VKPipelineBindPoint;
import com.longlinkislong.gloop2.vk.VKPipelineStage;
import com.longlinkislong.gloop2.vk.VKQueue;
import com.longlinkislong.gloop2.vk.VKSampler;
import com.longlinkislong.gloop2.vk.VKSamplerAddressMode;
import com.longlinkislong.gloop2.vk.VKSamplerInfo;
import com.longlinkislong.gloop2.vk.VKSharingMode;
import com.longlinkislong.gloop2.vk.VKWriteDescriptorSet;
import com.runouw.renderer.Constants;
import com.runouw.renderer.FontLayer;
import com.runouw.renderer.FontLayerInfo;
import com.runouw.renderer.TextInfo;
import static com.runouw.renderer.vk.VKUtil.matrixFlip;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.Semaphore;
import org.lwjgl.system.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class VKFontLayer implements FontLayer, VKLayer {

    private static final Logger LOGGER = LoggerFactory.getLogger(VKFontLayer.class);

    private static final int OFF_UNIFORM = 0;
    private static final int OFF_INDIRECT = 16 * Float.BYTES;
    private static final int OFF_VERTEX = 16 * Float.BYTES + 4 * Integer.BYTES;
    private static final int SIZE_UNIFORM = 16 * Float.BYTES;
    private static final int SIZE_INDIRECT = 4 * Integer.BYTES;
    private static final int BYTES_PER_VERTEX = (2 + 2 + 4) * Float.BYTES;
    private static final int VERTICES_PER_CHARACTER = 6;
    private static final int BYTES_PER_CHARACTER = BYTES_PER_VERTEX * VERTICES_PER_CHARACTER;

    private final FontLayerInfo info;
    private final FloatBuffer projData;
    private final FontImage font;
    private final VKImageView textureView;
    private final VKManagedImage texture;
    private final VKManagedBuffer textBuffer;
    private final VKManagedBuffer cmdBuffer;
    private final VKManagedBuffer projBuffer;
    private final VKManagedBuffer stagingBuffer;
    private boolean verticesChanged = false;
    private boolean projectionChanged = false;
    private boolean drawLimitChanged = false;
    private final Semaphore copyLock = new Semaphore(1);
    private VKManagedBuffer.MappedBuffer stagingMapping;
    private final IntBuffer cmdData;
    private final FloatBuffer textData;
    private VKDescriptorSet descriptorSet;
    private final VKSampler sampler;
    private int characters;
    private int pushCharacters;

    @Override
    public int getMaxDrawLimit() {
        return this.characters;
    }

    @Override
    public void setDrawLimit(final int drawLimit) {
        assert drawLimit <= this.getMaxDrawLimit();

        try {
            this.copyLock.acquire();
            this.checkMapping();

            this.cmdData.put(0, drawLimit * 6)
                    .put(1, 1)
                    .put(2, 0)
                    .put(3, 0);
            this.drawLimitChanged = true;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting draw limit!", ex);
        } finally {
            this.copyLock.release();
        }
    }

    public VKFontLayer(final VKContext ctx, final FontLayerInfo info) throws IOException {
        this.info = info;

        final long textBufferSize = OFF_VERTEX + BYTES_PER_CHARACTER * info.maxCharacters;
        final VKConstants constants = VKConstants.getConstants(ctx);
        final VKQueue stagingQueue = constants.stagingQueue;
        final VKSharingMode sharingMode = stagingQueue.family == ctx.mainQueue.family
                ? VKSharingMode.EXCLUSIVE
                : VKSharingMode.CONCURRENT;

        this.stagingBuffer = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.TRANSFER_SRC)
                .withSize(textBufferSize)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));

        this.textBuffer = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.VERTEX, VKBufferInfo.Usage.TRANSFER_DST)
                .withSize(textBufferSize)
                .withQueueFamilies(ctx.mainQueue.family, stagingQueue.family)
                .withSharingMode(sharingMode)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));

        this.cmdBuffer = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.INDIRECT, VKBufferInfo.Usage.TRANSFER_DST)
                .withSize(SIZE_INDIRECT)
                .withQueueFamilies(ctx.mainQueue.family, stagingQueue.family)
                .withSharingMode(sharingMode)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));

        this.projBuffer = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.UNIFORM, VKBufferInfo.Usage.TRANSFER_DST)
                .withSize(SIZE_UNIFORM)
                .withQueueFamilies(ctx.mainQueue.family, stagingQueue.family)
                .withSharingMode(sharingMode)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));

        try (InputStream inFont = info.fontFile.openStream()) {
            this.font = FontImage.loadASCII(inFont, info.fontPointSize * Constants.PIXELS_PER_POINT);
        }

        final ByteBuffer imgData = this.font.getData();
        final VKManagedBuffer fontStaging = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.TRANSFER_SRC)
                .withSize(imgData.remaining())
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));

        try (VKManagedBuffer.MappedBuffer mapping = fontStaging.map()) {
            mapping.write(imgData);
        } finally {
            this.font.close();
        }

        this.texture = new VKImageInfo()
                .withImageType(VKImageType.IMAGE_2D)
                .withExtent(this.font.getWidth(), this.font.getHeight(), 1)
                .withMipLevels(1)
                .withArrayLayers(1)
                .withFormat(VKFormat.R8_UNORM)
                .withTiling(VKImageTiling.OPTIMAL)
                .withInitialLayout(VKImageLayout.UNDEFINED)
                .withUsage(VKImageUsage.TRANSFER_DST, VKImageUsage.SAMPLED)
                .withSharingMode(VKSharingMode.EXCLUSIVE)
                .withSamples(1)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));

        this.font.close();

        final VKFence uploadComplete = new VKFence(ctx, Collections.emptySet());
        final VKCommandBuffer cmdbuf = constants.initCmdPool.newPrimaryCommandBuffer();

        cmdbuf.begin(VKCommandBufferBeginInfo.PRIMARY_ONE_TIME_USE);

        cmdbuf.fillBuffer(this.cmdBuffer, 0L, SIZE_INDIRECT, 0);
        cmdbuf.fillBuffer(this.projBuffer, 0L, SIZE_UNIFORM, 0);

        cmdbuf.stageImageLayoutTransition(
                texture, texture.getFullRange(),
                EnumSet.of(VKPipelineStage.TOP_OF_PIPE), VKImageLayout.UNDEFINED,
                EnumSet.of(VKPipelineStage.TRANSFER), VKImageLayout.TRANSFER_DST_OPTIMAL);

        cmdbuf.copyBufferToImage(
                fontStaging, texture,
                VKImageLayout.TRANSFER_DST_OPTIMAL,
                new VKBufferImageCopy()
                        .withImageSubresource(texture.getBaseLayer(0))
                        .withImageExtent(font.getWidth(), font.getHeight(), 1));

        cmdbuf.stageImageLayoutTransition(
                texture, texture.getFullRange(),
                EnumSet.of(VKPipelineStage.TRANSFER), VKImageLayout.TRANSFER_DST_OPTIMAL,
                EnumSet.of(VKPipelineStage.FRAGMENT_SHADER), VKImageLayout.SHADER_READ_ONLY_OPTIMAL);

        cmdbuf.end();

        ctx.mainQueue.submit(VKQueue.SubmitInfo.wrap(cmdbuf), uploadComplete);

        VKConstants.ASYNC.submit(() -> {
            try {
                this.copyLock.acquire();
                fontStaging.close();
                uploadComplete.waitFor();
                uploadComplete.close();                
                LOGGER.trace("FontImage uploaded to {}!", VKFontLayer.this);
            } catch (InterruptedException ex) {
                throw new RuntimeException("Interrupted while waiting on upload of FontImage!", ex);
            } finally {
                this.copyLock.release();
            }
        });

        this.textureView = VKImageView.fullView(ctx, texture);

        try (VKManagedBuffer.MappedBuffer mapping = this.stagingBuffer.map()) {
            final long pStagingData = MemoryUtil.memAddress(mapping.getPointer());

            this.projData = MemoryUtil.memFloatBuffer(pStagingData + OFF_UNIFORM, SIZE_UNIFORM);
            this.textData = MemoryUtil.memFloatBuffer(pStagingData + OFF_VERTEX, (int) textBufferSize);
            this.cmdData = MemoryUtil.memIntBuffer(pStagingData + OFF_INDIRECT, SIZE_INDIRECT);
        }

        this.sampler = new VKSamplerInfo()
                .withAddressModeU(VKSamplerAddressMode.CLAMP_TO_EDGE)
                .withAddressModeV(VKSamplerAddressMode.CLAMP_TO_EDGE)
                .withAddressModeW(VKSamplerAddressMode.CLAMP_TO_EDGE)
                .withMagFilter(VKFilter.NEAREST)
                .withMinFilter(VKFilter.NEAREST)
                .withAnisotropyEnable(false)
                .withMaxAnisotropy(1.0F)
                .create(ctx);
    }

    @Override
    public void initDescriptors(VKContext ctx, VKDescriptorPool descPool) {
        final VKConstants constants = VKConstants.getConstants(ctx);

        this.descriptorSet = descPool.allocate(constants.fontPipeline.descriptorSetLayout);

        ctx.device.updateDescriptorSets(
                new VKWriteDescriptorSet[]{
                    new VKWriteDescriptorSet()
                            .withDstSet(descriptorSet)
                            .withDstBinding(0)
                            .withDescriptorType(VKDescriptorType.UNIFORM_BUFFER)
                            .withBufferInfo(new VKDescriptorBufferInfo()
                                    .withBuffer(this.projBuffer)
                                    .withRange(SIZE_UNIFORM)),
                    new VKWriteDescriptorSet()
                            .withDstSet(descriptorSet)
                            .withDstBinding(1)
                            .withDescriptorType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                            .withImageInfo(new VKDescriptorImageInfo()
                                    .withImageLayout(VKImageLayout.SHADER_READ_ONLY_OPTIMAL)
                                    .withImageView(textureView)
                                    .withSampler(sampler))
                }, null);
    }

    private void checkMapping() {
        if (this.stagingMapping == null) {
            this.stagingMapping = this.stagingBuffer.map();
        }
    }

    @Override
    public FontLayerInfo getInfo() {
        return this.info;
    }

    @Override
    public float getAscent() {
        return this.font.ascent;
    }

    @Override
    public float getDescent() {
        return this.font.descent;
    }

    @Override
    public float getLineGap() {
        return this.font.lineGap;
    }

    @Override
    public void pushText(TextInfo info) {
        try {
            this.copyLock.acquire();
            this.checkMapping();

            final List<FontImage.CharSprite> glyphs = this.font.encode(info.x, info.y, info.text);
            final float r = info.color.red;
            final float g = info.color.green;
            final float b = info.color.blue;
            final float a = info.color.alpha;

            for (FontImage.CharSprite glyph : glyphs) {
                this.textData.put(new float[]{
                    glyph.x0, glyph.y0, glyph.s0, glyph.t0, r, g, b, a,
                    glyph.x1, glyph.y0, glyph.s1, glyph.t0, r, g, b, a,
                    glyph.x0, glyph.y1, glyph.s0, glyph.t1, r, g, b, a,
                    glyph.x1, glyph.y0, glyph.s1, glyph.t0, r, g, b, a,
                    glyph.x0, glyph.y1, glyph.s0, glyph.t1, r, g, b, a,
                    glyph.x1, glyph.y1, glyph.s1, glyph.t1, r, g, b, a
                });
            }

            this.pushCharacters += glyphs.size();
            this.verticesChanged = true;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Write interrupted!", ex);
        } finally {
            this.copyLock.release();
        }
    }

    @Override
    public void clear() {
        // clear the draw command
        try {
            this.copyLock.acquire();
            this.checkMapping();

            this.cmdData.put(0, 0).put(1, 0).put(2, 0).put(3, 0);
            this.drawLimitChanged = true;
            this.characters = 0;
            this.pushCharacters = 0;
            this.textData.clear();
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while clearing!", ex);
        } finally {
            this.copyLock.release();
        }
    }

    @Override
    public void onFrameStart() {
    }

    @Override
    public void onFrameEnd() {
    }

    @Override
    public void setProjection(float[] projection) {
        assert projection.length == 16 : "Projection matrix must be 4x4!";

        try {
            this.copyLock.acquire();
            this.checkMapping();           
            
            this.projData.clear();
            this.projData.put(matrixFlip(projection));
            this.projectionChanged = true;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting projection matrix!", ex);
        } finally {
            this.copyLock.release();
        }
    }

    @Override
    public boolean isWritable() {
        return true;
    }

    @Override
    public void close() {
        if (this.stagingMapping != null) {
            this.stagingMapping.close();
        }

        this.cmdBuffer.close();
        this.projBuffer.close();
        this.stagingBuffer.close();
        this.textBuffer.close();

        this.sampler.close();
        this.textureView.close();
        this.texture.close();
    }

    @Override
    public boolean hasUpdates() {
        try {
            this.copyLock.acquire();
            return this.projectionChanged || this.verticesChanged || this.drawLimitChanged;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while checking for changes!", ex);
        } finally {
            this.copyLock.release();
        }
    }

    @Override
    public VKGraphicsPipeline getPipeline(final VKContext ctx) {
        return VKConstants.getConstants(ctx).fontPipeline.pipeline;
    }

    @Override
    public void recordUpdates(VKContext ctx, VKCommandBuffer cmdbuf) {
        try {
            this.copyLock.acquire();

            this.stagingMapping.close();
            this.stagingMapping = null;            

            if (this.verticesChanged) {
                final long dstOffset = this.characters * BYTES_PER_CHARACTER;

                this.characters += this.pushCharacters;
                this.pushCharacters = 0;
                this.textData.flip();

                final long copySize = this.textData.remaining() * Float.BYTES;

                cmdbuf.copyBuffer(this.stagingBuffer, this.textBuffer, new VKBufferCopy()
                        .withSrcOffset(OFF_VERTEX)
                        .withDstOffset(dstOffset)
                        .withSize(copySize));

                this.textData.clear();
                this.verticesChanged = false;
            }

            if (this.projectionChanged) {
                cmdbuf.copyBuffer(this.stagingBuffer, this.projBuffer, new VKBufferCopy()
                        .withSrcOffset(OFF_UNIFORM)
                        .withSize(SIZE_UNIFORM));

                this.projectionChanged = false;
            }

            if (this.drawLimitChanged) {
                cmdbuf.copyBuffer(this.stagingBuffer, this.cmdBuffer, new VKBufferCopy()
                        .withSrcOffset(OFF_INDIRECT)
                        .withSize(SIZE_INDIRECT));

                this.drawLimitChanged = false;
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Copy interrupted!", ex);
        } finally {
            this.copyLock.release();
        }
    }

    @Override
    public void recordDraw(VKContext ctx, VKCommandBuffer cmdbuf) {
        final VKConstants constants = VKConstants.getConstants(ctx);

        cmdbuf.bindVertexBuffer(0, this.textBuffer, 0);
        cmdbuf.bindDescriptorSet(VKPipelineBindPoint.GRAPHICS, constants.fontPipeline.pipelineLayout, 0, descriptorSet);
        cmdbuf.drawIndirect(this.cmdBuffer, 0L, 1, 0);
    }

    @Override
    public int getNeededDescriptorSets() {
        return 1;
    }

    @Override
    public int getNeededUniformBufferDescriptors() {
        return 1;
    }

    @Override
    public int getNeededImageSamplerDescriptors() {
        return 1;
    }
}
