/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

import com.longlinkislong.gloop2.Tools;
import com.longlinkislong.gloop2.vk.VKBlendFactor;
import com.longlinkislong.gloop2.vk.VKBlendOp;
import com.longlinkislong.gloop2.vk.VKCommandPool;
import com.longlinkislong.gloop2.vk.VKCommandPoolInfo;
import com.longlinkislong.gloop2.vk.VKContext;
import com.longlinkislong.gloop2.vk.VKDescriptorSetLayout;
import com.longlinkislong.gloop2.vk.VKDescriptorSetLayoutInfo;
import com.longlinkislong.gloop2.vk.VKDescriptorType;
import com.longlinkislong.gloop2.vk.VKDynamicState;
import com.longlinkislong.gloop2.vk.VKFormat;
import com.longlinkislong.gloop2.vk.VKGraphicsPipeline;
import com.longlinkislong.gloop2.vk.VKGraphicsPipelineInfo;
import com.longlinkislong.gloop2.vk.VKPipelineCache;
import com.longlinkislong.gloop2.vk.VKPipelineCacheInfo;
import com.longlinkislong.gloop2.vk.VKPipelineColorBlendAttachmentState;
import com.longlinkislong.gloop2.vk.VKPipelineColorBlendStateInfo;
import com.longlinkislong.gloop2.vk.VKPipelineDepthStencilStateInfo;
import com.longlinkislong.gloop2.vk.VKPipelineDynamicStateInfo;
import com.longlinkislong.gloop2.vk.VKPipelineInputAssemblyStateInfo;
import com.longlinkislong.gloop2.vk.VKPipelineLayout;
import com.longlinkislong.gloop2.vk.VKPipelineLayoutInfo;
import com.longlinkislong.gloop2.vk.VKPipelineMultisampleStateInfo;
import com.longlinkislong.gloop2.vk.VKPipelineRasterizationStateInfo;
import com.longlinkislong.gloop2.vk.VKPipelineShaderStageInfo;
import com.longlinkislong.gloop2.vk.VKPipelineVertexInputStateInfo;
import com.longlinkislong.gloop2.vk.VKPipelineViewportStateInfo;
import com.longlinkislong.gloop2.vk.VKPushConstantRange;
import com.longlinkislong.gloop2.vk.VKQueue;
import com.longlinkislong.gloop2.vk.VKRenderPass;
import com.longlinkislong.gloop2.vk.VKRenderPassInfo;
import com.longlinkislong.gloop2.vk.VKShaderModule;
import com.longlinkislong.gloop2.vk.VKShaderStage;
import com.longlinkislong.gloop2.vk.VKVertexInputRate;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import org.lwjgl.system.MemoryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class VKConstants {

    private static final Logger LOGGER = LoggerFactory.getLogger(VKConstants.class);
    private static final Path APPDATA;
    private static final Path PIPELINE_CACHE;

    static {
        final String OSNAME = System.getProperty("os.name").toLowerCase();

        if (OSNAME.contains("win")) {
            APPDATA = Paths.get(System.getenv("APPDATA"), "com.runouw", "renderer");
        } else if (OSNAME.contains("nux")) {
            APPDATA = Paths.get(System.getProperty("user.home"), ".local/share/com.runouw", "renderer");

        } else {
            APPDATA = Paths.get(".");
        }

        PIPELINE_CACHE = APPDATA.resolve("PIPELINE_CACHE.bin");

        try {
            Files.createDirectories(APPDATA);
        } catch (IOException ex) {
            throw new RuntimeException("Unable to create APPDATA folder!", ex);
        }

        LOGGER.debug("Pipeline cache: {}", PIPELINE_CACHE);
    }

    private static final String BASE_PROP = "com.runouw.renderer.vk.VKConstants";
    static final ExecutorService ASYNC = Executors.newCachedThreadPool(new ThreadFactory() {
        private final AtomicInteger id = new AtomicInteger(0);

        @Override
        public Thread newThread(Runnable r) {
            final Thread out = new Thread(r);

            out.setName("Vulkan Async Worker " + id.getAndIncrement());

            return out;
        }
    });

    static final class ImagePipeline {

        private static final String VERTEX_SHADER_URL = System.getProperty(BASE_PROP + ".ImagePipeline.vertex_shader_url", "ImageLayer.vert.spirv");
        private static final String FRAGMENT_SHADER_URL = System.getProperty(BASE_PROP + ".ImagePipeline.fragment_shader_url", "ImageLayer.frag.spirv");

        final VKDescriptorSetLayout descriptorSetLayout;
        final VKGraphicsPipeline pipeline;
        final VKPipelineLayout pipelineLayout;
        final VKShaderModule vsh;
        final VKShaderModule fsh;

        ImagePipeline(final VKContext ctx, final VKPipelineCache cache, final VKGraphicsPipelineInfo basePipelineInfo) throws IOException {
            try (InputStream vin = VKConstants.class.getResourceAsStream(VERTEX_SHADER_URL)) {
                this.vsh = Tools.withStreamData(vin, data -> new VKShaderModule(ctx, data));
            }

            try (InputStream fin = VKConstants.class.getResourceAsStream(FRAGMENT_SHADER_URL)) {
                this.fsh = Tools.withStreamData(fin, data -> new VKShaderModule(ctx, data));
            }

            this.descriptorSetLayout = new VKDescriptorSetLayoutInfo()
                    .withBindings(
                            new VKDescriptorSetLayoutInfo.Binding()
                                    .withBinding(0)
                                    .withDescriptorCount(1)
                                    .withDescriptorType(VKDescriptorType.UNIFORM_BUFFER)
                                    .withStageFlags(VKShaderStage.VERTEX),
                            new VKDescriptorSetLayoutInfo.Binding()
                                    .withBinding(1)
                                    .withDescriptorCount(1)
                                    .withDescriptorType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                                    .withStageFlags(VKShaderStage.FRAGMENT),
                            new VKDescriptorSetLayoutInfo.Binding()
                                    .withBinding(2)
                                    .withDescriptorCount(1)
                                    .withDescriptorType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                                    .withStageFlags(VKShaderStage.FRAGMENT))
                    .create(ctx);

            this.pipelineLayout = new VKPipelineLayoutInfo()
                    .withPushConstantRanges(new VKPushConstantRange()
                            .withSize(2 * Integer.BYTES)
                            .withStageFlags(VKShaderStage.VERTEX, VKShaderStage.FRAGMENT))
                    .withSetLayouts(this.descriptorSetLayout)
                    .create(ctx);

            VKGraphicsPipelineInfo pipelineInfo = basePipelineInfo
                    .withAssemblyState(VKPipelineInputAssemblyStateInfo.DEFAULT_TRIANGLE_STRIP)
                    .withLayout(pipelineLayout)
                    .withStages(
                            VKPipelineShaderStageInfo.vertexShader(vsh),
                            VKPipelineShaderStageInfo.fragmentShader(fsh))
                    .withVertexInputState(new VKPipelineVertexInputStateInfo());

            this.pipeline = new VKGraphicsPipeline(ctx, pipelineInfo, cache);
        }
    }

    static final class JFXPipeline {

        private static final String VERTEX_SHADER_URL = System.getProperty(BASE_PROP + ".JFXPipeline.vertex_shader_url", "JFXLayer.vert.spirv");
        private static final String FRAGMENT_SHADER_URL = System.getProperty(BASE_PROP + ".JFXPipeline.fragment_shader_url", "JFXLayer.frag.spirv");

        final VKDescriptorSetLayout descriptorSetLayout;
        final VKGraphicsPipeline pipeline;
        final VKPipelineLayout pipelineLayout;
        final VKShaderModule vsh;
        final VKShaderModule fsh;

        JFXPipeline(final VKContext ctx, final VKPipelineCache cache, final VKGraphicsPipelineInfo basePipelineInfo) throws IOException {
            try (InputStream vin = VKConstants.class.getResourceAsStream(VERTEX_SHADER_URL)) {
                this.vsh = Tools.withStreamData(vin, data -> new VKShaderModule(ctx, data));
            }

            try (InputStream fin = VKConstants.class.getResourceAsStream(FRAGMENT_SHADER_URL)) {
                this.fsh = Tools.withStreamData(fin, data -> new VKShaderModule(ctx, data));
            }

            this.descriptorSetLayout = new VKDescriptorSetLayoutInfo()
                    .withBindings(
                            new VKDescriptorSetLayoutInfo.Binding()
                                    .withBinding(0)
                                    .withDescriptorCount(1)
                                    .withDescriptorType(VKDescriptorType.UNIFORM_BUFFER)
                                    .withStageFlags(VKShaderStage.VERTEX),
                            new VKDescriptorSetLayoutInfo.Binding()
                                    .withBinding(1)
                                    .withDescriptorCount(1)
                                    .withDescriptorType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                                    .withStageFlags(VKShaderStage.FRAGMENT))
                    .create(ctx);
            
            this.pipelineLayout = new VKPipelineLayoutInfo()
                    .withSetLayouts(this.descriptorSetLayout)
                    .create(ctx);
            
            final VKGraphicsPipelineInfo pipelineInfo = basePipelineInfo
                    .withAssemblyState(VKPipelineInputAssemblyStateInfo.DEFAULT_TRIANGLE_STRIP)
                    .withLayout(pipelineLayout)
                    .withStages(
                            VKPipelineShaderStageInfo.vertexShader(vsh),
                            VKPipelineShaderStageInfo.fragmentShader(fsh))
                    .withVertexInputState(new VKPipelineVertexInputStateInfo());

            this.pipeline = new VKGraphicsPipeline(ctx, pipelineInfo, cache);
        }
    }

    static final class FontPipeline {

        private static final String VERTEX_SHADER_URL = System.getProperty(BASE_PROP + ".FontPipeline.vertex_shader_url", "FontLayer.vert.spirv");
        private static final String FRAGMENT_SHADER_URL = System.getProperty(BASE_PROP + ".FontPipeline.fragment_shader_url", "FontLayer.frag.spirv");

        final VKDescriptorSetLayout descriptorSetLayout;
        final VKGraphicsPipeline pipeline;
        final VKPipelineLayout pipelineLayout;
        final VKShaderModule vsh;
        final VKShaderModule fsh;

        FontPipeline(final VKContext ctx, final VKPipelineCache cache, final VKGraphicsPipelineInfo basePipelineInfo) throws IOException {

            try (InputStream vin = VKConstants.class.getResourceAsStream(VERTEX_SHADER_URL)) {
                this.vsh = Tools.withStreamData(vin, data -> new VKShaderModule(ctx, data));
            }

            try (InputStream fin = VKConstants.class.getResourceAsStream(FRAGMENT_SHADER_URL)) {
                this.fsh = Tools.withStreamData(fin, data -> new VKShaderModule(ctx, data));
            }

            this.descriptorSetLayout = new VKDescriptorSetLayoutInfo()
                    .withBindings(
                            new VKDescriptorSetLayoutInfo.Binding()
                                    .withBinding(0)
                                    .withDescriptorCount(1)
                                    .withDescriptorType(VKDescriptorType.UNIFORM_BUFFER)
                                    .withStageFlags(VKShaderStage.VERTEX),
                            new VKDescriptorSetLayoutInfo.Binding()
                                    .withBinding(1)
                                    .withDescriptorCount(1)
                                    .withDescriptorType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
                                    .withStageFlags(VKShaderStage.FRAGMENT))
                    .create(ctx);

            this.pipelineLayout = new VKPipelineLayoutInfo()
                    .withSetLayouts(this.descriptorSetLayout)
                    .create(ctx);

            final VKGraphicsPipelineInfo pipelineInfo = basePipelineInfo
                    .withAssemblyState(VKPipelineInputAssemblyStateInfo.DEFAULT_TRIANGLE_LIST)
                    .withLayout(this.pipelineLayout)
                    .withStages(
                            VKPipelineShaderStageInfo.vertexShader(vsh),
                            VKPipelineShaderStageInfo.fragmentShader(fsh))
                    .withVertexInputState(new VKPipelineVertexInputStateInfo()
                            .withBindingDescriptions(new VKPipelineVertexInputStateInfo.BindingDescription()
                                    .withBinding(0)
                                    .withStride((4 + 4) * Float.BYTES)
                                    .withInputRate(VKVertexInputRate.VERTEX))
                            .withAttributeDescriptions(
                                    new VKPipelineVertexInputStateInfo.AttributeDescription()
                                            .withBinding(0)
                                            .withLocation(0)
                                            .withFormat(VKFormat.VEC2)
                                            .withOffset(0),
                                    new VKPipelineVertexInputStateInfo.AttributeDescription()
                                            .withBinding(0)
                                            .withLocation(1)
                                            .withFormat(VKFormat.VEC2)
                                            .withOffset(2 * Float.BYTES),
                                    new VKPipelineVertexInputStateInfo.AttributeDescription()
                                            .withBinding(0)
                                            .withLocation(2)
                                            .withFormat(VKFormat.VEC4)
                                            .withOffset(4 * Float.BYTES)));

            this.pipeline = new VKGraphicsPipeline(ctx, pipelineInfo, cache);
        }
    }

    final VKRenderPass presentRenderpass;
    final JFXPipeline jfxPipeline;
    final FontPipeline fontPipeline;
    final ImagePipeline imagePipeline;
    final VKQueue stagingQueue;
    final VKCommandPool stagingCmdPool;
    final VKCommandPool initCmdPool;
    final VKGraphicsPipelineInfo baseInfo;
    final VKPipelineCache pipelineCache;

    public void dumpPipelineCache() throws IOException {
        Files.deleteIfExists(PIPELINE_CACHE);
        ByteBuffer tmp = null;

        try (FileChannel fc = FileChannel.open(PIPELINE_CACHE, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW)) {
            final long size = this.pipelineCache.getCacheDatasize();

            tmp = MemoryUtil.memAlloc((int) size);

            this.pipelineCache.getCacheData(tmp);
            fc.write(tmp);
        } finally {
            if (tmp != null) {
                MemoryUtil.memFree(tmp);
            }
        }
    }

    VKConstants(final VKContext ctx) throws IOException {
        final List<VKRenderPassInfo.Attachment> presentAttachments = ctx.getPresentAttachments();

        if (Files.exists(PIPELINE_CACHE)) {
            LOGGER.debug("Loading pipeline cache from file!");

            final Path readCopy = APPDATA.resolve("PIPELINE_CACHE.tmp");

            Files.copy(PIPELINE_CACHE, readCopy, StandardCopyOption.REPLACE_EXISTING);

            try (FileChannel fc = FileChannel.open(readCopy, StandardOpenOption.READ, StandardOpenOption.WRITE)) {
                this.pipelineCache = new VKPipelineCacheInfo()
                        .withInitialData(fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size()))
                        .create(ctx);
            }
        } else {
            this.pipelineCache = new VKPipelineCacheInfo().create(ctx);
        }

        this.presentRenderpass = VKRenderPassInfo.infer(presentAttachments).create(ctx);

        this.stagingQueue = ctx.device.queues()
                .filter(q -> q.family.canTransfer())
                .filter(q -> !q.family.canDraw())
                .filter(q -> !q.family.canCompute())
                .findFirst()
                .orElse(ctx.mainQueue);

        this.stagingCmdPool = new VKCommandPoolInfo()
                .withFlags(VKCommandPoolInfo.CreateFlag.TRANSIENT, VKCommandPoolInfo.CreateFlag.RESET_COMMAND_BUFFER)
                .withQueueFamily(this.stagingQueue.family)
                .create(ctx);

        this.initCmdPool = new VKCommandPoolInfo()
                .withFlags(VKCommandPoolInfo.CreateFlag.TRANSIENT)
                .withQueueFamily(ctx.mainQueue.family)
                .create(ctx);

        this.baseInfo = new VKGraphicsPipelineInfo()
                .withDynamicState(new VKPipelineDynamicStateInfo().withDynamicStates(VKDynamicState.VIEWPORT, VKDynamicState.SCISSOR))
                .withColorBlendState(new VKPipelineColorBlendStateInfo()
                        .withAttachments(new VKPipelineColorBlendAttachmentState()
                                .withBlendEnable(true)
                                .withAlphaBlendOp(VKBlendOp.ADD)
                                .withColorBlendOp(VKBlendOp.ADD)
                                .withSrcAlphaBlendFactor(VKBlendFactor.ONE)
                                .withDstAlphaBlendFactor(VKBlendFactor.ONE_MINUS_SRC_ALPHA)
                                .withSrcColorBlendFactor(VKBlendFactor.ONE)
                                .withDstColorBlendFactor(VKBlendFactor.ONE_MINUS_SRC_ALPHA)))
                .withDepthStencilState(VKPipelineDepthStencilStateInfo.DEFAULT_OPENGL_DEPTH_TEST)
                .withMultisampleState(new VKPipelineMultisampleStateInfo().withRasterizationSamples(1))
                .withRasterizationState(VKPipelineRasterizationStateInfo.DEFAULT_OPENGL)
                .withRenderPass(this.presentRenderpass)
                .withSubpass(0)
                .withViewportState(VKPipelineViewportStateInfo.dynamicViewports(1));

        this.fontPipeline = new FontPipeline(ctx, this.pipelineCache, this.baseInfo);
        this.imagePipeline = new ImagePipeline(ctx, this.pipelineCache, this.baseInfo);
        this.jfxPipeline = new JFXPipeline(ctx, this.pipelineCache, this.baseInfo);
    }

    private static final Map<VKContext, VKConstants> CONSTANTS = new HashMap<>();
    private static final Semaphore CONSTANT_LOCK = new Semaphore(1);

    public static VKConstants getConstants(final VKContext ctx) {
        try {
            CONSTANT_LOCK.acquire();

            VKConstants out = CONSTANTS.get(ctx);

            if (out == null) {
                out = new VKConstants(ctx);
                CONSTANTS.put(ctx, out);
            }

            return out;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Constant retrieval was interrupted!", ex);
        } catch (IOException ex) {
            throw new RuntimeException("Unable to load shader(s)!", ex);
        } finally {
            CONSTANT_LOCK.release();
        }
    }
}
