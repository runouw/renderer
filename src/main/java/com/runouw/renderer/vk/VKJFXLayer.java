/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

import com.longlinkislong.gloop2.vk.VKAccess;
import com.longlinkislong.gloop2.vk.VKBufferImageCopy;
import com.longlinkislong.gloop2.vk.VKBufferInfo;
import com.longlinkislong.gloop2.vk.VKCommandBuffer;
import com.longlinkislong.gloop2.vk.VKContext;
import com.longlinkislong.gloop2.vk.VKDescriptorBufferInfo;
import com.longlinkislong.gloop2.vk.VKDescriptorImageInfo;
import com.longlinkislong.gloop2.vk.VKDescriptorPool;
import com.longlinkislong.gloop2.vk.VKDescriptorSet;
import com.longlinkislong.gloop2.vk.VKDescriptorType;
import com.longlinkislong.gloop2.vk.VKFilter;
import com.longlinkislong.gloop2.vk.VKFormat;
import com.longlinkislong.gloop2.vk.VKGraphicsPipeline;
import com.longlinkislong.gloop2.vk.VKImageInfo;
import com.longlinkislong.gloop2.vk.VKImageLayout;
import com.longlinkislong.gloop2.vk.VKImageMemoryBarrier;
import com.longlinkislong.gloop2.vk.VKImageSubresourceLayers;
import com.longlinkislong.gloop2.vk.VKImageTiling;
import com.longlinkislong.gloop2.vk.VKImageType;
import com.longlinkislong.gloop2.vk.VKImageUsage;
import com.longlinkislong.gloop2.vk.VKImageView;
import com.longlinkislong.gloop2.vk.VKManagedBuffer;
import com.longlinkislong.gloop2.vk.VKManagedImage;
import com.longlinkislong.gloop2.vk.VKMemoryProperty;
import com.longlinkislong.gloop2.vk.VKPipelineBarrier;
import com.longlinkislong.gloop2.vk.VKPipelineBindPoint;
import com.longlinkislong.gloop2.vk.VKPipelineStage;
import com.longlinkislong.gloop2.vk.VKSampler;
import com.longlinkislong.gloop2.vk.VKSamplerAddressMode;
import com.longlinkislong.gloop2.vk.VKSamplerInfo;
import com.longlinkislong.gloop2.vk.VKWriteDescriptorSet;
import com.runouw.renderer.AbstractJFXLayer;
import com.runouw.renderer.JFXLayerInfo;
import java.nio.IntBuffer;
import java.util.EnumSet;

/**
 *
 * @author zmichaels
 */
public final class VKJFXLayer extends AbstractJFXLayer implements VKLayer {

    private final VKManagedBuffer uniforms;
    private final VKManagedBuffer staging;
    private VKManagedBuffer.MappedBuffer stagingMapping;
    private final VKManagedImage raster;
    private final VKImageView rasterView;
    private final VKSampler rasterSampler;
    private VKDescriptorSet descriptorSet;
    private boolean rasterUpdate = false;

    public VKJFXLayer(final VKContext ctx, final JFXLayerInfo info) {
        super(info);

        if (info.initialScene != null) {
            this.setScene(info.initialScene);
        }

        this.raster = new VKImageInfo()
                .withExtent(info.width, info.height, 1)
                .withArrayLayers(1)
                .withFormat(VKFormat.R8G8B8A8_UNORM)
                .withInitialLayout(VKImageLayout.UNDEFINED)
                .withTiling(VKImageTiling.OPTIMAL)
                .withSamples(1)
                .withMipLevels(1)
                .withImageType(VKImageType.IMAGE_2D)
                .withUsage(VKImageUsage.TRANSFER_DST, VKImageUsage.SAMPLED)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.DEVICE_LOCAL));

        this.staging = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.TRANSFER_SRC)
                .withSize(info.width * info.height * Integer.BYTES)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));

        this.uniforms = new VKBufferInfo()
                .withUsage(VKBufferInfo.Usage.UNIFORM)
                .withSize(16 * Float.BYTES)
                .createManaged(ctx, EnumSet.of(VKMemoryProperty.HOST_VISIBLE, VKMemoryProperty.HOST_COHERENT));

        this.rasterView = VKImageView.fullView(ctx, raster);
        this.rasterSampler = new VKSamplerInfo()
                .withAddressModeU(VKSamplerAddressMode.CLAMP_TO_EDGE)
                .withAddressModeV(VKSamplerAddressMode.CLAMP_TO_EDGE)
                .withAnisotropyEnable(false)
                .withMaxAnisotropy(1.0F)
                .withMagFilter(VKFilter.LINEAR)
                .withMinFilter(VKFilter.LINEAR)
                .create(ctx);
    }

    @Override
    protected IntBuffer getImageTransferBuffer() {
        this.stagingMapping = this.staging.map();

        return this.stagingMapping.getPointer().asIntBuffer();
    }

    @Override
    protected void applyImageTransfer() {
        this.stagingMapping.close();
        this.rasterUpdate = true;
    }

    @Override
    public void onFrameStart() {
    }

    @Override
    public void setProjection(float[] projection) {
        try (VKManagedBuffer.MappedBuffer mapping = this.uniforms.map()) {
            mapping.getPointer().asFloatBuffer().put(projection);
        }
    }

    @Override
    public boolean hasUpdates() {
        return this.rasterUpdate;
    }

    @Override
    public void initDescriptors(VKContext ctx, VKDescriptorPool descriptorPool) {
        final VKConstants constants = VKConstants.getConstants(ctx);

        this.descriptorSet = descriptorPool.allocate(constants.jfxPipeline.descriptorSetLayout);

        final VKWriteDescriptorSet[] writes = {
            new VKWriteDescriptorSet()
            .withDstSet(this.descriptorSet)
            .withDstBinding(0)
            .withDescriptorType(VKDescriptorType.UNIFORM_BUFFER)
            .withBufferInfo(new VKDescriptorBufferInfo()
            .withBuffer(this.uniforms)
            .withRange(16 * Float.BYTES)),
            new VKWriteDescriptorSet()
            .withDstSet(this.descriptorSet)
            .withDstBinding(1)
            .withDescriptorType(VKDescriptorType.COMBINED_IMAGE_SAMPLER)
            .withImageInfo(new VKDescriptorImageInfo()
            .withImageLayout(VKImageLayout.SHADER_READ_ONLY_OPTIMAL)
            .withImageView(this.rasterView)
            .withSampler(this.rasterSampler))
        };

        ctx.device.updateDescriptorSets(writes, null);
    }

    @Override
    public VKGraphicsPipeline getPipeline(VKContext ctx) {
        return VKConstants.getConstants(ctx).jfxPipeline.pipeline;
    }

    @Override
    public void recordUpdates(VKContext ctx, VKCommandBuffer cmdbuf) {
        final JFXLayerInfo info = this.getInfo();

        cmdbuf.stageImageLayoutTransition(this.raster, this.raster.getFullRange(),
                EnumSet.of(VKPipelineStage.TOP_OF_PIPE), VKImageLayout.UNDEFINED,
                EnumSet.of(VKPipelineStage.TRANSFER), VKImageLayout.TRANSFER_DST_OPTIMAL);

        cmdbuf.copyBufferToImage(this.staging, this.raster, VKImageLayout.TRANSFER_DST_OPTIMAL, new VKBufferImageCopy()
                .withImageExtent(info.width, info.height, 1)
                .withImageSubresource(this.raster.getBaseLayer(0)));

        cmdbuf.pipelineBarrier(new VKPipelineBarrier()
                .withSrcStageMask(VKPipelineStage.TRANSFER)
                .withDstStageMask(VKPipelineStage.BOTTOM_OF_PIPE)
                .withImageMemoryBarriers(new VKImageMemoryBarrier()
                        .withSrcAccessMask(VKAccess.TRANSFER_WRITE)
                        .withDstAccessMask(VKAccess.MEMORY_READ)
                        .withImage(this.raster)
                        .withOldLayout(VKImageLayout.TRANSFER_DST_OPTIMAL)
                        .withNewLayout(VKImageLayout.SHADER_READ_ONLY_OPTIMAL)
                        .withSubresourceRange(this.raster.getFullRange())));        

        this.rasterUpdate = false;
    }

    @Override
    public void recordDraw(VKContext ctx, VKCommandBuffer cmdbuf) {
        cmdbuf.bindDescriptorSet(VKPipelineBindPoint.GRAPHICS, VKConstants.getConstants(ctx).jfxPipeline.pipelineLayout, 0, this.descriptorSet);
        cmdbuf.draw(4, 1, 0, 0);
    }

    @Override
    public int getNeededDescriptorSets() {
        return 1;
    }

    @Override
    public int getNeededUniformBufferDescriptors() {
        return 1;
    }

    @Override
    public int getNeededImageSamplerDescriptors() {
        return 1;
    }

    @Override
    public void close() {
        super.close();

        if (this.stagingMapping != null) {
            this.stagingMapping.close();
            this.stagingMapping = null;
        }

        this.staging.close();
        this.rasterView.close();
        this.raster.close();
        this.rasterSampler.close();
    }
}
