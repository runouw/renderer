/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer.vk;

import com.longlinkislong.gloop2.vk.VKCommandBuffer;
import com.longlinkislong.gloop2.vk.VKContext;
import com.longlinkislong.gloop2.vk.VKDescriptorPool;
import com.longlinkislong.gloop2.vk.VKGraphicsPipeline;
import com.runouw.renderer.Layer;

/**
 *
 * @author zmichaels
 */
public interface VKLayer extends Layer {
    boolean hasUpdates();
    
    void initDescriptors(VKContext ctx, VKDescriptorPool descriptorPool);
    
    VKGraphicsPipeline getPipeline(VKContext ctx);
    
    void recordUpdates(VKContext ctx, VKCommandBuffer cmdbuf);
    
    void recordDraw(VKContext ctx, VKCommandBuffer cmdbuf);
    
    int getNeededDescriptorSets();
    
    int getNeededUniformBufferDescriptors();
    
    int getNeededImageSamplerDescriptors();
}
