/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.sun.javafx.embed.EmbeddedSceneDSInterface;
import com.sun.javafx.embed.EmbeddedSceneDTInterface;
import com.sun.javafx.embed.EmbeddedSceneInterface;
import com.sun.javafx.embed.HostDragStartListener;
import com.sun.javafx.tk.Toolkit;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Semaphore;
import javafx.scene.input.TransferMode;

/**
 *
 * @author zmichaels
 */
final class JFXDNDHandler {

    private static final class DropInfo {

        private final EmbeddedSceneDSInterface dragSource;
        private final TransferMode dragAction;

        private DropInfo(final EmbeddedSceneDSInterface dragSource, final TransferMode dragAction) {
            this.dragAction = dragAction;
            this.dragSource = dragSource;
        }
    }

    private static final class DropHandlerData {

        private int x, y, sx, sy;
        private final EmbeddedSceneDTInterface dt;

        private DropHandlerData(final EmbeddedSceneDTInterface dropTarget) {
            this.dt = dropTarget;
        }

        private void handleDragLeave() {
            dt.handleDragLeave();
        }

        private void handleDragEnter() {
            dt.handleDragEnter(x, y, sx, sy, STATIC_DROP_INFO.dragAction, STATIC_DROP_INFO.dragSource);
        }

        private void handleDragOver() {
            dt.handleDragOver(x, y, sx, sy, STATIC_DROP_INFO.dragAction);
        }

        private TransferMode handleDragDrop() {
            return dt.handleDragDrop(x, y, x, y, STATIC_DROP_INFO.dragAction);
        }
    }

    private static DropInfo STATIC_DROP_INFO;
    private static final Semaphore STATIC_CONTENT_LOCK = new Semaphore(1);
    private static final List<DropHandlerData> DROP_TARGETS = new ArrayList<>();

    private final WeakReference<EmbeddedSceneInterface> scene;
    private DropInfo dropInfo;
    private EmbeddedSceneDTInterface dropTarget;
    private final HostDragStartListener dragStartListener = (dragSource, dragAction) -> {
        if (!Toolkit.getToolkit().isFxUserThread()) {
            throw new IllegalStateException("Drag operation outside of JavaFX thread!");
        }

        Objects.requireNonNull(dragSource, "DragSource cannot be null!");

        this.dropInfo = new DropInfo(dragSource, dragAction);

        try {
            STATIC_CONTENT_LOCK.acquire();
            STATIC_DROP_INFO = this.dropInfo;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while updating static drop info!", ex);
        } finally {
            STATIC_CONTENT_LOCK.release();
        }
    };

    JFXDNDHandler(final EmbeddedSceneInterface scene) {
        this.scene = new WeakReference<>(scene);

        scene.setDragStartListener(this.dragStartListener);
    }

    void updateMousePosition(final int x, final int y, final int sx, final int sy) {
        try {
            STATIC_CONTENT_LOCK.acquire();
            if (STATIC_DROP_INFO != null) {
                final DropHandlerData dt = DROP_TARGETS.stream()
                        .filter(data -> data.dt == this.dropTarget)
                        .findFirst()
                        .orElseGet(() -> {
                            this.dropTarget = this.scene.get().createDropTarget();

                            final DropHandlerData out = new DropHandlerData(this.dropTarget);

                            DROP_TARGETS.add(out);

                            return out;
                        });

                dt.x = x;
                dt.y = y;
                dt.sx = sx;
                dt.sy = sy;

                dt.handleDragLeave();
                dt.handleDragEnter();
                dt.handleDragOver();
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while processing mouse move!", ex);
        } finally {
            STATIC_CONTENT_LOCK.release();
        }
    }
    
    void updateMouseReleased(final int x, final int y, final int sx, final int sy) {
        try {
            STATIC_CONTENT_LOCK.acquire();
            
            if (STATIC_DROP_INFO != null && this.dropInfo == STATIC_DROP_INFO) {
                TransferMode finalMode = null;
                
                for (DropHandlerData dt : DROP_TARGETS) {
                    TransferMode newMode = dt.handleDragDrop();
                    
                    if (newMode != null) {
                        finalMode = newMode;
                    }
                }
                
                STATIC_DROP_INFO.dragSource.dragDropEnd(finalMode);
                
                this.dropInfo = null;
                STATIC_DROP_INFO = null;
                DROP_TARGETS.clear();
            }
                        
            this.dropTarget = null;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while processing mouse release!", ex);
        } finally {
            STATIC_CONTENT_LOCK.release();
        }
    }
}
