/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.longlinkislong.gloop2.Tools;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Objects;

/**
 * An immutable information structure used to construct an instance of ImageDB.
 *
 * @author zmichaels
 */
public final class ImageDBInfo {

    /**
     * The name of the cache. This will be the path to the database folder.
     * Default is ".imagecache".
     */
    public final Path cacheName;
    /**
     * The maximum cache size. Default is 100MB.
     */
    public final long size;

    /**
     * Constructs a new instance of ImageDBInfo using the default values.
     */
    public ImageDBInfo() {
        this(Tools.APPDATA.resolve(".imagecache"), 100 * 1024 * 1024);
    }

    /**
     * Constructs a new instance of ImageDBInfo using the supplied values.
     *
     * @param cacheName the name of the cache.
     * @param size the maximum cache size in bytes.
     */
    public ImageDBInfo(final Path cacheName, final long size) {
        this.cacheName = cacheName;
        this.size = size;
    }

    /**
     * Creates a new instance of ImageDBInfo with the new cache name.
     *
     * @param cacheName the new cache name.
     * @return the new instance of ImageDBInfo.
     */
    public ImageDBInfo withCacheName(final Path cacheName) {
        return new ImageDBInfo(cacheName, size);
    }

    /**
     * Creates a new instance of ImageDBInfo with the new maximum cache size.
     *
     * @param size the new maximum cache size.
     * @return the new instance of ImageDBInfo.
     */
    public ImageDBInfo withSize(final long size) {
        return new ImageDBInfo(cacheName, size);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (obj == this) {
            return true;
        } else if (obj instanceof ImageDBInfo) {
            return ((ImageDBInfo) obj).cacheName.equals(this.cacheName)
                    && ((ImageDBInfo) obj).size == this.size;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.cacheName);
        hash = 79 * hash + (int) (this.size ^ (this.size >>> 32));
        return hash;
    }

    /**
     * Creates a new instance of ImageDB. This will either create a new database
     * or load a previously created database.
     *
     * @return the instance of ImageDB.
     * @throws IOException if the load or create operation fails.
     */
    public ImageDB create() throws IOException {
        return new ImageDB(this);
    }
}
