/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

/**
 *
 * @author zmichaels
 */
public final class ImageView {
    public static final ImageView BLANK = new ImageView(-1, 0F, 0F, 0F, 0F);
    
    public final int index;
    public final float s0;
    public final float t0;
    public final float s1;
    public final float t1;
    
    public ImageView(final int index, final float s0, final float t0, final float s1, final float t1) {
        this.index = index;
        this.s0 = s0;
        this.t0 = t0;
        this.s1 = s1;
        this.t1 = t1;
    }
}
