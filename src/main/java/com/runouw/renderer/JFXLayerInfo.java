/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import com.sun.javafx.application.PlatformImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public final class JFXLayerInfo implements LayerInfo {

    private static final Logger LOGGER = LoggerFactory.getLogger(JFXLayerInfo.class);

    static {
        PlatformImpl.startup(() -> LOGGER.debug("JavaFX initialized!"));
    }
    public final int width;
    public final int height;
    public final javafx.scene.Scene initialScene;

    public JFXLayerInfo(final int width, final int height, final javafx.scene.Scene initialScene) {
        this.width = width;
        this.height = height;
        this.initialScene = initialScene;
    }

    public JFXLayerInfo() {
        this(0, 0, null);
    }

    public JFXLayerInfo withExtent(final int width, final int height) {
        return new JFXLayerInfo(width, height, initialScene);
    }

    public JFXLayerInfo withInitialScene(final javafx.scene.Scene initialScene) {
        return new JFXLayerInfo(width, height, initialScene);
    }
}
