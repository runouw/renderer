/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.renderer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class SoundLayer implements Layer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SoundLayer.class);
    private final SoundLayerInfo info;

    final Semaphore lock = new Semaphore(1);
    float gain = 1F;
    float pitch = 1F;

    private final List<Sound> sounds = new ArrayList<>();
    final float[] projection = new float[16];
    private long lastTime = -1L;

    /**
     * Constructs a new SoundLayer
     *
     * @param info immutable construction information for the SoundLayer
     */
    public SoundLayer(final SoundLayerInfo info) {
        this.info = info;
    }

    /**
     * Sets the SoundLayer's gain multiplier. Each Sound contained by this
     * SoundLayer has its gain multiplied by this value. Default value is 1.0.
     *
     * @param gain the gain multiplier.
     */
    public void setGain(final float gain) {
        try {
            this.lock.acquire();
            this.gain = gain;
            this.sounds.forEach(sound -> sound.gainChanged = true);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting layer gain!", ex);
        } finally {
            this.lock.release();
        }
    }

    /**
     * Retrieves the SoundLayer's gain multiplier.
     *
     * @return the gain multiplier.
     */
    public float getGain() {
        try {
            this.lock.acquire();
            return this.gain;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while getting layer gain!", ex);
        } finally {
            this.lock.release();
        }
    }

    /**
     * Sets the SoundLayer's pitch multiplier. Each Sound contained by this
     * SoundLayer has its pitch multiplied by this value. Default value is 1.0.
     *
     * @param pitch the pitch multiplier.
     */
    public void setPitch(final float pitch) {
        try {
            this.lock.acquire();
            this.pitch = pitch;
            this.sounds.forEach(sound -> sound.pitchChanged = true);
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting layer pitch!", ex);
        } finally {
            this.lock.release();
        }
    }

    /**
     * Retrieves the SoundLayer's pitch multiplier.
     *
     * @return the pitch multiplier
     */
    public float getPitch() {
        try {
            this.lock.acquire();
            return this.pitch;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while getting layer pitch!", ex);
        } finally {
            this.lock.release();
        }
    }

    /**
     * Disposes of a Sound owned by this sound layer. This will clear all
     * resources held by the Sound. Its probably better to just recycle Sound
     * objects.
     *
     * @param sound the Sound object.
     */
    public void dispose(final Sound sound) {
        try {
            this.lock.acquire();
            this.sounds.remove(sound);
            sound.active = false;
            sound.close();
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while disposing of Sound!", ex);
        } finally {
            this.lock.release();
        }
    }

    /**
     * Opens a new Sound object.
     *
     * @param info the immutable construction information for the Sound object.
     * @return the new Sound object.
     */
    public Sound open(final SoundInfo info) {
        try {
            this.lock.acquire();

            final Sound out = new Sound(this, info);

            sounds.add(out);

            return out;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while opening new Sound!", ex);
        } finally {
            this.lock.release();
        }
    }

    @Override
    public void setProjection(float[] projection) {
        try {
            this.lock.acquire();
            System.arraycopy(projection, 0, this.projection, 0, 16);
            this.sounds.forEach(sound -> sound.positionChanged = true);
            //TODO: transform positions by projection matrix
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while setting projection matrix!", ex);
        } finally {
            this.lock.release();
        }
    }

    @Override
    public void close() {
        try {
            this.lock.acquire();
            this.sounds.forEach(Sound::close);
            this.sounds.clear();
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while freeing SoundLayer resources!", ex);
        } finally {
            this.lock.release();
        }
    }

    @Override
    public void onFrameStart() {

    }

    @Override
    public void onFrameEnd() {
        try {
            this.lock.acquire();

            if (this.lastTime == -1L) {
                this.lastTime = System.currentTimeMillis();
            } else {
                final long now = System.currentTimeMillis();
                final float dT = (now - this.lastTime) * 0.001F;

                this.lastTime = now;
                this.sounds.forEach(sound -> sound.time += dT);
            }

            // deactivate all sounds that are active, but stopped
            // this happens when a sound is marked as "Playing" and finishes streaming
            this.sounds.stream()
                    .filter(sound -> sound.active)
                    .filter(sound -> sound.state == Sound.State.STOPPED)
                    .forEach(sound -> sound.deactivate());

            // retreive the total number of active sounds.
            int activeId = (int) this.sounds.stream()
                    .filter(sound -> sound.active)
                    .count();
            
            // process all active sounds
            for (int soundId = 0; activeId < info.maxConcurrentSounds && soundId < this.sounds.size(); soundId++) {
                final Sound sound = this.sounds.get(soundId++);

                // check if we can activate any sounds
                if (activeId < this.info.maxConcurrentSounds) {
                    // only PLAYING and LOOPING qualify
                    if (sound.state != Sound.State.STOPPED) {                        
                        if (!sound.active) {
                            sound.activate();
                        }

                        // increment the id regardless
                        activeId++;
                    }
                }
                
                sound.onFrameEnd();
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while updating Sound(s)!", ex);
        } finally {
            this.lock.release();
        }
    }

    @Override
    public boolean isWritable() {
        return true;
    }
}
