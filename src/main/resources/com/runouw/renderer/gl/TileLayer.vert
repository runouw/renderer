#version 330

const vec2[4] VERTEX_SELECT = vec2[] (
    vec2(0.0, 0.0), vec2(0.0, 1.0),
    vec2(1.0, 0.0), vec2(1.0, 1.0));

layout (location = 0) in vec4 vPos;
layout (location = 1) in vec4 vLayer; // mostly just padding

uniform mat4 uProjection;

out vec3 fUV;

void main() {
    vec2 select = VERTEX_SELECT[gl_VertexID];
    vec2 pos = mix(vPos.xy, vPos.zw, select);

    fUV = vec3(select, vLayer.x);
    
    gl_Position = uProjection * vec4(pos, vLayer.zw);    
}
