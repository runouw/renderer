#version 330

in vec2 fTex;
in vec4 fColor;

uniform sampler2D uFont;

out vec4 color;

void main() {
    float alpha = fColor.a * texture(uFont, fTex).r;
    
    color = vec4(fColor.rgb * alpha, alpha);
}
