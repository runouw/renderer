#version 330

const vec2[4] VERTEX_SELECT = vec2[] (
    vec2(0.0, 0.0), vec2(0.0, 1.0),
    vec2(1.0, 0.0), vec2(1.0, 1.0));

const vec4 VERTEX = vec4(-1.0, 1.0, 1.0, -1.0);

uniform mat4 uProjection;

out vec2 fUV;

void main() {
    vec2 select = VERTEX_SELECT[gl_VertexID];

    fUV = select;
    gl_Position = uProjection * vec4(mix(VERTEX.xy, VERTEX.zw, select), 0.0, 1.0);
}