#version 330

//HEADER

#ifndef MAX_COLOR_TRANSFORMS
#define MAX_COLOR_TRANSFORMS 128
#endif

const vec2 VERTICES[4] = vec2[] (
    vec2(0.0, 0.0), vec2(1.0, 0.0),
    vec2(0.0, 1.0), vec2(1.0, 1.0));

const mat3 IDENTITY_HSV = mat3(
    1.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 1.0);

layout (location = 0) in vec2 vPos;
layout (location = 1) in vec2 vOff;
layout (location = 2) in vec2 vScale;
layout (location = 3) in vec2 vSkew;
layout (location = 4) in float vRot;
layout (location = 5) in vec3 vSpriteView;
layout (location = 6) in vec3 vMaskView;
layout (location = 7) in int vColorTransform;

uniform mat4 uProjection;

struct ColorTransform_T {
    vec4 hsv;
    vec4 rgbaOffset;
    vec4 rgbaScale;
};

layout (std140) uniform uColorTransformBlock {
    ColorTransform_T colorTransform[MAX_COLOR_TRANSFORMS];
};

out vec3 fSpriteView;
out vec3 fMaskView;

flat out int fColorTransform;
flat out mat3 fHSV;
flat out vec4 fRGBAOffset;
flat out vec4 fRGBAScale;

struct UnpackedCT_T {
    mat3 hsv;
    vec4 rgbaOffset;
    vec4 rgbaScale;
};

uniform int uSupportsCT;

UnpackedCT_T skipCT() {
    UnpackedCT_T result;

    result.hsv = IDENTITY_HSV;
    result.rgbaOffset = vec4(0.0);
    result.rgbaScale = vec4(1.0);

    return result;
}

UnpackedCT_T unpackCT() {    
    UnpackedCT_T result;

    if (vColorTransform >= 0) {
        vec4 hsv = colorTransform[vColorTransform].hsv;
        float u = hsv.x;
        float w = hsv.y;
        float s = hsv.z;
        float v = hsv.w;
        float vsu = v * s * u;
        float vsw = v * s * w;

        result.hsv = mat3(
            v, 0.0, 0.0,
            0.0, vsu, vsw,
            0.0,-vsw, vsu);

        result.rgbaOffset = colorTransform[vColorTransform].rgbaOffset;
        result.rgbaScale = colorTransform[vColorTransform].rgbaScale;
    }

    return result;
}

void main() {
    if (isnan(vSpriteView.z)) {
        gl_Position = vec4(vSpriteView.z);

        fSpriteView = vec3(0.0);
        fMaskView = vec3(-1.0);
    } else {
        float ca = cos(vRot);
        float sa = sin(vRot);

        mat4 modelView = mat4(
            ca, sa, 0.0, 0.0,
            -sa, ca, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            vPos.x, vPos.y, 0.0, 1.0);

        vec2 p = VERTICES[gl_VertexID];

        gl_Position = uProjection * modelView * vec4(p * vScale - vOff, 0.0, 1.0);

        fSpriteView = vec3(mix(vec2(0.0), vSpriteView.xy, p), vSpriteView.z);        
        fMaskView = vec3(mix(vec2(0.0), vMaskView.xy, p), vMaskView.z);
        fColorTransform = vColorTransform;
    }    
    
    UnpackedCT_T ct;

    if (uSupportsCT == 0) {
        ct = skipCT();
    } else {
        ct = unpackCT();
    }

    fHSV = ct.hsv;
    fRGBAOffset = ct.rgbaOffset;
    fRGBAScale = ct.rgbaScale;    
}