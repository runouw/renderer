#version 330

const mat3 toYIQ = mat3(
    0.299, 0.596, 0.211,
    0.587,-0.274,-0.523,
    0.114,-0.321, 0.311);

const mat3 toRGB = mat3(
    1.0, 1.0, 1.0,
    0.956,-0.272,-1.107,
    0.621,-0.647, 1.705);

in vec3 fSpriteView;
in vec3 fMaskView;

flat in mat3 fHSV;
flat in vec4 fRGBAOffset;
flat in vec4 fRGBAScale;

uniform sampler2DArray uSpritesheet;
uniform int uSupportsMask;
uniform int uSupportsCT;

vec4 applyIdentityMask(vec4 color) {
    return color;
}

vec4 applyMask(vec4 color) {
    if (fMaskView.z > 1.0) {
        float mask = texture(uSpritesheet, fMaskView).r;

        return vec4(color.rgb, color.a * mask);
    } else {
        return color;
    }
}

vec4 applyIdentityCT(vec4 color) {
    return color;
}

vec4 applyCT(vec4 color) {
    return vec4(toRGB * fHSV * toYIQ * color.rgb, color.a) * fRGBAScale + fRGBAOffset;
}

out vec4 color;

void main() {
    vec4 baseColor = texture(uSpritesheet, fSpriteView);
    vec4 maskedColor;

    if (uSupportsMask == 0) {
        maskedColor = applyIdentityMask(baseColor);
    } else {
        maskedColor = applyMask(baseColor);
    }

    vec4 transformedColor;

    if (uSupportsCT == 0) {
        transformedColor = applyIdentityCT(maskedColor);
    } else {
        transformedColor = applyCT(maskedColor);
    }

    color = vec4(transformedColor.rgb * transformedColor.a, transformedColor.a);
}