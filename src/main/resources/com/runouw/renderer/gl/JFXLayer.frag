#version 330

in vec2 fUV;

uniform sampler2D uImage;

out vec4 result;

void main() {
    vec4 color = texture(uImage, fUV);

    result = vec4(color.rgb * color.a, color.a);
}