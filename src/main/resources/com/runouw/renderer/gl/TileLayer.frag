#version 330

in vec3 fUV;

uniform sampler2DArray uImage;

out vec4 result;

void main() {
    vec4 color = texture(uImage, fUV);

    result = vec4(color.rgb * color.a, color.a);
}