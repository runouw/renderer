#version 330

const vec2[4] VERTEX_SELECT = vec2[] (
    vec2(0.0, 0.0), vec2(0.0, 1.0),
    vec2(1.0, 0.0), vec2(1.0, 1.0));

const vec4 VERTEX = vec4(-1.0, 1.0, 1.0, -1.0);

const mat3 MAT3_IDENTITY = mat3(
    1.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 1.0);

layout (std140) uniform uDataBlock {
    vec4 imageScroll;
    vec4 maskScroll;
    vec4 hsv;
    vec4 rgbaOffset;
    vec4 rgbaScale;
};

uniform int uSupportsCT;

out vec2 fImageScroll;
out vec2 fMaskScroll;
flat out mat3 fHSV;
flat out vec4 fRGBAOffset;
flat out vec4 fRGBAScale;

void main() {
    vec2 select = VERTEX_SELECT[gl_VertexID];

    gl_Position = vec4(mix(VERTEX.xy, VERTEX.zw, select), 0.0, 1.0);   

    fImageScroll = mix(imageScroll.xy, imageScroll.zw, select);
    fMaskScroll = mix(maskScroll.xy, maskScroll.zw, select);

    if (uSupportsCT == 0) {
        fHSV = MAT3_IDENTITY;
        fRGBAOffset = vec4(0.0);
        fRGBAScale = vec4(1.0);
    } else {
        float u = hsv.x;
        float w = hsv.y;
        float s = hsv.z;
        float v = hsv.w;
        float vsu = v * s * u;
        float vsw = v * s * w;

        fHSV = mat3(
            v, 0.0, 0.0,
            0.0, vsu, vsw,
            0.0,-vsw, vsu);
        fRGBAOffset = rgbaOffset;
        fRGBAScale = rgbaScale;
    }    
}
