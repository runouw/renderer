#version 450

layout (location = 0) in vec2 vPos;
layout (location = 1) in vec2 vTex;
layout (location = 2) in vec4 vColor;

layout (set = 0, binding = 0) uniform Matrices {
    mat4 uProjection;
};

layout (location = 0) out vec2 fTex;
layout (location = 1) out vec4 fColor;

void main() {
    gl_Position = uProjection * vec4(vPos, 0.0, 1.0);
    fTex = vTex;
    fColor = vColor;
}
