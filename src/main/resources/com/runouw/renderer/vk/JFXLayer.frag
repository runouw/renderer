#version 450

layout (location = 0) in vec2 fUV;

layout (set = 0, binding = 1) uniform sampler2D uImage;

layout (location = 0) out vec4 result;

void main() {
    vec4 color = texture(uImage, fUV);

    result = vec4(color.rgb * color.a, color.a);
}
