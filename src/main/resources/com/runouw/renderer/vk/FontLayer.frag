#version 450

layout (location = 0) in vec2 fTex;
layout (location = 1) in vec4 fColor;

layout (set = 0, binding = 1) uniform sampler2D uFont;

layout (location = 0) out vec4 color;

void main() {
    float alpha = fColor.a * texture(uFont, fTex).r;
    
    color = vec4(fColor.rgb * alpha, alpha);
}
