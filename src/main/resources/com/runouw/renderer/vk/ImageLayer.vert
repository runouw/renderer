#version 450

const vec2[4] VERTEX_SELECT = vec2[] (
    vec2(0.0, 0.0), vec2(0.0, 1.0),
    vec2(1.0, 0.0), vec2(1.0, 1.0));

const vec4 VERTEX = vec4(-1.0, -1.0, 1.0, 1.0);

const mat3 IDENTITY_HSV = mat3(
    1.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 1.0);

const vec4 IDENTITY_RGBA_OFFSET = vec4(0.0);

const vec4 IDENTITY_RGBA_SCALE = vec4(1.0);

layout (push_constant) uniform Settings {
    int uSupportsMask;
    int uSupportsCT;
};

layout (set = 0, binding = 0) uniform DataBlock {
    vec4 imageScroll;
    vec4 maskScroll;
    vec4 hsv;
    vec4 rgbaOffset;
    vec4 rgbaScale;
};

layout (location = 0) out vec2 fImageScroll;
layout (location = 1) out vec2 fMaskScroll;
layout (location = 2) flat out mat3 fHSV;
layout (location = 5) flat out vec4 fRGBAOffset;
layout (location = 6) flat out vec4 fRGBAScale;

void main() {
    vec2 select = VERTEX_SELECT[gl_VertexIndex];

    gl_Position = vec4(mix(VERTEX.xy, VERTEX.zw, select), 0.0, 1.0);

    fImageScroll = mix(imageScroll.xy, imageScroll.zw, select);
    fMaskScroll = mix(maskScroll.xy, maskScroll.zw, select);

    if (uSupportsCT == 0) {
        fHSV = IDENTITY_HSV;
        fRGBAOffset = IDENTITY_RGBA_OFFSET;
        fRGBAScale = IDENTITY_RGBA_SCALE;
    } else {
        float u = hsv.x;
        float w = hsv.y;
        float s = hsv.z;
        float v = hsv.w;
        float vsu = v * s * u;
        float vsw = v * s * w;

        fHSV = mat3(
            v, 0.0, 0.0,
            0.0, vsu, vsw,
            0.0,-vsw, vsu);
        fRGBAOffset = rgbaOffset;
        fRGBAScale = rgbaScale;
    }
}