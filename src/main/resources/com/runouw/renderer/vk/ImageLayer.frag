#version 450

const mat3 toYIQ = mat3(
    0.299, 0.596, 0.211,
    0.587,-0.274,-0.523,
    0.114,-0.321, 0.311);

const mat3 toRGB = mat3(
    1.0, 1.0, 1.0,
    0.956,-0.272,-1.107,
    0.621,-0.647, 1.705);

layout (location = 0) in vec2 fImageScroll;
layout (location = 1) in vec2 fMaskScroll;
layout (location = 2) flat in mat3 fHSV;
layout (location = 5) flat in vec4 fRGBAOffset;
layout (location = 6) flat in vec4 fRGBAScale;

layout (push_constant) uniform Settings {
    int uSupportsMask;
    int uSupportsCT;
};

layout (set = 0, binding = 1) uniform sampler2D uImage;
layout (set = 0, binding = 2) uniform sampler2D uMask;

layout (location = 0) out vec4 result;

vec4 drawMaskedImage() {
    vec4 color = texture(uImage, fImageScroll);
    float maskAlpha = texture(uMask, fMaskScroll).r;

    return vec4(color.rgb, color.a * maskAlpha);
}

vec4 drawImage() {
    return texture(uImage, fImageScroll);
}

vec4 applyIdentityCT(vec4 color) {
    return color;
}

vec4 applyCT(vec4 color) {
    return vec4(toRGB * fHSV * toYIQ * color.rgb, color.a) * fRGBAScale + fRGBAOffset;
}

void main() {
    vec4 color;

    if (uSupportsMask == 0) {        
        color = drawImage();
    } else {
        color = drawMaskedImage();        
    }

    vec4 transformedColor;

    if (uSupportsCT.x == 0) {        
        transformedColor = applyIdentityCT(color);
    } else {        
        transformedColor = applyCT(color);        
    }

    result = vec4(transformedColor.rgb * transformedColor.a, transformedColor.a);
}